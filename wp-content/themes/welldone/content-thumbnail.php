<?php
/**
 * Template for showing thumbnail
 */
global $post, $thumbnail_size;
$blog_settings = welldone_get_blog_settings ();

?>
	<?php if($blog_settings['blog_type']=="classic" && !is_single()){ ?>
	<div class="entry-media">
		<div class="list-blog-img">
	        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
	            <span class="trim">
	            <?php 
				if(isset($thumbnail_size)){
					$url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $thumbnail_size );
				?>
				
				<img src="<?php echo esc_url($url[0]); ?>" title="<?php the_title(); ?> " alt="<?php  esc_attr(the_title()); ?>"/>
				<?php 
					
				}else{
					the_post_thumbnail();
				}
				 ?>
				</span>
				<time class="img-circle" datetime=""><span><?php echo get_the_date('j'); ?></span><?php echo get_the_date('M'); ?></time>
	        </a>
	    </div>
	</div><!-- End .entry-media -->
    <?php } elseif($blog_settings['blog_type']=='blog-masonry' && !is_single()) { ?>
    <a class="post__image" href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
        <?php 
			if(isset($thumbnail_size)){
				$url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $thumbnail_size );
			?>
			
			<img src="<?php echo esc_url($url[0]); ?>" title="<?php the_title(); ?> " alt="<?php  esc_attr(the_title()); ?>"/>
			<?php 
				
			}else{
				the_post_thumbnail();
			}
			 ?>
    </a>
    <?php } ?>
   <?php if( is_single() ){ ?>
   	<figure class="post-img-full">
        <?php 
		if(isset($thumbnail_size)){
			$url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $thumbnail_size );
		?>
		
		<img class="img-responsive" src="<?php echo esc_url($url[0]); ?>" title="<?php the_title(); ?> " alt="<?php  esc_attr(the_title()); ?>"/>
		<?php 
			
		}else{
			the_post_thumbnail();
		}
		 ?>
    </figure>
	<?php } ?>