<?php
/*
Template Name: Blog
*/

global $welldone_settings,$args, $post,$thumbnail_size, $post_format, $blog_settings,$wp_query;
$welldone_layout_columns = welldone_page_columns ();
$class=welldone_main_container_class();
get_header ();
$blog_settings = welldone_get_blog_settings ();
get_template_part ( 'content-query' );
$wp_query = new WP_Query( $args);
$welldone_class_name =  welldone_class_name();
$post_format = get_post_format();
?>

            <div class="content blog">
                <div class="<?php echo esc_attr($class);?>">
                    
                    <div class="row">
                        <div class="col-md-9">
                            <?php if ( $wp_query->have_posts () ) { ?>
                                        <div class="list-blog">
                                            <?php while ( $wp_query->have_posts() ) {
                                                $wp_query->the_post();
                                                ?>

                                                <article id="post-<?php the_ID(); ?>" <?php post_class($class);?>>

                                                  <?php  if( ('video'==$post_format || 'gallery'==$post_format || 'audio'==$post_format) && !post_password_required()  ){ ?>
                                                        
                                                       <?php }if(has_post_thumbnail() && !post_password_required()){ ?>
                                                          <div class="entry-media">
                                                    <div class="list-blog-img">
                                                        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                                                            <span class="trim">
                                                            <?php 
                                                            if(isset($thumbnail_size)){
                                                                $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $thumbnail_size );
                                                            ?>
                                                            
                                                            <img src="<?php echo esc_url($url[0]); ?>" title="<?php the_title(); ?> " alt="<?php  esc_attr(the_title()); ?>"/>
                                                            <?php 
                                                                
                                                            }else{
                                                                the_post_thumbnail();
                                                            }
                                                             ?>
                                                            </span>
                                                            <time class="img-circle" datetime=""><span><?php echo get_the_date('j'); ?></span><?php echo get_the_date('M'); ?></time>
                                                        </a>
                                                    </div>
                                                </div>
                                                                
                                                        <?php }
                                                  ?>
                                                  
                                                       

                                                            <h2 class="entry-title post__title text-uppercase"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>

                                                           
                                                          <?php get_template_part('content',$post_format); ?> 
                                                            

                                                         <?php get_template_part("content","post"); ?>

                                                            <div class="entry-meta post__meta">
                                                            <?php if($post_format != 'aside') { ?>
                                                            <span class="post__meta__item"><span class="icon icon-clock"></span><?php the_time('F j, Y') ?></span>
                                                            <?php } elseif($post_format == 'aside'){ ?>
                                                                   <span class="post__meta__item"><span class="icon icon-chat"></span><?php esc_html_e( "Status on ", "welldone" ); ?><?php the_time('F j, Y') ?></span>
                                                            <?php }?>
                                                          
                                                            <?php if(!$blog_settings['hide_blog_post_author']){ ?>
                                                            <span class="post__meta__item"><span class="icon icon-user-circle"></span>
                                                                <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="post-author" title="<?php the_author(); ?>"><?php the_author(); ?></a>
                                                            </span>
                                                            <?php } ?>
                                                           <?php if(!$blog_settings['hide_blog_post_category'] && has_category()){ ?>
                                                                <span class="post__meta__item"><span class="icon icon-folder"></span><?php the_category(", "); ?></span>
                                                            <?php } ?>
                                                            <?php if(!$blog_settings['hide_blog_post_tags'] && !is_single() && has_tag()){ ?>
                                                                <span class="post__meta__item"><span class="icon icon-shop-label"></span><?php the_tags("", ", "); ?></span>
                                                            <?php } ?>
                                                        </div>

                                                                                                      
                                                        
                                                </article>
                                            <?php } ?>
                                         <?php if (  $wp_query->max_num_pages > 1 ) : ?>
                                          
                                       <?php endif; ?> 
                                        </div>
                                    <?php 
                                       
                                            welldone_pagination();
                                       
                                     } else {
                                        get_template_part( "content-none" );
                                     } ?>
                                    <?php wp_reset_postdata(); ?>
                        </div>
                            <div class="md-margin2x clearfix visible-sm visible-xs"></div><!-- space -->
                             <?php get_sidebar('right') ?>
                    </div>
                  
                </div>
            </div>

<?php get_footer(); ?>
