<?php
/**
 * The template for displaying the footer.
 *
 *
 * @package welldone
 */
global $welldone_settings;
?>

	<footer class="footer">
	<?php if ( $welldone_settings['welldone_show_footer_top'] ) : ?>
      <div class="footer__links hidden-xs">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <?php if ( $welldone_settings['welldone_show_footer_l_menu'] ) : ?>
            <div class="h-links-list">
              <?php 
                wp_nav_menu(array(
                  'theme_location' => 'footer-menu-left',
                  'container' => false,
                  'menu_class' => 'menu',
                  'depth' => 1
                  )
                );
              ?>
            </div>
            <?php endif; ?>
          </div>
          <div class="col-sm-6">
            <?php if ( $welldone_settings['welldone_show_footer_r_menu'] ) : ?>
            <div class="h-links-list text-right">
              <?php 
                wp_nav_menu(array(
                  'theme_location' => 'footer-menu-right',
                  'container' => false,
                  'menu_class' => 'menu',
                  'depth' => 1
                  )
                );
              ?>
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php if ( $welldone_settings['welldone_show_footer_mid'] ) : ?>
      <div class="footer__column-links">
      <div class="back-to-top"> <a href="#top" class="btn btn--round btn--round--lg"><span class="icon-arrow-up"></span></a></div>
      <div class="container">
      	<?php get_template_part('template-parts/footer/footer-widgets'); ?>
      </div>
    </div>
    <?php endif; ?>
    <?php if ( $welldone_settings['welldone_show_footer_bottom'] ) : ?>
      <div class="footer__subscribe">
      <div class="container">
        <div class="row">
          <div class="col-sm-7 col-md-6">
          	<div class="subscribe-form">
          		<?php 
                 if ( shortcode_exists( 'osd_subscribe' ) ) {
                      echo do_shortcode("[osd_subscribe categories='uncategorized' title='SUBSCRIBE' placeholder='Your e-mail address' button_text='SUBSCRIBE']");
                  }

              ?>
          	</div>
          </div>
          <div class="col-sm-5 col-md-6">
            <div class="social-links social-links--colorize social-links--large">
              <ul>
				<?php if ( !empty($welldone_settings[ 'footer_icon_fb' ] )): ?>
				    <li class="social-links__item"><a class="icon icon-facebook" href="<?php echo esc_attr( $welldone_settings['footer_icon_fb'] ); ?>" target="_blank"></a></li>
				<?php endif; ?>
				<?php if ( !empty($welldone_settings[ 'footer_icon_twitter' ] )): ?>
				    <li class="social-links__item"><a class="icon icon-twitter" href="<?php echo esc_attr( $welldone_settings['footer_icon_twitter'] ); ?>" target="_blank"></a></li>
				<?php endif; ?>
				<?php if ( !empty($welldone_settings[ 'footer_icon_gmail' ] )): ?>
				    <li class="social-links__item"><a class="icon icon-google" href="<?php echo esc_attr( $welldone_settings['footer_icon_gmail'] ); ?>" target="_blank"></a></li>
				<?php endif; ?>
				<?php if ( !empty($welldone_settings[ 'footer_icon_pin' ] )): ?>
				    <li class="social-links__item"><a class="icon icon-pinterest" href="<?php echo esc_attr( $welldone_settings['footer_icon_pin'] ); ?>" target="_blank"></a></li>
				<?php endif; ?>
				<?php if ( !empty($welldone_settings[ 'footer_icon_mail' ] )): ?>
				    <li class="social-links__item"><a class="icon icon-mail" href="mailto:<?php echo esc_attr( $welldone_settings['footer_icon_mail'] ); ?>" target="_blank"></a></li>
				<?php endif; ?>
	          </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
     <?php if ( $welldone_settings['welldone_show_copyright_block'] ) : ?>
      <div class="footer__bottom">      <div class="container">
      	<div class="pull-left text-uppercase"><?php echo htmlspecialchars_decode(esc_textarea($welldone_settings['welldone_copyright'])) ?></div>
        <div class="pull-right text-uppercase text-right"><?php echo htmlspecialchars_decode(esc_textarea($welldone_settings['welldone_footer_text'])) ?></div>
      </div>
	</div>
    <?php endif; ?>
  </footer>

<?php wp_footer(); ?>

</div><!-- #wrapper -->
</body>
</html>
