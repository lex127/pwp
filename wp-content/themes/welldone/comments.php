<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */

?>
<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="no-comments"><?php esc_html_e('This post is password protected. Enter the password to view comments.', 'welldone'); ?></p>
	<?php
		return;
	}
?>
<!-- You can start editing here. -->
<?php if ( have_comments() ) { ?>
	<div id="comments" class="content">
		<h2 class="text-uppercase text-left"><?php esc_html_e( 'Join the discussion', 'welldone' ); ?></h2>
		<div class="row comments">
			<h3><?php comments_number(esc_html__('Comments Posted(%)', 'welldone'));?></h3>
			<ul class="col-md-10">
				<?php wp_list_comments('callback=welldone_comment'); ?>
			</ul>
			<div class="comments-navigation">
			    <div class="alignleft"><?php previous_comments_link(); ?></div>
			    <div class="alignright"><?php next_comments_link(); ?></div>
			</div>
		</div>
	</div>
	<div class="divider divider--line divider--line--dark"></div>	
<?php }?>
<?php if ( comments_open() ) : ?>
	<?php
	function modify_comment_form_fields($fields){
		$commenter = wp_get_current_commenter();
		$req       = get_option( 'require_name_email' );


		$fields['author'] = ' <div class="row"><div class="col-sm-6"><div class="input-group input-group--wd">
                                             <input type="text" value="'. esc_attr( $commenter['comment_author'] ) .'" class="input--full" id="author" name="author" '.($req ? ' required':'').'>
                                             <span class="input-group__bar"></span>
                                            <label for="author" class="input-desc">'.esc_html__("Your Name",'welldone').' '.( $req ? '<span class="required-field">*</span>' : '' ).'</label>
                                            </div></div>';
		$fields['email'] = '<div class="col-sm-6"><div class="input-group input-group--wd">
                                            <input type="email" class="input--full" value="'. esc_attr( $commenter['comment_author_email'] ) .'" id="email" name="email" '.($req ? ' required':'').'>
                                            <span class="input-group__bar"></span>
                                            <label for="email" class="input-desc">'.esc_html__("Your Email",'welldone').' '.( $req ? '<span class="required-field">*</span>' : '' ).'</label>
                                            </div></div></div>';
        $fields['url'] = '<div class="row"><div class="col-sm-12"><div class="input-group input-group--wd">
                                            <input type="url" class="input--full" value="'. esc_attr( $commenter['comment_author_url'] ) .'" id="url" name="url">
                                            <span class="input-group__bar"></span>
                                            <label for="url" class="input-desc">'.esc_html__("Website",'welldone').'</label>
                                            </div></div></div>';
		return $fields;
	}
	add_filter('comment_form_default_fields','modify_comment_form_fields');
	
	$comments_args = array(
		'title_reply' => ''. esc_html__("Leave a Comment", "welldone").'',
		'title_reply_to' => ''. esc_html__("Leave a Comment", "welldone").'',
		'must_log_in' => '<p class="must-log-in">' .  sprintf( esc_html__( "You must be %slogged in%s to post a comment.", "welldone" ), '<a href="'.wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ).'">', '</a>' ) . '</p>',
		'logged_in_as' => '<p class="logged-in-as">' . esc_html__( "Logged in as","welldone" ).' <a href="' .admin_url( "profile.php" ).'">'.$user_identity.'</a>. <a href="' .wp_logout_url(get_permalink()).'" title="' . esc_html__("Log out of this account", "welldone").'">'. esc_html__("Log out &raquo;", "welldone").'</a></p>',
		'comment_notes_before' => '',
		'comment_notes_after' => '',
		'comment_field' => '<div class="input-group input-group--wd">
                                    <textarea class="textarea--full" id="comment" name="comment" tabindex="4"  required="required"></textarea>
                                    <span class="input-group__bar"></span>
                                    <label for="comment" class="input-desc">'.esc_html__("Your Message",'welldone').'</label>
                                </div>',
		'class_submit' => 'btn btn--wd text-uppercase wave waves-effect',
		'label_submit'=> esc_html__("Post Comment", "welldone"),
	);
	comment_form($comments_args);
	?>
<?php endif; // if you delete this the sky will fall on your head ?>