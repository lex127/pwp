<?php
global $welldone_layout_columns;


if($welldone_layout_columns=='left' || $welldone_layout_columns=='both' || is_tax( 'brand')){
    get_sidebar('left');
} ?>
<?php if($welldone_layout_columns=='right' || $welldone_layout_columns=='both'){
    get_sidebar('right');
} ?>