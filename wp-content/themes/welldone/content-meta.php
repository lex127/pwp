<?php
/**
 * The template for displaying posts metadata
 *
 * Used for both single and index/archive/blog/search.
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */
global $blog_settings, $wp_query;
$post_format = get_post_format();
 
?>
<div class="entry-meta post__meta">
    <?php if ($blog_settings[ 'blog_type' ] =='blog-masonry') { ?>
    <span class="post__meta__date pull-left"><span class="icon icon-clock"></span><?php the_time('F j, Y') ?></span>
    <?php } elseif($blog_settings[ 'blog_type' ] =='classic' && $post_format != 'aside') { ?>
    <span class="post__meta__item"><span class="icon icon-clock"></span><?php the_time('F j, Y') ?></span>
    <?php } elseif($blog_settings[ 'blog_type' ] =='classic' && $post_format == 'aside'){ ?>
           <span class="post__meta__item"><span class="icon icon-chat"></span><?php esc_html_e( "Status on ", "welldone" ); ?><?php the_time('F j, Y') ?></span>
    <?php }?>
    <?php if(!$blog_settings['hide_blog_post_author'] && $blog_settings[ 'blog_type' ] =='blog-masonry'){ ?>
    <span class="post__meta__author pull-right"><span class="icon icon-user-circle"></span>
        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="post-author" title="<?php the_author(); ?>"><?php the_author(); ?></a>
    </span>
    <?php } ?>
    <?php if(!$blog_settings['hide_blog_post_author'] && $blog_settings[ 'blog_type' ] =='classic'){ ?>
    <span class="post__meta__item"><span class="icon icon-user-circle"></span>
        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="post-author" title="<?php the_author(); ?>"><?php the_author(); ?></a>
    </span>
    <?php } ?>
   <?php if(!$blog_settings['hide_blog_post_category'] && $blog_settings[ 'blog_type' ] =='classic' && has_category()){ ?>
        <span class="post__meta__item"><span class="icon icon-folder"></span><?php the_category(", "); ?></span>
    <?php } ?>
    <?php if(!$blog_settings['hide_blog_post_tags'] && $blog_settings[ 'blog_type' ] =='classic' && !is_single() && has_tag()){ ?>
        <span class="post__meta__item"><span class="icon icon-shop-label"></span><?php the_tags("", ", "); ?></span>
    <?php } ?>
</div><!-- End .entry-meta -->