<aside class="col-md-3 sidebar" role="complementary">
    <?php
    $welldone_right_sidebar = welldone_right_sidebar();
    ?>
    <?php if ( !dynamic_sidebar ( $welldone_right_sidebar ) ) : ?>
        <div class="widget">
            <h3 class="widget-title"><?php echo ucwords( str_replace( array( '_', '-' ), ' ', $welldone_right_sidebar) ) ?></h3>
            <div>
                <p><?php esc_html_e( "Please configure this Widget Area in the Admin Panel under Appearance -> Widget", 'welldone' ) ?></p>
            </div>
        </div><!-- End .widget -->
    <?php endif; ?> 
</aside><!--sEnd .col-md-3 -->

