<?php
/**
 * The template for displaying posts content
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */
?>
<?php 
		global $post_format, $blog_settings, $post; ?>
		<div class="entry-content post__text">
		<?php
				$post_format = get_post_format();
				$quote = false;
		if($post_format=='quote'){
					$quote = get_post_meta( $post->ID, 'welldone_quote_content', true ) ? get_post_meta( $post->ID, 'welldone_quote_content', true ) : '';
					$quote_author = get_post_meta( $post->ID, 'welldone_quote_author', true ) ? get_post_meta( $post->ID, 'welldone_quote_author', true ) : '';
					$quote_icon = get_post_meta( $post->ID, 'welldone_quote_icon', true ) ? ' class="blockquote-icon"': '';
		?>
		<?php 
			if($quote){
				if ( post_password_required() ) {
					if(!is_single()) echo  get_the_password_form ();
				}else{ ?>
						<blockquote <?php echo esc_attr($quote_icon); ?>>
						<?php if(!is_single() && $blog_settings['blog_excerpt'] ){
									$quote = welldone_get_blockquote_excerpt($quote);
								} ?>
						<p><?php echo force_balance_tags($quote); ?></p>
					<?php   if($quote_author){ ?>
								<cite>-- <?php echo force_balance_tags($quote_author); ?></cite>
					<?php    } ?>
						</blockquote>
    <?php 		} 
			} 
		} ?>
<?php
    if(is_single()){
        the_content();
    }else{
        if(!empty($blog_settings['blog_excerpt'])){
            if( gallery_shortcode_exists() ){
                welldone_gallery();
            }else{
                if(!$quote) {
                    echo welldone_excerpt ( $blog_settings[ 'blog_excerpt_limit' ] );
                }
            }
        } else {
              the_content();
        }
    } ?>
</div><!-- End .entry-content -->

<?php if(!$blog_settings['hide_blog_post_tags'] && is_single() && has_tag()){ ?>
			<div class="divider divider--sm"></div>
            <div class="entry-tags">
       
                <?php the_tags('<ul class="tags-list"><li>','</li><li>','</li></ul>'); ?>
            </div>
<?php } ?>