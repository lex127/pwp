<?php
//remove woocommerce breradcrumb
remove_action ( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action ( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20, 0 );
remove_action ( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30, 0 );
remove_action ( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
remove_action ( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action ( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
remove_action ( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action ( "woocommerce_sidebar", "woocommerce_get_sidebar", 10 );
remove_action ( 'woocommerce_before_single_product_summary', "woocommerce_show_product_sale_flash", 10 );
remove_action ( 'woocommerce_proceed_to_checkout', 'woocommerce_button_proceed_to_checkout', 20 );
add_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );
remove_action ( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
remove_action ( "woocommerce_after_single_product_summary", "woocommerce_output_product_data_tabs", 10 );
remove_action ( "woocommerce_after_single_product_summary", "woocommerce_upsell_display", 15 );
remove_action ( "woocommerce_after_single_product_summary", "woocommerce_output_related_products", 20 );

add_action ( "woocommerce_single_product_summary", "welldone_add_social_share", 42 );
add_filter ( 'woocommerce_show_page_title', function ( $args ) {
    return false;
} );

add_action ( "welldone_shop_custom_action", "woocommerce_show_product_loop_sale_flash", 3 );
add_action ( "woocommerce_before_single_product_summary", "welldone_product_tabs", 3 );

add_action ( "woocommerce_before_single_product_summary", "welldone_upsells", 10 );
add_action ( "woocommerce_after_cart" , 'welldone_cross_sell',20);


 
 
add_action( 'show_user_profile', 'welldone_user_profile_fields' );
add_action( 'edit_user_profile', 'welldone_user_profile_fields' );

function welldone_user_profile_fields( $user ) { 

$r = get_user_meta( $user->ID, 'picture', true );
    ?>


<!-- Artist Photo Gallery -->
<h3><?php esc_html_e("Public Profile - Gallery", "welldone"); ?></h3>

<table class="form-table">

<tr>
        <th scope="row">Picture</th>
        <td><input type="file" name="picture" value="" />
        </td>
    
    </tr>
    <tr>
        <td>
        
           <?php //print_r($r); 
           if($r){
                    $r = $r['url'];
                    echo "<img width='200px' src='$r' />";
           }
           ?>
        </td>
    </tr>

</table> 



<?php
}
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );
    
    function save_extra_user_profile_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }

$_POST['action'] = 'wp_handle_upload';
$r = wp_handle_upload( $_FILES['picture'] );
update_user_meta( $user_id, 'picture', $r, get_user_meta( $user_id, 'picture', true ) );

}


add_action('user_edit_form_tag', 'make_form_accept_uploads');
function make_form_accept_uploads() {
    echo ' enctype="multipart/form-data"';
}
    
 
 
//woocommerce vendor End  
 


function welldone_product_tabs () {
    global $product, $post, $welldone_settings;
    $ver = welldone_single_product_version ();
 
        add_action ( "woocommerce_after_single_product", "woocommerce_output_product_data_tabs", 44 );

}

function welldone_upsells () {
    global $product, $post, $welldone_settings;
         add_action ( "woocommerce_after_single_product_summary", "woocommerce_upsell_display", 15 );
    }
function welldone_cross_sell () {
   woocommerce_cross_sell_display();
}


function welldone_shop_cat_list_grid_ver(){
    if( is_product_taxonomy() ){
        $tax_id = get_queried_object_id();
        $tax_data =  get_option( 'tax_meta_'.$tax_id);
        if(isset($tax_data["welldone_product_cat_view"]) && $tax_data["welldone_product_cat_view"] !=''){

            return $tax_data["welldone_product_cat_view"];
        }else{
            return "";
        }
    }else{
        return '';
    }
}
function welldone_shop_grid_version () {
    global $welldone_settings, $product, $post;
    $grid_version = '';
    $grid_version = welldone_shop_cat_list_grid_ver();
    if(!$grid_version) {
        $grid_version = ( $welldone_settings[ 'welldone_products_grid_version' ] ) ? $welldone_settings[ 'welldone_products_grid_version' ] : "v_1";
    }
    return apply_filters("welldone_filter_shop_grid_version",$grid_version);
}
function welldone_shop_list_version (){
    global $welldone_settings, $product, $post;
    $list_version = '';
    $list_version = welldone_shop_cat_list_grid_ver();
    if(!$list_version) {
        $list_version = ( $welldone_settings[ 'welldone_products_list_version' ] ) ? $welldone_settings[ 'welldone_products_list_version' ] : "list";
    }
    return apply_filters("welldone_filter_shop_list_version",$list_version);
}
function welldone_shop_columns(){
    global $welldone_settings;
    $columns = '';
    if ( is_tax( array( "product_cat","product_tag" ) ) ) {
        $tax_id = get_queried_object_id();
        $tax_data =  get_option( 'tax_meta_'.$tax_id);
        if(isset($tax_data["welldone_product_cat_columns"]) && $tax_data["welldone_product_cat_columns"] != ''){
            $columns = $tax_data["welldone_product_cat_columns"];
        }
    }
    if(!$columns){
        $columns = ( $welldone_settings[ 'welldone_products_grid_columns' ] ) ? $welldone_settings[ 'welldone_products_grid_columns' ] : "4";
    }
    return $columns;
}
function welldone_cart_version () {
    global $welldone_settings;
    $cart_ver = ( isset($welldone_settings[ 'welldone_woo_cart_ver' ]) && $welldone_settings[ 'welldone_woo_cart_ver' ] ) ? $welldone_settings[ 'welldone_woo_cart_ver' ] : "v_1";
    return apply_filters("welldone_filter_cart_version",$cart_ver);
}
//add_action("woocommerce_cart_collaterals","welldone_action_shipping_calc",1);
$cart_ver = welldone_cart_version ();
if ( $cart_ver == "v_1" ) {
    add_action ( "woocommerce_cart_collaterals", "welldone_shipping_calculator", 3 );
} elseif ( $cart_ver == "v_2" ) {
    add_action ( "woocommerce_cart_collaterals", "welldone_shipping_calculator", 23 );
}

function welldone_shipping_calculator () {
    // get_template_part ( "woocommerce/cart/cart-shipping" );
}

add_action ( 'woocommerce_proceed_to_checkout', 'welldone_woocommerce_button_proceed_to_checkout', 20 );
function welldone_woocommerce_button_proceed_to_checkout () {
    $checkout_url = WC ()->cart->get_checkout_url ();
    ?>
    <a href="<?php echo esc_url($checkout_url); ?>"
       class="btn btn--wd btn--xl"><?php esc_html_e( 'Proceed to Checkout','welldone' ); ?></a>
<?php
}

function welldone_single_product_version () {
    global $product, $post, $welldone_settings;

    $id = ( isset( $post->ID ) ? get_the_ID() : NULL );

    $ver = get_post_meta ( $id, 'welldone_product_page_style', true ) ? get_post_meta ( $id, 'welldone_product_page_style', true ) : '';
    if ( !$ver ) {
        $ver = isset( $welldone_settings[ 'welldone_product_page_style' ] ) ? $welldone_settings[ 'welldone_product_page_style' ] : 'v_1';
    }
    return $ver;
}

add_action ( "woocommerce_before_shop_loop", "welldone_woocommerce_before_shop_loop" );
function welldone_woocommerce_before_shop_loop () {  global $welldone_settings;  
    welldone_taxonomy_banner();?>
    <div class="filters-row row">
    <?php if(!woocommerce_products_will_display()){ ?></div>   <?php return;  } ?>

        <div class="col-sm-4 col-md-5 col-lg-3 col-1">
            <a class="filters-row__view active link-grid-view icon icon-keyboard"></a> <a class="filters-row__view link-row-view icon icon-list"></a>
            <a class="btn btn--wd btn--with-icon btn--sm wave" id="showFilter"><span class="icon icon-filter"></span>Filter</a> <a class="btn btn--wd btn--with-icon btn--sm wave" id="showFilterMobile"><span class="icon icon-filter"></span>Filter</a>
        </div>
        <div class="col-sm-8 col-md-7 col-lg-6 col-2">
                <?php woocommerce_result_count (); ?>
            <div class="filters-row__select">
                <label><?php esc_html_e( "Per Page", 'welldone' ) ?>:</label>
                <?php
                $products_perpage = ( isset( $welldone_settings[ 'welldone_products_perpage' ] ) && $welldone_settings[ 'welldone_products_perpage' ] ) ? $welldone_settings[ 'welldone_products_perpage' ] : '9,15,30';
                $pr_p_ar = explode ( ",", $products_perpage );
                if(isset($_GET['perpage'])){
                    $pr_p_ar[] = $_GET['perpage'];
                }
                $current = ( isset( $_GET[ 'perpage' ] ) && $_GET[ 'perpage' ] ) ? $_GET[ 'perpage' ] : $pr_p_ar[ 0 ];
                ?>
                <!-- <div class="select-wrapper"> -->
                    <form method="get">
                        <?php if ( isset( $_GET ) && !empty( $_GET ) ) {
                            foreach ( $_GET as $k => $v ) {
                                if ( $k != 'perpage' ) { ?>
                                    <input type="hidden" name="<?php echo esc_attr ( $k ) ?>"
                                           value="<?php echo esc_attr ( $v ); ?>"/>
                                <?php }
                            }
                        } ?>
                        <div class="select-wrapper">
                            <select id="number" name="perpage" class="select--wd select--wd--sm" onchange="this.form.submit()">
                                <?php foreach ( $pr_p_ar as $number ) { ?>
                                    <option
                                        value="<?php echo esc_attr($number); ?>" <?php selected ( $number, $current ); ?>><?php echo esc_attr($number); ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </form>
                <!-- </div> -->
            </div>
            <!-- End .normal-selectbox-->
            <div class="filters-row__select">
                <?php add_filter ( "woocommerce_catalog_orderby", function ( $catalog_orderby_options ) {
                    $catalog_orderby_options = array (
                        'menu_order' => esc_html__ ( 'Default Sorting', 'welldone' ),
                        'popularity' => esc_html__ ( 'Popularity', 'welldone' ),
                        'rating' => esc_html__ ( 'Average Rating', 'welldone' ),
                        'date' => esc_html__ ( 'Latest', 'welldone' ),
                        'price' => esc_html__ ( 'Lowest Price', 'welldone' ),
                        'price-desc' => esc_html__ ( 'Highest Price', 'welldone' )
                    );
                    return $catalog_orderby_options;
                } );
                woocommerce_catalog_ordering () ?>
            </div>
            <nav class="col-lg-3 visible-lg col-3 tar woocommerce-pagination">
                <?php woocommerce_pagination ();?>
            </nav>
        </div>
        <!-- End .filter-row-box -->
        <!-- <div class="clearfix visible-xs"></div> -->
        <!-- End .clearfix -->

        <!-- End .filter-row-box -->
    <!-- End .filter-row -->

<?php }
if ( isset( $_GET[ 'perpage' ] ) && $_GET[ 'perpage' ] ) {
    add_filter ( 'loop_shop_per_page', create_function ( '$cols', 'return ' . $_GET[ 'perpage' ] . ';' ), 20 );
} else {
    $products_perpage = ( isset( $welldone_settings[ 'welldone_products_perpage' ] ) && $welldone_settings[ 'welldone_products_perpage' ] ) ? $welldone_settings[ 'welldone_products_perpage' ] : '9,15,30';
    $products_perpage_array = explode ( ',', $products_perpage );

    add_filter ( 'loop_shop_per_page', create_function ( '$cols', 'return ' . $products_perpage_array[ 0 ] . ';' ), 20 );
}
add_action ( "woocommerce_after_shop_loop", "welldone_woocommerce_after_shop_loop", 9 );
function welldone_woocommerce_after_shop_loop () { ?>
   <?php if(!woocommerce_products_will_display()) { return ; } ?>
   <!--  <nav class="pagination-container">
        <?php woocommerce_result_count (); ?>
        <nav class="woocommerce-pagination">
            <?php woocommerce_pagination ();?>
        </nav>
    </nav> -->
<?php }
add_action ( 'welldone_shop_custom_action', 'welldone_shop_image_carousel', 5 );
function welldone_shop_image_carousel () {
    global $product, $post, $welldone_shop_page_settings,$columns;
    if(in_array($columns, array(1,2,3) ) ){
        $size = "shop_catalog";
     }else{
        $size = 'shop_catalog';
     }
     $multiple_images = apply_filters("welldone_shop_multiple_images",true);
    if(!$multiple_images){ ?>
    <!--owl-carousel product-slider-->
        <figure class="">
                <a href="<?php echo esc_url ( $product->get_permalink () ); ?>"
                   title="<?php echo esc_attr ( $product->get_title () ); ?>">
                    <?php echo $product->get_image($size,array("class"=>"product-image")); ?>
                </a>
        </figure>  
 <?php }else{
    $images = $attachment_ids = array ();
    if ( $product->is_type ( 'variation' ) ) {
        if ( has_post_thumbnail ( $product->get_variation_id () ) ) {
            // Add variation image if set
            $attachment_ids[ ] = get_post_thumbnail_id ( $product->get_variation_id () );

        } elseif ( has_post_thumbnail ( $product->id ) ) {

            // Otherwise use the parent product featured image if set
            $attachment_ids[ ] = get_post_thumbnail_id ( $product->id );
        }
    } else {
        // Add featured image
        if ( has_post_thumbnail ( $product->id ) ) {
            $attachment_ids[ ] = get_post_thumbnail_id ( $product->id );
        }
        // Add gallery images
      
    }
    if ( !empty( $attachment_ids ) ) { ?>
        <figure class="<?php echo esc_attr($size); ?> owl-carousel product-slider">
            <?php foreach ( $attachment_ids as $img ) { ?>
                <a href="<?php echo esc_url ( $product->get_permalink () ); ?>"
                   title="<?php echo esc_attr ( $product->get_title () ); ?>">
                    <?php $path = wp_get_attachment_image_src ( $img, $size ); ?>
                    <img src="<?php echo esc_url ( $path[ 0 ] ); ?>"
                         alt="<?php echo esc_attr ( $product->get_title () ); ?>" class="product-image">
                </a>
            <?php } ?>
        </figure>
    <?php }
    else{
        ?>
        <img src="<?php echo get_template_directory_uri(). '/images/shop-dumy.png' ?>" alt="#"/>
    <?php       
    }
    }
}
//actions for both grid and list
add_action ( "woocommerce_before_shop_loop_item_title", "welldone_product_cats" );
function welldone_product_cats () {
    global $welldone_settings, $post, $product;
    if ( $welldone_settings[ 'welldone_show_product_category' ] ) {
       
    }
}

function welldone_cart_total () {
    global $welldone_settings, $woocommerce;
    if ( class_exists ( "woocommerce" ) && $welldone_settings[ 'show-minicart' ] ) {
        if ( sizeof ( WC ()->cart->get_cart () ) > 0 ) {
        echo '<a id="welldone_minicart_price" href="'. get_permalink ( get_option ( 'woocommerce_cart_page_id' ) ).'" class="cart-link" title="'. esc_html__("Go to Cart",'welldone') .'">
        <i class="fa fa-shopping-cart"></i>
        <span class="header-text">'.__("Shopping Cart",'welldone').'</span>
        <span class="cart-text-price">'.WC ()->cart->get_cart_total ().'</span>
        </a>';
        }else{
            echo "<div id='welldone_minicart_price'>";
            echo esc_html__("Shopping Cart ".WC ()->cart->get_cart_total (),'welldone');
            echo "</div>";
echo '<a id="welldone_minicart_price" href="'. get_permalink ( get_option ( 'woocommerce_cart_page_id' ) ).'" class="cart-link" title="'. esc_html__("Go to Cart",'welldone') .'">
        <i class="fa fa-shopping-cart"></i>
        <span class="header-text">'.esc_html__("Shopping Cart",'welldone').'</span>
        <span class="cart-text-price">'.WC ()->cart->get_cart_total ().'</span>
        </a>';
        }
    }
}

function welldone_compare_wishlist_links ($text = true) {
    //function to show wishlist and link in header
    global $welldone_settings;

    if( function_exists( 'YITH_WCWL' )  && $welldone_settings['show-wishlist-button']){
        $wishlist_url = YITH_WCWL()->get_wishlist_url();
        $in = ($text)? esc_html__("Wishlist",'welldone'):'('.YITH_WCWL()->count_products().')';
            echo '<a href="'.$wishlist_url.'" class="header-link" title="'.esc_html__("wishlist",'welldone').'"><i class="fa fa-heart-o"></i><span class="header-text">'.$in.'</span></a>';
    }
}

function welldone_minicart ($cart_total = true, $show = "count") {
    global $welldone_settings, $woocommerce;
    if ( class_exists ( "woocommerce" ) && $welldone_settings[ 'show-minicart' ] ) {
        ?>
        <div class="header__cart pull-left" id="welldone_minicart_wrapper">
            <?php if($cart_total){ ?>
                <span class="header__cart__indicator hidden-xs"><?php
                    if ( sizeof ( WC ()->cart->get_cart () ) > 0 ) {
                        echo WC ()->cart->get_cart_subtotal ();
                    } else {
                        echo get_woocommerce_currency_symbol () . "0";
                    } ?>
                </span>
            <?php } ?>
            <div class="dropdown pull-right">
            <a class="btn dropdown-toggle btn--links--dropdown header__cart__button header__dropdowns__button" href="#" id="cart-dropdown" data-toggle="dropdown" aria-expanded="true">
                <span class="icon icon-bag-alt"></span>
                <?php if($show==='count'){ ?>
                        <span class="badge badge--menu"><?php echo  WC ()->cart->get_cart_contents_count(); ?></span>
                <?php } ?>
            </a>

                <div class="dropdown-menu animated fadeIn shopping-cart" id="welldone_minicart_wrapper_content" role="menu">
                <div class="shopping-cart__top text-uppercase"><?php esc_html_e("Items in cart",'welldone'); ?>(<?php echo  WC ()->cart->get_cart_contents_count(); ?>)</div>
                    <?php if ( sizeof ( WC ()->cart->get_cart () ) > 0 ) {
                        foreach ( WC ()->cart->get_cart () as $cart_item_key => $cart_item ) {
                            $_product = apply_filters ( 'woocommerce_cart_item_product', $cart_item[ 'data' ], $cart_item, $cart_item_key );
                            $product_id = apply_filters ( 'woocommerce_cart_item_product_id', $cart_item[ 'product_id' ], $cart_item, $cart_item_key );
                            if ( $_product && $_product->exists () && $cart_item[ 'quantity' ] > 0 && apply_filters ( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                                $product_name = apply_filters ( 'woocommerce_cart_item_name', $_product->get_title (), $cart_item, $cart_item_key );
                                $thumbnail = apply_filters ( 'woocommerce_cart_item_thumbnail', $_product->get_image ( "welldone-shop-widget" ), $cart_item, $cart_item_key );
                                $product_price = apply_filters ( 'woocommerce_cart_item_price', WC ()->cart->get_product_price ( $_product ), $cart_item, $cart_item_key ); ?>
                                 <div class="product clearfix">
                                  <div class="loader"></div>
                                </div><!-- End .product -->
                            <?php }
                        } ?>
                        <div class="cart-total">
                            <div class="dropdown-subtotal">
                                <span><?php esc_html_e( "Subtotal", 'welldone' ) ?>
                                    :</span> <?php echo WC ()->cart->get_cart_subtotal (); ?>
                            </div>
                            <!-- End .dropdown-subtotal -->
                            <div class="dropdown-total">
                                <span><?php esc_html_e( "Grand Total", 'welldone' ) ?>
                                    :</span> <?php echo WC ()->cart->get_cart_total (); ?>
                            </div>
                            <!-- End .dropdown-total -->
                        </div><!-- End .cart-total -->
                        <div class="cart-action clearfix">
                            <a href="<?php echo get_permalink ( get_option ( 'woocommerce_cart_page_id' ) ); ?>"
                               class="btn btn-custom4"><?php esc_html_e( "View Cart",'welldone' ) ?></a>
                            <a href="<?php echo get_permalink ( get_option ( 'woocommerce_checkout_page_id' ) ); ?>"
                               class="btn btn-custom"><?php esc_html_e( "Checkout",'welldone' ) ?></a>
                        </div><!-- End .cart-action -->
                    <?php } else { ?>
                        <div class="cart-total">
                            <?php esc_html__( 'No products in the cart.','welldone' ); ?>
                        </div>
                    <?php } ?>
                </div>
                <!-- End .dropdown-menu -->
            </div>
        </div><!-- End .cart-dropdown -->

    <?php
    }
}

// get ajax cart fragment
add_filter ( 'woocommerce_add_to_cart_fragments', 'welldone_mini_cart_ajax_load' );
function welldone_mini_cart_ajax_load($fragments) {
global $woocommerce;
ob_start ();
echo '<span class="cart-text-price">';
if ( sizeof ( WC ()->cart->get_cart () ) > 0 ) {
    echo WC ()->cart->get_cart_total ();
} else {
    echo get_woocommerce_currency_symbol () . "0";
}
    echo '</span>';
    $fragments[ '#welldone_minicart_wrapper > #cart-dropdown .cart-text-price' ] = ob_get_clean ();
    ob_start();
    echo '<span class="cart-text-count">('.WC ()->cart->get_cart_contents_count().')</span>';
    $fragments[ '#welldone_minicart_wrapper > #cart-dropdown .cart-text-count' ] = ob_get_clean ();
    ob_start ();
    echo '<div class="dropdown-menu animated fadeIn shopping-cart" id="welldone_minicart_wrapper_content" role="menu">';
    echo '<div class="shopping-cart__top text-uppercase">';
     esc_html_e("Items in cart",'welldone'); echo '(' . WC ()->cart->get_cart_contents_count() . ')';
    echo '</div>';
    echo '<ul>';
         if ( sizeof ( WC ()->cart->get_cart () ) > 0 ) {
             foreach ( WC ()->cart->get_cart () as $cart_item_key => $cart_item ) {
                 $_product = apply_filters ( 'woocommerce_cart_item_product', $cart_item[ 'data' ], $cart_item, $cart_item_key );
                 $product_id = apply_filters ( 'woocommerce_cart_item_product_id', $cart_item[ 'product_id' ], $cart_item, $cart_item_key );
                 if ( $_product && $_product->exists () && $cart_item[ 'quantity' ] > 0 && apply_filters ( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                     $product_name = apply_filters ( 'woocommerce_cart_item_name', $_product->get_title (), $cart_item, $cart_item_key );
                     $thumbnail = apply_filters ( 'woocommerce_cart_item_thumbnail', $_product->get_image ( "welldone-shop-widget" ), $cart_item, $cart_item_key );
                     $product_price = apply_filters ( 'woocommerce_cart_item_price', WC ()->cart->get_product_price ( $_product ), $cart_item, $cart_item_key );
                     echo '<li class="shopping-cart__item">';
                     echo '<div class="shopping-cart__item__image pull-left"><a href="' . esc_url ( $_product->get_permalink ( $cart_item ) ) . '" title="Product Name">' . $thumbnail . '</a></div>';
                     echo '<div class="shopping-cart__item__info">';
                     echo '<div class="shopping-cart__item__info__title"><h2><a href="' . esc_url ( $_product->get_permalink ( $cart_item ) ) . '">' . $product_name . '</a></h2></div>';
                     echo '<div class="shopping-cart__item__info__price">' . $product_price . '</div>';
                     echo '<div class="shopping-cart__item__info__qty">' . __ ( "Quantity", 'welldone' ) . ':' . $cart_item[ 'quantity' ] . '</div>';
                     echo apply_filters ( 'woocommerce_cart_item_remove_link', sprintf ( '<div class="shopping-cart__item__info__delete"><a href="%s" class="icon icon-clear" title="%s"></a></div>', esc_url ( WC ()->cart->get_remove_url ( $cart_item_key ) ), __ ( 'Remove this item','welldone' ) ), $cart_item_key );
                     echo '</div>';
                     echo '</li>';
                 }
             }
             echo '<div class="shopping-cart__bottom clearfix">';
             echo '<div class="dropdown-subtotal">';
             echo '' . __ ( "Subtotal", 'welldone' ) . ':' . '<span class="shopping-cart__total">' . WC ()->cart->get_cart_subtotal () . '</span>';
             echo '</div>';
             echo '<div class="pull-left">';
             echo '<a href="' . get_permalink ( get_option ( 'woocommerce_cart_page_id' ) ) . '"class="btn btn--wd text-uppercase">' . __ ( "View Cart",'welldone' ) . '</a>';
             echo '</div>';
             echo '<div class="pull-right">';
             echo '<a href="' . get_permalink ( get_option ( 'woocommerce_checkout_page_id' ) ) . '" class="btn btn--wd text-uppercase">'.__( "Checkout",'welldone' ) . '</a>';
             echo '</div>';
          echo '</div>';
         }else{
            echo '<div class="cart-total">';
                  echo __ ( 'No products in the cart.','welldone' );
            echo '</div>';
         }
    echo '</ul>';
    echo '</div>';

    $fragments[ '#welldone_minicart_wrapper_content' ] = ob_get_clean ();
    ob_start();
    welldone_cart_total ();
    $fragments['#welldone_minicart_price'] = ob_get_clean();
return $fragments;
}

function welldone_add_to_cart($product_id = ''){
    global $product, $welldone_settings;

    if($product_id){
        $product = wc_get_product( $product_id );
    }
    if ( $welldone_settings[ 'welldone_show_add_to_cart_button' ] ) {
    echo apply_filters( 'woocommerce_loop_add_to_cart_link',
        sprintf( '<a href="%s" rel="nofollow" data-product_id="%s" data-product_sku="%s" data-quantity="%s" class="button %s product_type_%s btn btn-custom2 btn-sm min-width-sm" title="%s">%s</a>',
            esc_url( $product->add_to_cart_url() ),
            esc_attr( $product->id ),
            esc_attr( $product->get_sku() ),
            esc_attr( isset( $quantity ) ? $quantity : 1 ),
            $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
            esc_attr( $product->product_type ),
            esc_html($product->add_to_cart_text()),
            esc_html($product->add_to_cart_text())
        ),
        $product );
        }
}

function welldone_woo_wishlist_attributes(){

}
function welldone_woo_cart_attributes($cart_item){
    if(isset($cart_item['variation']) && !empty($cart_item['variation'])){ ?>
        <ul class='product-desc-list'>
        <?php foreach($cart_item['variation'] as $att){ ?>
            <li><?php echo esc_html($att); ?></li>
        <?php } ?>
        </ul>
    <?php }
}

function welldone_woocommerce_show_product(){
     global $welldone_settings,$welldone_shop_page_settings;
    $welldone_shop_page_settings['grid_ver'] = 'v_1';
    $welldone_shop_page_settings['list_ver']  = '';
    $welldone_shop_page_settings['welldone_shop_view']  = "grid";
    $meta_query = WC()->query->get_meta_query();
    $selecttype= $welldone_settings['welldone_product_bottom_products'];
    $count=0;
    foreach($selecttype as  $select){
        if($select){
            $count++;
        }
    }
        if($count>0){
        ?>
           <section class="content product-ups">
               <div class="container">
                 <!-- Modal -->
                <div class="modal quick-view zoom" id="quickView"  style=" opacity: 1">
                  <div class="modal-dialog">
                    <div class="modal-content"> </div>
                  </div>
                </div>       
                <!-- /.modal -->
                       <!-- <div class="row"> -->
                           <!-- <div class="col-md-12"> -->
       
                               <!-- <div role="tabpanel" class="product-tab-carousel"> -->
                                   <!-- Nav tabs -->
                                  <!--  <ul class="nav nav-lava" role="tablist">
                                   <?php
                                   $active = "active";
                                       foreach($selecttype as $key => $select){
                                           if($select !='') {
                                               if ( $key == "TopRated" ) {
       
                                                   $label = __ ( "Top Rated", 'welldone' );
                                               } elseif ( $key == "Latest" ) {
       
                                                   $label = __ ( "New Arrivals", 'welldone' );
                                               } elseif ( $key == "Popular" ) {
       
                                                   $label = __ ( "Popular Products", 'welldone' );
                                               } elseif ( $key == "Featured" ) {
       
                                                   $label = __ ( "Featured Products", 'welldone' );
                                               }
       
                                               echo ' <h2 class="text-center text-uppercase product-slider-title">' . $label . '</h2>';
                                               $active = '';
                                           }
                                       }
                                   ?>
                                   
                                   <!-- Tab Panes -->
                                   <!-- <div class="tab-content"> -->
                                   <?php
                                     $active = " active";
                                       foreach($selecttype as $key => $select){
                                           if($select !=''){ ?>
                                           
                                           <?php $active = ''; 
                                             if ( $key == "TopRated" ) {
       
                                                   $label = __ ( "Top Rated", 'welldone' );
                                               } elseif ( $key == "Latest" ) {
       
                                                   $label = __ ( "New Arrivals", 'welldone' );
                                               } elseif ( $key == "Popular" ) {
       
                                                   $label = __ ( "Popular Products", 'welldone' );
                                               } elseif ( $key == "Featured" ) {
       
                                                   $label = __ ( "Featured Products", 'welldone' );
                                               }
       
                                               echo ' <h2 class="text-center text-uppercase product-slider-title">' . $label . '</h2>';
                                               $active = '';
                                           ?>

                                           <div class="row product-carousel mobile-special-arrows animated-arrows product-grid four-in-row">
                                           <?php
                                           $meta_query = WC()->query->get_meta_query();
                                           $args=array(
                                               'post_type' => 'product',
                                               'posts_per_page' => 10,
                                               'post_status'         => 'publish',
                                               );
       
                                           if( $key=="TopRated"){
                                               $atts = array(
                                                   'orderby' => 'title',
                                                   'order'   => 'asc'
                                                );
                                               $args['meta_query'] = $meta_query;
                                               add_filter('posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses'));
                                               $products = new WP_Query(apply_filters('woocommerce_shortcode_products_query', $args, $atts));
                                               remove_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );
       
                                           }elseif($key=="Latest"){
       
                                               $args['meta_query'] = $meta_query;
                                               $args['orderby'] = 'post_date';
                                               $args['order'] = 'desc';
                                               $args['meta_query'] = $meta_query;
                                               $products = new WP_Query($args);
       
                                           }elseif($key=="Popular"){
                                               $atts = array(
                                                   'per_page' => '10',
                                                   'columns'  => '4'
                                               );
                                               $args['meta_key']='total_sales';
                                               $args['orderby']='meta_value_num';
                                               $args['meta_query'] = $meta_query;
                                               $products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
       
                                           }elseif($key=="Featured"){
                                $meta_query[] = array(
                                                   'key'   => '_featured',
                                                   'value' => 'yes'
                                    );
                                    $args['meta_query'] = $meta_query;
                                               $args['orderby'] = 'date';
                                               $args['order']  = 'desc';
                                               $atts = array(
                                                   'orderby' => 'post_date',
                                                   'order'   => 'desc'
                                                );
                                               $products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
       
                                           }
                                           if ($products->have_posts()) :
                                               while ( $products->have_posts() ) : $products->the_post();
                                                   wc_get_template_part( 'content', 'product' );
                                               endwhile; // end of the loop.
                                           endif;
                                           $args = array();
                                           $atts = array();
                                           wp_reset_postdata();
                                           ?>
                                           </div>
                                       <!-- </div> -->
                                       <?php } } ?>
                                   <!-- </div> -->
                               <!-- </div> -->
                           <!-- </div> -->
                       <!-- </div> -->
                  </div>
           </section>
<?php
        }
}


// Woocommerce Vendor Function More Product


 if(class_exists('WC_Vendors') ){
     
function welldone_more_seller_product(){
global $product, $woocommerce_loop;
if ( empty( $product ) || ! $product->exists() ) {
    return;
}
global $post;
if ( ! WCV_Vendors::is_vendor( $post->post_author ) ) {
    return;
}
$meta_query = WC()->query->get_meta_query();
$args = array(
    'post_type'             => 'product',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'no_found_rows'         => 1,
    'posts_per_page'        => 10,
    'author'                => get_the_author_meta( 'ID' ),
    'meta_query'            => $meta_query,
    'orderby'               => 'rand'
);

$products = new WP_Query( $args );
if ( $products->have_posts() ) : ?>
    <div class="container">
        <h2 class="vendor-moreseller"><?php esc_html_e( 'More from this seller&hellip;', 'welldone' ); ?></h2>
        <?php woocommerce_product_loop_start(); ?>
        <div class="owl-carousel product-featured-carousel">
            <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                    <?php wc_get_template_part( 'content-product' ); ?>
            <?php endwhile; // end of the loop. ?>
        </div>
        <?php woocommerce_product_loop_end(); ?>
    </div>
<?php endif;
wp_reset_postdata();
}




 }