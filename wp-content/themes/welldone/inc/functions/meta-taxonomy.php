<?php
//include the main class file
require_once ( get_template_directory() ."/inc/plugins/taxonomy_meta_class/Tax-meta-class.php" );
class Tax_Meta_Validate{
    function welldone_validate_url ( $url ) {
        // must start with http:// or https://
        if ( 0 !== strpos ( $url, 'http://' ) && 0 !== strpos ( $url, 'https://' ) ) {
            return '';
        }
        // must pass validation
        if ( !filter_var ( $url, FILTER_VALIDATE_URL ) ) {
            return '';
        }
        return $url;
    }
}

if ( is_admin () ) {
    /*
     * prefix of meta keys, optional
     */
    $prefix = 'welldone_';
    /*
     * configure your meta box
     */

    $config = array (
        'id' => 'welldone_meta_box',          // meta box id, unique per meta box
        'title' => 'welldone Meta Box',       // meta box title
        'context' => 'advanced',
        'pages' => array ( "product_cat", "product_tag" ),  // where the meta box appear: normal (default), advanced, side; optional
        'fields' => array (),              // list of meta fields (can be added by field arrays)
        'local_images' => false,           // Use local or hosted images (meta box images for add/remove)
        'use_with_theme' => get_template_directory_uri() . '/inc/plugins/taxonomy_meta_class',       //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
    );
    $welldone_product_meta = new Tax_Meta_Class( $config );
    $welldone_product_meta->addSelect ( $prefix . 'product_cat_columns',
        array (
            '' => esc_html__( 'Default', 'welldone' ),
            '1' => "1",
            '2' => "2",
            '3' => "3",
            '4' => "4",
            '5' => "5",
            '6' => "6",
        ),
        array (
            'name' => esc_html__( 'Product Columns', 'welldone' ),
        )
    );
    $welldone_product_meta->addSelect ( $prefix . 'product_cat_view',
        array (
            '' => esc_html__( 'Default', 'welldone' ),
            'v_1' => esc_html__( "Grid Style 1", 'welldone' ),
            'v_2' => esc_html__( "Grid Style 2", 'welldone' ),
            'v_3' => esc_html__( "Grid Style 3", 'welldone' ),
            'v_4' => esc_html__( "Grid Style 4", 'welldone' ),
            'list' => esc_html__( "List", 'welldone' ),
            'list_right' => esc_html__( "List Right Aligned", 'welldone' ),
        ),
        array (
            'name' => esc_html__( 'Product Page Style', 'welldone' ),
        )
    );
    $config2 = array (
        'id' => 'welldone_meta_box',          // meta box id, unique per meta box
        'title' => 'welldone Meta Box',       // meta box title
        'context' => 'advanced',
        'pages' => array ( 'category', 'post_tag', 'product_cat', 'product_tag', 'portfolio-category', 'faq-category' ),  // where the meta box appear: normal (default), advanced, side; optional
        'fields' => array (),              // list of meta fields (can be added by field arrays)
        'local_images' => false,           // Use local or hosted images (meta box images for add/remove)
        'use_with_theme' => get_template_directory() . '/inc/plugins/taxonomy_meta_class',       //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
    );

   // $config[ 'page' ] = ;        // taxonomy name, accept categories, post_tag and custom taxonomies
    /*
     * Initiate your meta box
     */
    $welldone_meta = new Tax_Meta_Class( $config2 );
    /*
     * Add fields to your meta box
     */
    //radio field
    $welldone_meta->addRadio ( $prefix . 'taxonomy_layout',
        array (
            'default' => esc_html__( 'Default Layout', 'welldone' ),
            'left' => esc_html__( 'Left Sidebar', 'welldone' ),
            'right' => esc_html__( 'Right Sidebar', 'welldone' ),
            'both' => esc_html__( 'Both Sidebar', 'welldone' ),
            'no' => esc_html__( 'Full Width', 'welldone' )
        ),
        array (
            'name' => esc_html__( 'Page Layout', 'welldone' ),
            'std' => array ( 'default' )
        )
    );

    $wp_registered_sidebar = wp_get_sidebars_widgets ();
    $welldone_sidebar[] = esc_html__("Default",'welldone');
    foreach ( $wp_registered_sidebar as $sidebar => $sidebar_info ) {
        if ( $sidebar == 'wp_inactive_widgets' ) continue;
        $welldone_sidebar[ $sidebar ] = ucwords ( str_replace ( array ( '_', '-' ), ' ', $sidebar ) );
    }
    $welldone_meta->addSelect ( $prefix . 'taxonomy_left_sidebar',
        $welldone_sidebar,
        array (
            'name' => esc_html__( 'Left Sidebar', 'welldone' ),
        )
    );
    $welldone_meta->addSelect ( $prefix . 'taxonomy_right_sidebar',
        $welldone_sidebar,
        array (
            'name' => esc_html__( 'Right Sidebar', 'welldone' ),
        )
    );
    $welldone_meta->addRadio ( $prefix . 'taxonomy_banner_type',
        array (
            '' => esc_html__( 'No Banner', 'welldone' ),
            'image' => esc_html__( 'Image', 'welldone' ),
            'video' => esc_html__( 'Video', 'welldone' ),
            'rev_slider' => esc_html__( 'Revolution Slider', 'welldone' ),
            'custom_banner' => esc_html__( 'Custom Banner', 'welldone' ),
        ),
        array (
            'name' => esc_html__( 'Banner Type', 'welldone' ),
            'std' => array ( '' )
        )
    );
    //Image field
    $welldone_meta->addImage($prefix.'taxonomy_banner_image',
        array(
            'name'  => esc_html__('Banner Image ','welldone'),
            'desc'  => esc_html__( 'Only useful when the banner type is Image', 'welldone' ),
            'height'=>  '400px'
        )
    );
    //text field
    $welldone_meta->addText($prefix.'taxonomy_banner_video',
        array(
            'name'=>  esc_html__('Banner video URL','welldone'),
            'validate_func' => "welldone_validate_url",
            'style' => 'width: 100% !important;',
            'desc' => esc_html__('Only useful when the banner type is Video. Add a valid flash video url like: youtube, vimeo, dailymotion etc. If you put an invalid url the data will not be saved.','welldone')
        )
    );

    //wysiwyg field
    $welldone_meta->addWysiwyg($prefix.'taxonomy_banner_custom',
        array(
            'name'=> esc_html__('Custom Banner','welldone'),
            'desc' => esc_html__("Add custom banner from this editor",'welldone')
        )
    );
    /* Don't Forget to Close up the meta box decleration
     */
    //Finish Meta Box Decleration
    $welldone_meta->Finish ();
    $welldone_product_meta->Finish ();
}