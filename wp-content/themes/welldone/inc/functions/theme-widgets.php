<?php 

add_action( 'widgets_init', 'welldone_register_general_widgets' );
function welldone_register_general_widgets() {
      
    // WooCommerce widgets
    if ( class_exists( 'WooCommerce' ) ) {
        register_widget('Welldone_Widget_Product_Categories');
        register_widget('Welldone_WC_Widget_Products');
    }

    // WPML widgets
    if ( class_exists('woocommerce_wpml') ) {
        register_widget('Welldone_Language_selector_widget');
        register_widget('Welldone_WPML_Currency_Switcher_Widget');
    }

}



class Welldone_Widget_Product_Categories extends WP_Widget {

    var $woo_widget_cssclass;
    var $woo_widget_description;
    var $woo_widget_idbase;
    var $woo_widget_name;
    var $cat_ancestors;
    var $current_cat;

    /**
     * constructor
     *
     * @access public
     * @return void
     */
    function __construct() {

        /* Widget variable settings. */
        $this->woo_widget_cssclass = 'woocommerce widget_product_categories';
        $this->woo_widget_description = esc_html__( 'A list or dropdown of product categories.', 'welldone' );
        $this->woo_widget_idbase = 'woocommerce_product_categories';
        $this->woo_widget_name = esc_html__( 'WFT WooCommerce Product Categories', 'welldone' );

        /* Widget settings. */
        $widget_ops = array( 'classname' => $this->woo_widget_cssclass, 'description' => $this->woo_widget_description );

        /* Create the widget. */
        parent::__construct('product_categories', $this->woo_widget_name, $widget_ops);
    }


    /**
     * widget function.
     *
     * @see WP_Widget
     * @access public
     * @param array $args
     * @param array $instance
     * @return void
     */
    function widget( $args, $instance ) {
        extract( $args );

        $title = apply_filters('widget_title', empty( $instance['title'] ) ? esc_html__( 'Product Categories', 'welldone' ) : $instance['title'], $instance, $this->id_base);
        $c = $instance['count'] ? '1' : '0';
        $h = $instance['hierarchical'] ? true : false;
        $s = (isset($instance['show_children_only']) && $instance['show_children_only']) ? '1' : '0';
        $d = $instance['dropdown'] ? '1' : '0';
        $o = isset($instance['orderby']) ? $instance['orderby'] : 'order';

        echo "" . $before_widget;
        if ( $title ) echo "".$before_title . $title . $after_title;

        $cat_args = array( 'show_count' => $c, 'hierarchical' => $h, 'taxonomy' => 'product_cat' );

        $cat_args['menu_order'] = false;

        if ( $o == 'order' ) {

            $cat_args['menu_order'] = 'asc';

        } else {

            $cat_args['orderby'] = 'title';

        }

        if ( $d ) {

            // Stuck with this until a fix for http://core.trac.wordpress.org/ticket/13258
            woocommerce_product_dropdown_categories( $c, $h, 0, $o );

            ?>
            <script type='text/javascript'>
                /* <![CDATA[ */
                var product_cat_dropdown = document.getElementById("dropdown_product_cat");
                function onProductCatChange() {
                    if ( product_cat_dropdown.options[product_cat_dropdown.selectedIndex].value !=='' ) {
                        $url = home_url() . /?product_cat="+product_cat_dropdown.options[product_cat_dropdown.selectedIndex].value;
                        location.href = "<?php echo esc_url( $url ); ?>
                    }
                }
                product_cat_dropdown.onchange = onProductCatChange;
                /* ]]> */
            </script>
        <?php

        } else {

            global $wp_query, $post, $woocommerce;

            $this->current_cat = false;
            $this->cat_ancestors = array();

            if ( is_tax('product_cat') ) {

                $this->current_cat = $wp_query->queried_object;
                $this->cat_ancestors = get_ancestors( $this->current_cat->term_id, 'product_cat' );

            } elseif ( is_singular('product') ) {

                $product_category = wc_get_product_terms( $post->ID, 'product_cat', array( 'orderby' => 'parent' ) );

                if ( $product_category ) {
                    $this->current_cat   = end( $product_category );
                    $this->cat_ancestors = get_ancestors( $this->current_cat->term_id, 'product_cat' );
                }

            }

            $cat_args['walker']             = new WFT_Product_Cat_List_Walker;
            $cat_args['title_li']           = '';
            $cat_args['show_children_only'] = ( isset( $instance['show_children_only'] ) && $instance['show_children_only'] ) ? 1 : 0;
            $cat_args['pad_counts']         = 1;
            $cat_args['show_option_none']   = esc_html__('No product categories exist.', 'welldone' );
            $cat_args['current_category']   = ( $this->current_cat ) ? $this->current_cat->term_id : '';
            $cat_args['current_category_ancestors'] = $this->cat_ancestors;

            echo '<ul class="expander-list">';

            wp_list_categories( apply_filters( 'woocommerce_product_categories_widget_args', $cat_args ) );

            echo '</ul>';

        }

        echo "".$after_widget;
    }


    /**
     * update function.
     *
     * @see WP_Widget->update
     * @access public
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['orderby'] = strip_tags($new_instance['orderby']);
        $instance['count'] = !empty($new_instance['count']) ? 1 : 0;
        $instance['hierarchical'] = !empty($new_instance['hierarchical']) ? true : false;
        $instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;
        $instance['show_children_only'] = !empty($new_instance['show_children_only']) ? 1 : 0;

        return $instance;
    }


    /**
     * form function.
     *
     * @see WP_Widget->form
     * @access public
     * @param array $instance
     * @return void
     */
    function form( $instance ) {
        //Defaults
        $instance = wp_parse_args( (array) $instance, array( 'title' => '') );
        $title = esc_attr( $instance['title'] );
        $orderby = isset( $instance['orderby'] ) ? $instance['orderby'] : 'order';
        $count = isset($instance['count']) ? (bool) $instance['count'] :false;
        $hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
        $dropdown = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
        $show_children_only = isset( $instance['show_children_only'] ) ? (bool) $instance['show_children_only'] : false;
        ?>
        <p><label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e( 'Title:', 'welldone' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id('title') ); ?>" name="<?php echo esc_attr( $this->get_field_name('title') ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

        <p><label for="<?php echo esc_attr($this->get_field_id('orderby')); ?>"><?php esc_html_e( 'Order by:', 'welldone' ) ?></label>
            <select id="<?php echo esc_attr( $this->get_field_id('orderby') ); ?>" name="<?php echo esc_attr( $this->get_field_name('orderby') ); ?>">
                <option value="order" <?php selected($orderby, 'order'); ?>><?php esc_html_e( 'Category Order', 'welldone' ); ?></option>
                <option value="name" <?php selected($orderby, 'name'); ?>><?php esc_html_e( 'Name', 'welldone' ); ?></option>
            </select></p>

        <p><input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('dropdown') ); ?>" name="<?php echo esc_attr( $this->get_field_name('dropdown') ); ?>"<?php checked( $dropdown ); ?> />
            <label for="<?php echo esc_attr($this->get_field_id('dropdown')); ?>"><?php esc_html_e( 'Show as dropdown', 'welldone' ); ?></label><br />

            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('count') ); ?>" name="<?php echo esc_attr( $this->get_field_name('count') ); ?>"<?php checked( $count ); ?> />
            <label for="<?php echo esc_attr($this->get_field_id('count')); ?>"><?php esc_html_e( 'Show post counts', 'welldone' ); ?></label><br />

            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('hierarchical') ); ?>" name="<?php echo esc_attr( $this->get_field_name('hierarchical') ); ?>"<?php checked( $hierarchical ); ?> />
            <label for="<?php echo esc_attr($this->get_field_id('hierarchical')); ?>"><?php esc_html_e( 'Show hierarchy', 'welldone' ); ?></label><br/>

            <input type="checkbox" class="checkbox" id="<?php echo esc_attr( $this->get_field_id('show_children_only') ); ?>" name="<?php echo esc_attr( $this->get_field_name('show_children_only') ); ?>"<?php checked( $show_children_only ); ?> />
            <label for="<?php echo esc_attr($this->get_field_id('show_children_only')); ?>"><?php esc_html_e( 'Only show children for the current category', 'welldone' ); ?></label></p>
    <?php
    }

}

if ( class_exists( 'WooCommerce' ) ) {
class Welldone_WC_Widget_Products extends WC_Widget {

    /**
     * Constructor.
     */
    public function __construct() {
        $this->widget_cssclass    = 'woocommerce widget_products';
        $this->widget_description = esc_html__( 'Display a list of your products on your site.','welldone' );
        $this->widget_id          = 'woocommerce_products';
        $this->widget_name        = esc_html__( 'Welldone WooCommerce Products','welldone' );
        $this->settings           = array(
            'title'  => array(
                'type'  => 'text',
                'std'   => esc_html__( 'Products','welldone' ),
                'label' => esc_html__( 'Title','welldone' )
            ),
            'number' => array(
                'type'  => 'number',
                'step'  => 1,
                'min'   => 1,
                'max'   => '',
                'std'   => 5,
                'label' => esc_html__( 'Number of products to show','welldone' )
            ),
            'show' => array(
                'type'  => 'select',
                'std'   => '',
                'label' => esc_html__( 'Show','welldone' ),
                'options' => array(
                    ''         => esc_html__( 'All Products','welldone' ),
                    'featured' => esc_html__( 'Featured Products','welldone' ),
                    'onsale'   => esc_html__( 'On-sale Products','welldone' ),
                )
            ),
            'orderby' => array(
                'type'  => 'select',
                'std'   => 'date',
                'label' => esc_html__( 'Order by','welldone' ),
                'options' => array(
                    'date'   => esc_html__( 'Date','welldone' ),
                    'price'  => esc_html__( 'Price','welldone' ),
                    'rand'   => esc_html__( 'Random','welldone' ),
                    'sales'  => esc_html__( 'Sales','welldone' ),
                )
            ),
            'order' => array(
                'type'  => 'select',
                'std'   => 'desc',
                'label' => _x( 'Order', 'Sorting order','welldone' ),
                'options' => array(
                    'asc'  => esc_html__( 'ASC','welldone' ),
                    'desc' => esc_html__( 'DESC','welldone' ),
                )
            ),
            'hide_free' => array(
                'type'  => 'checkbox',
                'std'   => 0,
                'label' => esc_html__( 'Hide free products','welldone' )
            ),
            'show_hidden' => array(
                'type'  => 'checkbox',
                'std'   => 0,
                'label' => esc_html__( 'Show hidden products','welldone' )
            )
        );

        parent::__construct();
    }

    /**
     * Query the products and return them.
     * @param  array $args
     * @param  array $instance
     * @return WP_Query
     */
    public function get_products( $args, $instance ) {
        $number  = ! empty( $instance['number'] ) ? absint( $instance['number'] ) : $this->settings['number']['std'];
        $show    = ! empty( $instance['show'] ) ? sanitize_title( $instance['show'] ) : $this->settings['show']['std'];
        $orderby = ! empty( $instance['orderby'] ) ? sanitize_title( $instance['orderby'] ) : $this->settings['orderby']['std'];
        $order   = ! empty( $instance['order'] ) ? sanitize_title( $instance['order'] ) : $this->settings['order']['std'];

        $query_args = array(
            'posts_per_page' => $number,
            'post_status'    => 'publish',
            'post_type'      => 'product',
            'no_found_rows'  => 1,
            'order'          => $order,
            'meta_query'     => array()
        );

        if ( empty( $instance['show_hidden'] ) ) {
            $query_args['meta_query'][] = WC()->query->visibility_meta_query();
            $query_args['post_parent']  = 0;
        }

        if ( ! empty( $instance['hide_free'] ) ) {
            $query_args['meta_query'][] = array(
                'key'     => '_price',
                'value'   => 0,
                'compare' => '>',
                'type'    => 'DECIMAL',
            );
        }

        $query_args['meta_query'][] = WC()->query->stock_status_meta_query();
        $query_args['meta_query']   = array_filter( $query_args['meta_query'] );

        switch ( $show ) {
            case 'featured' :
                $query_args['meta_query'][] = array(
                    'key'   => '_featured',
                    'value' => 'yes'
                );
                break;
            case 'onsale' :
                $product_ids_on_sale    = wc_get_product_ids_on_sale();
                $product_ids_on_sale[]  = 0;
                $query_args['post__in'] = $product_ids_on_sale;
                break;
        }

        switch ( $orderby ) {
            case 'price' :
                $query_args['meta_key'] = '_price';
                $query_args['orderby']  = 'meta_value_num';
                break;
            case 'rand' :
                $query_args['orderby']  = 'rand';
                break;
            case 'sales' :
                $query_args['meta_key'] = 'total_sales';
                $query_args['orderby']  = 'meta_value_num';
                break;
            default :
                $query_args['orderby']  = 'date';
        }

        return new WP_Query( apply_filters( 'woocommerce_products_widget_query_args', $query_args ) );
    }

    /**
     * Output widget.
     *
     * @see WP_Widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        if ( $this->get_cached_widget( $args ) ) {
            return;
        }

        ob_start();

        if ( ( $products = $this->get_products( $args, $instance ) ) && $products->have_posts() ) {
            $this->widget_start( $args, $instance );

            echo apply_filters( 'woocommerce_before_widget_product_list', '<div class="products-widget card"><div class="products-widget-carousel nav-dot">' );

            while ( $products->have_posts() ) {
                $products->the_post();
                wc_get_template( 'content-widget-product.php', array( 'show_rating' => false ) );
            }

            echo apply_filters( 'woocommerce_after_widget_product_list', '</div></div>' );

            $this->widget_end( $args );
        }

        wp_reset_postdata();

        echo $this->cache_widget( $args, ob_get_clean() );
    }
}
}
class Welldone_Language_selector_widget extends WP_Widget {

    public function __construct() {
        // указываем в конструкторе общие настройки которые будут видны в админке
        // название и описание виджета
        $widget_ops = array(
            'classname' => 'sidebar-box',
            'description' => esc_html__( "WPML Language selector for your site", "welldone") );
        parent::__construct('wft_lang_sel_widget','WFT Language Selector', $widget_ops);
        $this->alt_option_name = 'wft_lang_sel_widget';

        // при сохранении поста или удалении поста, а так же при смене темы
        // кеш виджета будет очищаться
        add_action( 'save_post', array(&$this, 'flush_widget_cache') );
        add_action( 'deleted_post', array(&$this, 'flush_widget_cache') );
        add_action( 'switch_theme', array(&$this, 'flush_widget_cache') );
    }

    // этот метод срабатывает непосредственно при выводе выджета на странице
    function widget($args, $instance) {

        // загружаем кеш, если есть кеш для данного виджета выводим его и выходим
        $cache = wp_cache_get('wft_lang_sel_widget', 'widget');

        global $wpdb;

        if ( !is_array($cache) ) {
            $cache = array();
        }

        if ( ! isset( $args['widget_id'] ) ) {
            $args['widget_id'] = $this->id;
        }

        if ( isset( $cache[ $args['widget_id'] ] ) ) {
            echo $cache[ $args['widget_id'] ];
            return;
        }

        extract($args);

        if ( function_exists('is_plugin_active') && is_plugin_active( 'woocommerce-multilingual/wpml-woocommerce.php' ) ) {

            ob_start();

            $langs = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');

            if ( !isset($no_wrapper) || !$no_wrapper ) {
                echo $before_widget;
            }

            if ( isset($title) && $title ) {
                echo $before_title . $title . $after_title;
            }

            echo '<span class="link_label">'. esc_html__('Language', 'welldone') . ':</span>';

            echo '<div class="fadelink">';

            echo '<a href="#">'.$langs[ICL_LANGUAGE_CODE]['native_name'].'</a>';

            echo '<div class="ul_wrapper" style="display: none; opacity: 1;"><ul>';

            foreach ( $langs as $lang ) {
                echo '<li><img alt="" src="'. $lang['country_flag_url'] . '"><a href="'. $lang['url'] . '">' . $lang['native_name'] . '</a></li>';
            }

            echo '</ul></div></div>';

            if ( !isset($no_wrapper) || !$no_wrapper ) {
                echo $after_widget;
            }

            // save cashe
            $cache[$args['widget_id']] = ob_get_flush();
            wp_cache_set('wft_lang_sel_widget', $cache, 'widget');
        } else {

            if ( !isset($no_wrapper) || !$no_wrapper ) {
                echo $before_widget;
            }

            if ( $title ) {
                echo $before_title . $title . $after_title;
            }

            esc_html_e('Language switcher disabled. WPML plugin is not active.', 'welldone');

            if ( !isset($no_wrapper) || !$no_wrapper ) {
                echo $after_widget;
            }

        }


    }

    function flush_widget_cache() {
        wp_cache_delete('wft_lang_sel_widget', 'widget');
    }

}
class Welldone_WPML_Currency_Switcher_Widget extends WP_Widget {

    public $widget_cssclass;
    public $widget_description;
    public $widget_idbase;
    public $widget_name;

    public function __construct() {

        /* Widget variable settings. */
        $this->widget_cssclass      = '';
        $this->widget_description   = esc_html__( 'Allows your customers to switch currency using WPML Plugin "Woocommerce Multilingual"','welldone' );
        $this->widget_idbase        = 'Welldone_WPML_Currency_Switcher_Widget';
        $this->widget_name          = esc_html__( 'Welldone WPML Currency Switcher','welldone' );

        /* Widget settings. */
        $widget_ops = array( 'classname' => $this->widget_cssclass, 'description' => $this->widget_description );

        /* Create the widget. */
        parent::__construct( 'Welldone_WPML_Currency_Switcher_Widget', $this->widget_name, $widget_ops );
    }

    /**
     * widget function.
     *
     * @see WP_Widget
     * @access public
     * @param array $args
     * @param array $instance
     * @return void
     */
    public function widget( $args, $instance ) {

        if ( !function_exists('is_plugin_active') || ! is_plugin_active( 'woocommerce-multilingual/wpml-woocommerce.php' ) ) {
            return;
        }

        global $woocommerce;

        extract( $args );

        echo $before_widget;

        do_shortcode('[welldone_wpml_currency_switcher]');

        echo $after_widget;

    }

}

 ?>
