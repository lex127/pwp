<?php
$meta_boxes[ ] = array (
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
    'id' => 'popup_options',
// Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => esc_html__('Popup Setting','welldone'),
// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array ( 'page', 'post', 'portfolio', 'clients', 'product','faq','testimonial' ),
// Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',
// Order of meta box: high (default), low. Optional.
    'priority' => 'low',
// Auto save: true, false (default). Optional.
    'autosave' => true,
// List of meta fields
    'fields' => array (
 
    )
);
?>