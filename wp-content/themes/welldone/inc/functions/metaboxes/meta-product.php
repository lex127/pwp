<?php
//global $welldone_sidebar;
$meta_boxes[ ] = array (
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
    'id' => 'client_options',

// Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' =>  esc_html__( 'Product Options', 'welldone' ),

// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array ( 'product' ),

// Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',

// Order of meta box: high (default), low. Optional.
    'priority' => 'high',

// Auto save: true, false (default). Optional.
    'autosave' => true,

// List of meta fields
    'fields' => array (

//Blog Template Options
// HEADING
        array (
            'type' => 'heading',
            'name' =>  esc_html__( 'Product Options', 'welldone' ),
            'id' => 'product_page_heading',
        ),
        array (
            'name' =>  esc_html__( 'Product Page Style', 'welldone' ),
            'id' => "{$prefix}product_page_style",
            'type' => 'select_advanced',
            'options' => array (
                'v_1'   =>  esc_html__('Style 1','welldone'),
                'v_2'   =>  esc_html__('Style 2','welldone'),
            ),
            'multiple' => false,
            'placeholder' =>  esc_html__( 'Select', 'welldone' ),
        ),
		array (
            'type' => 'text',
            'name' =>  esc_html__( 'Custom Tabs Heading 1', 'welldone' ),
            'id' => "{$prefix}tabs_custom_heading_one",
        ),
		 array(
            'name' => esc_html__( 'Custom Tabs Content 1', 'welldone' ),
            'id'   => "{$prefix}contact_tabs_content_one",
            'type' => 'wysiwyg',
            'raw'  => false,
            'options' => array(
                'textarea_rows' => 4,
                'teeny'         => false,
                'media_buttons' => false,
            ),
        ),
		array (
            'type' => 'text',
            'name' =>  esc_html__( 'Custom Tabs Heading 2', 'welldone' ),
            'id' => "{$prefix}tabs_custom_heading_two",
        ),
		 array(
            'name' => esc_html__( 'Custom Tabs Content 2', 'welldone' ),
            'id'   => "{$prefix}contact_tabs_content_two",
            'type' => 'wysiwyg',
            'raw'  => false,
            'options' => array(
                'textarea_rows' => 4,
                'teeny'         => false,
                'media_buttons' => false,
            ),
        ),
          array(
            'name' => esc_html__( 'Product_countdown', 'welldone' ),
            'id'   => "{$prefix}product_countdown",
            'type' => 'date',
        
        ),
    )
);
?>