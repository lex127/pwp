<?php
$meta_boxes[ ] = array (
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
    'id' => 'geenral_options',
// Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => esc_html__( 'General Options', 'welldone' ),
// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array ( 'page', 'post', 'portfolio', 'clients', 'product','faq','testimonial' ),
// Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',
// Order of meta box: high (default), low. Optional.
    'priority' => 'high',
// Auto save: true, false (default). Optional.
    'autosave' => true,
// List of meta fields
// List of meta fields
    'fields' => array (
        array (
            'type' => 'heading',
            'name' => esc_html__( 'Page Header Options ', 'welldone' ),
            'id' => 'page_header_heading',
        ),
        array (
            'name' => esc_html__( 'Select Header Type', 'welldone' ),
            'id' => "{$prefix}page_header",
            'type' => 'select_advanced',
            'options' => array (
                '1' => 'Header 1',
                '2' => 'Header 2',
                '3' => 'Header 3',
                '4' => 'Header 4',
            ),
            'multiple' => false,
            'placeholder' => esc_html__( 'Select', 'welldone' ),
            'desc' => 'Go to Appearance -> Theme Options to check headers previews'
        ),
		
		array (
            'name' => esc_html__( 'Hide Header', 'welldone' ),
            'id' => "{$prefix}header_hide",
            'type' => 'select_advanced',
            'desc' => 'Used in Hide Header',
            'options' => array(
                '' => esc_html__("Default",'welldone'),
                '' => esc_html__("Show",'welldone'),
                '1' => esc_html__("Hide",'welldone'),
            ),
        ),

        array (
            'type' => 'heading',
            'name' => esc_html__( 'Breadcrumb and Title', 'welldone' ),
            'id' => 'breadcrumb_title',
        ),
        array (
            'name' => esc_html__( 'Page Title', 'welldone' ),
            'id' => "{$prefix}hide_page_title",
            'type' => 'select_advanced',
            'options' => array(
                '' => esc_html__("Default",'welldone'),
                '2' => esc_html__("Show",'welldone'),
                '1' => esc_html__("Hide",'welldone'),
            ),
        ),
        array (
            'name' => esc_html__( 'Breadcrumb', 'welldone' ),
            'id' => "{$prefix}hide_breadcrumb",
            'type' => 'select_advanced',
              'options' => array(
                '' => esc_html__("Default",'welldone'),
                '2' => esc_html__("Show",'welldone'),
                '1' => esc_html__("Hide",'welldone'),
            ),
        ),
        array (
            'name' => esc_html__( 'Breadcrumb And Title Position', 'welldone' ),
            'id' => "{$prefix}breadcrumb_title_position",
            'type' => 'select_advanced',
            'options' => array (
                'text-left' => esc_html__( 'Left', 'welldone' ),
                'text-center' => esc_html__( 'Center', 'welldone' ),
                'text-right' => esc_html__( 'Right', 'welldone' ),
            ),
            'multiple' => false,
            'placeholder' => esc_html__( 'Select', 'welldone' ),
        ),
        array (
            'type' => 'heading',
            'name' => esc_html__( 'Page Body Options', 'welldone' ),
            'id' => 'page_body_options',
        ),
        array (
            'name' => esc_html__( 'Use Full Width Body Size', 'welldone' ),
            'id' => "{$prefix}container_size",
            'type' => 'select_advanced',
            'options' => array (
                ''    => esc_html__('Use Default',"welldone"),
                'yes' => esc_html__('Yes',"welldone"),
                'no'  => esc_html__('No',"welldone"),
            ),
            'std' => ''
        ),
        array (
            'name' => esc_html__( 'Page Wrapper', 'welldone' ),
            'id' => "{$prefix}theme_wrapper",
            'type' => 'select_advanced',
            'options' => array (
                ''  => esc_html__('Use Default',"welldone"),
                'wide'  => esc_html__('Wide',"welldone"),
                'boxed'  => esc_html__('Boxed',"welldone"),
                'fullwidth'  => esc_html__('Fullwidth',"welldone"),
            ),
               'std' => ''
        ),
        array (
            'name' => esc_html__( 'Background Mode', 'welldone' ),
            'id' => "{$prefix}bg_mode",
            'type' => 'select_advanced',
            'options' => array (
                'image'  => esc_html__('Image',"welldone"),
                'custom-image'  => esc_html__('Custom Image',"welldone"),
            ),
            'placeholder' => esc_html__('Select Background Mode','welldone'),
            'desc' => esc_html__("Select Background mode if Page wrapper is set to boxed","welldone")
        ),
        array (
            'name' => esc_html__( 'Select Image', 'welldone' ),
            'id' => "{$prefix}bg_select",
            'type' => 'image_select',
            'class' => 'select_lg_img',
            'options' => array (
                 get_template_directory_uri().'/images/bg-images/bg.jpg'  => get_template_directory_uri().'/images/bg-images/bg.jpg',
                 get_template_directory_uri().'/images/bg-images/bg1.jpg' => get_template_directory_uri().'/images/bg-images/bg1.jpg',
                 get_template_directory_uri().'/images/bg-images/bg2.jpg' => get_template_directory_uri().'/images/bg-images/bg2.jpg',
            ),
            'std' => '',
            'desc' => esc_html__('Select If background Mode is selected to Image','welldone'),
            'multiple' => false,
        ),
        array (
            'name' => esc_html__( 'Select Image', 'welldone' ),
            'id' => "{$prefix}bg_custom_select",
            'type' => 'image_advanced',
            'max_file_uploads' => 1,
            'desc' => esc_html__('Select If background Mode is selected to Custom Image','welldone'),
        ),
        array (
            'name' => esc_html__( 'Background Color', 'welldone' ),
            'id' => "{$prefix}bg_color",
            'type' => 'color',
        ),
        array(
            'id' => "{$prefix}bg_repeat",
            'type'     => 'select_advanced',
            'name'    => esc_html__('Background Repeat', 'welldone'),
            'options'  => array(
                'no-repeat' => esc_html__("No Repeat","welldone"),
                'repeat'    => esc_html__("Repeat All","welldone"),
                'repeat-x'  => esc_html__("Repeat Horizontally","welldone"),
                'repeat-y'  => esc_html__("Repeat Vertically","welldone"),
                'inherit'   => esc_html__("Inherit","welldone"),
            ),
            'placeholder'  => esc_html__('Background Repeat','welldone'),
        ),
        array(
            'id' => "{$prefix}bg_position",
            'type'     => 'select_advanced',
            'name'    => esc_html__('Background Position', 'welldone'),
            'options'  => array(
                "left top"      =>  esc_html__("Left Top",'welldone'),
                "left center"   =>  esc_html__("Left center",'welldone'),
                "left bottom"   =>  esc_html__("Left Bottom",'welldone'),
                "center top"    =>  esc_html__("Center Top",'welldone'),
                "center center" =>  esc_html__("Center Center",'welldone'),
                "center bottom" =>  esc_html__("Center Bottom",'welldone'),
                "right top"     =>  esc_html__("Right Top",'welldone'),
                "right center"  =>  esc_html__("Right center",'welldone'),
                "right bottom"  =>  esc_html__("Right Bottom",'welldone'),
            ),
            'placeholder'  => esc_html__('Background Position','welldone'),
        ),
        array(
            'id'        => "{$prefix}bg_size",
            'type'      => 'select_advanced',
            'name'      => esc_html__('Background Size', 'welldone'),
            'options'   => array(
                "cover"      =>  esc_html__("Cover",'welldone'),
                "inherit"   =>  esc_html__("Inherit",'welldone'),
                "contain"   =>  esc_html__("Contain",'welldone'),
            ),
            'placeholder'  => esc_html__('Background Size','welldone'),
        ),
        array (
            'name' => esc_html__( 'Page Layout', 'welldone' ),
            'id' => "{$prefix}page_layout",
            'type' => 'image_select',
            'options' => array (
				'' => get_template_directory_uri() . '/images/default/default.png',
                'no' => get_template_directory_uri() . '/images/default/1col.png',
                'left' => get_template_directory_uri() . '/images/default/2cl.png',
                'right' => get_template_directory_uri() . '/images/default/2cr.png',
                'both' => get_template_directory_uri() . '/images/default/3cm.png',
            ),
            'multiple' => false,
        ),
        array (
            'name' => esc_html__( 'Select Left Sidebar', 'welldone' ),
            'id' => "{$prefix}page_sidebar_left",
            'type' => 'select_advanced',
            'options' => $welldone_sidebar,
            'multiple' => false,
            'placeholder' => esc_html__( 'Select', 'welldone' ),
        ),
        array (
            'name' => esc_html__( 'Select Right Sidebar', 'welldone' ),
            'id' => "{$prefix}page_sidebar_right",
            'type' => 'select_advanced',
            'options' => $welldone_sidebar,
            'multiple' => false,
            'placeholder' => esc_html__( 'Select', 'welldone' ),
        ),

//Page Footer Options
        array (
            'type' => 'heading',
            'name' => esc_html__( 'Page Footer Options ', 'welldone' ),
            'id' => 'page_footer_heading',
        ),
        array (
            'name' => esc_html__( 'Footer Widget Columns', 'welldone' ),
            'id' => "{$prefix}footer_widget_columns",
            'type' => 'image_select',
            'desc' => 'Used in footer 1,2,6,7 and 8.',
            'options' => array (
				'' => get_template_directory_uri() . '/images/default/default.png',
                '1' => get_template_directory_uri().'/images/default/1col.png',
                '2' => get_template_directory_uri().'/images/default/2col.png',
                '3' => get_template_directory_uri().'/images/default/3col.png',
                '4' => get_template_directory_uri().'/images/default/4col.png',
                '5'=> get_template_directory_uri().'/images/default/5col.png',
            ),
            'multiple' => false,
        ),
    )
);
?>