<?php
//global $welldone_sidebar;
$meta_boxes[ ] = array (
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
    'id' => 'page_options',
// Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' =>  esc_html__( 'Page Options', 'welldone' ),
// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array ( 'page' ),
// Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',
// Order of meta box: high (default), low. Optional.
    'priority' => 'high',
// Auto save: true, false (default). Optional.
    'autosave' => true,
// List of meta fields
    'fields' => array (
//Blog Template Options
// HEADING
        array (
            'type' => 'heading',
            'name' =>  __ ( 'Blog Page Options (Use in Blog template)<br /><small>Note: If this page is not selected for front page as Your latest posts under Settings -> Reading </small>', 'welldone' ),
            'id' => 'blog_page_heading',
        ),
        array(
            'name' =>  esc_html__( 'Blog Page Type', 'welldone' ),
            'id' => "{$prefix}blog_type",
            'type' => 'select_advanced',
            'options' => array (
                'classic' =>  esc_html__('Classic(Default)',"welldone"),
                "blog-masonry"=> esc_html__("Blog Masonry","welldone")
            ),
            'placeholder' =>  esc_html__( 'Select', 'welldone' )
        ),
        array(
            'id'=>"{$prefix}blog_masonry_columns",
            'type' => 'image_select',
            'name' =>  esc_html__('Select Blog Masonry Columns',"welldone"),
            'options' => array(
			
				'' => get_template_directory_uri() . '/images/default/default.png',
                '2' => get_template_directory_uri().'/images/default/2col.png',
                '3' => get_template_directory_uri().'/images/default/3col.png',
                '4' => get_template_directory_uri().'/images/default/4col.png',
                '5'=> get_template_directory_uri().'/images/default/5col.png',
            ),
            'std' => '5',
            'desc' =>  esc_html__("Used in Blog Masonry Only","welldone")
        ),
        array (
            'name' =>  esc_html__( 'Posts Per Page', 'welldone' ),
            'id' => "{$prefix}no_of_posts",
            'type' => 'number',
            'desc' => 'input posts per page number'
        ),
        array (
            'name' =>  esc_html__( 'Exclude posts', 'welldone' ),
            'id' => "{$prefix}exclude_posts",
            'type' => 'text',
            'desc' => 'input ids comma seperated'
        ),
        array (
            'name' =>  esc_html__( 'Show Excerpt', 'welldone' ),
            'id' => "{$prefix}blog_excerpt",
            'type' => 'select_advanced',
            'options' => array (
                '' =>  esc_html__( 'Use Default', 'welldone' ),
                '1' =>  esc_html__( 'Show Excerpt', 'welldone' ),
            ),
        ),
        array (
            'name' =>  esc_html__( 'Excerpt Length', 'welldone' ),
            'id' => "{$prefix}blog_excerpt_length",
            'type' => 'number',
            'desc' => 'The number of words',
            'std' => '0'
        ),
        array (
            'name' =>  esc_html__( 'Blog Post Title', 'welldone' ),
            'id' => "{$prefix}hide_blog_post_title",
            'type' => 'select_advanced',
            'options' => array (
                '' =>  esc_html__( 'Use Default', 'welldone' ),
                '1' =>  esc_html__( 'Hide Blog Post Title', 'welldone' ),
            ),
        ),
        array (
            'name' =>  esc_html__( 'Excerpt Type', 'welldone' ),
            'id' => "{$prefix}excerpt_type",
            'type' => 'select_advanced',
            'options' => array (
                '' =>  esc_html__( 'Use Default', 'welldone' ),
                'text' =>  esc_html__( 'Text', 'welldone' ),
                'html' =>  esc_html__( 'HTML', 'welldone' ),
            ),
        //   'std' => '',
        ),
        array (
            'name' => esc_html__( 'Blog Posts Author Name', 'welldone' ),
            'id' => "{$prefix}hide_blog_post_author",
            'type' => 'select_advanced',
            'options' => array (
                '' =>  esc_html__( 'Use Default', 'welldone' ),
                '1' =>  esc_html__( 'Hide Blog Posts Author Name', 'welldone' ),
            ),
        ),
        array (
            'name' => esc_html__( 'Blog Posts Category', 'welldone' ),
            'id' => "{$prefix}hide_blog_post_category",
            'type' => 'select_advanced',
            'options' => array (
                '' =>  esc_html__( 'Use Default', 'welldone' ),
                '1' =>  esc_html__( 'Hide Blog Posts Category', 'welldone' ),
            ),
        ),
        array (
            'name' => esc_html__( 'Blog Posts Tags', 'welldone' ),
            'id' => "{$prefix}hide_blog_post_tags",
            'type' => 'select_advanced',
            'options' => array (
                '' =>  esc_html__( 'Use Default', 'welldone' ),
                '1' =>  esc_html__( 'Hide Blog Posts Tags', 'welldone' ),
            ),
        ),
        array (
            'name' => esc_html__( 'pagination Type', 'welldone' ),
            'id' => "{$prefix}blog_pagination_type",
            'type' => 'select_advanced',
            'options' => array (
                '' =>  esc_html__( 'Use Default', 'welldone' ),
                'pagination' => 'Pagination',
                'infinite_scroll' => 'Infinite Scroll',
            ),
        ),

        //contact page options
        // array (
        //     'type' => 'heading',
        //     'name' =>  esc_html__( 'Contact Page Options (Use in Contact template)', 'welldone' ),
        //     'id' => 'contact_page_heading',
        // ),
        // array(
        //     'name' => esc_html__( 'Address', 'welldone' ),
        //     'id'   => "{$prefix}contact_address",
        //     'type' => 'wysiwyg',
        //     // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
        //     'raw'  => false,
        //     'std' => '<h3>Company address</h3><address>8808 Ave Dermentum, Onsectetur Adipiscing<br>Tortor Sagittis, CA 880986,<br>United States<br>CA 90896,<br>United States</address>',
        //     // Editor settings, see wp_editor() function: look4wp.com/wp_editor
        //     'options' => array(
        //         'textarea_rows' => 4,
        //         'teeny'         => false,
        //         'media_buttons' => false,
        //     ),
        // ),
        // array(
        //     'name' => esc_html__( 'Contact Information', 'welldone' ),
        //     'id'   => "{$prefix}contact_info",
        //     'type' => 'wysiwyg',
        //     // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
        //     'raw'  => false,
        //     'std' => '<h3>Contact Informations</h3><address>Email: stores@domain.com<br>Toll-free: (1800) 000 8808</address>',
        //     // Editor settings, see wp_editor() function: look4wp.com/wp_editor
        //     'options' => array(
        //         'textarea_rows' => 4,
        //         'teeny'         => false,
        //         'media_buttons' => false,
        //     ),
        // ),
        // array(
        //     'name' => esc_html__( 'Contact Message', 'welldone' ),
        //     'id'   => "{$prefix}contact_message",
        //     'type' => 'wysiwyg',
        //     // Set the 'raw' parameter to TRUE to prevent data being passed through wpautop() on save
        //     'raw'  => false,
        //     'std' => "<h2 class='regular-title short'>Let's Stay in Touch</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet.</p>",
        //     'options' => array(
        //         'textarea_rows' => 4,
        //         'teeny'         => false,
        //         'media_buttons' => false,
        //     ),
        // ),
    
 )
);
?>