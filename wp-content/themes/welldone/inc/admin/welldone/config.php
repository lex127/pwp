/* welldone Config Styles */
/* Created at <?php echo date("Y-m-d H:i:s") ?> */
<?php 
global $welldone_settings;
$s =   get_option('welldone_settings');
?>

/* theme general color customization todo:tahir code start here*/
<?php
if ( !empty( $s[ 'welldone_body_typography' ] ) ) {
$a = $s[ 'welldone_body_typography' ]; ?>
body{
<?php if ( $a[ 'font-family' ] ) { ?>
	font-family : <?php echo $a[ 'font-family' ] . ( ( $a[ 'font-backup' ] ) ? ',' . $a[ 'font-backup' ] : '' ); ?>;
<?php } ?>
<?php if ( $a[ 'font-weight' ] ) { ?>
	font-weight :<?php echo $a[ 'font-weight' ] ?>;
<?php } ?>
<?php if ( $a[ 'font-style' ] ) { ?>
	font-style :<?php echo $a[ 'font-style' ] ?>;
<?php } ?>
<?php if ( $a[ 'color' ] ) { ?>
	color :<?php echo $a[ 'color' ] ?>;
<?php } ?>
	<?php if ( $a[ 'line-height' ] ) { ?>
		line-height :<?php echo $a[ 'line-height' ] ?>;
	<?php } ?>
	<?php if ( $a[ 'font-size' ] ) { ?>
		font-size :<?php echo $a[ 'font-size' ] ?>;
	<?php } ?>
}
<?php } ?>

<?php
echo "/*Color Customization*/";
if($s['welldone_site_color'] && $s['welldone_site_color'] != '#536dfe'){
	$theme_color = $s['welldone_site_color'];
	$theme_shade_1 = welldone_generate_theme_color_shades($theme_color,1);
	$theme_shade_2 = welldone_generate_theme_color_shades($theme_color,2);
	$theme_shade_3 = welldone_generate_theme_color_shades($theme_color,3);
	$theme_shade_4 = welldone_generate_theme_color_shades($theme_color,4);
	$theme_shade_5 = welldone_generate_theme_color_shades($theme_color,5);
	$theme_shade_6 = welldone_generate_theme_color_shades($theme_color,6);
	$theme_shade_7 = welldone_generate_theme_color_shades($theme_color,7);
	?>
	/*for theme color*/

	::-moz-selection{
		background-color : <?php echo $theme_color ?>;
	}

	::selection{
		background-color : <?php echo $theme_color ?>;
	}

	<!-- a, -->
	.logo div span,
	.header--transparent .stuck .logo div span,
	.header--semi-transparent .stuck .logo div span,
	.navbar.navbar-wd .nav > li > a:hover,
	.navbar.navbar-wd .nav .open > a,
	.navbar.navbar-wd .nav .open > a:focus,
	.navbar.navbar-wd .nav .open > a:hover,
	.shopping-cart a:not(.btn):hover,
	.navbar-nav--vertical > li > a .badge,
	.widget_categories ul li a:hover,
	.single-product .product-details .variations .reset_variations,

	.header--only-logo .logo span,
	.logo div span,
	.header--transparent .stuck .logo div span,
	.header--semi-transparent .stuck .logo div span,
	.shopping-cart a:not(.btn):hover,
	.footer__column-links--variant2 .logo--footer span,
	a.custom-color,
	.custom-color,
	.icon-enable,
	.category-list li a:hover,
	.post p a,
	.post__title a:hover,
	.compare-box__header__toggle .icon:hover,
	.compare-box__items__list__item__delete a:hover,
	.filter-list li a.icon:hover,
	.review__comments a,
	.social-links--colorize .icon-mail:hover,
	.shopping-cart-table [class^="icon-"]:hover,
	.shopping-cart-table [class*=" icon-"]:hover,
	.slick-dots li.slick-active button:before,
	.blog .entry-content th > a,
	.list-blog .post h2 a:hover,
	.list-recent h4 a:hover,
	.slick-dots li.slick-active button::before {
		color: <?php echo $theme_color ?>;
	}
	

	.wp-tag-cloud li a:hover,
	.ui-slider-handle,
	.price_slider_amount button,
	.ui-slider-range.ui-widget-header.ui-corner-all,
	.ui-slider-handle.ui-state-default.ui-corner-all,
	.woocommerce .widget_layered_nav ul.yith-wcan-label li a:hover, 
	.woocommerce-page .widget_layered_nav ul.yith-wcan-label li a:hover, 
	.woocommerce .widget_layered_nav ul.yith-wcan-label li.chosen a, 
	.woocommerce-page .widget_layered_nav ul.yith-wcan-label li.chosen a,
	.yith-wcan-reset-navigation.button,
	.woocommerce table.wishlist_table .add-to-cart-product .button.add_to_cart_button,

	.btn-plus__links__icon,
	.btn-plus__links__icon:hover,
	.btn-plus__links__icon:active,
	.btn-plus__links__icon.focus,
	.btn-plus__links__icon:focus,
	.filters-by-category ul li a:hover,
	.filters-by-category ul li a .selected,
	.tags-list li a:hover,
	.hint:hover .tool .tip,
	.navbar-nav--vertical > li > a .badge,
	#slidemenu .navbar-nav > li a .badge,
	.mark--color,
	.badge.badge--menu,
	.badge.badge--squared,
	.banner--icon,
	.post--fill,
	.btn--wd,
	.btn--wd:hover,
	.btn--wd:active,
	.btn--wd.focus,
	.btn--wd:focus,
	.btn--round,
	.btn--round:hover,
	.btn--round:active,
	.btn--round.focus,
	.btn--round:focus,
	.input-group__bar:before,
	.input-group__bar:after,
	input[type=checkbox]:checked ~ label .box,
	.radio input:focus + .outer .inner,
	.radio .inner,
	.tag:hover,
	.tags-list li a:hover,
	.hint,
	.hint__dot,
	.hint:hover .tool,
	.hint:hover .tool .tip,
	.review__helpful a:hover,
	.tgl.checked + .tgl-btn,
	.animated-arrows .slick-prev .icon-wrap::before,
	.animated-arrows .slick-prev .icon-wrap::after,
	.animated-arrows .slick-next .icon-wrap::before,
	.animated-arrows .slick-next .icon-wrap::after,
	.tparrows.default.tp-leftarrow .tp-arr-allwrapper::before,
	.tparrows.default.tp-leftarrow .tp-arr-allwrapper::after,
	.tparrows.default.tp-rightarrow .tp-arr-allwrapper::before,
	.tparrows.default.tp-rightarrow .tp-arr-allwrapper::after,
	.list-blog .post .list-blog-img time,
	.list-blog time,
	.list-recent-img time {
		background-color: <?php echo $theme_color; ?>;
	}

	.input--line {
	  background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 96%, <?php echo $theme_color; ?> 4%);
	}

	.post.format-aside,

	blockquote.blockquote--wd p,
	.post--status,
	blockquote cite {
		border-left-color : <?php echo $theme_color; ?>;
	}

	blockquote.blockquote-reverse,
	.highlight.first-color,
	.tooltip.top .tooltip-arrow,
	.popover.right > .arrow{
	border-right-color : <?php echo $theme_color; ?>;
	}
	
	.wp-tag-cloud li a:hover,
	.woocommerce .widget_layered_nav ul.yith-wcan-label li a:hover, 
	.woocommerce-page .widget_layered_nav ul.yith-wcan-label li a:hover, 
	.woocommerce .widget_layered_nav ul.yith-wcan-label li.chosen a, 
	.woocommerce-page .widget_layered_nav ul.yith-wcan-label li.chosen a,

	.tags-list li a:hover,
	.compare-box__items__list__item:hover,
	input[type=checkbox]:checked ~ label .box,
	.radio input:checked + .outer,
	.tag:hover,
	.tags-list li a:hover,
	.hint__dot {
		border-color : <?php echo $theme_color ?>;
	}

	.nav-tabs.nav-tabs-inverse > li.active > a,
	.nav-tabs.nav-tabs-inverse > li.active > a:hover,
	.nav-tabs.nav-tabs-inverse > li.active > a:focus,
	.progress-bar-custom .progress-tooltip:after,
	.popover.top > .arrow,

	#loader,
	#modalLoader,
	.circle-loader{
	border-top-color:<?php echo $theme_color ?>;
	}

	.dropdown-wd,
	.header__dropdowns-container .dropdown--wd,
	.header__dropdowns .dropdown-menu,
	.navbar-nav--vertical > li .dropdown-menu,
	.header__search__input:focus,

	.footer__settings .dropdown .dropdown-menu,
	.ul-row .li-col h4,
	.navbar-nav--vertical > li .dropdown-menu,
	.header__search__input:focus,
	#mega_main_menu .mega_main_menu_ul > li.default_dropdown > .mega_dropdown,
	#mega_main_menu .mega_main_menu_ul > li.default_dropdown > .mega_dropdown > li > ul {
	border-bottom-color: <?php echo $theme_color ?>;
	}

	@media (min-width: 768px) {
	  .navbar.navbar-wd .dropdown-menu {
	    border-bottom-color: <?php echo $theme_color ?>;
	  }
	}


	 <!-- Main Menu -->

	.navbar.navbar-wd .nav > li > a:hover,
	.navbar.navbar-wd .nav > li > a:focus {
		color: <?php echo $s[ 'header_menu_font_color_hover' ]; ?>;
	}




	.progress-tooltip:after {
	border-color: <?php echo $theme_color ?> transparent transparent transparent;
	}


	#header .header-link:hover,
	#header .header-link:focus,
	#header .dropdown:hover >.dropdown-toggle,
	#header .open > .dropdown-toggle {
	color:<?php echo $theme_color ?> !important;
	}


	


	/*for theme color shade 1*/
	.product-box.new-box{
	background-color : <?php echo $theme_shade_1; ?>;
	}

	/*for theme color shade 2*/
	.welldone_newsletter_popup form input[type="submit"],
	.progress-bar-custom2,
	.progress-bar-custom2 .progress-tooltip,
	#newsletter-section form  p input[type="submit"]:hover{
		background-color : <?php echo $theme_shade_2; ?>;
	}

	/*Custom CSS Here*/

	<?php echo $s['css_editor']; ?>

<?php }

if($s['welldone_site_alternate_color'] && $s['welldone_site_alternate_color'] != '#000000' ){
	$theme_alternate_color= $s['welldone_site_alternate_color'];
	$theme_alternate_shade_1= welldone_generate_theme_alter_color_shades($theme_alternate_color,1);
	$theme_alternate_shade_2= welldone_generate_theme_alter_color_shades($theme_alternate_color,2);
	$theme_alternate_shade_3= welldone_generate_theme_alter_color_shades($theme_alternate_color,3);
	$theme_alternate_shade_4= welldone_generate_theme_alter_color_shades($theme_alternate_color,4);
	$theme_alternate_shade_5= welldone_generate_theme_alter_color_shades($theme_alternate_color,5);
	$theme_alternate_shade_6= welldone_generate_theme_alter_color_shades($theme_alternate_color,6);
	$theme_alternate_shade_7= welldone_generate_theme_alter_color_shades($theme_alternate_color,7); ?>


	.checkout h3,
	.nav-text-big,
	.cart-text-price,
	.megamenu .megamenu-footer,
	#side-menu-footer .copyright a:hover,
	.side-menu2 #side-menu-footer .social-icon:hover,
	.side-menu2 #side-menu-footer .copyright a,
	ul.yith-wcan-label.yith-wcan.yith-wcan-group li a:hover,
	ul.yith-wcan-label.yith-wcan.yith-wcan-group li.chosen a{
	color : <?php echo $theme_alternate_color; ?>;
	}

	.owl-carousel .owl-video-wrapper,
	.price_slider_wrapper .button{
	background-color : <?php echo $theme_alternate_color; ?>;
	}

	.side-menu.dark,
	ul.yith-wcan-label.yith-wcan.yith-wcan-group li.chosen a,
	.price_slider_wrapper .button{
	border-color  :<?php echo $theme_alternate_color; ?>;
	}
	.portfolio-custom .portfolio-meta {
	background: -moz-linear-gradient(top, <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0) ?> 12%, <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.17) ?> 58%, <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.34) ?> 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(12%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0) ?>), color-stop(58%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.17) ?>), color-stop(100%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.4) ?>));
	background: -webkit-linear-gradient(top,  <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0) ?> 12%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.17) ?> 58%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.4) ?> 100%);
	background: -o-linear-gradient(top,  <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0) ?> 12%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.17) ?> 58%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.4) ?> 100%);
	background: -ms-linear-gradient(top,  <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0) ?> 12%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.17) ?> 58%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.4) ?> 100%);
	background: linear-gradient(to bottom,  <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0) ?> 12%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.17) ?> 58%,<?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.4) ?> 100%);
	}

	.menu ul,
	.menu .megamenu {
	box-shadow:0 2px 7px <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.1) ?>;
	-webkit-box-shadow:0 2px 7px <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.1) ?>;
	}

	.mobile-menu li a:hover,
	#mobile-menu .social-icon:hover,
	.portfolio-btn{
	background-color : <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.2) ?>;
	}

	#mobile-menu-overlay{
	background-color : <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.4) ?>;
	}

	.portfolio-meta{
	background-color : <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.5) ?>;
	}

	.owl-carousel.product-slider .owl-prev:hover,
	.owl-carousel.product-slider .owl-next:hover{
	background-color : <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.05) ?>;
	}

	.portfolio-btn:hover,
	.portfolio-btn:focus{
	background-color : <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.6) ?>;
	}
	.gallery .gallery-caption{
	background-color : <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.7) ?>;
	}

	.dropdown-menu {
	-webkit-box-shadow: 0 4px 9px <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.15) ?>;
	box-shadow: 0 4px 9px <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.15) ?>;
	}

	.sticky-menu.fixed,
	#header.sticky-menu.fixed,
	#header.header-absolute.sticky-menu.fixed,
	#header.header13 #header-top.sticky-menu.fixed {
	box-shadow:0 3px 10px <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.22) ?>;
	-webkit-box-shadow:0 3px 10px <?php echo welldone_generate_alternate_color_levels($theme_alternate_color,0.22) ?>;
	}

	/*alter shade 1*/

	.dropcap,
	.dropcap-bg,
	.btn.btn-border.btn-custom2,
	.mfp-close-btn-in .newsletter-popup .mfp-close,
	.product-details .product-price,
	.product-details .product-price-container ins>.amount,
	.product-details .product-price-container .price>.amount{
		color : <?php echo $theme_alternate_shade_1; ?>;
	}


	.dropcap-bg,
	.btn-custom:hover,
	.btn-custom:focus,
	.btn-custom.focus,
	.btn-custom:active,
	#coming-soon .btn.btn-custom:hover,
	#coming-soon .btn.btn-custom:focus,
	.welldone_newsletter_popup form input[type="submit"]:hover,
	header#header.welldone_header17.no_banner_bg,
	header#header.welldone_header9.no_banner_bg{
		background-color : <?php echo $theme_alternate_shade_1; ?>;
	}


	.btn-custom:hover,
	.btn-custom:focus,
	.btn-custom.focus,
	.btn-custom:active,
	#coming-soon .btn.btn-custom:hover,
	#coming-soon .btn.btn-custom:focus,
	.welldone_newsletter_popup form input[type="submit"]:hover{
		border-color : <?php echo $theme_alternate_shade_1; ?>;
	}

	.panel-icon {
		color:<?php echo $theme_alternate_shade_1; ?> !important;
	}

	/*alter shade 2*/

	#header-top.dark,
	.btn-dark:hover,
	.btn-dark:focus,
	.btn-dark.focus,
	.btn-dark:active,
	.btn-dark.active,
	.open > .dropdown-toggle.btn-dark{
		background-color : <?php echo $theme_alternate_shade_2; ?>;
	}

	.btn-dark:hover,
	.btn-dark:focus,
	.btn-dark.focus,
	.btn-dark:active,
	.btn-dark.active,
	.open > .dropdown-toggle.btn-dark{
		border-color : <?php echo $theme_alternate_shade_2; ?>;
	}



	/*alter shade 3*/

	h1,.h1,
	h2,.h2,
	h3,.h3,
	h4,.h4,
	h5,.h5,
	h6,.h6,
	.btn.btn-border.btn-default,
	.nav-lava > li.active > a,
	.nav-lava > li > a:hover,
	.nav-lava > li > a:focus,
	.close:hover,
	.close:focus,
	.alert-success,
	.alert-success .alert-link,
	.alert-info,
	.alert-info .alert-link,
	.alert-warning,
	.alert-warning .alert-link,
	.alert-danger,
	.alert-danger .alert-link,
	.confirm-box h4 > a ,
	.dropdown-menu > li > a,
	.header1 .header-search-container .form-control,
	.header .header-search-container .form-control::-moz-placeholder,
	.error-title,
	.widget-title{
		color : <?php echo $theme_alternate_shade_3; ?>;
	}


	.tip:after,
	.tip .tip-arrow,
	.tip .tip-arrow,
	.btn-dark{
		border-color : <?php echo $theme_alternate_shade_3; ?>;
	}


	.side-menu.dark,
	.side-menu.dark #side-menu-footer,
	#scroll-top,
	#scroll-top:hover{
		background-color : <?php echo $theme_alternate_shade_3; ?>;
	}


	#scroll-top:hover{
		box-shadow:0 0 0 2px <?php echo $theme_alternate_shade_3; ?>;
		-webkit-box-shadow:0 0 0 2px <?php echo $theme_alternate_shade_3; ?>;
	}

	/*alter shade 4*/

	#header.header7 #header-top.dark .cart-dropdown .dropdown-toggle,
	.mobile-menu ul,
	.page-header.dark,
	#header.header8 .custom .header-box-icon{
		background-color : <?php echo $theme_alternate_shade_4; ?>;
	}

	.product-mini .product-price,
	.widget .hours-list li > span,
	.header-box-content h6{
		color : <?php echo $theme_alternate_shade_4; ?>;
	}

	/*alter shade 5*/

	.mobile-menu ul ul,
	.banner-info.banner-info-icon:hover{
		background-color : <?php echo $theme_alternate_shade_5; ?>;
	}

	.filter-size-box,
	.filter-size-box.active,
	.filter-size-box:hover,
	#footer .copyright a:focus,
	#footer .footer-menu li a,
	.bootstrap-touchspin .input-group-btn-vertical > .btn{
		color : <?php echo $theme_alternate_shade_5; ?>;
	}

	/*alter shade 6*/

	.banner.banner-long .banner-content h3,
	.megamenu .menu-quick-tags a{
		color : <?php echo $theme_alternate_shade_5; ?>;
	}


	/*Custom CSS Here*/

	<?php echo $s['css_editor']; ?>

<?php }


/*Header Customization*/

	/*************************** header 1 styles ****************************/

	// Top Line
	if ( !empty( $s[ 'header_top_background_dark' ] ) ) { ?>
		.header-line {
			background-color:<?php echo $s[ 'header_top_background_dark' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_top_text_dark' ] ) ) {
		?>
		.header-line a {
			color:<?php echo $s[ 'header_top_text_dark' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_top_text_dark_hover' ] ) ) {
		?>
		.header-line a:hover, 
		.header-line a:focus {
			color:<?php echo $s[ 'header_top_text_dark_hover' ]; ?>;
		}
	<?php } ?>

	<!-- Header -->

	<?php
	if( ! empty( $s['header_background'] ) ) {
		$a = $s['header_background'];
		echo ".navbar.navbar-wd {";
		if ( $a['background-color'] ) {
			echo "background-color :" . $a['background-color'] . ";";
		}

		if ( $a['background-image'] ) {

			echo "background-image :url('" . $a['background-image'] . "');";
		}

		if ( $a['background-repeat'] ) {
			echo "background-repeat:" . $a['background-repeat'] . ";";
		}

		if ( $a['background-size'] ) {
			echo "background-size: " . $a['background-size'] . ";";
		}
		if ( $a['background-position'] ) {
			echo "background-position:" . $a['background-position'] . ";";
		}

		if ( $a['background-attachment'] ) {
			echo "background-attachment:" . $a['background-attachment'] . ";";
		}
		echo "}";
	}

	?>

	<?php
	if ( !empty( $s[ 'header_text' ] ) ) {
		?>
		.navbar.navbar-wd,
		.navbar.navbar-wd a,
		.header__dropdowns__button {
			color:<?php echo $s[ 'header_text' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_hover' ] ) ) {
		?>
		.navbar.navbar-wd a:hover {
			color:<?php echo $s[ 'header_hover' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_cart_bg_dark' ] ) ) {
		?>
		#welldone_minicart_wrapper_content {
			background-color:<?php echo $s[ 'header_cart_bg_dark' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_cart_text_dark' ] ) ) {
		?>
		#welldone_minicart_wrapper_content,
		.shopping-cart a:not(.btn) {
			color:<?php echo $s[ 'header_cart_text_dark' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_cart_text_dark_hover' ] ) ) {
		?>
		.shopping-cart a:hover:not(.btn) {
			color:<?php echo $s[ 'header_cart_text_dark_hover' ]; ?>;
		}
	<?php } ?>

<?php
/*Menu Customization*/

	/*************************** header 1 menu styles ****************************/
	if ( !empty( $s[ 'header_menu_font_color' ] ) ) { ?>
		#menu-main > li > a {
			color:<?php echo $s[ 'header_menu_font_color' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_menu_font_color_hover' ] ) ) {
		?>
		.navbar.navbar-wd .nav > li > a:hover{
			color:<?php echo $s[ 'header_menu_font_color_hover' ]; ?>;
		}
	<?php } ?>
	
	<?php
	if ( !empty( $s[ 'header_background_submenu_hover' ] ) ) {
		?>
		.dropdown-menu > li > a:hover, 
		.dropdown-menu > li > a:focus {
			background-color:<?php echo $s[ 'header_background_submenu_hover' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_font_submenu' ] ) ) {
		?>
		.dropdown-menu > li > a, 
		.dropdown-menu > li > a {
			color:<?php echo $s[ 'header_font_submenu' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'header_font_submenu_hover' ] ) ) {
		?>
		.dropdown-menu > li > a:hover, 
		.dropdown-menu > li > a:focus {
			color:<?php echo $s[ 'header_font_submenu_hover' ]; ?>;
		}
	<?php } ?>

	<?php
/************ page color Content Setting ******/

if ( !empty( $s[ 'welldone_content_background' ] ) ) {
	$a = $s[ 'welldone_content_background' ];
	echo "#pageContent > .content {";

	if ( $a[ 'background-color' ] ) {
		echo "background-color :" . $a[ 'background-color' ] . ";";
	}

	if ( $a[ 'background-image' ] ) {
		echo "background-image :url('" . $a[ 'background-image' ] . "');";
		echo "-moz-background-image :url('" . $a[ 'background-image' ] . "');";
		echo "-o-background-image :url('" . $a[ 'background-image' ] . "');";
		echo "-webkit-background-image :url('" . $a[ 'background-image' ] . "');";
	}

	if ( $a[ 'background-repeat' ] ) {
		echo "background-repeat:" . $a[ 'background-repeat' ] . ";";
	}

	if ( $a[ 'background-size' ] ) {
		echo "background-size: " . $a[ 'background-size' ] . ";";
		echo "-moz-background-size: " . $a[ 'background-size' ] . ";";
		echo "-o-background-size : " . $a[ 'background-size' ] . ";";
		echo "-webkit-background-size : " . $a[ 'background-size' ] . ";";
	}

	if ( $a[ 'background-position' ] ) {
		echo "background-position:" . $a[ 'background-position' ] . ";";
	}

	if ( $a[ 'background-attachment' ] ) {
		echo "background-attachment:" . $a[ 'background-attachment' ] . ";";
	}
	echo "}";
} ?>


<!-- Footer Customization -->

<?php


	/*************************** footer top ****************************/
	if ( !empty( $s[ 'welldone_bg_custom_footer_top' ] ) ) { ?>
		.footer__links {
			background-color:<?php echo $s[ 'welldone_bg_custom_footer_top' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_top_link_color' ] ) ) {
		?>
		.footer__links a:not(.btn){
			color:<?php echo $s[ 'footer_top_link_color' ]; ?>;
		}
	<?php } ?>
	
	<?php
	if ( !empty( $s[ 'footer_top_hover_color' ] ) ) {
		?>
		.footer__links a:not(.btn):hover {
			color:<?php echo $s[ 'footer_top_hover_color' ]; ?>;
		}
	<?php } ?>

	<!-- Footer Middle -->
	
	<?php
	if ( !empty( $s[ 'welldone_bg_custom_footer' ] ) ) {
		?>
		footer__column-links {
			background-color:<?php echo $s[ 'welldone_bg_custom_footer' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_heading_color' ] ) ) {
		?>
		.footer__column-links .title {
			color:<?php echo $s[ 'footer_heading_color' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_text_color' ] ) ) {
		?>
		.footer__column-links {
			color:<?php echo $s[ 'footer_text_color' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_link_color' ] ) ) {
		?>
		.footer__column-links a:not(.btn) {
			color:<?php echo $s[ 'footer_link_color' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_link_hover_color' ] ) ) {
		?>
		.footer__column-links a:not(.btn):hover {
			color:<?php echo $s[ 'footer_link_hover_color' ]; ?>;
		}
	<?php } ?>

	<!-- Footer Bottom -->

	<?php
	if ( !empty( $s[ 'welldone_bg_custom_footer_bottom' ] ) ) {
		?>
		.footer__subscribe {
			background-color:<?php echo $s[ 'welldone_bg_custom_footer_bottom' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_bottom_color' ] ) ) {
		?>
		.footer__subscribe {
			color:<?php echo $s[ 'footer_bottom_color' ]; ?>;
		}
	<?php } ?>

	<!-- Footer Copyright Section -->
	
	<?php
	if ( !empty( $s[ 'footer_copyright_background_color' ] ) ) {
		?>
		.footer__bottom {
			background-color:<?php echo $s[ 'footer_copyright_background_color' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_copyright_text_color' ] ) ) {
		?>
		.footer__bottom {
			color:<?php echo $s[ 'footer_copyright_text_color' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_copyright_link_color' ] ) ) {
		?>
		.footer__bottom a {
			color:<?php echo $s[ 'footer_copyright_link_color' ]; ?>;
		}
	<?php } ?>

	<?php
	if ( !empty( $s[ 'footer_copyright_link_hover_color' ] ) ) {
		?>
		.footer__bottom a:hover {
			color:<?php echo $s[ 'footer_copyright_link_hover_color' ]; ?>;
		}
	<?php } ?>



