<?php  
/**
 * The template for displaying searchform
 *
 *
 * @package welldone
 */
?>
<form id="search-form" class="search-form outer" action="<?php echo esc_url( home_url() ); ?>" role="search" method="get">
  <div class="input-group input-group--wd">

    <?php  
     if ( shortcode_exists( 'yith_woocommerce_ajax_search' ) ) {
          echo do_shortcode('[yith_woocommerce_ajax_search]'); 
      }
    ?> 

    <span class="input-group__bar"></span> </div>
  <button class="btn btn--wd text-uppercase wave waves-effect sim" name="submit"><?php esc_attr_e('Search','welldone'); ?></button>
</form>
