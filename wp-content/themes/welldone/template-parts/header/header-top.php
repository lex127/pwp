<?php
/**
 * The template for header social icons
 *
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */
global $welldone_settings;
?>
<div class="header-line hidden-xs">
  <div class="container">
    <div class="pull-left">
      <div class="social-links social-links--colorize">
        <ul>
			<?php if ( !empty($welldone_settings[ 'header_icon_fb' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-facebook" href="<?php echo esc_attr( $welldone_settings['header_icon_fb'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_twitter' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-twitter" href="<?php echo esc_attr( $welldone_settings['header_icon_twitter'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_gmail' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-google" href="<?php echo esc_attr( $welldone_settings['header_icon_gmail'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_pin' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-pinterest" href="<?php echo esc_attr( $welldone_settings['header_icon_pin'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_mail' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-mail" href="mailto:<?php echo esc_attr( $welldone_settings['header_icon_mail'] );?>" target="_blank"></a></li>
			<?php endif; ?>
        </ul>
      </div>
    </div>
    <div class="pull-right">
      <div class="user-links">
        <ul>
		  <?php if ( $welldone_settings['header_show_auth'] ) : ?>
			<li class="user-links__item"><?php if ( !is_user_logged_in() ) { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php esc_html_e('Login / Register','welldone'); ?>"><?php esc_html_e('Login / Register','welldone'); ?></a>
                   <?php } ?></li>
			<li class="user-links__item"><?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php esc_html_e('My Account','welldone'); ?>"><?php esc_html_e('My Account','welldone'); ?></a>
                   <?php } ?></li>
  		  <?php endif; ?>
        </ul>
      </div>
    </div>
  </div>
</div>