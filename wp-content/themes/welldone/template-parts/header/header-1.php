<?php
/**
 * The template for header 1
*
*
* @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */
  global $welldone_settings, $mobile_menu,$search_button_class,$filter;

?>

  <header class="header 
  <?php if ($welldone_settings[ 'sticky-header' ]) {echo 'header--sticky';} ?>">
    <?php 
    if ($welldone_settings['welldone_header_top']) {
      get_template_part('template-parts/header/header-top');
    } ?>
    <div class="header__dropdowns-container">
      <div class="header__dropdowns">
        <?php if ( $welldone_settings['header_fb_tw_login'] ) : ?>
          <div class="header__search pull-left"> <a href="#" class="btn dropdown-toggle btn--links--dropdown header__dropdowns__button search-open"><span class="icon icon-search"></span></a> </div>
        <?php endif; ?>
        <?php if ( $welldone_settings['show-currency-switcher'] || $welldone_settings['show-wpml-switcher'] || $welldone_settings['show-woo-pages'] ) : ?>
        <div class="dropdown pull-right"> <a href="#" class="btn dropdown-toggle btn--links--dropdown header__dropdowns__button" data-toggle="dropdown"><span class="icon icon-dots"></span></a>
          <ul class="dropdown-menu ul-row animated fadeIn" role="menu">
            <?php if ( $welldone_settings['show-currency-switcher'] ) : ?>
            <li class='li-col currency'>
            <h4>Currency</h4>
              <ul>
                <li class="currency__item currency__item--active"><a href="#">$</a></li>
                <li class="currency__item"><a href="#">&euro; Euro</a></li>
                <li class="currency__item"><a href="#">&pound; British Pound</a></li>
              </ul>
            </li>
            <?php endif; ?>
            <?php if ( $welldone_settings['show-wpml-switcher'] ) : ?>
            <li class='li-col languages languages--flag'>
            <h4>Language</h4>
              <ul>
                <li class="languages__item languages__item--active"><a href="#"><span class="languages__item__flag flag"><img src="<?php echo get_template_directory_uri(). '/images/flags/gb.png' ?>" alt=""/></span><span class="languages__item__label">En</span></a></li>
                <li class="languages__item"><a href="#"><span class="languages__item__flag flag"><img src="<?php echo get_template_directory_uri(). '/images/flags/de.png' ?>" alt=""/></span><span class="languages__item__label">De</span></a></li>
                <li class="languages__item"><a href="#"><span class="languages__item__flag flag"><img src="<?php echo get_template_directory_uri(). '/images/flags/fr.png' ?>" alt=""/></span><span class="languages__item__label">Fr</span></a></li>
              </ul>
            </li>
            <?php endif; ?>
            <?php if ( $welldone_settings['show-woo-pages'] ) : ?>
            <li class='li-col list-user-menu'>
            <h4>My Account</h4>
              <ul>
                <li>
                  <?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php esc_html_e('My Account','welldone'); ?>"><?php esc_html_e('My Account','welldone'); ?></a>
                   <?php } 
                   else { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php esc_html_e('Login / Register','welldone'); ?>"><?php esc_html_e('Login / Register','welldone'); ?></a>
                   <?php } ?>
                </li>
                <li>
                  <?php if( function_exists( 'YITH_WCWL' ) && YITH_WCWL()->count_products() ): ?>
                  <a href="<?php echo YITH_WCWL()->get_wishlist_url() ?>"><?php esc_html_e( 'Wishlist', 'welldone' ) ?></a>
                  <?php endif; ?>
                </li>
                <li>
                  <?php 
                    global $woocommerce;
                    $cart_contents = ( isset( $woocommerce->cart->cart_contents ) ? $woocommerce->cart->cart_contents : NULL );
                    if ( sizeof( $cart_contents) > 0 ) :
                      echo '<a href="' . $woocommerce->cart->get_checkout_url() . '" title="' . esc_html__( 'Checkout', 'welldone' ) . '">' . esc_html__( 'Checkout', 'welldone' ) . '</a>';
                    endif;
                  ?>
                </li>
              </ul>
            </li>
            <?php endif; ?>
          </ul>
        </div>
        <?php endif; ?>
        <?php if ( $welldone_settings['show-minicart'] ) : ?>
          <?php welldone_minicart(); ?>
        <?php endif; ?>
      </div>
    </div>
    <nav class="navbar navbar-wd" id="navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" id="slide-nav"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <!--  Logo  --> 
          <a class="logo" href="<?php echo esc_url(home_url()); ?>" title="<?php echo get_bloginfo("description"); ?>"> 
            <img class="logo-default" src="<?php echo esc_url($welldone_settings['logo']['url']);?>" alt="<?php bloginfo("title") ?>"/> 
            <img class="logo-mobile" src="<?php echo esc_url($welldone_settings['logo-mobile']['url']);?>" alt="<?php bloginfo("title") ?>"/> 
            <img class="logo-transparent" src="<?php echo esc_url($welldone_settings['logo-transparent']['url']);?>" alt="<?php bloginfo("title") ?>"/>
          </a> 
          <!-- End Logo --> 
        </div>
        <div class="pull-left search-focus-fade" id="slidemenu">
          <div class="slidemenu-close visible-xs">&#x2715;</div>
        
                <?php
                  if (has_nav_menu('main-menu')) {
                    wp_nav_menu(array(
                      'theme_location' => 'main-menu',
                      'container' => false,
                      'menu_class' => 'nav navbar-nav navbar-right',
                      'walker' => new Welldone_Walker_Nav_Primary()
                      )
                    );
                }
                ?>
        </div>
      </div>
    </nav>
  </header>