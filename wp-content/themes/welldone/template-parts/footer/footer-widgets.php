<div class="row">
    <?php
    global $welldone_settings , $post;

    $id = ( isset( $post->ID ) ? get_the_ID() : NULL );

    $welldone_footer = get_post_meta ( $id, 'welldone_footer_widget_columns', true ) ? get_post_meta ( $id, 'welldone_footer_widget_columns', true ) : '';
    if(!$welldone_footer){
        $welldone_footer = ( isset( $welldone_settings[ 'welldone_footer_widget_columns' ] ) ) ? $welldone_settings[ 'welldone_footer_widget_columns' ] : 5;
    }
  
        if($welldone_footer==5) {
        $wid_class[1] = 'col-md-3 hidden-xs hidden-sm';
        $wid_class[2] = 'col-md-2 col-sm-3';
        $wid_class[3] = 'col-md-2 col-sm-3';
        $wid_class[4] = 'col-md-2 col-sm-3';
        $wid_class[5] = 'col-sm-3 col-md-3 mobile-collapse mobile-collapse--last';
    }else{
        $wid_class_ = 12/$welldone_footer;
        $wid_class[1] = 'col-md-'.$wid_class_;
        $wid_class[2] = 'col-md-'.$wid_class_;
        $wid_class[3] = 'col-md-'.$wid_class_;
        $wid_class[4] = 'col-md-'.$wid_class_;
        $wid_class[5] = 'col-md-'.$wid_class_;
    }
    ?>
    <?php for($i=1;$i<=$welldone_footer; $i++){ ?>
        <div class="<?php echo esc_attr($wid_class[$i]); ?>">
           <?php if ( !dynamic_sidebar ( 'footer_'.$i ) ) : ?>
            <div class="widget">
                <h5 class="widget-title title text-uppercase mobile-collapse__title">
                    <?php echo esc_html__("Footer Widget", 'welldone' ).' '.$i; ?>
                </h5>
                <div>
                    <p><?php esc_html_e( "To configure this widget area you need open the admin panel - Appearance - Widgets", 'welldone' ) ?></p>
                </div>
            </div><!-- End corporate-widget -->
            <?php endif; ?>
        </div><!-- End .widget -->
        <div class="clearfix visible-sm"></div><!-- End clearfix -->
    <?php } ?>
</div><!-- End .row -->