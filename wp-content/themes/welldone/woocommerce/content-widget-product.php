<?php
/**
 * The template for displaying product widget entries
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-widget-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product; ?>

<div class="products-widget__item">
	<div class="products-widget__item__image pull-left">
		<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
			<?php echo $product->get_image ( "welldone-shop-widget-2" ); ?>
		</a>
	</div>
	<div class="products-widget__item__info">
		<div class="products-widget__item__info__title">
			<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>">
				<h2 class="text-uppercase"><?php echo $product->get_title(); ?></h2>
			</a>
		</div>
		<?php if ( $rating_html = $product->get_rating_html() ) :
			$rating = $product->get_average_rating();
			$rating_html = $product->get_rating_html();
			$count = 0; ?>
			<div class="rating">
			<?php for( $i = 0; $i <(int)$rating; $i ++ ) {
			$count ++;
			echo '<span class="icon-star"></span>';
			}

			if( $rating -(int)$rating >= 0.5 ) {
			$count ++;
			echo '<span class="star active-half"></span>';
			}

			for( $i = $count; $i < 5; $i ++ ) {
			$count ++;
			echo '<span class="icon-star empty-star"></span>';
			} ?>

			<span class="rating-amount">
            <?php
            $rev_count = $product->get_review_count();
            echo $rev_count." ";
            if($rev_count==0 || $rev_count>1){
                esc_html_e("Reviews",'welldone');
            }elseif($rev_count==1){
                esc_html_e("Review",'welldone');
            }
            ?>
			</span>
		</div>
		<?php endif; ?>
		<div class="price-box"><?php echo $product->get_price_html(); ?></div>
	</div>
</div>
