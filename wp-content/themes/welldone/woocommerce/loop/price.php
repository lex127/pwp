<?php
/**
 * Loop Price
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product, $welldone_settings;

?>
  <?php if ( $welldone_settings[ 'welldone_show_price' ] ) {
 if ( $price_html = $product->get_price_html() ) : ?>
	<div class="price price-box"><?php echo $price_html; ?></div>
<?php endif; 
  }
?>