<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.5.0
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop, $welldone_shop_page_settings;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Ensure visibility
if ( ! $product || ! $product->is_visible() ) {
	return;
}

// Increase loop count
$woocommerce_loop['loop']++;


// Extra post classes
$classes = array();
$classes[] = 'welldone_product product';

?>
<?php global  $welldone_settings;?>
<div <?php post_class( $classes ); ?>>

<?php
require(get_template_directory() . '/woocommerce/quick_view.php');
?>
<div class="product-preview">
	<div class="product-preview__image">
		<?php do_action('welldone_shop_custom_action'); ?>
		<?php echo welldone_product_countdown(); ?>
	</div>
	
	<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
	</a>
	<div class="product-preview__info text-center">
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<div class="product-preview__info__btns">
			<?php if ( $welldone_settings[ 'welldone_show_add_to_cart_button' ] ) {
                woocommerce_template_loop_add_to_cart ();
            }
			?>
			<?php
			/***** Quick view button start here ****/
			if($welldone_settings['welldone_show_quick_button'])
			{
			?>
				<a class="btn btn--round btn--dark btn-quickview open-product" data-id="<?php echo the_ID();?>" data-toggle="modal" data-target="#quickView"><span class="icon icon-eye"></span>
					</a>	
			<?php 
			}
			?>
		</div>
		<div class="product-preview__info__title">
			<h2 class="product-title">	<a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></h2>
		</div>
	
	<?php
		/**
		 * woocommerce_after_shop_loop_item_title hook
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */
	
		 //woocommerce_template_loop_rating();
		do_action( 'woocommerce_after_shop_loop_item_title' );
	?>
		<div class="product-preview__info__description">
			 <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?> 
		</div>
	<div class="product-preview__info__link">
		
		 <?php  do_action( 'woocommerce_' . $product->product_type . '_add_to_cart'  ); ?> 
		<?php  
			if ( $welldone_settings[ 'welldone_show_wishlist_button' ] ) {
	            if ( shortcode_exists ( 'yith_wcwl_add_to_wishlist' ) ) {
	                echo do_shortcode ( '[yith_wcwl_add_to_wishlist]' );
	            }
	        }
	        if ( $welldone_settings[ 'welldone_show_compare_button' ] ) {
                if ( shortcode_exists ( 'yith_compare_button' ) ) {
                    echo do_shortcode ( '[yith_compare_button]' );
                }
            }
		?>
		<?php 
		
		?>
	</div>
	</div><a href="#" title="empty">
		<?php
	
			/**
			 * woocommerce_after_shop_loop_item hook
			 *
			 * @hooked woocommerce_template_loop_add_to_cart - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item' );
	
		?>
	</div>
</div><!-- end product-preview -->
