<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
$ver = welldone_single_product_version ();
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>
        </div><!-- end .row.wrapper-start > .products_container or column -->
        <?php if($ver == "v_1") { get_sidebar( 'shop' ); } ?>
    </div><!-- end .container > .row.wrapper-start -->
</div><!-- end section.content > .container -->
</section><!-- end section.content -->

<section class="content content--fill">
	<div class="container">
		<?php do_action( 'woocommerce_after_single_product' ); ?>
	</div>
</section>
<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
<?php 
global $welldone_settings;
 if(class_exists('WC_Vendors') ){
if($welldone_settings['welldone_wcvendors_product_moreproducts']){
if(is_product()){
	welldone_more_seller_product();
	
}
}}
 if(is_product()){ welldone_woocommerce_show_product();  } 
 
 ?>