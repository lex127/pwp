<?php
/**
 * Single Product tabs
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
global $post;
/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$custom_tab_title1 = get_post_meta(get_the_id(), 'welldone_tabs_custom_heading_one', true);
$custom_tab_content1 = get_post_meta(get_the_id(), 'welldone_contact_tabs_content_one', true);
$custom_tab_title2 = get_post_meta(get_the_id(), 'welldone_tabs_custom_heading_two', true);
$custom_tab_content2 = get_post_meta(get_the_id(), 'welldone_contact_tabs_content_two', true);

$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div class="woocommerce-tabs row">
        <div class="col-md-12">
            <div role="tabpanel" class="product-details-tab">
                <ul class="tabs nav nav-tabs nav-tabs--wd" role="tablist">
                    <?php $current  = "active";
                    foreach ( $tabs as $key => $tab ) :
					if(class_exists('WC_Vendors') ){
			
						if($tab['title'] == 'Additional Information'){
							$tab['title']= "Items Details";
						}
					}
					
					?>
                        <li class="<?php echo esc_attr( $key ); ?>_tab <?php echo esc_attr($current); ?> text-uppercase">
                            <a href="#tab-<?php echo $key ?>" aria-controls="tab-<?php echo $key; ?>" role="tab" data-toggle="tab" ><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', $tab['title'], $key ) ?></a>
                        </li>
						
                    <?php $current = "";
                    endforeach; ?>
					<?php if($custom_tab_title1 && $custom_tab_content1){ ?>
					   <li class="custom_tab1">
                            <a href="#tab-1" aria-controls="tab-<?php echo $key; ?>" role="tab" data-toggle="tab" >
							<?php echo esc_attr($custom_tab_title1)?></a>
                       </li>
					<?php }  ?>
					<?php if($custom_tab_title2 && $custom_tab_content2){ ?>
					   <li class="custom_tab2">
                            <a href="#tab-2" aria-controls="tab-<?php echo $key; ?>" role="tab" data-toggle="tab" >
							<?php echo esc_attr($custom_tab_title2);?></a>
                       </li>
					<?php }  ?>
                </ul>
                <div class="tab-content tab-content--wd">
            <?php foreach ( $tabs as $key => $tab ) : ?>
                <div role="tabpanel" class="tab-pane panel entry-content" id="tab-<?php echo esc_attr( $key ); ?>">
                    <?php call_user_func( $tab['callback'], $key, $tab ) ?>
                </div>
            <?php endforeach; ?>
			<?php  if($custom_tab_title1 && $custom_tab_content1){?>
                <div role="tabpanel" class="tab-pane panel entry-content custom-bottom" id="tab-1">
                    <?php echo $custom_tab_content1;?>
                </div>
				<?php } ?>
				<?php  if($custom_tab_title2 && $custom_tab_content2){?>
                <div role="tabpanel" class="tab-pane panel entry-content custom-bottom" id="tab-2">
                    <?php echo $custom_tab_content2;?>
                </div>
				<?php } ?>
                </div>
				
            </div>
	    </div>
    </div>

<?php endif; ?>
