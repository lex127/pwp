<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product,$welldone_settings,$post;
$attachment_ids = $product->get_gallery_attachment_ids();
if(has_post_thumbnail()){
    if(empty($attachment_ids)){
        $attachment_ids[] = get_post_thumbnail_id($post->ID);
        $loop = 0;
    }else{
        array_unshift ( $attachment_ids ,  get_post_thumbnail_id($post->ID) );
        $loop = 0;
    }
}
?>

<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
	<div class="product-info__sku pull-right"><span><?php esc_html_e( 'SKU: ','welldone' ); ?></span><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A','welldone' ); ?>.</div>
<?php endif; ?>


<ul class="visible-xs singleGallery">
<?php 
		foreach ( $attachment_ids as $attachment_id ) {

			$image        = wp_get_attachment_image_src( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_single' ) );
            $image_zoom   = wp_get_attachment_image_src( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'full' ) );
			$image_title = esc_attr( get_the_title( $attachment_id ) );
			$image_class = 'product-gallery-item';
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li><a href="#" data-image="%s" data-zoom-image="%s" class="%s" title="%s"><img src="%s" alt="%s"/></a></li>', $image[0],$image_zoom[0],$image_class, $image_title, $image[0],$image_title), $attachment_id, $post->ID, $image_class );
			$loop++;
		}
?>
</ul>
 
<?php
if ( $welldone_settings[ 'welldone_show_price' ] ) {
?>
<div class="price-box product-info__price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	<p class="price"><?php echo $product->get_price_html(); ?></p>
	<meta itemprop="price" content="<?php echo $product->get_price(); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
</div>
<?php } ?>