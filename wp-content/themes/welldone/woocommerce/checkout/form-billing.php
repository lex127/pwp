<?php
/**
 * Checkout billing information form
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.1.2
 */

if ( !defined ( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/** @global WC_Checkout $checkout */

global $welldone_count_checkout_tabs, $welldone_settings;

?>

<div class="panel panel-checkout" role="tablist">
    <?php if ( WC ()->cart->ship_to_billing_address_only () && WC ()->cart->needs_shipping () ) : ?>
        <?php $title = __ ( 'Billing &amp; Shipping','welldone' ); ?>
    <?php else : ?>
        <?php $title = __ ( 'Billing Details','welldone' ); ?>
    <?php endif; ?>
 <div class="panel-heading" role="tab" id="heading-billing">
        <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapse-billing">
                <?php echo $title; ?>
            </a>
			<span class=""></span>
        </h4>
        <div class="panel-heading__number step-box"><?php echo $welldone_count_checkout_tabs; ?></div>
        <?php $welldone_count_checkout_tabs ++; ?>
    </div>
    <!-- End .panel-heading -->
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel">
		<div class="panel-body">
            <?php do_action ( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

            <?php foreach ( $checkout->checkout_fields[ 'billing' ] as $key => $field ) : ?>
                <?php

                $field[ 'label_class' ][ ] = 'input-desc';
                $field['input_class'][] = 'input--wd input--wd--full';
                ?>
                <?php woocommerce_form_field ( $key, $field, $checkout->get_value ( $key ) ); ?>

            <?php endforeach; ?>

            <?php do_action ( 'woocommerce_after_checkout_billing_form', $checkout ); ?>

            <?php if ( !is_user_logged_in () && $checkout->enable_signup ) : ?>

                <?php if ( $checkout->enable_guest_checkout ) : ?>

                    <p class="form-row form-row-wide create-account checkbox">
                        <label for="createaccount" class="checkbox custom-checkbox-wrapper">
                    <span class="custom-checkbox-container">
                        <input class="input-checkbox"
                               id="createaccount" <?php checked ( ( true === $checkout->get_value ( 'createaccount' ) || ( true === apply_filters ( 'woocommerce_create_account_default_checked', false ) ) ), true ) ?>
                               type="checkbox" name="createaccount" value="1"/>
                        <span class="custom-checkbox-icon"></span>
                            </span>
                            <span><?php esc_html_e( 'Create an account?','welldone' ); ?></span>
                        </label>
                    </p>
                <?php endif; ?>

                <?php do_action ( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

                <?php if ( !empty( $checkout->checkout_fields[ 'account' ] ) ) : ?>

                    <div class="create-account">

                        <p><?php esc_html_e( 'Create an account by entering the information below. If you are a returning customer please login at the top of the page.','welldone' ); ?></p>

                        <?php foreach ( $checkout->checkout_fields[ 'account' ] as $key => $field ) : ?>
                            <?php
                            //$field[ 'class' ][ ] = "form-group";
                            $field[ 'label_class' ][ ] = 'input-desc';
                            woocommerce_form_field ( $key, $field, $checkout->get_value ( $key ) ); ?>

                        <?php endforeach; ?>

                        <div class="clear"></div>

                    </div>

                <?php endif; ?>

                <?php do_action ( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

            <?php endif; ?>
        </div>
    </div>
</div>