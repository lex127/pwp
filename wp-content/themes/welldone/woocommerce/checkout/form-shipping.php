<?php
/**
 * Checkout shipping information form
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.2.0
 */

if ( !defined ( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
global $welldone_count_checkout_tabs, $welldone_settings;
?>
<?php if ( WC ()->cart->needs_shipping_address () === true ) : ?>
    <div class="panel panel-checkout" role="tablist">
        <?php
        if ( empty( $_POST ) ) {

            $ship_to_different_address = get_option ( 'woocommerce_ship_to_destination' ) === 'shipping' ? 1 : 0;
            $ship_to_different_address = apply_filters ( 'woocommerce_ship_to_different_address_checked', $ship_to_different_address );

        } else {
            $ship_to_different_address = $checkout->get_value ( 'ship_to_different_address' );
        }
        ?>
        <div class="panel-heading" role="tab" id="heading-coupon">
        <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapseThree3" aria-expanded="true"
                   aria-controls="collapse-shipping">
				   
                 <?php esc_html_e( 'Ship to a different address?','welldone' ); ?>
                </a>
            </h4>
            <div class="panel-heading__number step-box"><?php echo $welldone_count_checkout_tabs; ?></div>
            <?php $welldone_count_checkout_tabs ++; ?>
        </div>
        <!-- End .panel-heading -->

        <div id="collapseThree3" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <div id="ship-to-different-address" class="checkbox">
                    <label for="ship-to-different-address-checkbox" class="custom-checkbox-wrapper">
                            <span class="custom-checkbox-container">
                                <input id="ship-to-different-address-checkbox"
                                       class="input-checkbox" <?php checked ( $ship_to_different_address, 1 ); ?>
                                       type="checkbox" name="ship_to_different_address" value="1"/>
                                <span class="custom-checkbox-icon"></span>
                            </span>
                        <span><?php esc_html_e( 'Ship to a different address?','welldone' ); ?></span>
                    </label>
                </div>
                <div class="shipping_address">

                    <?php do_action ( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

                    <?php foreach ( $checkout->checkout_fields[ 'shipping' ] as $key => $field ) : ?>
                        <?php
                        // $field[ 'class' ][ ] = "form-group";
                        $field[ 'label_class' ][ ] = 'input-desc';
                        $field['input_class'][] = 'input--wd input--wd--full';
                        woocommerce_form_field ( $key, $field, $checkout->get_value ( $key ) ); ?>

                    <?php endforeach; ?>

                    <?php do_action ( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php do_action ( 'woocommerce_before_order_notes', $checkout ); ?>
<?php if ( apply_filters ( 'woocommerce_enable_order_notes_field', get_option ( 'woocommerce_enable_order_comments', 'yes' ) === 'yes' ) ) : ?>
    <div class="panel panel-checkout" role="tablist">
        <div class="panel-heading" role="tab" id="heading-order-notes">
        <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapseThree" aria-expanded="true"
                   aria-controls="collapse-order-notes">
                    <?php esc_html_e( 'Order Notes','welldone' ); ?>
                </a>
            </h4>
            <div class="panel-heading__number step-box"><?php echo $welldone_count_checkout_tabs; ?></div>
            <?php $welldone_count_checkout_tabs ++; ?>
        </div>
        <!-- End .panel-heading -->
		
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel">
            <div class="panel-body">
                <?php if ( !WC ()->cart->needs_shipping () || WC ()->cart->ship_to_billing_address_only () ) : ?>

                    <h3><?php esc_html_e( 'Additional Information','welldone' ); ?></h3>

                <?php endif; ?>

                <?php foreach ( $checkout->checkout_fields[ 'order' ] as $key => $field ) :
                    $field[ 'class' ][ ] = "form-group";
                    $field[ 'label_class' ][ ] = 'input-desc';
                    $field['input_class'][] = 'form-control';
                    woocommerce_form_field ( $key, $field, $checkout->get_value ( $key ) ); ?>

                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php do_action ( 'woocommerce_after_order_notes', $checkout ); ?>