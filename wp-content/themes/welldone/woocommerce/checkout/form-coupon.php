<?php
/**
 * Checkout coupon form
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.2
 */

if ( !defined ( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( !WC ()->cart->coupons_enabled () ) {
    return;
}
global $welldone_count_checkout_tabs, $welldone_settings;
$info_message = apply_filters ( 'woocommerce_checkout_coupon_message', __ ( 'Have a coupon? ','welldone' ) . __ ( 'Click here to enter your code','welldone' ) );
?>
<div class="panel panel-checkout" role="tablist">
    <div class="panel-heading  <?php if ( is_user_logged_in() ) { echo "open active";} ?>" role="tab" id="heading-coupon">
        <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapse-coupon">
                <?php
                wc_print_notice ( $info_message, 'notice' );
                ?>
            </a>
        </h4>
        <div class="panel-heading__number step-box"><?php echo $welldone_count_checkout_tabs; ?></div>
        <?php $welldone_count_checkout_tabs ++; ?>
    </div>
	

   <div id="collapseOne" class="panel-collapse collapse <?php if ( is_user_logged_in() ) { echo "in";} ?>" role="tabpanel">
        <div class="panel-body">
            <form class="checkout_coupon" method="post">

                <p class="form-row form-group form-row-first">
                    <label class="input-desc" for="coupon_code"><?php esc_html_e( 'Coupon code','welldone' ); ?></label>
                    <input type="text" name="coupon_code" class="input-text form-control"
                           placeholder="<?php esc_html_e( 'Coupon code','welldone' ); ?>" id="coupon_code" value=""/>
                </p>

                <p class="form-row form-group form-row-last">
                    <input type="submit" class="btn btn--wd" name="apply_coupon"
                    value="<?php esc_html_e( 'Apply Coupon','welldone' ); ?>"/>
                </p>

                <div class="clear"></div>
            </form>
        </div>
    </div>
</div>