<?php
/**
 * Checkout Payment Section
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $welldone_count_checkout_tabs, $welldone_settings;
?>

<?php if ( !is_ajax () ) : ?>
    <?php do_action ( 'woocommerce_review_order_before_payment' ); ?>
<?php endif; ?>
<?php if( !is_ajax()){ ?>
<div class="panel panel-checkout" role="tablist">
    <div class="panel-heading" role="tab" id="heading-payment">
        <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapseFive" aria-expanded="true" aria-controls="collapse-payment">
                <?php esc_html_e( "Payment Information", 'welldone' ); ?>
            </a>
            <!-- <span class="step-box"><?php echo $welldone_count_checkout_tabs; ?></span> -->
        </h4>
        <div class="panel-heading__number step-box"><?php echo $welldone_count_checkout_tabs; ?></div>
        <?php $welldone_count_checkout_tabs ++; ?>
    </div>
    <!-- End .panel-heading -->


    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel">

     <div class="panel-body">
        <?php } ?>
        <div id="payment" class="woocommerce-checkout-payment">
            <?php if ( WC ()->cart->needs_payment () ) : ?>
                <ul class="payment_methods methods">
                        <?php
                        if ( !empty( $available_gateways ) ) {
                            foreach ( $available_gateways as $gateway ) {
                                wc_get_template ( 'checkout/payment-method.php', array ( 'gateway' => $gateway ) );
                            }
                        } else {
                            if ( !WC ()->customer->get_country () ) {
                                $no_gateways_message = __ ( 'Please fill in your details above to see available payment methods.','welldone' );
                            } else {
                                $no_gateways_message = __ ( 'Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.','welldone' );
                            }

                            echo '<p>' . apply_filters ( 'woocommerce_no_available_payment_methods_message', $no_gateways_message ) . '</p>';
                        }
                        ?>
                    </ul>
                <?php endif; ?>
                <div class="form-row place-order">
                    <noscript><?php esc_html_e( 'Since your browser does not support JavaScript, or it is disabled, please ensure you click the <em>Update Totals</em> button before placing your order. You may be charged more than the amount stated above if you fail to do so.','welldone' ); ?>
                        <br/><input type="submit" class="button alt button alt btn btn-custom2 min-width-sm" name="woocommerce_checkout_update_totals"
                                    value="<?php esc_html_e( 'Update totals','welldone' ); ?>"/></noscript>

                    <?php wp_nonce_field ( 'woocommerce-process_checkout' ); ?>

                    <?php do_action ( 'woocommerce_review_order_before_submit' ); ?>

                    <?php echo apply_filters ( 'woocommerce_order_button_html', '<input type="submit" class="button alt btn btn--wd btn-custom2 min-width-sm" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr ( $order_button_text ) . '" data-value="' . esc_attr ( $order_button_text ) . '" />' ); ?>

                    <?php if ( wc_get_page_id ( 'terms' ) > 0 && apply_filters ( 'woocommerce_checkout_show_terms', true ) ) : ?>
                        <div class="checkbox form-row terms">
                            <label for="terms" class="checkbox custom-checkbox-wrapper">
                            <span class="custom-checkbox-container">
                                <input type="checkbox"
                                       name="terms" <?php checked ( apply_filters ( 'woocommerce_terms_is_checked_default', isset( $_POST[ 'terms' ] ) ), true ); ?>
                                       id="terms">
                                <span class="custom-checkbox-icon"></span>
                            </span>
                                <span><?php printf ( __ ( 'I&rsquo;ve read and accept the <a href="%s" target="_blank">terms &amp; conditions</a>','welldone' ), esc_url ( wc_get_page_permalink ( 'terms' ) ) ); ?></span>
                            </label>
                        </div>
                    <?php endif; ?>

                    <?php do_action ( 'woocommerce_review_order_after_submit' ); ?>
                </div>
                <div class="clear"></div>
            </div>
<?php if(!is_ajax()){ ?>
        </div>
    </div>
</div>
<?php } ?>
<?php if ( !is_ajax () ) : ?>
    <?php do_action ( 'woocommerce_review_order_after_payment' ); ?>
<?php endif; ?>