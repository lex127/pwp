<?php global $welldone_settings; ?>

<table class="table shopping-cart-table text-center">
    <thead>
    <tr>
        <th class="product-name"><?php esc_html_e( 'Remove From Cart:','welldone' ); ?></th>
        <th class="product-name"><?php esc_html_e( 'Product Name','welldone' ); ?></th>
        <th class="product-name">&nbsp;</th>
        <th class="product-price"><?php esc_html_e( 'Unit Price','welldone' ); ?></th>
        <th class="product-quantity"><?php esc_html_e( 'Quantity','welldone' ); ?></th>
        <th class="product-subtotal"><?php esc_html_e( 'Subtotal','welldone' ); ?></th>
        <th class="product-remove">&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php do_action( 'woocommerce_before_cart_contents' ); ?>

    <?php
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
            ?>
            <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                <td class="product-remove">
                    <?php
                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="icon-clear shopping-cart-table__delete" title="%s"></a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), esc_html__( 'Remove this item','welldone' ) ), $cart_item_key );
                    ?>
                </td>
                <td class="product-name">
                    <div class="product clearfix">
                        <div class="product-top">
                            <figure>
                            <?php
                            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image("welldone-shop-image"), $cart_item, $cart_item_key );
                            $thumbnail = str_replace('class="','class="product-image ',$thumbnail);
                            if ( ! $_product->is_visible() ) { ?>
                                    <?php echo $thumbnail; ?>
                            <?php } else {
                            printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
                            }
                            ?>
                            </figure>
                        </div><!-- End .product-top -->
                      
                  
                    </div>
                </td>
                <td>
                      <?php echo $_product->get_categories( ',', '<div class="product-cats">','</div>' ); ?>
                        <h6 class="shopping-cart-table__product-name text-left text-uppercase">
                            <?php
                            if ( ! $_product->is_visible() ) {
                                echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                            } else {
                                echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                            } ?>
                        </h6>
                        
                         <?php  
                         if(class_exists('WC_Vendors') ){
                                
                        if($welldone_settings['welldone_wcvendors_cartpage_soldby']){
                        ?>
                        <div class="product-cats"><?php welldone_wc_vendors_sold_by_meta(); ?></div>
                         <?php } }?>
                        
                        <?php
                        welldone_woo_cart_attributes($cart_item);
                        if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                        echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder','welldone' ) . '</p>';
                        } ?>
                </td>
<!--                product-price-->
                <td class="price-col">
                    <?php
                    echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                    ?>
                </td>
<!--                product-quantity-->
                <td class="quantity-col">
                    <?php
                    if ( $_product->is_sold_individually() ) {
                        $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                    } else {
                        $product_quantity = woocommerce_quantity_input( array(
                            'input_name'  => "cart[{$cart_item_key}][qty]",
                            'input_value' => $cart_item['quantity'],
                            'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                            'min_value'   => '0'
                        ), $_product, false );
                    }

                    echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
                    ?>
                </td>
<!--                product-subtotal-->
                <td class="price-total-col">
                    <?php
                    echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
                    ?>
                </td>
            </tr>
        <?php
        }
    }

    do_action( 'woocommerce_cart_contents' );
    ?>

    <?php do_action( 'woocommerce_after_cart_contents' ); ?>
    </tbody>
</table>
    <div class="hr"></div>
    <div class="divider divider--xs"></div>
    <div class="row shopping-cart-btns">
        <div class="col-sm-4 col-md-4">
            
        </div>
        <div class="col-sm-12 col-md-12">
            <a href="<?php echo esc_url(get_permalink( woocommerce_get_page_id( 'shop' ) ) ); ?>" class="btn btn--wd pull-left"><?php esc_html_e("CONTINUE SHOPPING",'welldone'); ?></a>
            <input type="submit" class="btn btn--wd pull-right" name="update_cart" value="<?php esc_html_e( 'Update Cart','welldone' ); ?>" />
            <?php do_action( 'woocommerce_cart_actions' ); ?>
            <?php wp_nonce_field( 'woocommerce-cart' ); ?>
        </div>
    </div>
    
