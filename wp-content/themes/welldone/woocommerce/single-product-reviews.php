<?php
/**
 * Display single product reviews (comments)
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.2
 */
global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews">
	<div id="comments">
		<h5><?php
			if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' && ( $count = $product->get_review_count() ) )
				printf( _n( '%s review for %s', '%s reviews for %s', $count,'welldone' ), $count, get_the_title() );
			else
				esc_html_e( 'Reviews','welldone' );
		?></h5>

		<?php if ( have_comments() ) : ?>

			<ol class="comments-list media-list">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				$links = paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
                    'prev_text'    => "<span aria-hidden='true'><i class='fa fa-angle-left'></i></span>",
                    'next_text'    => "<span aria-hidden='true'><i class='fa fa-angle-right'></i></span>",
					'type'      => 'array',
                    'echo'      => false,
				) ) );
                echo "<ul class='pagination'>";
                //print_r($links);
                foreach($links as $link){
                    if(preg_match("/^<span class='page-numbers current'>/", $link)){
                        echo "<li class='active'>";
                    }else{
                        echo "<li>";
                    }
                    echo $link;
                    echo "</li>";
                }
                echo "</ul>";
				echo '</nav>';
			endif; ?>

		<?php else : ?>

			<h5 class="woocommerce-noreviews"><?php esc_html_e( 'There are no reviews yet.','welldone' ); ?></h5>

		<?php endif; ?>
	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->id ) ) : ?>

		<div id="review_form_wrapper">
			<div id="review_form" class="review-respond contact-form">
				<?php
					$commenter = wp_get_current_commenter();

					$comment_form = array(
						'title_reply'          => have_comments() ? esc_html__( 'Add a review','welldone' ) : esc_html__( 'Be the first to review','welldone' ) . ' &ldquo;' . get_the_title() . '&rdquo;',
						'title_reply_to'       => esc_html__( 'Leave a Reply to %s','welldone' ),
						'comment_notes_before' => '',
						'comment_notes_after'  => '',
						'fields'               => array(
							'author' => '<div class="row"><div class="col-sm-12"><div class="form-group comment-form-author"><label class="input-desc" for="author">' . esc_html__( 'Name','welldone' ) . ' <span class="required-field">*</span></label> ' .
							            '<input id="author" class="form-control input--wd input--wd--full" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" placeholder="'.__("Your name (required)",'welldone').'" required /></div>',
							'email'  => '<div class="form-group comment-form-email"><label class="input-desc" for="email">' . esc_html__( 'Email','welldone' ) . ' <span class="required-field">*</span></label> ' .
							            '<input id="email" name="email"  class="form-control input--wd input--wd--full" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" placeholder="'.__("Your email (required)",'welldone').'" required /></div></div></div>',
						),
						'label_submit'  => esc_html__( 'Send Review', 'welldone' ),
						'logged_in_as'  => '',
                        'class_submit' => 'btn btn--wd text-uppercase wave waves-effect',
						'comment_field' => ''
					);

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<div class="form-group comment-form-rating"><label class="input-desc" for="rating">' . esc_html__( 'Your Rating', 'welldone' ) .'</label><select name="rating" id="rating">
							<option value="">' . esc_html__( 'Rate&hellip;','welldone' ) . '</option>
							<option value="5">' . esc_html__( 'Perfect','welldone' ) . '</option>
							<option value="4">' . esc_html__( 'Good','welldone' ) . '</option>
							<option value="3">' . esc_html__( 'Average','welldone' ) . '</option>
							<option value="2">' . esc_html__( 'Not that bad','welldone' ) . '</option>
							<option value="1">' . esc_html__( 'Very Poor','welldone' ) . '</option>
						</select></div>';
					}



					$comment_form['comment_field'] .= '<div class="form-group last comment-form-comment"><label for="comment"  class="input-desc">' . esc_html__( 'Your Review','welldone' ) . '<span class="required-field">*</span></label><textarea id="comment" class="textarea--wd input--wd--full" rows="7" name="comment" cols="45" placeholder="'.__("Your review",'welldone').'" required aria-required="true"></textarea></div>';
					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>

	<?php else : ?>

		<p class="woocommerce-verification-required"><?php esc_html_e( 'Only logged in customers who have purchased this product may leave a review.','welldone' ); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
</div>