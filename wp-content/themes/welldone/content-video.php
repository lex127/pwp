<?php 
		global $post;
		$video_embed = get_post_meta($post->ID, 'welldone_video_embed', true); 
?>
<?php
		if($video_embed){ 
?>
		<div class="entry-media embed-responsive embed-responsive-16by9">
			<?php echo wp_oembed_get( esc_url($video_embed), array( 'height' => '430') ); ?>
		</div><!-- End .entry-media -->
<?php 	} ?>