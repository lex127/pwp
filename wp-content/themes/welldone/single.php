<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */

global $welldone_settings, $post;
$welldone_layout_columns = welldone_page_columns ();
get_header ();
$blog_settings = welldone_get_blog_settings ();
$welldone_class_name = welldone_class_name ();
$containerClass = welldone_main_container_class();
?>
        <?php if ( !$welldone_settings['welldone_hide_breadcrumb'] ) : ?>
            <section class="breadcrumbs breadcrumbs-boxed hidden-xs">
                <div class="container"><?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(); ?></div>
            </section>
        <?php endif; ?>
        <section class="content fill">
        <div class="<?php echo esc_attr($containerClass); ?>">
            <div class="row">
                <div class="<?php echo esc_attr($welldone_class_name); ?>">
                    <div class="blog-content blog-content-bottom-none blog-content-top">
                    <?php if ( have_posts () ) {
                            while ( have_posts () ) {
                                the_post ();
                                get_template_part ( 'content-loop' );
                            } 
                       ?>
                       <div class="panel-inside-the-post">                
			                <div class="panel panel-default panel-table">
			                  <div class="panel-body">
			                    <div class="tr">
			                      <div class="td col-md-4 text-center">
			                      <div class="social-links social-links--colorize social-links--dark social-links--large">
			                        <ul>
			                        <li class="social-links__item"><a class="icon icon-facebook tooltip-link" href="#" data-placement="top" data-toggle="tooltip" data-original-title="Share on facebook"></a></li>
			                        <li class="social-links__item"><a class="icon icon-twitter tooltip-link" href="#" data-placement="top" data-toggle="tooltip" data-original-title="Share on twitter"></a></li>
			                        <li class="social-links__item"><a class="icon icon-google tooltip-link" href="#" data-placement="top" data-toggle="tooltip" data-original-title="Share on google"></a></li>
			                        <li class="social-links__item"><a class="icon icon-linkedin tooltip-link" href="#" data-placement="top" data-toggle="tooltip" data-original-title="Share on linkedin"></a></li>
			                        <li class="social-links__item"><a class="icon icon-pinterest tooltip-link" href="#" data-placement="top" data-toggle="tooltip" data-original-title="Share on pinterest"></a></li>
			                        </ul>
			                      </div>
			                      </div>
			                      <div class="td col-md-4 text-center">
			                      <h5><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><span class="icon icon-write"></span><?php esc_html_e("ARTICLES","welldone") ?></a></h5>
			                      </div>
			                      <div class="td col-md-4 text-center">
			                      <h5><a href="<?php comments_link(); ?>"><span class="icon icon-chat"></span> <?php echo get_comments_number() ?> <?php  ?> <?php esc_html_e( 'COMMENTS', 'welldone' ); ?></a></h5>
			                      </div>
			                    </div>
			                  </div>
			                </div>
			                <!-- / -->
			              </div>
                    </div>
                   <?php if($welldone_settings['welldone_post_page_nav']){ ?>
                        
                        
                        <div class="content content--parallax--short next-post" style="background:<?php if($welldone_settings['next_post_block_background']) { echo esc_attr( $welldone_settings['next_post_block_background']['background-color'] ); } ?> url(<?php if($welldone_settings['next_post_block_background']) { echo esc_attr( $welldone_settings['next_post_block_background']['background-image'] ); } ?>) 0 0 no-repeat;">
			              <div class="blog-post-title blog-post-title--light">
                      <h6 class="text-uppercase"><em><?php previous_post_link( '%link', ''.__("Previous Post",'welldone'), FALSE ); ?></em></h6>
                      <h2 class="blog-post-title__title text-uppercase"><?php previous_post_link('%link', '%title', false); ?></h2>
			                <h6 class="text-uppercase"><em><?php next_post_link( '%link', ''.__("Next Post",'welldone'), FALSE ); ?></em></h6>
			                <h2 class="blog-post-title__title text-uppercase"><?php next_post_link('%link', '%title', false); ?></h2>
			              </div>
			            </div>
                       
                   <?php } ?>
                   <?php
                       if($welldone_settings['welldone_post_comments']) {
                          echo '<div class="divider divider--line divider--line--dark"></div>';
                           comments_template ();
                       }
                       } else {
                               get_template_part ( "content", "none" );
                       } 
                   ?>
                </div><!-- End .col-md-9 -->
                <div class="md-margin2x clearfix visible-sm visible-xs"></div><!-- space -->
              <?php  get_sidebar() ?>
            </div><!-- End .row -->
        </div><!-- End .container -->
        <div class="lg-margin hidden-xs hidden-sm"></div><!-- space -->
    </section><!-- End #content -->
<?php get_footer () ?>