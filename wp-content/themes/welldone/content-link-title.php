<?php
/**
 * The template for displaying posts title in the Link post format
 *
 * Used for single and index/archive/blog/search.
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */
?>



<?php 
		global $post, $blog_settings;
		$post_format = get_post_format( $post->ID );
    $num_comments = get_comments_number();
?>    <?php if($blog_settings['blog_type']=='blog-masonry') : ?>
      <div class="post__view-btn">
        <?php if ( $post->comment_count != 0  ) : ?>
        <div class="btn-plus">
          <div class="btn-plus__links">
            <a class="btn-plus__links__icon" href="<?php the_permalink(); ?>">
              <span class="icon icon-eye"></span>
            </a>
            <a class="btn-plus__links__icon" href="<?php echo get_comments_link(); ?>">
              <span class="icon icon-message"></span>
            </a>
          </div>
          <a class="btn-plus__toggle btn btn--round" href="#">
            <span class="icon icon-dots-horizontal"></span>
          </a>
        </div>
        <?php else : ?>
          <div class="text-center"> <a class="btn btn--round" href="<?php the_permalink(); ?>"><span class="icon icon-eye"></span></a> </div>
        <?php endif; ?>
      </div>
      <?php endif; ?>
      <?php if($blog_settings[ 'blog_type' ] =='classic' && !is_single()) : ?>
            <h2 class="entry-title post__title text-uppercase"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
      <?php elseif($blog_settings[ 'blog_type' ] =='blog-masonry' && !is_single()) : ?>
            <h5 class="entry-title post__title text-uppercase"><a target="_blank" href="<?php the_permalink(); ?>"><?php the_title();?></a></h5>
      <?php endif; ?>
     <?php
 
	 if($post_format!="aside"):
			if( is_single() && !$blog_settings['hide_blog_post_title'] ):
				if ( 'link' == $post_format  ) : ?>
           <h5 class="entry-title post__title text-uppercase"><a target="_blank" href="<?php echo esc_url( $ext_link ); ?>"><?php the_title();?></a></h5>
        <?php else: ?>
            <h2 class="entry-title post__title text-uppercase"><?php the_title();?></h2>
        <?php endif; ?>
     <?php endif; ?>
     <?php if( is_single() && !$blog_settings['hide_blog_post_author'] || is_search() ): ?>
        <div class="blog-post-title__meta text-left">
          <div class="blog-post-title__meta__image">
            <a href="<?php the_author_link (); ?>">
              <?php $author_email = get_the_author_meta('email'); echo get_avatar($author_email,'70');?>
            </a>
          </div>
          <div class="blog-post-title__meta__text">
            <h4 class="blog-post-title__meta__text__name text-uppercase"><a href="<?php the_author_link (); ?>"><?php the_author(); ?></a></h4>
            <div class="blog-post-title__meta__text__date"><?php the_time('F j, Y'); ?></div>
          </div>
        </div>
     <?php endif; ?>
  <?php endif; ?>