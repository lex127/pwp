<?php
/**
 * The default template for displaying posts loop
 *
 * Used for both single and index/archive/blog/search.
 *
 * @package welldone
 * @subpackage welldone
 * @since welldone 1.0
 */
?>
<?php  
        global $blog_settings, $wp_query ,$thumbnail_size;  ?>
<?php
        $post_format = get_post_format();
        $catslug = '';
        $col4 = false;
        $thumbnail_size = 'welldone-large';
        
    if(!is_single()) {
        if ( $blog_settings[ 'blog_type' ] =='blog-list' ||  $blog_settings[ 'blog_type' ] == 'blog-masonry' ) {
            $thumbnail_size = 'full';
        } elseif ( $blog_settings[ 'blog_type' ] =='classic'  ) {
            $thumbnail_size = 'full';
        }
    }

    foreach(get_the_category() as $category) {
        $catslug .= $category->slug . ' ';
    }
    
    if($blog_settings['blog_type']=='blog-masonry'){
        $class = 'entry entry-grid post post--column';
    } else{
        $class = "entry";
    }
    
    if(is_single()){
        $class = 'entry single';
    }
?>

<article id="post-<?php the_ID(); ?>" <?php post_class($class);?>>

  <?php  if( ('video'==$post_format || 'gallery'==$post_format || 'audio'==$post_format) && !post_password_required()  ){ ?>
        <?php if($blog_settings['blog_type']=='blog-list' && !is_single()){?>
            <div class="col-md-4">
            <?php $col4 = true;
        } ?>
           
                <?php if(isset($col4) && $col4==true){?>
                </div>
                <?php } ?>
       <?php }elseif(has_post_thumbnail() && !post_password_required()){ ?>
          <?php   get_template_part("content","thumbnail"); ?>
                <?php if(isset($col4)  && $col4==true){?>
                    </div>
                <?php } ?>
        <?php }
  ?>
    <?php  if(!is_single()) { ?>
    <?php if($blog_settings['blog_type']!='timeline'){ ?>

         <div class="entry-wrapper">
        <?php } ?>
    <?php } ?>
        <?php if($blog_settings[ 'blog_type' ] =='classic' && !is_single()){ 

            get_template_part("content",'link-title');

           
            get_template_part('content',$post_format);
            

            get_template_part("content","post");

            get_template_part("content","meta");

        } ?>
       <?php if('audio'==$post_format ){

        }?>
        <?php if($blog_settings['blog_type']=='blog-masonry' && !is_single()){ 

            get_template_part("content",'link-title'); 

            get_template_part("content","post");

            get_template_part("content",'meta');
        }?>
        <?php if (is_single()) {
            get_template_part("content",'link-title');
            get_template_part('content',$post_format);
            get_template_part("content","post");
        }
        ?>
</article> 