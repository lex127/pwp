<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package welldone
 */
global $welldone_settings, $post;
$welldone_layout_columns = welldone_page_columns ();
$welldone_class_name = welldone_class_name ();
$containerClass = welldone_main_container_class();
get_header(); ?>
         <!-- Modal -->
        <div class="modal quick-view zoom" id="quickView"  style="opacity: 1">
            <div class="modal-dialog">
                <div class="modal-content"> </div>
            </div>
        </div>
        <!-- /.modal -->
        <div class="content"><!-- .content -->
            <div class="<?php echo esc_attr($containerClass); ?>"><!-- containeer -->
                <div class="row"><!-- row -->
                    <div class="<?php echo esc_attr($welldone_class_name); ?>"><!-- col-md-* -->
                            <?php while(have_posts()) : the_post(); ?>
                                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>><!-- post -->
                                    <div class="page"><!-- page -->
                                        <?php the_content(); ?>
                                            <?php
                                               wp_link_pages( array(
                                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'welldone' ),
                                                'after'  => '</div>',
                                               ) );
                                            ?>
                                        <?php welldone_add_social_share(); ?>
                                    </div><!-- End page -->
                                </div><!-- End post -->
                         
                        <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                    <!-- <br><br> -->
                    <?php comments_template(); ?>
                    </div><!-- End .col-md-* -->
                    <div class="clearfix visible-sm visible-xs"></div>
                    <?php get_sidebar() ?>
                </div><!-- End .row -->
            </div><!-- End .container -->
        </div><!-- #content -->
        </div><!-- pageContent -->


<?php get_footer(); ?>
