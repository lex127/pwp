<?php
/**
 * The template for header social icons
 *
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */
global $welldone_settings;
?>
<div class="header-line hidden-xs">
  <div class="container">
    <div class="pull-left">
      <div class="social-links social-links--colorize">
        <ul>
			<?php if ( !empty($welldone_settings[ 'header_icon_fb' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-facebook" href="<?php echo esc_attr( $welldone_settings['header_icon_fb'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
	        <?php if ( !empty($welldone_settings[ 'header_icon_linkedin' ] )): ?>
                <li style="font-size: 22px; margin-right: 25px;" class="social-links__item"><a class="fa fa-linkedin" href="<?php echo esc_attr( $welldone_settings['header_icon_linkedin'] ); ?>" target="_blank"></a></li>
	        <?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_twitter' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-twitter" href="<?php echo esc_attr( $welldone_settings['header_icon_twitter'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_gmail' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-google" href="<?php echo esc_attr( $welldone_settings['header_icon_gmail'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_pin' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-pinterest" href="<?php echo esc_attr( $welldone_settings['header_icon_pin'] ); ?>" target="_blank"></a></li>
			<?php endif; ?>
			<?php if ( !empty($welldone_settings[ 'header_icon_mail' ] )): ?>
			    <li class="social-links__item"><a class="icon icon-mail" href="mailto:<?php echo esc_attr( $welldone_settings['header_icon_mail'] );?>" target="_blank"></a></li>
			<?php endif; ?>
        </ul>
      </div>
    </div>
    <div class="pull-right">
      <div class="user-links">
        <ul>
		  <?php if ( $welldone_settings['header_show_auth'] ) : ?>
			<li class="user-links__item"><?php if ( !is_user_logged_in() ) { ?>
                    <a href="#" title="<?php esc_html_e('Sign in','welldone'); ?>" class="home-page-login"><?php esc_html_e('Sign in','welldone'); ?></a>
                    <span style="font-family:Gotham Rounded Light;font-size: 12px;">or</span>
                    <a href="#" class="popmake-register-global" title="<?php esc_html_e('Register','welldone'); ?>" class="home-page-login"><?php esc_html_e('Register','welldone'); ?></a>

				<?php } ?></li>
			<li class="user-links__item"><?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo get_edit_user_link(); ?>" title="<?php esc_html_e('My Account','welldone'); ?>"><?php esc_html_e('My Account','welldone'); ?></a>
                    <a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Log out">Log out</a>
                   <?php } ?></li>
  		  <?php endif; ?>
        </ul>
      </div>
    </div>
  </div>
</div>