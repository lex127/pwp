<?php
/*
 * @package welldone
 *
 */
global $welldone_settings,$args, $post;
$welldone_layout_columns = welldone_page_columns ();
$class=welldone_main_container_class();
get_header ();
$blog_settings = welldone_get_blog_settings ();
get_template_part ( 'content-query' );
$wp_query = new WP_Query( $args);
$welldone_class_name =  welldone_class_name();
?>

            <div class="content">
    			<div class="<?php echo esc_attr($class);?>">
    			    <?php if($blog_settings['blog_type']=="classic"){ ?>  
                    <div class="row">
                        <div class="<?php echo esc_attr($welldone_class_name);?>">
                            <?php if ( $wp_query->have_posts () ) { ?>
                                        <div class="list-blog">
                                            <?php while ( $wp_query->have_posts() ) {
                                                $wp_query->the_post();
                                                get_template_part( 'content','loop' );
                                            } ?>
                                        </div>
                                         
                                    <?php 
                                        welldone_pagination();
                                     } else {
                                        get_template_part( "content-none" );
                                     } ?>
                                    <?php wp_reset_postdata(); ?>
                        </div>
                            <div class="md-margin2x clearfix visible-sm visible-xs"></div><!-- space -->
                             <?php  get_sidebar(); ?>
                    </div>
                    <?php } else { ?>
                    <?php if ( $wp_query->have_posts () ) { ?>
                   
                                    <div class="posts-isotope" id="blog-item-container" data-max-col="<?php echo esc_attr($blog_settings['blog_masonry_cols']); ?>">

                                   <?php while ( $wp_query->have_posts() ) {
                                        $wp_query->the_post();
                                        get_template_part( 'content','loop' );
                                    } ?>

                                </div>
                 

                              <?php 
                              welldone_pagination();
                             } else {
                                get_template_part( "content-none" );
                             } ?>
                            <?php wp_reset_postdata(); ?>
                    <div class="md-margin2x clearfix visible-sm visible-xs"></div><!-- space -->
                    <?php } ?>
		        </div>
    		</div>

<?php get_footer(); ?>
