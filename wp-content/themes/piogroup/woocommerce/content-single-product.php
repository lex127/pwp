<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class("row product-info-outer"); ?>>
	<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		 
		 
global $welldone_settings;
if ( $welldone_settings[ 'welldone_show_add_to_cart_button' ] ) {


}else{
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
}
		 
		do_action( 'woocommerce_before_single_product_summary' );
	?>
	
<div class="product-info col-sm-7 col-md-7 col-lg-6">
	<div class="summary entry-summary">
        <div class="product-details text-left">
            <?php
            /**
			 * woocommerce_single_product_summary hook
			 *
             *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );

		?>
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
    	<div class="addthis_sharing_toolbox"></div>
        </div>
	</div><!-- .summary -->

	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />
	<?php if($welldone_settings[ 'welldone_show_add_to_cart_button' ]==0){ ?>
<div class="send-enquiry">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne2">
				<h4 class="panel-title enq">
					<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
						<i class="fa fa-envelope"></i> <?php esc_html_e('Send Enquiry','welldone');?>
						<span class="panel-icon"></span>
					</a>
				</h4>
			</div><!-- End .panel-heading -->
			<div id="collapseOne2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
				<div class="panel-body">
				<?php echo do_shortcode($welldone_settings['welldone_send_enquiry']); ?>
				</div><!-- End .panel-body -->
			</div><!-- End .panel-collapse -->
		</div><!-- End .panel -->
	</div><!-- End .panel-group -->
 </div>
<?php } ?>
</div><!--product-info-->
</div><!-- #product-<?php the_ID(); ?> -->
