<?php
/**
 * Edit account form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>


<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div class="card card--form">
			<form action="" method="post" class="contact-form">
			
				<?php do_action( 'woocommerce_edit_account_form_start' ); ?>
			
				<p class="form-row input-group form-row-first">
					<label class="input-desc" for="account_first_name"><?php esc_html_e( 'First name','welldone' ); ?> <span class="required">*</span></label>
					<input type="text" class="input--wd input--wd--full" name="account_first_name" id="account_first_name" value="<?php echo esc_attr( $user->first_name ); ?>" />
				</p>
				<p class="form-row input-group form-row-last">
					<label class="input-desc" for="account_last_name"><?php esc_html_e( 'Last name','welldone' ); ?> <span class="required">*</span></label>
					<input type="text" class="input--wd input--wd--full" name="account_last_name" id="account_last_name" value="<?php echo esc_attr( $user->last_name ); ?>" />
				</p>
				<div class="clear"></div>
			
				<p class="form-row input-group form-row-wide">
					<label class="input-desc" for="account_email"><?php esc_html_e( 'Email address','welldone' ); ?> <span class="required">*</span></label>
					<input type="email" class="input--wd input--wd--full" name="account_email" id="account_email" value="<?php echo esc_attr( $user->user_email ); ?>" />
				</p>
			
				<fieldset>
					<legend><?php esc_html_e( 'Password Change','welldone' ); ?></legend>
			
					<p class="form-row input-group form-row-wide">
						<label class="input-desc" for="password_current"><?php esc_html_e( 'Current Password (leave blank to leave unchanged)','welldone' ); ?></label>
						<input type="password" class="input--wd input--wd--full" name="password_current" id="password_current" />
					</p>
					<p class="form-row input-group form-row-wide">
						<label class="input-desc" for="password_1"><?php esc_html_e( 'New Password (leave blank to leave unchanged)','welldone' ); ?></label>
						<input type="password" class="input--wd input--wd--full" name="password_1" id="password_1" />
					</p>
					<p class="form-row input-group form-row-wide">
						<label class="input-desc" for="password_2"><?php esc_html_e( 'Confirm New Password','welldone' ); ?></label>
						<input type="password" class="input--wd input--wd--full" name="password_2" id="password_2" />
					</p>
				</fieldset>
				<div class="clear"></div>
			
				<?php do_action( 'woocommerce_edit_account_form' ); ?>
				<div class="divider divider--xs"></div>
				<p class="input-group">
					<?php wp_nonce_field( 'save_account_details' ); ?>
					<input type="submit" class="button btn btn--wd text-uppercase wave btn-custom2 min-width" name="save_account_details" value="<?php esc_html_e( 'Save changes','welldone' ); ?>" />
					<input type="hidden" name="action" value="save_account_details" />
				</p>
				<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
			</form>
		</div>
	</div>
</div>