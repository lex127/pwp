<?php

/**

 * Content wrappers

 *

 * @author      WooThemes

 * @package     WooCommerce/Templates

 * @version     1.6.4

 */



if ( ! defined( 'ABSPATH' ) ) {

    exit; // Exit if accessed directly

}



global $welldone_settings,$args,$welldone_layout_columns, $post,$woocommerce_loop, $woocommerce, $product;

if ( empty( $woocommerce_loop['columns'] ) ) {

    $woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

}

$ver = welldone_single_product_version ();

$welldone_layout_columns = welldone_page_columns();

$welldone_class_name = welldone_class_name ();

?>

<section class="content"><!-- section.content -->


    <div class="container"><!-- section.content > .container -->

   <div class="row wrapper-start"><!-- .container > .row.wrapper-start -->
        <?php if(!is_product()){ ?>

    <div id="products_container" class="col-md-12"><!-- .row.wrapper-start > .products_container -->
        <?php }elseif($ver == "v_1"){ ?>
            <div class="<?php echo esc_attr($welldone_class_name); ?>">
        <?php } elseif($ver == "v_2"){ ?>
            <div class="col-md-12">
        <?php } ?>