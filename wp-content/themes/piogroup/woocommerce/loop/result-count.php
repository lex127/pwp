<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce, $wp_query, $wpdb;

if ( ! woocommerce_products_will_display() )
	return;
?>
<div class="woocommerce-result-count filters-row__items">
	<?php
	$paged    = max( 1, $wp_query->get( 'paged' ) );
	$per_page = $wp_query->get( 'posts_per_page' );
	$total    = $wp_query->found_posts;
	$first    = ( $per_page * $paged ) - $per_page + 1;
	$last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );

	if ( 1 == $total ) {
		esc_html_e( 'The single result', 'welldone' );
	} elseif ( ($total <= $per_page) || (-1 == $per_page) ) {
		printf( esc_html__( 'Items all %d results', 'welldone' ), $total );
	} else {
		printf( _x( 'Items %1$d to %2$d of %3$d', '%1$d = first, %2$d = last, %3$d = total', 'welldone' ), $first, $last, $total );
	}
	?>
</div>