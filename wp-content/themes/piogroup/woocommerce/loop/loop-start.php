<?php
/**
 * Product Loop Start
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */
global $woocommerce_loop, $welldone_shop_page_settings,$columns,$welldone_settings;

$welldone_shop_page_settings = array();


$columns = '';
if(is_shop()  || is_product_taxonomy() ){
    $columns = welldone_shop_columns();
}else{
    $columns = $woocommerce_loop['columns'];
}

if(isset($_GET['product_cols']) && $_GET['product_cols']){
    $cols = array(1,2,3,4,5,6);
    if(in_array($_GET['product_cols'], $cols)) {
        $columns = $_GET[ 'product_cols' ];
    }
}
$pr_id = apply_filters("welldone_filter_product_container_id","product_list");
$pr_class = apply_filters("welldone_filter_product_container_class","products-grid products-listing products-col products-isotope");
?>
<?php


if (!function_exists('openedFilter')) {
    function openedFilter(){
        $opened=Mage::helper('welldone')->getLayoutOption('general/opened_filter');
        $filterOpened=Mage::app()->getRequest()->getParam('filterOpened');
        if($opened){
            if(isset($filterOpened)){
                if(!$filterOpened)return false;
            }
        }
        if($opened || $filterOpened)return true;
    }
}

?>
<div class="outer"><!-- .outer -->
<?php if (is_shop() || is_product_category() || is_product_tag() || is_tax( 'brand')) : ?>
    <div id="leftCol">
        <div id="filtersCol" class="filters-col">
            <div class="filters-col__close" id="filtersColClose"><a href="#" class="icon icon-clear"></a></div>
            <?php woocommerce_get_sidebar(); ?>
        </div>
    </div>
<?php endif; ?>
    <div class="centerCol"><!-- .centerCol -->
    <!-- Modal -->
        <div class="modal quick-view zoom" id="quickView"  style="opacity: 1">
            <div class="modal-dialog">
                <div class="modal-content"> </div>
            </div>
        </div>
        <!-- /.modal -->
        <div class="<?php echo  esc_attr($pr_class); ?> clearfix products <?php echo ($columns)?$columns:1; ?>"  data-columns="<?php echo ($columns)?$columns:1; ?>" ><!-- .products-listing -->