<?php
/**
 * Product loop sale flash
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $post, $product, $welldone_settings;
$percentage="";
// sale flash
if ( $product->is_on_sale() && $welldone_settings['welldone_product_sale_label'] ) :
    $classes = array();
    $text_type = $welldone_settings['welldone_product_sale_label_text_type'];
    if($text_type=='custom-text') {
        $text = ($welldone_settings['welldone_product_sale_label_text'])?$welldone_settings['welldone_product_sale_label_text']:__('Sale','welldone');
    }else{
        if($product->product_type == 'simple') {
            $percentage = round ( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );

        }elseif($product->product_type=='variable'){
            $available_variations = $product->get_available_variations();
            $maximumper = 0;
            for ($i = 0; $i < count($available_variations); ++$i) {
                $variation_id=$available_variations[$i]['variation_id'];
                $variable_product1= new WC_Product_Variation( $variation_id );
                $regular_price = $variable_product1 ->regular_price;
                $sales_price = $variable_product1 ->sale_price;
                $percentage= round((( ( $regular_price - $sales_price ) / $regular_price ) * 100)) ;
                if ($percentage > $maximumper && $sales_price) {
                    $maximumper = $percentage;
                }
            }
            $percentage = $maximumper;
        }
        $text =  esc_html__('SALE','welldone') . '<br>' . "-" . $percentage . "%";
    }
    echo apply_filters( 'woocommerce_sale_flash', '<div class="product-preview__label product-preview__label--right product-preview__label--sale"><span>' . $text . '</span></div>', $post, $product );
 endif; //end sale flash

//new
if($welldone_settings['welldone_product_new_label']):
    $d = ($welldone_settings['welldone_product_new_label_time'])?$welldone_settings['welldone_product_new_label_time']:7;
    if(!is_numeric($d) || $d<=0 ){
        $d = 7;
    }
    if(strtotime( $post->post_date ) >= strtotime('-'.$d.' day')) {
        $classes = array ();
        $text = ($welldone_settings['welldone_product_new_label_text'])?$welldone_settings['welldone_product_new_label_text']:__("New",'welldone');
        echo '<div class="product-preview__label product-preview__label--left product-preview__label--new"><span>' .$text. '</span></div>';
}
endif; //end new
?>