<?php
/**
 * The template for displaying product category thumbnails within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product_cat.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) ) {
	$woocommerce_loop['loop'] = 0;
}

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) ) {
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
}

// Increase loop count
$woocommerce_loop['loop'] ++;
?>
<div class="product-category hover-squared">
	<?php do_action( 'woocommerce_before_subcategory', $category ); ?>


		<?php
			/**
			 * woocommerce_before_subcategory_title hook
			 *
			 * @hooked woocommerce_subcategory_thumbnail - 10
			 */
			do_action( 'woocommerce_before_subcategory_title', $category );
		?>
		<?php 
		if ( is_product_category() ){
		    global $wp_query;
		    // get the query object
		    $cat = $wp_query->get_queried_object();
		    // get the thumbnail id user the term_id
		    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true );
		    // get the image URL
		    $image = wp_get_attachment_image( $thumbnail_id, 'feature-image' );
		    // print the IMG HTML
		    echo $image;
    	}

 ?>


			<div class="product-category__hover caption"></div>
			<div class="product-category__info">
				<div class="product-category__info__ribbon">
					<h5 class="product-category__info__ribbon__title"><?php echo $category->name; ?></h5>
					<div class="product-category__info__ribbon__count">
						<?php
							if ( $category->count > 0 )
								echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">' . $category->count . '</mark>', $category );
								esc_html_e( 'products','welldone' );
						?>
					</div>
				</div>
			</div>

		<?php
			/**
			 * woocommerce_after_subcategory_title hook
			 */
			do_action( 'woocommerce_after_subcategory_title', $category );
		?>

	

	<?php do_action( 'woocommerce_after_subcategory', $category ); ?>
</div>
