<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
if ( $order ) : ?>
	<?php if ( $order->has_status( 'failed' ) ) : ?>
		<p class="first-color"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.','welldone' ); ?></p>
		<p class="first-color"><?php
			if ( is_user_logged_in() )
				esc_html_e( 'Please attempt your purchase again or go to your account page.','welldone' );
			else
				esc_html_e( 'Please attempt your purchase again.','welldone' );
		?></p>
		<p class="first-color">
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php esc_html_e( 'Pay','welldone' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My Account','welldone' ); ?></a>
			<?php endif; ?>
		</p>
	<?php else : ?>
		<p class="first-color"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.','welldone' ), $order ); ?></p>
		<ul class="order_details list-group">
			<li class="order list-group-item">
				<?php esc_html_e( 'Order Number:','welldone' ); ?>
				<strong><?php echo $order->get_order_number(); ?></strong>
			</li>
			<li class="date list-group-item">
				<?php esc_html_e( 'Date:','welldone' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total list-group-item">
				<?php esc_html_e( 'Total:','welldone' ); ?>
				<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
			<li class="method list-group-item">
				<?php esc_html_e( 'Payment Method:','welldone' ); ?>
				<strong><?php echo $order->payment_method_title; ?></strong>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>
<?php else : ?>
	<p class="first-color"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.','welldone' ), null ); ?></p>
<?php endif; ?>