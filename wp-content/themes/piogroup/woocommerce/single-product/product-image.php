<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.3
 */

 if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product, $welldone_settings;
$ver = welldone_single_product_version ();
?>

<div class="col-sm-5 col-md-5 col-lg-6">
<?php if ( $ver == "v_1" ) { ?>
	<div class="images product-gallery-container product-main-image">
	    <div class="product-top product-main-image__item">

		<?php
	        woocommerce_show_product_sale_flash();

			if ( has_post_thumbnail() ) {

				$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );

				$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;

				$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );

				$image       	= get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(

					'title'	=> $image_title,

					'alt'	=> $image_title

					) );

	            $image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'shop_single' ); //single-product //

	            $image = $image['0'];

	            $zoom_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );

	            $zoom_img = $zoom_img['0'];
				
				
	                echo apply_filters("woocommerce_single_product_image_html", sprintf('<img class="product-zoom" src="%s" data-zoom-image="%s" alt="%s"/>',$image,$zoom_img,$image_title),$post->ID );

			} else {

	    			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img class="product-zoom" src="%s"  data-zoom-image="%s" alt="%s" />', wc_placeholder_img_src(),wc_placeholder_img_src(), esc_html__( 'Placeholder','welldone' ) ), $post->ID );
			}
		?>

	    </div>

	<div class="product-main-image__zoom"></div>
	</div>
	<?php do_action( 'woocommerce_product_thumbnails' ); 
}
?>




<?php 
if ( $ver == "v_2" ) {

	$attachment_ids = $product->get_gallery_attachment_ids();
	if(has_post_thumbnail()){
	    if(empty($attachment_ids)){
	        $attachment_ids[] = get_post_thumbnail_id($post->ID);
	    }else{
	        array_unshift ( $attachment_ids ,  get_post_thumbnail_id($post->ID) );
	    }
	}

	if ( $attachment_ids ) {
		$loop 		= 0;
		$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 5 );
		?>
		<ul id="singleGalleryVertical">
		    <?php
		
				foreach ( $attachment_ids as $attachment_id ) {
		
		
					$image        = wp_get_attachment_image_src( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'full' ) );
		        $image_zoom   = wp_get_attachment_image_src( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'full' ) );
					$image_class = 'product-gallery-item';
					$image_title = esc_attr( get_the_title( $attachment_id ) );
					echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<li><a href="#" data-image="%s" data-zoom-image="%s" class="%s" title="%s"><img src="%s" alt="%s"/></a></li>', $image[0],$image_zoom[0],$image_class, $image_title, $image[0],$image_title), $attachment_id, $post->ID, $image_class );
					$loop++;
				}
		
			?></ul>
		<?php
	}
}
?>
</div>