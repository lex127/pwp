<?php
/**
 * @package welldone
 *
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php global $welldone_settings, $current_header , $post, $current_page; ?>

<?php $current_page =  welldone_current_page_id(); ?>

<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
	<?php if($welldone_settings['favicon']): ?>
	    <link rel="shortcut icon" href="<?php echo esc_url($welldone_settings['favicon']['url']); ?>" type="image/x-icon" />
	<?php endif; ?>
<?php } ?>

  
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php get_template_part("dynamic-style"); ?> 

<?php wp_head(); ?>
</head>
	
<body <?php body_class(); ?>>
<div id="loader-wrapper">
	<div id="loader"></div>
	<div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<div id="wrapper" class="<?php echo welldone_wrapper_class() ?>"><!-- Wrapper -->

<!-- Modal Search -->
<div class="overlay overlay-scale">
  <button type="button" class="overlay-close"> &#x2715; </button>
  <div class="overlay__content">
    <?php get_template_part('template-parts/searchform'); ?>
  </div>
</div>
<!-- / end Modal Search -->


	<?php 

	$current_header = welldone_current_header();

	get_template_part('template-parts/header/header',$current_header);
	?>

	<div id="pageContent" class="<?php if ( $welldone_settings['welldone_theme_wrapper'] =='boxed' ) { echo "page-content"; } ?>"><!-- .pageContent -->

	<?php if( !is_single() ){ ?>
	<?php welldone_page_title_header(); ?>
	<?php } ?>
