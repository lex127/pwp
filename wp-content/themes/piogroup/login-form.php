<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
    <div class="col2-set row" id="customer_login">
    <div class="col-1 col-md-6">
<?php endif; ?>
    <h2 class="h-pad-sm text-uppercase text-center"><?php esc_html_e( 'Already Registered?','welldone' ); ?></h2>
    <h6 class="text-uppercase text-center">If you have an account with us, please log in.</h6>
    <div class="divider divider--sm"></div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
            <div class="tml-login card card--form" id="theme-my-login<?php $template->the_instance(); ?>"> <a href="#" class="icon card--form__icon icon-user-circle"></a>
	            <?php $template->the_action_template_message( 'login' ); ?>
	            <?php $template->the_errors(); ?>
                <h2><?php esc_html_e( 'Login','welldone' ); ?></h2>
                <form class="login" name="loginform" id="loginform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'login', 'login_post' ); ?>" method="post">
					<?php do_action( 'woocommerce_login_form_start' ); ?>
                    <p class="form-row form-group form-row-wide">
                        <label class="input-desc" for="username"><?php esc_html_e( 'Email address','welldone' ); ?> <span class="required">*</span></label>
                        <input placeholder="<?php
                        if ( 'username' == $theme_my_login->get_option( 'login_type' ) ) {
	                        _e( 'Username', 'theme-my-login' );
                        } elseif ( 'email' == $theme_my_login->get_option( 'login_type' ) ) {
	                        _e( 'E-mail', 'theme-my-login' );
                        } else {
	                        _e( 'E-mail', 'theme-my-login' );
                        }
                        ?>" type="text" class="input--wd input--wd--full" name="log" id="user_login<?php $template->the_instance(); ?>" value="<?php $template->the_posted_value( 'log' ); ?>" />
                    </p>
                    <p class="form-row form-group form-row-wide">
                        <label class="input-desc" for="password"><?php esc_html_e( 'Password','welldone' ); ?> <span class="required">*</span></label>
                        <input placeholder="<?php _e( 'Password', 'theme-my-login' ); ?>" class="input--wd input--wd--full" type="password" name="pwd" id="user_pass<?php $template->the_instance(); ?>" autocomplete="off" />
                    </p>
					<?php do_action( 'login_form' ); ?>
                    <p class="form-row form-group">

                    <div class="checkbox-group">
                        <input name="rememberme" type="checkbox" id="rememberme<?php $template->the_instance(); ?>" value="forever" />
                        <label for="rememberme<?php $template->the_instance(); ?>" class="inline checkbox-inline custom-checkbox-wrapper"> <span class="check"></span> <span class="box"></span><?php esc_html_e( 'Remember me','welldone' ); ?></label>
                    </div>
                      <p>  <input type="submit"  class="button btn btn--wd text-uppercase wave btn-custom2" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="<?php esc_attr_e( 'Log In', 'theme-my-login' ); ?>" />
                        <input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>" />
                        <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
                        <input type="hidden" name="action" value="login" /></p>
                    <div class="divider divider--xs"></div>
                    </p>
					<?php do_action( 'woocommerce_login_form_end' ); ?>
                </form>
                <div class="card--form__footer lost_password"><a class="btn btn-custom3" href="<?php echo esc_url( wc_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?','welldone' ); ?></a></div>
            </div>
        </div>
    </div>
<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
    </div>
    <div class="col-2 col-md-6">
        <h2 class="h-pad-sm text-uppercase text-center">New Here?</h2>
        <h6 class="text-uppercase text-center">Registration is free and easy!</h6>
        <div class="divider divider--sm"></div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                <div class="card card--form">
                    <h2><?php esc_html_e( 'Register','welldone' ); ?></h2>
                    <form method="post" class="register">
						<?php do_action( 'woocommerce_register_form_start' ); ?>
						<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>
                            <p class="form-row form-group form-row-wide">
                                <label class="input-desc" for="reg_username"><?php esc_html_e( 'Username','welldone' ); ?> <span class="required">*</span></label>
                                <input type="text" class="input--wd input--wd--full" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                            </p>
						<?php endif; ?>
                        <p class="form-row form-group form-row-wide">
                            <label class="input-desc" for="reg_email"><?php esc_html_e( 'Email address','welldone' ); ?> <span class="required">*</span></label>
                            <input type="email" class="input--wd input--wd--full" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
                        </p>
						<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
                            <p class="form-row form-group form-row-wide">
                                <label class="input-desc" for="reg_password"><?php esc_html_e( 'Password','welldone' ); ?> <span class="required">*</span></label>
                                <input type="password" class="input--wd input--wd--full" name="password" id="reg_password" />
                            </p>
						<?php endif; ?>
                        <!-- Spam Trap -->
                        <div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;">
                            <label for="trap"><?php esc_html_e( 'Anti-spam','welldone' ); ?></label>
                            <input type="text" name="email_2" id="trap" tabindex="-1" />
                        </div>

						<?php do_action( 'woocommerce_register_form' ); ?>
						<?php do_action( 'register_form' ); ?>

                        <p class="form-row form-group">
							<?php wp_nonce_field( 'woocommerce-register' ); ?>
                            <input type="submit" class="button btn btn--wd text-uppercase wave btn-custom" name="register" value="<?php esc_html_e( 'Register','welldone' ); ?>" />
                        </p>

						<?php do_action( 'woocommerce_register_form_end' ); ?>

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php endif; ?>