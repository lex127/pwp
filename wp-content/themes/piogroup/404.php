<?php
/**
 * The template for displaying 404 pages (not found).
 * @package welldone
 */
global $welldone_settings;
get_header(); ?>
	
	<?php if ( !$welldone_settings['welldone_hide_breadcrumb'] ) : ?>
    <section class="breadcrumbs  hidden-xs">
      <div class="container">
        <ol class="breadcrumb breadcrumb--wd pull-left">
          <li><a href="<?php echo esc_url(home_url()); ?>"><?php esc_html_e( 'Home', 'welldone' ); ?></a></li>
          <li class="active"><?php esc_html_e( 'Error 404', 'welldone' ); ?></li>
        </ol>
      </div>
    </section>
    <?php endif; ?>
	 <!-- Content section -->
    <section class="content">
      <div class="container">
			<div class="not-found-box">
				<div class="not-found-box__image">
					<img src="<?php echo get_template_directory_uri(). '/images/page-404.png' ?>" alt="Error 404">
					<div class="not-found-box__text">
						<h2 class="text-left"><?php esc_html_e( '404 ERROR. ', 'welldone' ); ?><br><span><?php esc_html_e( 'PAGE NOT FOUND', 'welldone' ); ?></span></h2>
						<h4 class="text-left"><?php esc_html_e( 'You can start from the home page or come back to the previous page', 'welldone' ); ?></h4>
					</div>
				</div>
			</div>
      </div>
    </section>
    <!-- End Content section --> 

<?php get_footer(); ?>
