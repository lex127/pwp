jQuery(document).ready(function ($) {

    //added title and link to show dynamically and check h2 title ( need only when click Donate button )
    $(document).on('click', '.popmake-register', function () {
        ///      $('#login').find('h2').show();

        title = $(this).attr('title');
        link = $(this).attr('link');
        // PUM.open(1005)
        //    PUM.open(10725)
        $('.title-proj-dynamic').text(title);

        $('.glob-pwp-pr-link').val(link);

        linkredirect = $('.glob-pwp-pr-link').val();

    });

    // $(document).on('click', '.home-page-login', function () {
    //   //  PUM.open(10725);
    //     PUM.open(1005)
    //
    //     $('#login').find('h2').hide();
    //
    //     $('.popmake-register').addClass("home-page-register");
    //
    // });

    $(document).on('click', '.popmake-login', function () {

        title = $('.title-proj-dynamic').text();

        link = $('.pwp-pr-link').val();

        linkredirect = $('.glob-pwp-pr-link').val();

        // PUM.open(1009)
        ///  PUM.open(10727)

        // if ($(this).hasClass("home-page-register")) {
        //     $('#register').find('h2').hide();
        // } else {
        //     $('#register').find('h2').show();
        // }

    });

    //$('title-proj-dynamic');
    $('#pop_login, #pop_signup').live('click', function (e) {
        formToFadeOut = $('form#register');
        formtoFadeIn = $('form#login');
        if ($(this).attr('id') == 'pop_signup') {
            formToFadeOut = $('form#login');
            formtoFadeIn = $('form#register');
        }
        formToFadeOut.fadeOut(500, function () {
            formtoFadeIn.fadeIn();
        });
        return false;
    });

    //END


    // Close popup
    $(document).on('click', '.login_overlay, .close', function () {
        $('form#login, form#register').fadeOut(500, function () {
            $('.login_overlay').remove();
        });
        return false;
    });

    // Show the login/signup popup on click
    $('#show_login, #show_signup').on('click', function (e) {
        $('body').prepend('<div class="login_overlay"></div>');
        if ($(this).attr('id') == 'show_login')
            $('form#login').fadeIn(500);
        else
            $('form#register').fadeIn(500);
        e.preventDefault();
    });

    // Perform AJAX login/register on form submit
    $('form#login, form#register, form#register-1,form#login-1').on('submit', function (e) {
        if (!$(this).valid()) return false;
        $('p.status', this).show().text(ajax_auth_object.loadingmessage);
        action = 'ajaxlogin';
        username = $('form#login #username').val();
        firstname = '';
        lastname = '';
        password = $('form#login #password').val();
        email = '';
        security = $('form#login #security').val();

        if ($(this).attr('id') == 'login-1') {
            username = $('form#login-1 #username').val();
            firstname = '';
            lastname = '';
            password = $('form#login-1 #password').val();
            email = '';
            security = $('form#login-1 #security-1').val();
        }

        if ($(this).attr('id') == 'register') {
            action = 'ajaxregister';
            username = $('#signonname').val();
            firstname = $('#firstname').val();
            lastname = $('#lastname').val();
            password = $('#signonpassword').val();
            email = $('#email').val();
            security = $('#signonsecurity').val();

        }
        if ($(this).attr('id') == 'register-1') {
            action = 'ajaxregister';
            username = $('#signonname-1').val();
            firstname = $('#firstname-1').val();
            lastname = $('#lastname-1').val();
            password = $('#signonpassword-1').val();
            email = $('#email-1').val();
            security = $('#signonsecurity-1').val();



        }
        ctrl = $(this);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_auth_object.ajaxurl,
            data: {
                'action': action,
                'username': username,
                'firstname': firstname,
                'lastname': lastname,
                'password': password,
                'email': email,
                'security': security
            },
            success: function (data) {
                console.log(data);
                $('p.status', ctrl).html(data.message);
                if (data.loggedin == true) {
                    linkredirect = $('.glob-pwp-pr-link').val();
                    document.location.href = linkredirect;
                    //  document.location.href = ajax_auth_object.redirecturl;
                }
            }
        });
        e.preventDefault();
    });

    // Client side form validation
    if (jQuery("#register").length)
        jQuery("#register").validate(
            {
                rules: {
                    password2: {
                        equalTo: '#signonpassword',
                        minlength: 6
                    }
                }
            }
        );
    else if (jQuery("#login").length)
        jQuery("#login").validate();
});