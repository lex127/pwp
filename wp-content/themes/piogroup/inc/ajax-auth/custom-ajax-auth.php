<?php
function ajax_auth_init() {
	wp_register_style( 'ajax-auth-style', get_template_directory_uri() . '/inc/ajax-auth/css/ajax-auth-style.css' );
	//wp_enqueue_style('ajax-auth-style');

	wp_register_script( 'validate-script', get_template_directory_uri() . '/inc/ajax-auth/js/jquery.validate.js', array( 'jquery' ) );
	wp_enqueue_script( 'validate-script' );

	wp_register_script( 'ajax-auth-script', get_template_directory_uri() . '/inc/ajax-auth/js/ajax-auth-script.js', array( 'jquery' ) );
	wp_enqueue_script( 'ajax-auth-script' );

	wp_localize_script( 'ajax-auth-script', 'ajax_auth_object', array(
		'ajaxurl'        => admin_url( 'admin-ajax.php' ),
		'redirecturl'    => get_the_permalink(),
		'loadingmessage' => __( 'Sending user info, please wait...' ),
	) );

	// Enable the user with no privileges to run ajax_login() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
	// Enable the user with no privileges to run ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxregister', 'ajax_register' );
}

// Execute the action only if the user isn't logged in
if ( ! is_user_logged_in() ) {
	add_action( 'init', 'ajax_auth_init' );
}

function ajax_login() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	// Call auth_user_login
	auth_user_login( $_POST['username'], $_POST['password'], 'Login' );

	die();
}

function set_name() {
	$f_name = get_transient( 'time_first_name' );

	return $f_name;

};

function ajax_register() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-register-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	$info                  = array();
	$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user( $_POST['email'] );
	$info['first_name']    = sanitize_text_field( $_POST['firstname'] );
	$info['last_name']     = sanitize_text_field( $_POST['lastname'] );
	$info['user_pass']     = sanitize_text_field( $_POST['password'] );
	$info['user_email']    = sanitize_email( $_POST['email'] );
	$_POST['pass1']        = $info['user_pass'];
	$_POST['pass2']        = $info['user_pass'];
	$f_name                = $info['first_name'];
	set_transient( 'time_first_name', $f_name, 60 * 60 );
	// Register the user
	add_filter( 'pre_user_first_name', 'set_name' );
	$user_register = register_new_user( $info['user_nicename'], $info['user_email'] );

	///$info['ID']    = $user_ID;
	///$user_register = wp_insert_user( $info );
//	do_action( 'tml_new_user_notification', $user_register );
	///var_dump(add_action( 'tml_new_user_notification', $user_register ));
//	tml_new_user_notification( $user_register,null, $notify = 'both' );

	//Theme_My_Login_Custom_Email::new_user_notification($user_register);

	$respons = array( 'loggedin' => false, 'message' => false );
	if ( ! empty( $info['first_name'] ) ) {
//		update_user_meta( $user_register, 'first_name', $info['first_name'] );
	}
	if ( ! empty( $info['last_name'] ) ) {
		update_user_meta( $user_register, 'last_name', $info['last_name'] );
	}
	if ( is_wp_error( $user_register ) ) {
		$error   = $user_register;
		$respons = array( 'message' => $error->get_error_message() );
		wp_send_json( $respons );

	} else {
		auth_user_login( $info['nickname'], $info['user_pass'], 'Registration' );
	}
	die();
}

function auth_user_login( $user_login, $password, $login ) {
	$info                  = array();
	$info['user_login']    = $user_login;
	$info['user_password'] = $password;
	$info['remember']      = true;

	$user_signon = wp_signon( $info, false );
	if ( is_wp_error( $user_signon ) ) {
		echo json_encode( array( 'loggedin' => false, 'message' => __( 'Wrong username or password.' ) ) );
	} else {
		wp_set_current_user( $user_signon->ID );
		echo json_encode( array( 'loggedin' => true, 'message' => __( $login . ' successful, redirecting...' ) ) );
	}

	die();
}