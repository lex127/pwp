<?php

add_action( 'wp', 'custom_css_in_pages' );
function custom_css_in_pages() {

	if ( is_page( 'why-projects-with-purpose' ) ) {
		$base = get_stylesheet_directory_uri();
		wp_enqueue_style( 'piogroup', $base . '/css/piogroup.css' );
	}

	if ( is_page( 'about-us' ) ) {
		$base = get_stylesheet_directory_uri();
		wp_enqueue_style( 'piogroup', $base . '/css/piogroup.css' );
	}
	if ( is_front_page() ) {
		$base = get_stylesheet_directory_uri();
		wp_enqueue_style( 'piogroup', $base . '/css/piogroup.css' );
	}
}
