<?php

add_shortcode( 'pwp_login', 'pwp_login_func' );

function pwp_login_func( $atts, $content ) { ?>
    <div class="card card--form">
    <form id="login-1" class="ajax-auth" action="login" method="post">
        <!--        <h3>New to site? <a id="pop_signup" href="">Create an Account</a></h3>-->
        <!--        <hr />-->
        <h2>To Donate to <span class="title-proj-dynamic"></span></br>
        Please login</h2>
        <p class="status"></p>
		<?php wp_nonce_field( 'ajax-login-nonce', 'security-1' ); ?>
        <label for="username"></label>
            <input placeholder="Email" id="username" type="text" class="required input--wd input--wd--full  required-entry" name="username">
      <label for="password"></label>
            <input placeholder="Password" id="password" type="password" class="required input--wd input--wd--full" name="password">
        <!--        <a class="text-link" href="--><?php
		//		echo wp_lostpassword_url(); ?><!--">Lost password?</a>-->
        <div class="divider divider--xs"></div>
        <input class="submit_button btn btn--wd" type="submit" value="Login">
<!--        <a class="popmake-register btn btn--wd" href="#" type="submit">Register</a>-->
        <input type="hidden" value="" class="pwp-pr-link">
        <div class="card--form__footer">
            <a href="<?php echo  get_bloginfo('url').'/lostpassword/' ; ?>">Forgot Your Password?</a>
            NEW HERE?
            <a class="popmake-register" href="#">REGISTRATION</a>
            IS FREE AND EASY!
        </div>
    </form>
    </div>
	<?php


}
add_shortcode( 'pwp_register', 'pwp_register_func' );

function pwp_register_func( $atts, $content ) { ?>
    <div class="card card--form">

        <form id="register-1" class="ajax-auth" action="register" method="post">
            <h2>To Donate to <span class="title-proj-dynamic"></span></br>
                Please enter your details below</h2>
            <p class="status"></p>
			<?php wp_nonce_field( 'ajax-register-nonce', 'signonsecurity-1' ); ?>
<!--            <label for="signonname"></label>-->
<!--            <input placeholder="Username" id="signonname-1" type="text" name="signonname" class="required input--wd input--wd--full">-->
            <label for="firstname"></label>
            <input placeholder="First Name" id="firstname-1" type="text" name="firstname" class="input--wd input--wd--full">
            <label for="lastname"></label>
            <input placeholder="Last Name" id="lastname-1" type="text" name="lastname" class="input--wd input--wd--full">
            <label for="email"></label>
            <input placeholder="Email" id="email-1" type="text" class="required email input--wd input--wd--full" name="email" >
            <label for="signonpassword"></label>
            <input placeholder="Password" name="pass1" id="signonpassword-1" type="password" class="required input--wd input--wd--full" name="signonpassword">

            <label for="password2"></label>
            <input placeholder="Confirm Password"  name="pass2" pattern=".{6,}" type="password" id="password2-1" class="required input--wd input--wd--full" name="password2">
            <div class="divider divider--xs"></div>

            <input class="submit_button btn btn--wd" type="submit" value="Sign up">
            <!--    Confirm Password-->
            <input type="hidden" value="" class="pwp-pr-link">

            <div class="card--form__footer">Have an account?
                <a class="popmake-login " href="#" type="submit">Please Login</a>
            </div>

        </form>
    </div>
	<?php
}

///added popup in home page

add_shortcode( 'pwp_login-global', 'pwp_login_func_global' );

function pwp_login_func_global( $atts, $content ) { ?>
    <div class="card card--form">
        <form id="login" class="ajax-auth" action="login" method="post">
            <!--        <h3>New to site? <a id="pop_signup" href="">Create an Account</a></h3>-->
            <!--        <hr />-->

            <p class="status"></p>
			<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
            <label for="username"></label>
            <input placeholder="Email" id="username" type="text" class="required input--wd input--wd--full  required-entry" name="username">
            <label for="password"></label>
            <input placeholder="Password" id="password" type="password" pattern=".{6,}" class="required input--wd input--wd--full" name="password">
            <!--        <a class="text-link" href="--><?php
			//		echo wp_lostpassword_url(); ?><!--">Lost password?</a>-->
            <div class="divider divider--xs"></div>
            <input class="submit_button btn btn--wd" type="submit" value="Login">
            <!--        <a class="popmake-register btn btn--wd" href="#" type="submit">Register</a>-->
            <input type="hidden" value="" class="pwp-pr-link">
            <div class="card--form__footer">
                <a href="<?php echo  get_bloginfo('url').'/lostpassword/' ; ?>">Forgot Your Password?</a>
                NEW HERE?
                <a class="popmake-register-global" href="#">REGISTRATION</a>
                IS FREE AND EASY!
            </div>
        </form>
    </div>
	<?php


}

add_shortcode( 'pwp_register-global', 'pwp_register_func_global' );

function pwp_register_func_global( $atts, $content ) { ?>
    <div class="card card--form">

        <form id="register" class="ajax-auth" action="register" method="post">

            <p class="status"></p>
			<?php wp_nonce_field( 'ajax-register-nonce', 'signonsecurity' ); ?>
<!--            <label for="signonname"></label>-->
<!--            <input placeholder="Username" id="signonname" type="text" name="signonname" class="required input--wd input--wd--full">-->
            <label for="firstname"></label>
            <input placeholder="First Name" id="firstname" type="text" name="firstname" class="input--wd input--wd--full">
            <label for="lastname"></label>
            <input placeholder="Last Name" id="lastname" type="text" name="lastname" class="input--wd input--wd--full">
            <label for="email"></label>
            <input placeholder="Email" id="email" type="text" class="required email input--wd input--wd--full" name="email" >
            <label for="signonpassword"></label>
            <input placeholder="Password" id="signonpassword" type="password" class="required input--wd input--wd--full" name="signonpassword">

            <label for="password2"></label>
            <input placeholder="Confirm Password" type="password" id="password2" class="required input--wd input--wd--full" name="password2">
            <div class="divider divider--xs"></div>

            <input class="submit_button btn btn--wd" type="submit" value="Sign up">
            <!--    Confirm Password-->
            <input type="hidden" value="" class="pwp-pr-link">

            <div class="card--form__footer">Have an account?
                <a class="popmake-login-global" href="#" type="submit">Please Login</a>
            </div>

        </form>
    </div>
	<?php
}

