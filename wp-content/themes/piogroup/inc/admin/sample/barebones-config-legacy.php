<?php

    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux_Framework_welldone_settings' ) ) {

        class Redux_Framework_welldone_settings {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }

            }

            public function initSettings() {

                // Set the default arguments
                $this->setArguments();

                // Set a few help tabs so you can see how it's done
                $this->setHelpTabs();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            function compiler_action ( $options, $css, $changed_values ) {
            }

            function dynamic_section ( $sections ) {
                return $sections;
            }

            function change_arguments ( $args ) {
                return $args;
            }

            function change_defaults ( $defaults ) {
                return $defaults;
            }



            function remove_demo () {
            }

            public function setSections() {

                $wp_registered_sidebars = wp_get_sidebars_widgets ();
                  $welldone_sidebar=array ();  
                        foreach ( $wp_registered_sidebars as $sidebar => $sidebar_info ) {
                            if ( $sidebar == 'wp_inactive_widgets' ) continue;
                            $welldone_sidebar[ $sidebar ] = ucwords ( str_replace ( array ( '_', '-' ), ' ', $sidebar ) );
                        }
                // ACTUAL DECLARATION OF SECTIONS
                $this->sections[] = array(
                    'title'  => __( 'Home Settings', 'redux-framework-demo' ),
                    'desc'   => __( 'These fields can be fully translated by WPML (WordPress Multi-Language). This serves as an example for you to implement. For extra details look at our <a href="http://docs.reduxframework.com/core/advanced/wpml-integration/" target="_blank">WPML Implementation</a> documentation.', 'redux-framework-demo' ),
                    'icon'   => 'el el-home',
                    // 'submenu' => false, // Setting submenu to false on a given section will hide it from the WordPress sidebar menu!
                    'fields' => array(

                        array(
                            'id'       => 'textid',
                            'type'     => 'text',
                            'title'    => __( 'Example Text87', 'redux-framework-demo' ),
                            'compiler' => 'true',
                            // Can be set to false to allow any media type, or can also be set to any mime type.
                            'desc'     => __( 'Example description.', 'redux-framework-demo' ),
                            'subtitle' => __( 'Example subtitle.', 'redux-framework-demo' ),
                            'hint'     => array(
                                //'title'     => '',
                                'content' => 'This is a <b>hint</b> tool-tip for the webFonts field.<br/><br/>Add any HTML based text you like here.',
                            )
                        ),
                    )
                );

                 // Skin tahir
    $this->sections[] = array (
        'icon' => 'el-icon-broom',
        'icon_class' => 'icon',
        'title' => __ ( 'Skin Options', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'welldone_site_color',
                'type' => 'color',
                'title' => __ ( 'Main Theme Color', 'welldone' ),
                'default' => '#536dfe',
                'validate' => 'color',
            ),
            array (
                'id' => 'welldone_site_alternate_color',
                'type' => 'color',
                'title' => __ ( 'Alternate Theme Color', 'welldone' ),
                'default' => '#000000',
                'validate' => 'color',
            ),
            array (
                'id' => 'welldone_body_typography',
                'type' => 'typography',
                'title' => __ ( 'Body Fonts', 'welldone' ),
                'google' => true,
               // 'font-size' => false,
                'subsets' => false,
               // 'line-height' => false,
                'font-backup' => true,
                'text-align' => false,
                'default' => array (
                    'color' => '#212121',
                )
            ),
            array (
                'id' => 'welldone_compile_css',
                'type' => 'switch',
                'title' => __ ( 'Compile Css', 'welldone' ),
                'compiler' => true,
                'desc'=> __('Switch to "Yes" to generate a css file for skin options.','welldone'),
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array(
                'id'       => 'css_editor',
                'type'     => 'ace_editor',
                'title'    => __('Custom CSS', 'welldone'),
                'subtitle' => __('Add Your Custom Css code here.', 'welldone'),
                'mode'     => 'css',
                'theme'    => 'monokai',
            )
        ) );
    //body page tahir done
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Body, Page', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Body', 'welldone' ),
            ),
            array (
                'id' => 'welldone_container_size',
                'type' => 'button_set',
                'title' => __('Use Full Width Body Size','welldone'),
                'options' => array (
                    'yes' => __ ( 'Yes', 'welldone' ),
                    'no' => __ ( "No", 'welldone' ),
                ),
                'default' => 'no'
            ),
            array (
                'id' => 'welldone_theme_wrapper',
                'type' => 'button_set',
                'title' => __ ( 'Theme Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'wide' => __ ( 'Wide', 'welldone' ),
                    'boxed' => __ ( "Boxed", 'welldone' ),
                    'boxed-long' => __ ( "Boxed From Sides", 'welldone' ),
                ),
               'default' => 'wide'
            ),
            array (
                'id' => 'welldone_bg_mode',
                'type' => 'button_set',
                'title' => __ ( 'Background Mode', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'image' => __ ( "Image", 'welldone' ),
                    'custom-image' => __ ( "Custom Image", 'welldone' ),
                ),
                'default' => 'image',
                'required' => array ( "welldone_theme_wrapper", "!=", "wide" ),
            ),
            array (
                'id' => 'welldone_bg_select',
                'type' => 'image_select',
                'title' => __ ( 'Select Image', 'welldone' ),
                'compiler' => true,
                'tiles' => true,
                'options' => array (
                    get_template_directory_uri () . '/images/bg-images/bg.jpg',
                    get_template_directory_uri () . '/images/bg-images/bg1.jpg',
                    get_template_directory_uri () . '/images/bg-images/bg2.jpg'
                ),
                'default' => get_template_directory_uri () . '/images/bg-images/bg.jpg',
                'required' => array ( "welldone_bg_mode", "=", "image" ),
            ),
            array (
                'id' => 'welldone_bg_custom_select',
                'type' => 'background',
                'title' => __ ( 'Select Background', 'welldone' ),
                'background-position' => false,
                'transparent' => false,
                'preview_media' => true,
                'background-color' => false,
                'background-size' => false,
                'background-attachment' => false,
                'background-repeat' => false,
                'compiler' => true,
               'required' => array ( "welldone_bg_mode", "=", "custom-image" ),
            ),
            array (
                'id' => 'welldone_bg_color',
                'type' => 'color',
                'title' => __ ( 'Background Color', 'welldone' ),
                'compiler' => true,
                'required' => array ( "welldone_theme_wrapper", "!=", "wide" ),
            ),
            array (
                'id' => 'welldone_bg_repeat',
                'type' => 'select',
                'title' => __ ( 'Background Repeat', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no-repeat' => __ ( "No Repeat", 'welldone' ),
                    'repeat' => __ ( "Repeat All", 'welldone' ),
                    'repeat-x' => __ ( "Repeat Horizontally", 'welldone' ),
                    'repeat-y' => __ ( "Repeat Vertically", 'welldone' ),
                    'inherit' => __ ( "Inherit", 'welldone' ),
                ),
                'placeholder' => __ ( 'Background Repeat', 'welldone' ),
                'required' => array ( "welldone_theme_wrapper", "!=", "wide" ),
            ),
            array (
                'id' => 'welldone_bg_position',
                'type' => 'select',
                'title' => __ ( 'Background Position', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    "left top" => __ ( "Left Top", 'welldone' ),
                    "left center" => __ ( "Left center", 'welldone' ),
                    "left bottom" => __ ( "Left Bottom", 'welldone' ),
                    "center top" => __ ( "Center Top", 'welldone' ),
                    "center center" => __ ( "Center Center", 'welldone' ),
                    "center bottom" => __ ( "Center Bottom", 'welldone' ),
                    "right top" => __ ( "Right Top", 'welldone' ),
                    "right center" => __ ( "Right center", 'welldone' ),
                    "right bottom" => __ ( "Right Bottom", 'welldone' ),
                ),
                'select2' => array ( 'allowClear' => false ),
                'placeholder' => __ ( 'Background Position', 'welldone' ),
                'required' => array ( "welldone_theme_wrapper", "!=", "wide" ),
            ),
            array (
                'id' => 'welldone_bg_size',
                'type' => 'select',
                'title' => __ ( 'Background Size', 'welldone' ),
                'compiler' => true,
                'options' => array (
                     "cover" => __ ( "Cover", 'welldone' ),
                    "inherit" => __ ( "Inherit", 'welldone' ),
                    "contain" => __ ( "Contain", 'welldone' ),
                ),
                'placeholder' => __ ( 'Background Size', 'welldone' ),
                'required' => array ( "welldone_theme_wrapper", "!=", "wide" ),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Page', 'welldone' ),
            ),
            array (
                'id' => 'welldone_content_background',
                'type' => 'background',
                'title' => __ ( 'Page Background', 'welldone' ),
                'preview_media' => true,
                'default' => array (
                    'background-color' => '#FFFFFF',
                )
            )
        ));
    //header sajad
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Header', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'welldone_customize_header',
               'type' => 'switch',
                'title' => __('Customize Headers','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'desc' => __("Switch to 'Yes' if you want to customize header",'welldone')
            ),
            array (
                'id' => '2',
                'type' => 'info',
                'desc' => __ ( 'Headers  1 , 2 , 3 , 4 , 5 setting', 'welldone' ),
               // 'required' => array ( 'welldone_customize_header', '=', '1' )
            ),
            array (
                'id' => 'header_background_light',
                'type' => 'background',
                'title' => __ ( 'Select Background', 'welldone' ),
                'compiler' => true,
                'preview_media' => true,
            ),
            array (
                'id' => 'header_text_light',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#747474',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_hover_light',
                'type' => 'color',
                'title' => __ ( 'Hover Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_box_light',
                'type' => 'color',
                'title' => __ ( 'Box Color ', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
            array (
               'id' => '3',
                'type' => 'info',
               'desc' => __ ( 'Headers 6 & 7 setting', 'welldone' ),
            ),
            array (
                'id' => 'header_background_dark',
                'type' => 'background',
                'title' => __ ( 'Select Top Background', 'welldone' ),
                'compiler' => true,
               'preview_media' => true,
            ),
            array (
                'id' => 'header_background_dark_bottom',
                'type' => 'background',
                'preview_media' => true,
                'title' => __ ( 'Select Bottom Background', 'welldone' ),
                //'default' => '#fff',
                'compiler' => true,
           ),
            array (
                'id' => 'header_text_dark',
               'type' => 'color',
               'title' => __ ( 'Text Color', 'welldone' ),
               'default' => '#fff',
               'validate' => 'color',
            ),
            array (
               'id' => 'header_text_dark_hover',
               'type' => 'color',
               'title' => __ ( 'Hover Color', 'welldone' ),
               'default' => '#ca1515',
               'validate' => 'color',
           ),
            array (
                'id' => 'header_cart_bg_dark',
                'type' => 'color',
                'title' => __ ( 'Cart Background Color', 'welldone' ),
                'default' => '#000',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_cart_bg_dark_hover',
                'type' => 'color',
                'title' => __ ( 'Cart Background Color Hover', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_cart_text_dark',
                'type' => 'color',
                'title' => __ ( 'Cart Text Color', 'welldone' ),
                'default' => '#8c8c8c',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_cart_text_dark_hover',
                'type' => 'color',
                'title' => __ ( 'Cart Text Color Hover', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
            array (
                'id' => '6',
                'type' => 'info',
                'desc' => __ ( 'Header 11 Settings', 'welldone' ),
            ),
            array (
                'id' => 'header_background_red',
                'type' => 'background',
                'title' => __ ( 'Select Background', 'welldone' ),
                //'default' => '#d62020',
                'preview_media' => true,
                'compiler' => true,
            ),

           array (
                'id' => 'header_text_red',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#f68e8e',
                'validate' => 'color',
            ),
           array (
                'id' => 'header_prize_red',
                'type' => 'color',
                'title' => __ ( 'Prize Color', 'welldone' ),
                'default' => '#fff',
                'validate' => 'color',
           ),
          array (
                'id' => 'header_box_red',
                'type' => 'color',
                'title' => __ ( 'Box Color', 'welldone' ),
                //'default' => '##ca1515',
                'validate' => 'color',
          ),
          array (
                'id' => 'header_hover_red',
                'type' => 'color',
                'title' => __ ( 'Hover Color', 'welldone' ),
                'default' => '#fff',
                'validate' => 'color',
          ),
          array (
                'id' => '4',
               'type' => 'info',
                'desc' => __ ( 'Header 8 & 16 Settings', 'welldone' ),
          ),

            array (
                'id' => 'header_background_custom',
                'type' => 'background',
                'title' => __ ( 'Select Top Background', 'welldone' ),
                // 'default' => '#b30f0f',
                'compiler' => true,
                'preview_media' => true,
            ),

            array (
                'id' => 'header_background_custom_bottom',
                'type' => 'background',
                'title' => __ ( 'Select Bottom Background', 'welldone' ),
//                        'default' => '#fff',
                'preview_media' => true,
                'compiler' => true,
            ),

            array (
                'id' => 'header_text_custom',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#fff',
                'validate' => 'color',
            ),

            array (
                'id' => 'header_text_custom_hover',
                'type' => 'color',
                'title' => __ ( 'Hover Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),

            array (
                'id' => 'header_box_custom',
                'type' => 'color',
                'title' => __ ( 'Box Color ', 'welldone' ),
                'default' => '#f5f5f5',
                'validate' => 'color',
            ),

            array (
                'id' => 'header_cart_bg_custom',
                'type' => 'color',
                'title' => __ ( 'Cart Background Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),

            array (
                'id' => 'header_cart_bg_custom_hover',
                'type' => 'color',
                'title' => __ ( 'Cart Background Color Hover', 'welldone' ),
                'default' => '#262626',
                'validate' => 'color',
            ),

            array (
                'id' => 'header_cart_text_custom',
                'type' => 'color',
                'title' => __ ( 'Cart Text Color', 'welldone' ),
                'default' => '#f68e8e',
                'validate' => 'color',
            ),

            array (
                'id' => 'header_cart_text_custom_hover',
                'type' => 'color',
                'title' => __ ( 'Cart Text Color Hover', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            )
        ));
    //menu sajad
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Main Menu', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'welldone_customize_menu',
                'type' => 'switch',
                'title' => __('Customize Menu','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'desc' => __("Switch to 'Yes' if you want to customize menu",'welldone')
             ),
            array (
                'id' => 'menu_customize_label_1',
                'type' => 'info',
                'desc' => __ ( 'Header 1 and Header 2 Menu Setting', 'welldone' ),
                //'required' => array ( 'welldone_customize_menus', '=', '1' )
            ),
            array (
                'id' => 'header_background_one_two',
                'type' => 'color',
                'title' => __ ( 'Background Color', 'welldone' ),
                'default' => '#323232',
                'validate' => 'color',
                //'required' => array ( 'welldone_customize_menus', '=', '1' )
            ),
           array (
                'id' => 'header_font_one_two',
                'type' => 'color',
                'title' => __ ( 'Font Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',

            ),
            array (
                'id' => 'hover_font_color_h_1_2',
                'type' => 'color',
                'title' => __ ( 'Hover Font Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_background_chil_one_two',
                'type' => 'color',
                'title' => __ ( 'Background Submenu Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_font_chil_one_two',
                'type' => 'color',
                'title' => __ ( 'Font  Submenu Color', 'welldone' ),
                'default' => '#323232',
                'validate' => 'color',
            ),
         array (
               'id' => 'hover_font_submenu_color_h_1_2',
                'type' => 'color',
                'title' => __ ( 'Hover Font Submenu Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
                'default' => '',
            ),
            array (
                'id' => 'menu_customize_label_4',
                'type' => 'info',
                'desc' => __ ( 'Header 3 Menu Settings', 'welldone' ),
            ),
            array (
                'id' => 'header_background_three',
                'type' => 'color',
                'title' => __ ( 'Background Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_font_three',
                'type' => 'color',
                'title' => __ ( 'Font Color', 'welldone' ),
                'default' => '#3d3d3d',
                'validate' => 'color',
            ),
        array (
              'id' => 'hover_font_color_h_3',
              'type' => 'color',
              'title' => __ ( 'Hover Font Color', 'welldone' ),
              'default' => '#3d3d3d',
              'validate' => 'color',
          ),
        array (
               'id' => 'header_background_chil_three',
               'type' => 'color',
               'title' => __ ( 'Background Submenu Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
           ),
        array (
                'id' => 'header_font_chil_three',
                'type' => 'color',
                'title' => __ ( 'Font  Submenu Color', 'welldone' ),
                'default' => '#323232',
                'validate' => 'color',
            ),
            array (
                'id' => 'hover_font_submenu_color_h_3',
                'type' => 'color',
                'title' => __ ( 'Hover Font Submenu Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
            array (
                'id' => 'menu_customize_label_5',
                'type' => 'info',
                'desc' => __ ( 'Header 4 Menu Settings', 'welldone' ),
            ),
            array (
                'id' => 'header_background_four',
                'type' => 'color',
                'title' => __ ( 'Background Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_font_four',
                'type' => 'color',
                'title' => __ ( 'Font Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
                ),
            array (
                'id' => 'hover_font_color_h_4',
                'type' => 'color',
                'title' => __ ( 'Hover Font Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
           ),
            array (
                'id' => 'header_background_chil_four',
                'type' => 'color',
                'title' => __ ( 'Background Submenu Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),

           array (

               'id' => 'header_font_chil_four',
               'type' => 'color',
               'title' => __ ( 'Font  Submenu Color', 'welldone' ),
               'default' => '#323232',
               'validate' => 'color',
            ),

            array (
                'id' => 'hover_font_submenu_color_h_4',
                'type' => 'color',
                'title' => __ ( 'Hover Font Submenu Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            )
        ));

    //mobile menu tahir done
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Mobile Panel', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'mobile_menu_enable_size',
                'type' => 'slider',
                'title' => __ ( 'Mobile Menu Activation Size', 'welldone' ),
                'min' => 320,
                'max' => 1400,
                'default' => 992,
                'display_value' => 'text',
                'desc' => __ ( 'Screen Width on which you want to show mobile menu', 'welldone' )
            ),

            array (
                'id' => 'welldone_customize_mobile_menu',
                'type' => 'switch',
                'title' => __ ( 'Customize Mobile Menu', 'welldone' ),
                'compiler' => true,
                'default' => '0',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'desc' => __("Switch to 'Yes' if you want to customize Mobile menu",'welldone')
            ),

            array (
                'id' => 'Mobile_Back_menu_color',
                'type' => 'color',
                'title' => __ ( 'Background  Color', 'welldone' ),
                'default' => '#3d3d3d',
                'validate' => 'color',
                'required' => array ( "welldone_customize_mobile_menu", "=", '1' )
            ),

            array (
                'id' => 'mobile_text_menu_color',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
                'required' => array ( "welldone_customize_mobile_menu", "=", '1' )
            ),

            array (
                'id' => 'mobile_link_menu_color',
                'type' => 'color',
                'title' => __ ( 'Link Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
                'required' => array ( "welldone_customize_mobile_menu", "=", '1' )
            ),
        ));

    ////breadcrumb and title done
    $this->sections[] = array (
        'icon' => 'el-icon-minus',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Breadcrumb And title', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'welldone_bread_title_bg',
                'type' => 'button_set',
                'title' => __('Breadcrumb And Title Background','welldone'),
                'compiler' => true,
                'options' => array ( 'bg-img' => 'Background Image', 'bg-color' => 'Background Color' ),
                'default' => 'bg-img',
            ),

            array (
                'id' => 'welldone_bread_title_image',
                'type' => 'media',
                'title' => __('Select Background Image','welldone'),
                'compiler' => true,
                'default' => array (
                    'url' => get_template_directory_uri () . '/images/default/header-lightbg.jpg',
                ),
                'required' => array ( 'welldone_bread_title_bg', '=', 'bg-img' ),
            ),

            array (
                'id' => 'welldone_use_parallax',
                'type' => 'switch',
                'title' => __('Use Parallax','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'required' => array ( 'welldone_bread_title_bg', '=', 'bg-img' ),
            ),
            array (
                'id' => 'welldone_bread_title_bg_color',
                'type' => 'color',
                'title' => __('Background Color','welldone'),
                'compiler' => true,
                'default' => '#3483c0',
                'required' => array ( 'welldone_bread_title_bg', '=', 'bg-color' ),
            ),
            array (
                'id' => 'welldone_bread_title_border_color',
                'type' => 'color',
                'title' => __('Border Color','welldone'),
                'compiler' => true,
                'default' => '#4f94c8',
            ),
            array (
                'id' => 'welldone_bread_title_color',
                'type' => 'color',
                'title' => __('Text Color','welldone'),
                'compiler' => true,
            ),
        ));

    ///footer asif done
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Footer', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'welldone_enable_footer_customization',
                'type' => 'switch',
                'title' => __ ( 'Customize Footer', 'welldone' ),
                'default' => '0',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'desc' => __ ( "Switch to 'Yes' if you want to customize footer area", 'welldone' )
            ),
             array (
                'id' => 'welldone_footer_typography',
                'type' => 'typography',
                'title' => __ ( 'Footer Fonts', 'welldone' ),
                'google' => true,
                'font-size' => false,
                'subsets' => false,
                'line-height' => false,
                'font-backup' => false,
                'text-align' => false,
                'default' => array (
                    'color' => '#808080',
                )
            ),
            array (
                'id' => 'info_warning_ss',
                'type' => 'info',
                'desc' => __ ( 'Customizations for footers 1,6,7,8,9,10,11 ', 'welldone' ),
                //'required' => array ( 'welldone_enable_footer_customization', '=', '1' )
            ),
            array (
                'id' => 'welldone_bg_custom_footer_light',
                'type' => 'background',
                'title' => __ ( 'Select Background', 'welldone' ),
                'compiler' => true,
                'preview_media' => true,
            ),
           array (
                'id' => 'footer_heading_color',
                'type' => 'color',
                'title' => __ ( 'Heading Color', 'welldone' ),
                'default' => '#343434',
                'validate' => 'color',
            ),
           array(
                'id' => 'footer_text_color',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
           ),
           array (
                'id' => 'footer_link_color',
                //'type' => 'link_color',
                'type' => 'color',
                'title' => __ ( 'Link  Color', 'welldone' ),
                'default' => '#989898',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_link_hover_color',
                // 'type' => 'link_color',
                'type' => 'color',
                'title' => __ ( 'Link  Hover Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
           array (
                'id' => 'footer_bottom_link_color',
                'type' => 'color',
                'title' => __ ( 'Bottom Link Color', 'welldone' ),
                'default' => '#464646',
                'validate' => 'color',
           ),
            array (
                'id' => 'footer_bottom_link_hover_color',
                'type' => 'color',
                'title' => __ ( 'Bottom Link Hover  Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_text_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Text Color', 'welldone' ),
                'default' => '#7f7f7f',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_background_color_1_3',
                'type' => 'color',
                'title' => __ ( 'Copyright Background Color', 'welldone' ),
                'default' => '#7f7f7f',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Color', 'welldone' ),
                'default' => '#ca1515',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_hover_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Hover Color', 'welldone' ),
                'default' => '#989898',
                'validate' => 'color',
           ),
            array (
                 'id' => 'info_warning_dark',
                 'type' => 'info',
                 'desc' => __ ( 'Customizations for footers 2,4,5 ', 'welldone' ),
                 ),
            array (
                   'id' => 'welldone_bg_custom_footer_dark',
                  'type' => 'background',
                  'title' => __ ( 'Select Background', 'welldone' ),
                   'compiler' => true,
                   'preview_media' => true,
            ),
            array (
                'id' => 'footer_heading_dark_color',
                'type' => 'color',
                'title' => __ ( 'Heading Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
           ),
            array (
                'id' => 'footer_text_dark_color',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_link_dark_color',
                'type' => 'link_color',
                'type' => 'color',
                'title' => __ ( 'Link  Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_link_hover_dark_color',
                'type' => 'link_color',
                'type' => 'color',
               'title' => __ ( 'Link  Hover Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_bottom_link_dark_color',
                'type' => 'color',
                'title' => __ ( 'Bottom Link Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_bottom_link_hover_dark__color',
                'type' => 'color',
                'title' => __ ( 'Bottom Link Hover  Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_text_dark_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Text Color', 'welldone' ),
                'default' => '#7f7f7f',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_background_dark_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Background Color  for two', 'welldone' ),
                'default' => '#7f7f7f',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_dark_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Color 1', 'welldone' ),
                'default' => '#b30f0f',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_hover_dark_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Hover Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            array (
                'id' => 'info_warning_dark_3',
                'type' => 'info',
                'desc' => __ ( 'Customizations for footers 3 ', 'welldone' ),
            ),
            array (
                'id' => 'welldone_bg_custom_footer_three',
                'type' => 'background',
                'title' => __ ( 'Select Background', 'welldone' ),
                'compiler' => true,
                'preview_media' => true,
            ),
            array (
                'id' => 'footer_bottom_link_three_color',
                'type' => 'color',
                'title' => __ ( 'Bottom Link Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_bottom_link_hover_three__color',
                'type' => 'color',
                'title' => __ ( 'Bottom Link Hover  Color', 'welldone' ),
                'default' => '#b30f0f',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_text_three_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Text Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_background_three_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Background Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_three_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Color 2', 'welldone' ),
                'default' => '#b30f0f',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_hover_three_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Hover Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
            ),
            //foooter top
           array (
               'id' => 'info_warning_dark_3s',
               'type' => 'info',
               'desc' => __ ( 'Customizations for footers top ', 'welldone' ),
           ),
           array (
                'id' => 'welldone_bg_custom_footer_top',
               'type' => 'background',
               'title' => __ ( 'Select Background', 'welldone' ),
               'compiler' => true,
               'preview_media' => true,
           ),
           array (
                'id' => 'footer_top_title_color',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
           ),
           array (
                'id' => 'footer_top_link_color',
                'type' => 'color',
                'title' => __ ( 'Link Color', 'welldone' ),
                'default' => '#b30f0f',
                'validate' => 'color',
           ),
           array (
                'id' => 'footer_top_hover_color',
                'type' => 'color',
                'title' => __ ( 'Hover Color', 'welldone' ),
                'default' => '#a6a6a6',
                'validate' => 'color',
           ),
        ));

   $this->sections[] = array (
        'title' => __ ( 'General', 'welldone' ),
        'icon' => 'el-icon-cogs',
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Logo, Icon', 'welldone' ),
            ),
            array (
                'id' => 'logo',
                'type' => 'media',
                'url' => true,
                'title' => __ ( 'Logo', 'welldone' ),
                'compiler' => true,
                'default' => array (
                'url' => get_template_directory_uri () . '/images/logo.png',
                 )
            ),
            array (
                'id' => 'logo-mobile',
                'type' => 'media',
                'url' => true,
                'title' => __ ( 'Logo Mobile', 'welldone' ),
                'compiler' => true,
                'default' => array (
                'url' => get_template_directory_uri () . '/images/logo-mobile.png',
                 )
            ),
             array (
                'id' => 'logo-transparent',
                'type' => 'media',
                'url' => true,
                'title' => __ ( 'Logo Transparent', 'welldone' ),
                'compiler' => true,
                'default' => array (
                'url' => get_template_directory_uri () . '/images/logo-transparent.png',
                 )
            ),
           array (
                'id' => 'favicon',
                'type' => 'media',
                'title' => __ ( 'Favicon', 'welldone' ),
                'compiler' => true,
                'default' => array (
              'url' => get_template_directory_uri () . '/images/favicon.ico',
              )
            ),
           // array (
           //      'id' => 'icon-iphone',
           //      'type' => 'media',
           //      'url' => true,
           //      'title' => __ ( 'Apple iPhone Icon', 'welldone' ),
           //      'compiler' => true,
           //      'desc' => __('Icon for Apple iPhone (57px X 57px)','welldone'),
           //      'default' => array (
           //      'url' => get_template_directory_uri () . '/images/icons/faviconx57.png',
           //      )
           // ),
           // array (
           //      'id' => 'icon-iphone-retina',
           //      'type' => 'media',
           //      'url' => true,
           //      'title' => __ ( 'Apple iPhone Retina Icon', 'welldone' ),
           //      'compiler' => true,
           //      'desc' => __('Icon for Apple iPhone Retina (114px X 114px)','welldone'),
           //      'default' => array (
           //      'url' => get_template_directory_uri () . '/images/icons/faviconx57@2x.png',
           //     )
           //  ),
           // array (
           //      'id' => 'icon-ipad',
           //      'type' => 'media',
           //      'url' => true,
           //      'title' => __ ( 'Apple iPad Icon', 'welldone' ),
           //      'compiler' => true,
           //      'desc' => __('Icon for Apple iPad (72px X 72px)','welldone'),
           //      'default' => array (
           //      'url' => get_template_directory_uri () . '/images/icons/faviconx72.png',
           //      )
           //  ),
           // array (
           //      'id' => 'icon-ipad-retina',
           //      'type' => 'media',
           //      'url' => true,
           //      'title' => __ ( 'Apple iPad Retina Icon', 'welldone' ),
           //      'compiler' => true,
           //      'desc' => __('Icon for Apple iPad Retina (144px X 144px)','welldone'),
           //      'default' => array (
           //      'url' => get_template_directory_uri () . '/images/icons/faviconx72@2x.png',
           //      )
           //  ),
           // array (
           //      'id' => 'info_warning',
           //      'type' => 'info',
           //      'desc' => __ ( 'Javascript Code', 'welldone' ),
           //  ),
           // array (
           //      'id' => 'welldone_jscode',
           //      'type' => 'ace_editor',
           //      'title' => __ ( 'JS Code', 'welldone' ),
           //      'compiler' => true,
           //      'subtitle' => __ ( 'Paste your JS code here.', 'welldone' ),
           //      'mode' => 'javascript',
           //      'theme' => 'monokai',
           //      'default' => "(function ($) {\n\n})(jQuery);"
           // )        //"jQuery(document).ready(function(){\n\n});" )
        ));
 //Header Settings
    $this->sections[] = array (
        'title' => __ ( 'Header Options', 'welldone' ),
        'id'    => 'header-settings',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon' => 'el-icon-website'
    );
    // Header top line
    $this->sections[] = array (
        'title'      => __( 'Header Top Line', 'welldone' ),
        'id'         => 'header-top-set',
        'subsection' => true,
        'fields'     => array(
             array (
                'id' => 'welldone_header_top',
                'type' => 'switch',
                'title' => __ ( 'Show Header Top Line', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
             array (
                'id' => 'head_icons_info',
                'type' => 'section',
                'title' => __ ( 'Social Icons', 'welldone' ),
                'subtitle' => __ ( 'Icons that you want to hide, just leave their fields empty.', 'welldone' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
                'required' => array( 'welldone_header_top', '=', '1' )
            ),
            array (
                'id' => 'header_icon_fb',
                'type' => 'text',
                'title' => __ ( 'Facebook', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'validate' => 'url',
                'placeholder' => 'https://www.facebook.com/'
            ),
            array (
                'id' => 'header_icon_twitter',
                'type' => 'text',
                'title' => __ ( 'Twitter', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'https://www.twitter.com/'
            ),
            array (
                'id' => 'header_icon_gmail',
                'type' => 'text',
                'validate' => 'url',
                'title' => __ ( 'Google+', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'https://plus.google.com/'
            ),
            array (
                'id' => 'header_icon_pin',
                'type' => 'text',
                'title' => __ ( 'Pinterest', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'http://pinterest.com/'
            ),
             array (
                'id' => 'header_icon_mail',
                'type' => 'text',
                'title' => __ ( 'Email', 'welldone' ),
                'validate' => 'email',
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'info@company.com'
            ),
            array(
                'id'     => 'section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
            array (
                'id' => 'header_show_auth',
                'type' => 'switch',
                'title' => __ ( 'Login / Register Buttons', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'required' => array( 'welldone_header_top', '=', '1' )
            ),
            array (
                'id' => 'header_fb_tw_login',
                'type' => 'switch',
                'title' => __ ( 'Facebook / Twitter Login', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'required' => array( 'welldone_header_top', '=', '1' )
            )
        ));
    //Header 
    $this->sections[] = array (
        'title'      => __( 'Header', 'welldone' ),
        'id'         => 'header-main',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_site_header',
                'type' => 'image_select',
                //'tiles' => true,
                'title' => __ ( 'Site Header Type', 'welldone' ),
                'compiler' => true,
                'options' => array (
                '1' => array (
                              'alt' => 'Header 1',
                              'img' => get_template_directory_uri () . '/images/headers/Header1.png'
                    ),
                '2' => array (
                        'alt' => 'Header 2',
                        'img' => get_template_directory_uri () . '/images/headers/Header2.png'
                    ),
                '3' => array (
                        'alt' => 'Header 3',
                        'img' => get_template_directory_uri () . '/images/headers/Header3.png'
                ),
                '4' => array (
                        'alt' => 'Header 4',
                        'img' => get_template_directory_uri () . '/images/headers/Header4.png'
                ),
                '5' => array (
                        'alt' => 'Header 5',
                        'img' => get_template_directory_uri () . '/images/headers/Header5.png'
                ),
           ),
               'default' => '1'
            ),
            // array (
            //     'id' => 'transparent-header',
            //     'type' => 'switch',
            //     'title' => __('Transparent Header','welldone'),
            //     'default' => '0',
            //     'compiler' => true,
            //     'on' => __('Yes','welldone'),
            //     'off' => __('No','welldone'),
            // ),
            array (
                'id' => 'sticky-header',
                'type' => 'switch',
                'title' => __('Enable Sticky Header','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'header-search',
                'type' => 'switch',
                'title' => __('Show Search','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'show-minicart',
                'type' => 'switch',
                'title' => __('Show Mini cart','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
           array (
                'id' => 'show-currency-switcher',
               'type' => 'switch',
               'title' => __('Show Currency Switcher','welldone'),
               'default' => '1',
               'compiler' => true,
               'on' => __('Yes','welldone'),
               'off' => __('No','welldone'),
           ),
           array (
                'id' => 'show-wpml-switcher',
                'type' => 'switch',
                'title' => __('Show WPML language Switcher','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
           ),
           array (
                'id' => 'show-woo-pages',
                'type' => 'switch',
                'title' => __('Show Woocommerce User Menu Pages','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
           ),
        ));
   // Breadcrumbs And Tilte Settings
    $this->sections[] = array (
        'icon' => 'el-icon-minus',
        'icon_class' => 'icon',
        'title' => __('Title and Breadcrumbs','welldone'),
        'fields' => array (
            array (
                'id' => 'welldone_hide_breadcrumb',
                'type' => 'switch',
                'title' => __('Hide Breadcrumbs','welldone'),
                'compiler' => true,
                'default' => '0',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_hide_page_title',
                'type' => 'switch',
                'title' => __('Hide Title','welldone'),
                'compiler' => true,
                'default' => '0',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            // array (
            //     'id' => 'breadcrumbs-separator',
            //     'type' => 'select',
            //     'title' => __('Separator','welldone'),
            //     'compiler' => true,
            //     'options' => array ( '>' => '>', '<' => '<', '|' => '|', '/' => '/' ),
            //     'default' => '>',
            //     'select2' => array ( 'allowClear' => false )
            // ),
            // array (
            //     'id' => 'welldone_breadcrumb_title_position',
            //     'type' => 'button_set',
            //     'title' => __('Breadcrumb and Title Position','welldone'),
            //     'compiler' => true,
            //     'options' => array (
            //         'text-left' => __ ( 'Left', 'welldone' ),
            //         'text-center' => __ ( 'Center', 'welldone' ),
            //         'text-right' => __ ( 'Right', 'welldone' ),
            //     ),
            //     'default' => 'left',
            // ),
            // array (
            //     'id' => 'welldone_breadcrumb_use_full',
            //     'type' => 'switch',
            //     'title' => __('Use 2 Columns','welldone'),
            //     'compiler' => true,
            //     'default' => '0',
            //     'on' => __('Yes','welldone'),
            //     'off' => __('No','welldone'),
            // ),
            // array (
            //     'id' => 'welldone_bread_title_size',
            //     'type' => 'button_set',
            //     'title' => __('Breadcrumb and Title Size','welldone'),
            //     'compiler' => true,
            //     'options' => array (
            //         'small' => 'Small',
            //         'larger' => 'Medium',
            //         'largest' => 'Large'
            //     ),
            //     'default' => 'small',
            // )
        ));
    //Body Settings
    $this->sections[] = array (
        'title' => __ ( 'Body, Page', 'welldone' ),
        'icon' => 'el-icon-laptop',
        'fields' => array (
           array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Page Layout', 'welldone' ),
                'desc' => __ ( 'Select default page layout for the theme', 'welldone' ),
            ),
            array (
                'id' => 'welldone_page_layout',
                'type' => 'image_select',
                'title' => __ ( 'Page Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'left'
            ),
            array (
                'id' => 'welldone_page_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Page Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'select2' => array ( 'allowClear' => false ),
                'default' => 'page-sidebar-1',
                'required' => array ( "welldone_page_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_page_sidebar_right',
                'type' => 'select',
                'select2' => array ( 'allowClear' => false ),
                'title' => __ ( 'Page Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'page-sidebar-2',
                'required' => array ( "welldone_page_layout", "=", array ( "right", "both" ) )
            ),
           array (
               'id' => 'layout_boxed',
                'type' => 'switch',
                'title' => __('Boxed Layout','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
           array (
               'id' => 'page-comment',
                'type' => 'switch',
                'title' => __('Show Comment Form on Page','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Coming Soon', 'welldone' ),
            ),
            array (
                'id' => 'welldone_coming_soon_mode',
                'type' => 'switch',
                'title' => __ ( 'Coming Soon Mode', 'welldone' ),
                'compiler' => true,
                'default' => '0',
                'on' => __('Activate','welldone'),
                'off' => __('Deactivate','welldone'),
            ),
           array (
                'id' => 'welldone_coming_soon_page',
                'type' => 'select',
                'select2' => array ( 'allowClear' => false ),
                'title' => __ ( 'Select Coming Soon Page', 'welldone' ),
                'compiler' => true,
                'desc' => __ ( 'Pages That has Coming Soon Template', 'welldone' ),
                // 'options' => welldone_get_coming_soon_pages (),
                'required' => array ( "welldone_coming_soon_mode", "=", 1 )
            ),
           array (
               'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Social Share Plugin', 'welldone' ),
           ),
           array (
                'id' => 'welldone_social_share',
                'type' => 'select',
                'title' => __('Hide Social Share','welldone'),
                'desc' => __('select Post Type on which you want to hide social share plugin.Works only if the Font Awesome share Icons plugin is active','welldone'),
                'compiler' => true,
                'placeholder' => __('Select post Types','welldone'),
                'data' => 'post_types',
                'multi' => true
            ),
           array (
               'id' => 'welldone_social_share_label',
              'type' => 'textarea',
                'title' => __('Social Share Label','welldone'),
                'compiler' => true,
                'allowed_html' => array ( 'a', 'span', 'em', 'strong' ),
                'placeholder' => __('Social Share Label','welldone'),
                'validate' => 'html',
                'default' => 'Share social:',
            ),
        ));
   //Footer Settings
    $this->sections[] = array (
        'title' => __ ( 'Footer Options', 'welldone' ),
        'id'    => 'footer-settings',
        'desc'  => __( '', 'redux-framework-demo' ),
        'icon' => 'el-icon-website'
    );
    // Footer Top
    $this->sections[] = array (
        'title'      => __( 'Footer Top', 'welldone' ),
        'id'         => 'footer-top-set',
        'subsection' => true,
        'fields'     => array(
             array (
                'id' => 'welldone_show_footer_top',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Top Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
              array (
                'id' => 'welldone_show_footer_l_menu',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Top Left Menu', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
              array (
                'id' => 'welldone_show_footer_r_menu',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Top Right Menu', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
        ));
    //Header 
    $this->sections[] = array (
        'title'      => __( 'Footer Middle', 'welldone' ),
        'id'         => 'footer-middle',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_show_footer_mid',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Middle Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
            array (
                'id' => 'welldone_footer_widget_columns',
                'type' => 'image_select',
                'title' => __ ( 'Footer Widget Columns', 'welldone' ),
                'compiler' => true,
                'default' => '5',
                'options' => array (
                    '1' => array (
                        'alt' => __ ( '1 Column', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/1col.png'
                    ),
                    '2' => array (
                        'alt' => __ ( '2 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/2col.png'
                    ),
                    '3' => array (
                        'alt' => __ ( '3 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/3col.png'
                    ),
                    '4' => array (
                        'alt' => __ ( '4 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/4col.png'
                    ),
                    '5' => array (
                        'alt' => __ ( '4 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/5col.png'
                    )
                ),
            ),
            array (
                'id' => 'welldone_show_footer_top_widgets',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Middle Widgets', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                // 'description' => __('Used in footer 1,2,6 and 7.','welldone'),
            ),
        ));
    $this->sections[] = array (
        'title'      => __( 'Footer Bottom', 'welldone' ),
        'id'         => 'footer-bottom',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_show_footer_bottom',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Bottom Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
            // array (
            //     'id' => 'footer_bottom_social_icons',
            //     'type' => 'switch',
            //     'title' => __ ( 'Show Social Icons', 'welldone' ),
            //     'compiler' => true,
            //     'default' => '1',
            //     'on' => __('Yes','welldone'),
            //     'off' => __('No','welldone'),
            //     'required' => array( 'welldone_show_footer_bottom', '=', '1' )
            // ),
            array (
                'id' => 'footer_bottom_subscribe',
                'type' => 'switch',
                'title' => __ ( 'Show Subscribe Form', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'required' => array( 'welldone_show_footer_bottom', '=', '1' )
            ),
             array (
                'id' => 'footer_icons_info',
                'type' => 'section',
                'title' => __ ( 'Social Icons', 'welldone' ),
                'subtitle' => __ ( 'Icons that you want to hide, just leave their fields empty.', 'welldone' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
                'required' => array( 'welldone_show_footer_bottom', '=', '1' )
            ),
            array (
                'id' => 'footer_icon_fb',
                'type' => 'text',
                'title' => __ ( 'Facebook', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'validate' => 'url',
                'placeholder' => 'https://www.facebook.com/'
            ),
            array (
                'id' => 'footer_icon_twitter',
                'type' => 'text',
                'title' => __ ( 'Twitter', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'https://www.twitter.com/'
            ),
            array (
                'id' => 'footer_icon_gmail',
                'type' => 'text',
                'validate' => 'url',
                'title' => __ ( 'Google+', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'https://plus.google.com/'
            ),
            array (
                'id' => 'footer_icon_pin',
                'type' => 'text',
                'title' => __ ( 'Pinterest', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'http://pinterest.com/'
            ),
             array (
                'id' => 'footer_icon_mail',
                'type' => 'text',
                'title' => __ ( 'Email', 'welldone' ),
                'validate' => 'email',
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'info@company.com'
            ),
            array(
                'id'     => 'footer-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
        ));
    $this->sections[] = array (
        'title'      => __( 'Copyright Footer Section', 'welldone' ),
        'id'         => 'footer-copy',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_show_copyright_block',
                'type' => 'switch',
                'title' => __ ( 'Show Copyright Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
            array (
                'id' => 'welldone_copyright',
                'type' => 'textarea',
                'title' => __ ( 'Copyright Text', 'welldone' ),
                'default' => '© 2016 <a href="#">Welldone</a>. All right reserved',
                'compiler' => true,
                'validate' => 'html_custom',
                'required' => array( 'welldone_show_copyright_block', '=', '1' ),
                'allowed_html' => array (
                    'a' => array (
                        'href' => array (),
                        'title' => array ()
                    ),
                    'br' => array (),
                    'em' => array (),
                    'strong' => array (),
                    'span' => array ()
                ),
                'desc' => __ ( 'HTML allowed tags a, br, em, strong, span', 'welldone' ),
            ),
            array (
                'id' => 'welldone_footer_text',
                'type' => 'textarea',
                'title' => __ ( 'Text Info To The Right', 'welldone' ),
                'default' => 'with love <span class="icon-favorite color-heart"></span> from <a href="http://themeforest.net/user/etheme">etheme</a>',
                'compiler' => true,
                'validate' => 'html_custom',
                'required' => array( 'welldone_show_copyright_block', '=', '1' ),
                'allowed_html' => array (
                    'a' => array (
                        'href' => array (),
                        'title' => array ()
                    ),
                    'br' => array (),
                    'em' => array (),
                    'strong' => array (),
                    'span' => array (
                        'class' => array ()
                    )
                ),
                'desc' => __ ( 'HTML allowed tags a, br, em, strong, span', 'welldone' ),
            ),
        ));
    // $this->sections[] = array (
    //     'title' => __ ( 'Footer', 'welldone' ),
    //     'icon' => 'el-icon-website',
    //     'fields' => array (
    //         array (
    //             'id' => 'welldone_top_footer_widget_columns',
    //             'type' => 'image_select',
    //             'title' => __ ( 'Top Footer Widgets Columns', 'welldone' ),
    //             'compiler' => true,
    //             'default' => '3',
    //             'options' => array (
    //                 '1' => array (
    //                     'alt' => __ ( '1 Column', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/1col.png'
    //                 ),
    //                 '2' => array (
    //                     'alt' => __ ( '2 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/2col.png'
    //                 ),
    //                 '3' => array (
    //                     'alt' => __ ( '3 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/3col.png'
    //                 ),
    //                 '4' => array (
    //                     'alt' => __ ( '4 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/4col.png'
    //                 )
    //             ),
    //             'description' => __('Used in footer 1,2,6 and 7.','welldone'),
    //         ),
    //         array (
    //             'id' => 'welldone_top_footer_widget_v2_columns',
    //             'type' => 'image_select',
    //             'title' => __ ( 'Top Footer Widgets v2 Columns', 'welldone' ),
    //             'compiler' => true,
    //             'default' => '3',
    //             'options' => array (
    //                 '1' => array (
    //                     'alt' => __ ( '1 Column', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/1col.png'
    //                 ),
    //                 '2' => array (
    //                     'alt' => __ ( '2 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/2col.png'
    //                 ),
    //                 '3' => array (
    //                     'alt' => __ ( '3 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/3col.png'
    //                 ),
    //                 '4' => array (
    //                     'alt' => __ ( '4 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/4col.png'
    //                 )
    //             ),
    //             'description' => __('Used in footer 9,10 and 11.','welldone'),
    //         ),
    //         array (
    //             'id' => 'welldone_footer_widget_columns',
    //             'type' => 'image_select',
    //             'title' => __ ( 'Footer Widget Columns', 'welldone' ),
    //             'compiler' => true,
    //             'default' => '5',
    //             'options' => array (
    //                 '1' => array (
    //                     'alt' => __ ( '1 Column', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/1col.png'
    //                 ),
    //                 '2' => array (
    //                     'alt' => __ ( '2 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/2col.png'
    //                 ),
    //                 '3' => array (
    //                     'alt' => __ ( '3 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/3col.png'
    //                 ),
    //                 '4' => array (
    //                     'alt' => __ ( '4 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/4col.png'
    //                 ),
    //                 '5' => array (
    //                     'alt' => __ ( '4 Columns', 'welldone' ),
    //                     'img' => get_template_directory_uri () . '/images/default/5col.png'
    //                 )
    //             ),
    //             'description' => __('Used in footer 1,2,6,7 and 8','welldone'),
    //         ),
    //         array (
    //             'id' => 'welldone_hide_footer_top_widgets',
    //             'type' => 'switch',
    //             'title' => __ ( 'Hide Footer Top Widgets', 'welldone' ),
    //             'compiler' => true,
    //             'default' => 0,
    //             'on' => __('Yes','welldone'),
    //             'off' => __('No','welldone'),
    //             'description' => __('Used in footer 1,2,6 and 7.','welldone'),
    //         ),

    //         array (
    //             'id' => 'welldone_hide_footer_menu',
    //             'type' => 'switch',
    //             'title' => __ ( 'Hide Footer Menu', 'welldone' ),
    //             'compiler' => true,
    //             'default' => 0,
    //             'on' => __('Yes','welldone'),
    //             'off' => __('No','welldone'),
    //         ),
    //         array (
    //             'id' => 'welldone_copyright',
    //             'type' => 'textarea',
    //             'title' => __ ( 'Copyright Text', 'welldone' ),
    //             'default' => 'Created with by <a href="#">SW theme</a>. All right reserved',
    //             'compiler' => true,
    //             'validate' => 'html_custom',
    //             'allowed_html' => array (
    //                 'a' => array (
    //                     'href' => array (),
    //                     'title' => array ()
    //                 ),
    //                 'br' => array (),
    //                 'em' => array (),
    //                 'strong' => array (),
    //                 'span' => array ()
    //             ),
    //             'desc' => __ ( 'HTML allowed tags a, br, em, strong, span (color white is applied to span text)', 'welldone' ),
    //         ),
    //         array (
    //             'id' => 'welldone_hide_payments',
    //             'type' => 'switch',
    //             'title' => __ ( 'Hide Payments Image', 'welldone' ),
    //             'compiler' => true,
    //             'default' => 0,
    //             'on' => __('Yes','welldone'),
    //             'off' => __('No','welldone'),
    //         ),
    //         array (
    //             'id' => 'welldone_payments_image',
    //             'type' => 'media',
    //             'url' => true,
    //             'title' => __('Payments Image','welldone'),
    //             'compiler' => true,
    //             'default' => array (
    //                 'url' => get_template_directory_uri () . '/images/default/payments.png',
    //             )
    //         ),
    //     ));
    //blog and single post
    $this->sections[] = array (
        'icon' => 'el-icon-blogger',
        'title' => __ ( 'Blog & Single Post', 'welldone' ),
        'fields' => array (
            // blog options
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Blog Options', 'welldone' ),
            ),
            array (
                'id' => 'welldone_blog_layout',
                'type' => 'image_select',
                'title' => __ ( 'Blog Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'left'
            ),
            array (
                'id' => 'welldone_blog_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Blog Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'select2' => array ( 'allowClear' => false ),
                'default' => 'blog-sidebar-1',
                'required' => array ( "welldone_blog_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_blog_sidebar_right',
                'type' => 'select',
                'select2' => array ( 'allowClear' => false ),
                'title' => __ ( 'Blog Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'blog-sidebar-2',
                'required' => array ( "welldone_blog_layout", "=", array ( "right", "both" ) )
            ),
            array (
                'id' => 'welldone_blog_type',
                'type' => 'button_set',
                'title' => __('Blog Type','welldone'),
                'compiler' => true,
                'options' => array (
                    'classic' => __ ( 'Classic(Default)', 'welldone' ),
                    "blog-masonry" => __ ( "Blog Masonry", 'welldone' )
                ),
                'default' => 'classic'
            ),
            array (
                'id' => 'welldone_blog_masonry_columns',
                'type' => 'image_select',
                'title' => __ ( 'Select Blog Masonry Columns', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    '2' => array (
                        'alt' => '2 Columns',
                        'img' => get_template_directory_uri () . '/images/default/2col.png',
                    ),
                    "3" => array (
                        'alt' => '3 Columns',
                        'img' => get_template_directory_uri () . '/images/default/3col.png',
                    ),
                    "4" => array (
                        'alt' => '4 Columns',
                        'img' => get_template_directory_uri () . '/images/default/4col.png',
                    ),
                    "5" => array (
                        'alt' => '5 Columns',
                        'img' => get_template_directory_uri () . '/images/default/5col.png',
                    ),
                ),
                'default' => '4',
                'required' => array ( 'welldone_blog_type', '=', 'blog-masonry' ),
            ),
           //skip posts
            array (
                'id' => 'welldone_exclude_posts',
                'type' => 'text',
                'validate' => 'comma_numeric',
                'title' => __ ( 'Exclude Posts', 'welldone' ),
                'compiler' => true,
                'description' => __('input post ids comma seperated','welldone')
            ),
            // blog title
            array (
                'id' => 'welldone_blog_title',
                'type' => 'text',
                'validate' => 'no_html',
                'title' => __ ( 'Blog Page Title', 'welldone' ),
                'compiler' => true,
                'default' => 'Blog'
            ),
            // blog pagination type
            array (
                'id' => 'welldone_blog_pagination_type',
                'type' => 'select',
                'title' => __ ( 'Pagination Type', 'welldone' ),
                'compiler' => true,
                'select2' => array ( 'allowClear' => false ),
                // Must provide key => value pairs for select options
                'options' => array (
                    'pagination' => 'Pagination',
                    'infinite_scroll' => 'Infinite Scroll',
                ),
                'default' => 'pagination'
            ),
            // hide blog post title
            array (
                'id' => 'welldone_hide_blog_post_title',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Title', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            // hide blog post author
            array (
                'id' => 'welldone_blog_excerpt',
                'type' => 'switch',
                'title' => __('Show Excerpt','welldone'),
                'default' => '0',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_blog_excerpt_length',
                'type' => 'text',
                'title' => __('Excerpt Length','welldone'),
                'desc' => __('The number of words','welldone'),
                'default' => '80',
            ),
            array (
                'id' => 'welldone_excerpt_type',
                'type' => 'button_set',
                'title' => __('Excerpt Type','welldone'),
                'options' => array ( 'text' => 'Text', 'html' => 'HTML' ),
                'default' => 'html'
            ),
            array (
                'id' => 'welldone_hide_blog_post_author',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Author Name', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_hide_blog_post_category',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Category', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_hide_blog_post_tags',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Tags', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Single Post Options', 'welldone' ),
            ),
            array (
                'id' => 'welldone_post_layout',
                'type' => 'image_select',
                'title' => __ ( 'Post Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                   'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                   'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                   ),
                  'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    )
                ),
                'default' => 'left'
            ),
            array (
                'id' => 'welldone_post_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Post Left Sidebar', 'welldone' ),
                'compiler' => true,
                'select2' => array ( 'allowClear' => false ),
                'options' => $welldone_sidebar,
                'default' => 'single-post-sidebar',
                'required' => array ( "welldone_post_layout", "=", array ( "left", "both" ) )
            ),
            array (
               'id' => 'welldone_post_sidebar_right',
                'type' => 'select',
                'select2' => array ( 'allowClear' => false ),
                'title' => __ ( 'Post Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'single-post-sidebar',
                'required' => array ( "welldone_post_layout", "=", array ( "right", "both" ) )
            ),
            //Show Prev/Next Navigation
            array (
                'id' => 'welldone_post_page_nav',
                'type' => 'switch',
                'title' => __ ( 'Show Prev/Next Navigation', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            array(         
                'id'       => 'prev_post_block_background',
                'type'     => 'background',
                'title'    => __('Previous Post Block Navigation Background', 'welldone'),
                'background-position' => false,
                'transparent' => false,
                // 'preview_media' => true,
                'background-color' => true,
                'background-size' => false,
                'background-attachment' => false,
                // 'background-repeat' => false,
                'required' => array( 'welldone_post_page_nav', '=', '1' ),
                'default'  => array(
                    'background-color' => '#1e73be',
                    'background-image' => get_template_directory_uri () . '/images/bg-images/next_prev_post_bg.jpg',
                )
            ),

            array(         
                'id'       => 'next_post_block_background',
                'type'     => 'background',
                'title'    => __('Next Post Block Navigation Background', 'welldone'),
                'background-position' => false,
                'transparent' => false,
                // 'preview_media' => true,
                'background-color' => true,
                'background-size' => false,
                'background-attachment' => false,
                // 'background-repeat' => false,
                'required' => array( 'welldone_post_page_nav', '=', '1' ),
                'default'  => array(
                    'background-color' => '#1e73be',
                    'background-image' => get_template_directory_uri () . '/images/bg-images/next_prev_post_bg.jpg',
                )
            ),


            //Show Author Info
            array (
                'id' => 'welldone_hide_post_author',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Author Name', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            array (
                'id' => 'welldone_hide_post_category',
                'type' => 'switch',
                'title' => __ ( 'Hide Posts Category', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            array (
                'id' => 'welldone_hide_post_tags',
                'type' => 'switch',
                'title' => __ ( 'Hide Posts Tags', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            //Show Comments
            array (
                'id' => 'welldone_post_comments',
                'type' => 'switch',
                'title' => __ ( 'Show Comments', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            //Show Addthis Buttons above Content
        ));

    $this->sections[] = array (
       'title' => __ ( 'Woocommerce', 'welldone' ),
        'icon' => 'el-icon-gift',
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'General Woocommerce Settings', 'welldone' )
            ),
            array (
                'id' => 'welldone_product_featured_label',
                'type' => 'switch',
                'title' => __ ( 'Show Featured Label', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __( 'Yes', 'welldone' ),
                'off' => __( 'No', 'welldone' ),
            ),
            array (
                'id' => 'welldone_product_featured_label_pos',
                'type' => 'button_set',
                'title' => __ ( 'Featured Label Position', 'welldone' ),
                'options' => array (
                    '' => __ ( 'Top Left', 'welldone' ),
                    'top-right' => __ ( 'Top Right', 'welldone' ),
                    'bottom-left' => __ ( 'Bottom Left', 'welldone' ),
                    'bottom-right' => __ ( 'Bottom Right', 'welldone' ),
                ),
                'default' => '',
                'required' => array ( 'welldone_product_featured_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_featured_label_type',
                'type' => 'button_set',
                'title' => __ ( 'Featured Label Type', 'welldone' ),
                'options' => array (
                    'label-default' => __ ( 'Default', 'welldone' ),
                    'label-primary' => __ ( 'Primary', 'welldone' ),
                    'label-success' => __ ( 'Success', 'welldone' ),
                    'label-info' => __ ( 'Info', 'welldone' ),
                    'label-warning' => __ ( 'Warning', 'welldone' ),
                    'label-popular' => __ ( 'Popular', 'welldone' ),
                    'label-new' => __ ( 'New', 'welldone' ),
                ),
                'default' => 'label-default',
                'required' => array ( 'welldone_product_featured_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_featured_label_text',
                'type' => 'text',
                'title' => __ ( 'Featured Label Text', 'welldone' ),
                'default' => 'Hot',
                'required' => array ( 'welldone_product_featured_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_sale_label',
                'type' => 'switch',
                'title' => __ ( 'Show Sale Label', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                 'on' => __ ( 'Yes', 'welldone' ),
                 'off' => __ ( 'No', 'welldone' ),
            ),
            array (
                'id' => 'welldone_product_sale_label_pos',
                'type' => 'button_set',
                'title' => __ ( 'Sale Label Position', 'welldone' ),
                'options' => array (
                    '' => __ ( 'Top Left', 'welldone' ),
                    'top-right' => __ ( 'Top Right', 'welldone' ),
                    'bottom-left' => __ ( 'Bottom Left', 'welldone' ),
                    'bottom-right' => __ ( 'Bottom Right', 'welldone' ),
                ),
                'default' => '',
               'required' => array ( 'welldone_product_sale_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_sale_label_type',
                'type' => 'button_set',
                'title' => __ ( 'Sale Label Type', 'welldone' ),
                'options' => array (
                    'label-default' => __ ( 'Default', 'welldone' ),
                    'label-primary' => __ ( 'Primary', 'welldone' ),
                    'label-success' => __ ( 'Success', 'welldone' ),
                    'label-info' => __ ( 'Info', 'welldone' ),
                    'label-warning' => __ ( 'Warning', 'welldone' ),
                    'label-popular' => __ ( 'Popular', 'welldone' ),
                    'label-new' => __ ( 'New', 'welldone' ),
                ),
                'default' => 'label-default',
                'required' => array ( 'welldone_product_sale_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_sale_label_text_type',
                'type' => 'button_set',
                'title' => __ ( 'Sale Label Text Type', 'welldone' ),
                'options' => array (
                    'custom-text' => __ ( 'Custom Text', 'welldone' ),
                    'per_sale_price' => __ ( 'Percentage saved', 'welldone' ),
                ),
                'default' => 'per_sale_price',
                'required' => array ( 'welldone_product_sale_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_sale_label_text',
                'type' => 'text',
                'title' => __ ( 'Sale Label Text', 'welldone' ),
                'default' => 'Sale',
                'required' => array ( 'welldone_product_sale_label_text_type', '=', 'custom-text' )
            ),
            array (
                'id' => 'welldone_product_new_label',
                'type' => 'switch',
                'title' => __('Show New Label','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_product_new_label_time',
                'type' => 'text',
                'title' => __ ( 'New Label Time Period', 'welldone' ),
                'default' => '7',
                'validate' => 'numeric',
                'desc' => __ ( 'Number Of Days a product remains as new.Default is 7 days.', 'welldone' ),
                'required' => array ( 'welldone_product_new_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_new_label_pos',
                'type' => 'button_set',
                'title' => __ ( 'New Label Position', 'welldone' ),
                'options' => array (
                    '' => __ ( 'Top Left', 'welldone' ),
                    'top-right' => __ ( 'Top Right', 'welldone' ),
                    'bottom-left' => __ ( 'Bottom Left', 'welldone' ),
                    'bottom-right' => __ ( 'Bottom Right', 'welldone' ),
                ),
                'default' => '',
                'required' => array ( 'welldone_product_new_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_new_label_type',
                'type' => 'button_set',
                'title' => __ ( 'New Label Type', 'welldone' ),
                'options' => array (
                    'label-default' => __ ( 'Default', 'welldone' ),
                    'label-primary' => __ ( 'Primary', 'welldone' ),
                    'label-success' => __ ( 'Success', 'welldone' ),
                    'label-info' => __ ( 'Info', 'welldone' ),
                    'label-warning' => __ ( 'Warning', 'welldone' ),
                    'label-popular' => __ ( 'Popular', 'welldone' ),
                    'label-new' => __ ( 'New', 'welldone' ),
                ),
                'default' => 'label-default',
                'required' => array ( 'welldone_product_new_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_new_label_text',
                'type' => 'text',
                'title' => __ ( 'New Label Text', 'welldone' ),
                'default' => 'New',
                'required' => array ( 'welldone_product_new_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_show_add_to_cart_button',
                'type' => 'switch',
                'title' => __ ( 'Show Add To Cart', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
           array (
            'id' => 'welldone_show_quick_button',
            'type' => 'switch',
            'title' => __ ( 'Show Quick View', 'wellldone' ),
            'compiler' => true,
            'default' => '1',
            'on' => __('Yes','welldone'),
            'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_price',
                'type' => 'switch',
                'title' => __ ( 'Show Product Price', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_wishlist_button',
                'type' => 'switch',
                'title' => __ ( 'Show Wishlist', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_compare_button',
                'type' => 'switch',
                'title' => __ ( 'Show Compare', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_product_category',
                'type' => 'switch',
                'title' => __ ( 'Show Category', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_product_tags',
                'type' => 'switch',
                'title' => __ ( 'Show Tags', 'welldone' ),
                'compiler' => true,
                'desc' => __ ( 'Tags visible on single product page only', 'welldone' ),
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Shop and Category Page', 'welldone' )
            ),
            array (
                'id' => 'welldone_products_view',
                'type' => 'button_set',
                'title' => __ ( 'Products View', 'welldone' ),
                'options' => array (
                    'grid' => __ ( 'Grid', 'welldone' ),
                    'list' => __ ( 'List', 'welldone' ),
                ),
                'default' => 'grid'
            ),
            array (
                'id' => 'welldone_products_grid_version',
                'type' => 'select',
                'title' => __ ( 'Select Grid Style', 'welldone' ),
                'options' => array (
                    'v_1' => __ ( 'Version 1', 'welldone' ),
                    'v_2' => __ ( 'Version 2', 'welldone' ),
                    'v_3' => __ ( 'Version 3', 'welldone' ),
                    'v_4' => __ ( 'Version 4', 'welldone' ),
                ),
                'default' => 'v_1',
                'select2' => array ( 'allowClear' => false )
            ),
            array (
                'id' => 'welldone_products_list_version',
                'type' => 'select',
                'title' => __ ( 'Select List Style', 'welldone' ),
                'options' => array (
                    'list' => __ ( 'List Left Aligned', 'welldone' ),
                    'list_right' => __ ( 'List Right Aligned', 'welldone' ),
                ),
                'default' => 'list',
                'select2' => array ( 'allowClear' => false )
            ),
            array (
                'id' => 'welldone_products_grid_columns',
                'type' => 'select',
                'title' => __ ( 'Select Grid Columns', 'welldone' ),
                'options' => array (
                    '1' => "1",
                    '2' => "2",
                    '3' => "3",
                    '4' => "4",
                    '5' => "5",
                    '6' => "6",
                ),
                'select2' => array ( 'allowClear' => false ),
                'default' => '4'
            ),
            array (
                'id' => 'welldone_products_perpage',
                'type' => 'text',
                'title' => __ ( 'Products per Page', 'welldone' ),
                'compiler' => true,
                'validate' => 'comma_numeric',
                'description' => __ ( 'Comma-seperated. Default: 9,15,30', 'welldone' ),
                'default' => '9,15,30'
            ),
            array (
                'id' => 'welldone_shop_layout',
                'type' => 'image_select',
                'title' => __ ( 'Shop Page Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'left'
            ),
            array (
                'id' => 'welldone_shop_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Shop Page Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'select2' => array ( 'allowClear' => false ),
                'default' => 'page-sidebar-1',
                'required' => array ( "welldone_shop_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_shop_sidebar_right',
                'type' => 'select',
                'title' => __ ( 'Shop Page Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'page-sidebar-2',
                'select2' => array ( 'allowClear' => false ),
                'required' => array ( "welldone_shop_layout", "=", array ( "right", "both" ) )
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Product Page', 'welldone' )
            ),
            array (
                'id' => 'welldone_product_page_style',
                'type' => 'select',
                'title' => __ ( 'Product Page Style', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'v_1' => __ ( 'Style 1', 'welldone' ),
                    'v_2' => __ ( 'Style 2', 'welldone' ),
                ),
                'default' => 'v_1',
                'select2' => array ( 'allowClear' => false ),
                'desc' => __ ( 'Choose Style for the product page. Default is style 1', 'welldone' ),
            ),
            array (
                'id' => 'welldone_product_layout',
                'type' => 'image_select',
                'title' => __ ( 'Shop Page Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'left'
            ),
            array (
                'id' => 'welldone_product_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Product Page Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'single-product-sidebar',
                'select2' => array ( 'allowClear' => false ),
                'required' => array ( "welldone_product_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_product_sidebar_right',
                'type' => 'select',
                'title' => __ ( 'Product Page Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'single-product-sidebar',
                'select2' => array ( 'allowClear' => false ),
                'required' => array ( "welldone_product_layout", "=", array ( "right", "both" ) )
           ),
            array (
                'id' => 'welldone_product_bottom_products',
                'type' => 'sortable',
                'title' => __ ( 'Select Product Types To Show at the bottom of Product', 'welldone' ),
                'compiler' => true,
                'mode' => 'checkbox',
                'options' => array (
                    'Featured' => __ ( 'Featured Products', 'welldone' ),
                    'Popular' => __ ( 'Popular Products', 'welldone' ),
                    'TopRated' => __ ( 'Top Rated Products', 'welldone' ),
                    'Latest' => __ ( 'Latest Products', 'welldone' )
                ),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Cart Page', 'welldone' )
            ),
            array (
                'id' => 'welldone_woo_cart_ver',
                'type' => 'button_set',
                'title' => __ ( 'Cart Page Version', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'v_1' => __ ( 'Version 1', 'welldone' ),
                    'v_2' => __ ( 'Version 2', 'welldone' ),
                ),
                'default' => 'v_1',
            ),
        ));
    $this->sections[] = array (
        'title' => __ ( '404 Page', 'welldone' ),
        'icon' => 'el-icon-ban-circle',
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Add 404 Content Here.', 'welldone' ),
            ),
            array (
                'id' => 'welldone_404_title',
                'type' => 'text',
                'compiler' => true,
                'title' => __ ( '404 Page Title', 'welldone' ),
                'placeholder' => __ ( '404 Page Title', 'welldone' ),
                'default' => '404'
            ),
            array (
                'id' => 'welldone_404_subtitle',
                'type' => 'text',
                'compiler' => true,
                'title' => __ ( '404 Page Sub Title', 'welldone' ),
                'placeholder' => __ ( '404 Page Sub Title', 'welldone' ),
                'default' => __ ( "Page Not Found", 'welldone' )
            ),
            array (
                'id' => 'welldone_404_content',
                'type' => 'editor',
                'compiler' => true,
                'default' => __ ( 'The page you requested does not exist. <a href="#" title="Homepage">Click here</a> to continue shopping.', 'welldone' ),
                'title' => __ ( '404 Page Content', 'welldone' )
            ),
        ));
    $this->sections[] = array (
        'title' => __ ( 'Contact', 'welldone' ),
        'icon' => 'el-icon-phone-alt',
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Add Your Address here. This will be added into the contact template of your theme.', 'welldone' ),
            ),
            array (
                'id' => 'welldone_google_map_address',
                'type' => 'textarea',
                'compiler' => true,
                'title' => __ ( 'Google Map Address', 'welldone' ),
                'placeholder' => __ ( 'Your Address Here', 'welldone' )
            ),
            array (
                'id' => 'welldone_map_social_icons',
                'type' => 'switch',
                'title' => __ ( 'Show Social Icons', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
        ));
   $this->sections[] = array (
        'title' => __ ( 'Social Links', 'welldone' ),
        'icon' => 'el-icon-group',
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Icons that you want to hide, just leave their fields empty.', 'welldone' ),
            ),
            array (
                'id' => 'welldone_mail',
                'type' => 'text',
                'title' => __ ( 'Email', 'welldone' ),
                'validate' => 'email',
                'compiler' => true,
                'placeholder' => 'info@company.com'
            ),
            array (
                'id' => 'welldone_fb',
                'type' => 'text',
                'title' => __ ( 'Facebook', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'https://www.facebook.com/'
            ),
            array (
                'id' => 'welldone_twitter',
                'type' => 'text',
                'title' => __ ( 'Twitter', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'placeholder' => 'https://www.twitter.com/'
            ),
            array (
                'id' => 'welldone_gmail',
                'type' => 'text',
                'validate' => 'url',
                'title' => __ ( 'Google+', 'welldone' ),
                'compiler' => true,
                'placeholder' => 'https://plus.google.com/'
            ),
            array (
                'id' => 'welldone_li',
                'type' => 'text',
                'validate' => 'url',
                'title' => __ ( 'LinkedIn', 'welldone' ),
                'compiler' => true,
                'placeholder' => 'http://www.linkedin.com/'
            ),
            array (
                'id' => 'welldone_pin',
                'type' => 'text',
                'title' => __ ( 'Pinterest', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'placeholder' => 'http://pinterest.com/'
            ),
            array (
                'id' => 'welldone_vimeo',
                'type' => 'text',
                'title' => __ ( 'Vimeo', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://vimeo.com/'
            ),
            array (
               'id' => 'welldone_youtube',
                'type' => 'text',
                'title' => __ ( 'Youtube', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://www.youtube.com/'
            ),
            array (
                'id' => 'welldone_flickr',
                'type' => 'text',
                'title' => __ ( 'Flickr', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://www.flickr.com/'
            ),
            array (
                'id' => 'welldone_instagram',
                'type' => 'text',
                'title' => __ ( 'Instagram', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'https://instagram.com/'
            ),
            array (
                'id' => 'welldone_behance',
                'type' => 'text',
                'title' => __ ( 'Behance', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'https://www.behance.net/'
            ),
            array (
                'id' => 'welldone_dribbble',
                'type' => 'text',
                'title' => __ ( 'Dribbble', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'https://dribbble.com/'
            ),
            array (
                'id' => 'welldone_skype',
                'type' => 'text',
                'title' => __ ( 'Skype ', 'welldone' ),
                'compiler' => true,
                'validate_callback' => 'welldone_validate_skype_username',
                'placeholder' => 'skype username'
            ),
            array (
                'id' => 'welldone_skype_tell',
                'type' => 'text',
                'title' => __ ( 'Skype Number', 'welldone' ),
                'compiler' => true,
                'validate_callback' => 'welldone_validate_skype_number',
               'placeholder' => '+1234567789',
                //    'default'  => '+123456789'
            ),
            array (
                'id' => 'welldone_soundcloud',
                'type' => 'text',
                'title' => __ ( 'Soundcloud', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'https://soundcloud.com/'
            ),
            array (
                'id' => 'welldone_yelp',
                'type' => 'text',
                'title' => __ ( 'Yelp', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://www.yelp.com/'
            ),
            array (
                'id' => 'welldone_tumblr',
                'type' => 'text',
                'title' => __ ( 'Tumblr', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'https://www.tumblr.com/'
            ),
            array (
                'id' => 'welldone_deviantart',
                'type' => 'text',
                'title' => __ ( 'Deviantart', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://www.deviantart.com/'
            ),
            array (
                'id' => 'welldone_weibo',
                'type' => 'text',
                'title' => __ ( 'Weibo', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://weibo.com/'
            ),
            array (
                'id' => 'welldone_github',
                'type' => 'text',
                'title' => __ ( 'Github', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'https://github.com/'
            ),
            array (
                'id' => 'welldone_slideshare',
                'type' => 'text',
                'title' => __ ( 'Slideshare', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://www.slideshare.net/'
            ),
            array (
                'id' => 'welldone_reddit',
                'type' => 'text',
                'title' => __ ( 'Reddit', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://www.reddit.com/'
           ),
            array (
                'id' => 'welldone_digg',
                'type' => 'text',
                'title' => __ ( 'Digg', 'welldone' ),
                'compiler' => true,
                'validate' => 'url',
                'placeholder' => 'http://digg.com/'
            ),
                    array (
                        'id' => 'welldone_xing',
                        'type' => 'text',
                        'title' => __ ( 'Xing', 'welldone' ),
                        'compiler' => true,
                        'validate' => 'url',
                        'placeholder' => 'http://xing.com/'
                    ),
                )
            );
            $this->sections[] = array (
                'title' => __ ( 'Import / Export', 'welldone' ),
                'desc' => __ ( 'Import and Export your settings from file, text or URL.', 'welldone' ),
                'icon' => 'el-icon-refresh',
                'fields' => array (
                    array (
                        'id' => 'opt-import-export',
                        'type' => 'import_export',
                        'title' => __('Import Export','welldone'),
                        'subtitle' => __('Save and restore your Redux options','welldone'),
                        'full_width' => false,
                    ),
                ),
            );
            $this->sections[] = array (
                'type' => 'divide',
            );
            if ( file_exists ( trailingslashit ( get_template_directory () . '/inc/admin/README.html' ) ) ){
                $tabs[ 'docs' ] = array (
                    'icon' => 'el-icon-book',
                    'title' => __ ( 'Documentation', 'welldone' ),
                    //'content' => nl2br ( $wp_filesystem->get_contents(trailingslashit ( get_template_directory () . '/inc/admin/README.html') )
                    'content' => nl2br ( file_get_contents ( trailingslashit ( get_template_directory () . '/inc/admin/README.html' ) ) )
                );
            }
        }

            public function setHelpTabs() {}

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                'opt_name' => 'welldone_settings',
                'display_name' => $theme->get ( 'Name' ) . ' ' . 'Settings',
                'display_version' => $theme->get ( 'Version' ),
                'menu_type' => 'menu',
                'allow_sub_menu' => true,
                'menu_title' => __ ( 'Theme Options', 'welldone' ),
                'page_title' => __ ( 'Theme Options', 'welldone' ),
                'google_api_key' => '',
                'async_typography' => false,
                'admin_bar' => true,
                'global_variable' => '',
                'dev_mode' => false,
                'customizer' => true,
                 'page_priority' => null,
                'page_parent' => 'themes.php',
                'page_permissions' => 'manage_options',
                'menu_icon' => '',
                'last_tab' => '',
                'page_icon' => 'icon-themes',
                'page_slug' => 'welldone_options',
                'save_defaults' => true,
                'default_show' => false,
                'default_mark' => '',
                'show_import_export' => true,
                'transient_time' => 60 * MINUTE_IN_SECONDS,
                'output' => true,
                'output_tag' => true,
                // 'footer_credit'     => '',
                'database' => '',
                'system_info' => false,
                'class' => 'welldone_settings-panel',

                    // HINTS
                    'hints'              => array(
                        'icon'          => 'icon-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );


                // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
                $this->args['share_icons'][] = array(
                    'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
                    'title' => __('Visit us on GitHub','welldone'),
                    'icon'  => 'el el-github'
                    //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
                    'title' => __('Like us on Facebook','welldone'),
                    'icon'  => 'el el-facebook'
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'http://twitter.com/reduxframework',
                    'title' => __('Follow us on Twitter','welldone'),
                    'icon'  => 'el el-twitter'
                );
                $this->args['share_icons'][] = array(
                    'url'   => 'http://www.linkedin.com/company/redux-framework',
                    'title' => __('Find us on LinkedIn','welldone'),
                    'icon'  => 'el el-linkedin'
                );

                // Panel Intro text -> before the form
                if ( ! isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
                    if ( ! empty( $this->args['global_variable'] ) ) {
                        $v = $this->args['global_variable'];
                    } else {
                        $v = str_replace( '-', '_', $this->args['opt_name'] );
                    }
                    $this->args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
                } else {
                    $this->args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
                }

                // Add content after the form.
                $this->args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );
            }

        }

        global $reduxwelldoneSettings;
        $reduxwelldoneSettings = new Redux_Framework_welldone_settings();
    }
