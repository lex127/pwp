<?php

    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux_Framework_welldone_settings' ) ) {

        class Redux_Framework_welldone_settings {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }

            }

            public function initSettings() {

                // Set the default arguments
                $this->setArguments();

                // Set a few help tabs so you can see how it's done
                $this->setHelpTabs();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            function compiler_action ( $options, $css, $changed_values ) {
            }

            function dynamic_section ( $sections ) {
                return $sections;
            }

            function change_arguments ( $args ) {
                return $args;
            }

            function change_defaults ( $defaults ) {
                return $defaults;
            }



            function remove_demo () {
            }

            public function setSections() {

                $wp_registered_sidebars = wp_get_sidebars_widgets ();
                  $welldone_sidebar=array ();  
                        foreach ( $wp_registered_sidebars as $sidebar => $sidebar_info ) {
                            if ( $sidebar == 'wp_inactive_widgets' ) continue;
                            $welldone_sidebar[ $sidebar ] = ucwords ( str_replace ( array ( '_', '-' ), ' ', $sidebar ) );
                        }
                // ACTUAL DECLARATION OF SECTIONS


                 // Skin tahir
    $this->sections[] = array (
        'icon' => 'el-icon-broom',
        'icon_class' => 'icon',
        'title' => __ ( 'Skin Options', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'welldone_site_color',
                'type' => 'color',
                'title' => __ ( 'Main Theme Color', 'welldone' ),
                'default' => '#536dfe',
                'validate' => 'color',
            ),
            array (
                'id' => 'welldone_site_alternate_color',
                'type' => 'color',
                'title' => __ ( 'Alternate Theme Color', 'welldone' ),
                'default' => '#000000',
                'validate' => 'color',
            ),
            array (
                'id' => 'welldone_body_typography',
                'type' => 'typography',
                'title' => __ ( 'Body Fonts', 'welldone' ),
                'google' => true,
                'subsets' => false,
                'font-backup' => true,
                'text-align' => false,
                'default' => array (
                    'font-family' => 'Roboto',
                    'font-style' => '400',
                    'color' => '#212121',
                    'line-height' => '1.4em',
                    'font-size' => '1.3em',
                )
            ),
            array (
                'id' => 'welldone_compile_css',
                'type' => 'switch',
                'title' => __ ( 'Compile Css', 'welldone' ),
                'compiler' => true,
                'desc'=> __('Switch to "Yes" to generate a css file for skin options.','welldone'),
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array(
                'id'       => 'css_editor',
                'type'     => 'ace_editor',
                'title'    => __('Custom CSS', 'welldone'),
                'subtitle' => __('Add Your Custom Css code here.', 'welldone'),
                'mode'     => 'css',
                'theme'    => 'monokai',
            )
        ) );
    //body page tahir done
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Body, Page', 'welldone' ),
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Body', 'welldone' ),
            ),
            array (
                'id' => 'welldone_container_size',
                'type' => 'button_set',
                'title' => __('Use Full Width Body Size','welldone'),
                'options' => array (
                    'yes' => __ ( 'Yes', 'welldone' ),
                    'no' => __ ( "No", 'welldone' ),
                ),
                'default' => 'no'
            ),
            array (
                'id' => 'welldone_theme_wrapper',
                'type' => 'button_set',
                'title' => __ ( 'Theme Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'boxed' => __ ( "Boxed", 'welldone' ),
                    'wide' => __ ( 'Wide', 'welldone' ),
                    'fullwidth' => __ ( "Fullwidth", 'welldone' ),
                ),
               'default' => 'wide'
            ),
            array (
                'id' => 'welldone_bg_mode',
                'type' => 'button_set',
                'title' => __ ( 'Background Mode', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'image' => __ ( "Image", 'welldone' ),
                    'custom-image' => __ ( "Custom Image", 'welldone' ),
                ),
                'default' => 'image',
                'required' => array ( "welldone_theme_wrapper", "=", "boxed" ),
            ),
            array (
                'id' => 'welldone_bg_select',
                'type' => 'image_select',
                'title' => __ ( 'Select Image', 'welldone' ),
                'compiler' => true,
                'tiles' => true,
                'options' => array (
                    get_template_directory_uri () . '/images/bg-images/bg-boxed.jpg',
                    get_template_directory_uri () . '/images/bg-images/bg1.jpg',
                    get_template_directory_uri () . '/images/bg-images/bg2.jpg'
                ),
                'default' => get_template_directory_uri () . '/images/bg-images/bg-boxed.jpg',
                'required' => array ( "welldone_bg_mode", "=", "image" ),
            ),
            array (
                'id' => 'welldone_bg_custom_select',
                'type' => 'background',
                'title' => __ ( 'Select Background', 'welldone' ),
                'background-position' => false,
                'transparent' => false,
                'preview_media' => true,
                'background-color' => false,
                'background-size' => false,
                'background-attachment' => false,
                'background-repeat' => false,
                'compiler' => true,
               'required' => array ( "welldone_bg_mode", "=", "custom-image" ),
            ),
            array (
                'id' => 'welldone_bg_color',
                'type' => 'color',
                'title' => __ ( 'Background Color', 'welldone' ),
                'compiler' => true,
                'required' => array ( "welldone_theme_wrapper", "=", "boxed" ),
            ),
            array (
                'id' => 'welldone_bg_repeat',
                'type' => 'select',
                'title' => __ ( 'Background Repeat', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no-repeat' => __ ( "No Repeat", 'welldone' ),
                    'repeat' => __ ( "Repeat All", 'welldone' ),
                    'repeat-x' => __ ( "Repeat Horizontally", 'welldone' ),
                    'repeat-y' => __ ( "Repeat Vertically", 'welldone' ),
                    'inherit' => __ ( "Inherit", 'welldone' ),
                ),
                'placeholder' => __ ( 'Background Repeat', 'welldone' ),
                'required' => array ( "welldone_theme_wrapper", "=", "boxed" ),
            ),
            array (
                'id' => 'welldone_bg_position',
                'type' => 'select',
                'title' => __ ( 'Background Position', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    "left top" => __ ( "Left Top", 'welldone' ),
                    "left center" => __ ( "Left center", 'welldone' ),
                    "left bottom" => __ ( "Left Bottom", 'welldone' ),
                    "center top" => __ ( "Center Top", 'welldone' ),
                    "center center" => __ ( "Center Center", 'welldone' ),
                    "center bottom" => __ ( "Center Bottom", 'welldone' ),
                    "right top" => __ ( "Right Top", 'welldone' ),
                    "right center" => __ ( "Right center", 'welldone' ),
                    "right bottom" => __ ( "Right Bottom", 'welldone' ),
                ),
                'select2' => array ( 'allowClear' => false ),
                'placeholder' => __ ( 'Background Position', 'welldone' ),
                'required' => array ( "welldone_theme_wrapper", "=", "boxed" ),
            ),
            array (
                'id' => 'welldone_bg_size',
                'type' => 'select',
                'title' => __ ( 'Background Size', 'welldone' ),
                'compiler' => true,
                'options' => array (
                     "cover" => __ ( "Cover", 'welldone' ),
                    "inherit" => __ ( "Inherit", 'welldone' ),
                    "contain" => __ ( "Contain", 'welldone' ),
                ),
                'placeholder' => __ ( 'Background Size', 'welldone' ),
                'required' => array ( "welldone_theme_wrapper", "=", "boxed" ),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Page', 'welldone' ),
            ),
            array (
                'id' => 'welldone_content_background',
                'type' => 'background',
                'title' => __ ( 'Page Background', 'welldone' ),
                'preview_media' => true,
                'default' => array (
                    'background-color' => '#FFFFFF',
                )
            )
        ));
    //header sajad
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Header', 'welldone' ),
        'fields' => array (

             array (
               'id' => 'top_line_section',
                'type' => 'info',
               'desc' => __ ( 'Header Top Line Settings', 'welldone' ),
            ),
            array (
                'id' => 'header_top_background_dark',
                'type' => 'color',
                'title' => __ ( 'Select Top Background', 'welldone' ),
                'compiler' => true,
                'default' => '#2b383e',
            ),
            array (
                'id' => 'header_top_text_dark',
               'type' => 'color',
               'title' => __ ( 'Text Color', 'welldone' ),
               'default' => '#fff',
               'validate' => 'color',
            ),
            array (
               'id' => 'header_top_text_dark_hover',
               'type' => 'color',
               'title' => __ ( 'Hover Color', 'welldone' ),
               'default' => '#fff',
               'validate' => 'color',
           ),
            array (
               'id' => 'header_settings',
                'type' => 'info',
               'desc' => __ ( 'Header Setting', 'welldone' ),
            ),
            array (
                'id' => 'header_background',
                'type' => 'background',
                'title' => __ ( 'Select Background', 'welldone' ),
                'compiler' => true,
                'preview_media' => true,
            ),
            array (
                'id' => 'header_text',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#212121',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_hover',
                'type' => 'color',
                'title' => __ ( 'Hover Color', 'welldone' ),
                'default' => '#212121',
                'validate' => 'color',
            ),      
            array (
                'id' => 'header_cart_bg_dark',
                'type' => 'color',
                'title' => __ ( 'Cart Background Color', 'welldone' ),
                'default' => '#fff',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_cart_text_dark',
                'type' => 'color',
                'title' => __ ( 'Cart Text Color', 'welldone' ),
                'default' => '#212121',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_cart_text_dark_hover',
                'type' => 'color',
                'title' => __ ( 'Cart Text Color Hover', 'welldone' ),
                'default' => '#536dfe',
                'validate' => 'color',
            ),
        ));
    //menu sajad
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Main Menu', 'welldone' ),
        'fields' => array (

           array (
                'id' => 'header_menu_font_color',
                'type' => 'color',
                'title' => __ ( 'Font Color', 'welldone' ),
                'default' => '#212121',
                'validate' => 'color',

            ),
            array (
                'id' => 'header_menu_font_color_hover',
                'type' => 'color',
                'title' => __ ( 'Hover Font Color', 'welldone' ),
                'default' => '#536dfe',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_background_submenu_hover',
                'type' => 'color',
                'title' => __ ( 'Background Submenu Hover', 'welldone' ),
                'default' => '#f4f4f4',
                'validate' => 'color',
            ),
            array (
                'id' => 'header_font_submenu',
                'type' => 'color',
                'title' => __ ( 'Font  Submenu Color', 'welldone' ),
                'default' => '#212121',
                'validate' => 'color',
            ),
         array (
               'id' => 'header_font_submenu_hover',
                'type' => 'color',
                'title' => __ ( 'Hover Font Submenu Color', 'welldone' ),
                'default' => '#212121',
                'validate' => 'color',
            ),
        ));

    ///footer asif done
    $this->sections[] = array (
        'icon' => 'el-icon-cogs',
        'icon_class' => 'icon',
        'subsection' => true,
        'title' => __ ( 'Footer', 'welldone' ),
        'fields' => array (
             array (
               'id' => 'info_warning_dark_3s',
               'type' => 'info',
               'desc' => __ ( 'Customizations for footers top ', 'welldone' ),
           ),
           array (
                'id' => 'welldone_bg_custom_footer_top',
               'type' => 'color',
               'title' => __ ( 'Select Background', 'welldone' ),
               'compiler' => true,
               'default' => '#eaeeef',
           ),
           array (
                'id' => 'footer_top_link_color',
                'type' => 'color',
                'title' => __ ( 'Link Color', 'welldone' ),
                'default' => '#000000',
                'validate' => 'color',
           ),
           array (
                'id' => 'footer_top_hover_color',
                'type' => 'color',
                'title' => __ ( 'Hover Color', 'welldone' ),
                'default' => '#000000',
                'validate' => 'color',
           ),
            array (
               'id' => 'info_footer_middle',
               'type' => 'info',
               'desc' => __ ( 'Customizations for footer middle', 'welldone' ),
           ),
            array (
                'id' => 'welldone_bg_custom_footer',
                'type' => 'color',
                'title' => __ ( 'Select Background', 'welldone' ),
                'default' => '#374850',
                'compiler' => true,
            ),
           array (
                'id' => 'footer_heading_color',
                'type' => 'color',
                'title' => __ ( 'Heading Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
           array(
                'id' => 'footer_text_color',
                'type' => 'color',
                'title' => __ ( 'Text Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
           ),
           array (
                'id' => 'footer_link_color',
                'type' => 'color',
                'title' => __ ( 'Link  Color', 'welldone' ),
                'default' => '#bbc2c6',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_link_hover_color',
                'type' => 'color',
                'title' => __ ( 'Link  Hover Color', 'welldone' ),
                'default' => '#bbc2c6',
                'validate' => 'color',
            ),

            array (
               'id' => 'info_footer_bottom',
               'type' => 'info',
               'desc' => __ ( 'Customizations for footer middle', 'welldone' ),
           ),

           array (
                'id' => 'welldone_bg_custom_footer_bottom',
                'type' => 'color',
                'title' => __ ( 'Select Background', 'welldone' ),
                'default' => '#374850',
                'compiler' => true,
            ),
           array (
                'id' => 'footer_bottom_color',
                'type' => 'color',
                'title' => __ ( 'Bottom Link Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
           ),
             array (
               'id' => 'info_footer_copyright',
               'type' => 'info',
               'desc' => __ ( 'Customizations for copyright section', 'welldone' ),
           ),
            array (
                'id' => 'footer_copyright_background_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Background Color', 'welldone' ),
                'default' => '#ffffff',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_text_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Text Color', 'welldone' ),
                'default' => '#2b2b2b',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Color', 'welldone' ),
                'default' => '#2b2b2b',
                'validate' => 'color',
            ),
            array (
                'id' => 'footer_copyright_link_hover_color',
                'type' => 'color',
                'title' => __ ( 'Copyright Link Hover Color', 'welldone' ),
                'default' => '#2b2b2b',
                'validate' => 'color',
           ),
        ));

   $this->sections[] = array (
        'title' => __ ( 'General', 'welldone' ),
        'icon' => 'el-icon-cogs',
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Logo, Icon', 'welldone' ),
            ),
            array (
                'id' => 'logo',
                'type' => 'media',
                'url' => true,
                'title' => __ ( 'Logo', 'welldone' ),
                'compiler' => true,
                'default' => array (
                'url' => get_template_directory_uri () . '/images/logo.png',
                 )
            ),
            array (
                'id' => 'logo-mobile',
                'type' => 'media',
                'url' => true,
                'title' => __ ( 'Logo Mobile', 'welldone' ),
                'compiler' => true,
                'default' => array (
                'url' => get_template_directory_uri () . '/images/logo-mobile.png',
                 )
            ),
             array (
                'id' => 'logo-transparent',
                'type' => 'media',
                'url' => true,
                'title' => __ ( 'Logo Transparent', 'welldone' ),
                'compiler' => true,
                'default' => array (
                'url' => get_template_directory_uri () . '/images/logo-transparent.png',
                 )
            ),
           array (
                'id' => 'favicon',
                'type' => 'media',
                'title' => __ ( 'Favicon', 'welldone' ),
                'compiler' => true,
                'default' => array (
              'url' => get_template_directory_uri () . '/images/favicon.ico',
              )
            ),
        ));
 //Header Settings
    $this->sections[] = array (
        'title' => __ ( 'Header Options', 'welldone' ),
        'id'    => 'header-settings',
        'desc'  => esc_html__( '', 'welldone' ),
        'icon' => 'el-icon-website'
    );
    // Header top line
    $this->sections[] = array (
        'title'      => esc_html__( 'Header Top Line', 'welldone' ),
        'id'         => 'header-top-set',
        'subsection' => true,
        'fields'     => array(
             array (
                'id' => 'welldone_header_top',
                'type' => 'switch',
                'title' => __ ( 'Show Header Top Line', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
             array (
                'id' => 'head_icons_info',
                'type' => 'section',
                'title' => __ ( 'Social Icons', 'welldone' ),
                'subtitle' => __ ( 'Icons that you want to hide, just leave their fields empty.', 'welldone' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
                'required' => array( 'welldone_header_top', '=', '1' )
            ),
            array (
                'id' => 'header_icon_fb',
                'type' => 'text',
                'title' => __ ( 'Facebook', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'validate' => 'url',
                'placeholder' => 'https://www.facebook.com/'
            ),
	        array (
		        'id' => 'header_icon_linkedin',
		        'type' => 'text',
		        'title' => __ ( 'Linkedin', 'welldone' ),
		        'compiler' => true,
		        'required' => array( 'welldone_header_top', '=', '1' ),
		        'validate' => 'url',
		        'placeholder' => 'https://www.linkedin.com/'
	        ),
            array (
                'id' => 'header_icon_twitter',
                'type' => 'text',
                'title' => __ ( 'Twitter', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'https://www.twitter.com/'
            ),
            array (
                'id' => 'header_icon_gmail',
                'type' => 'text',
                'validate' => 'url',
                'title' => __ ( 'Google+', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'https://plus.google.com/'
            ),
            array (
                'id' => 'header_icon_pin',
                'type' => 'text',
                'title' => __ ( 'Pinterest', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'http://pinterest.com/'
            ),
             array (
                'id' => 'header_icon_mail',
                'type' => 'text',
                'title' => __ ( 'Email', 'welldone' ),
                'validate' => 'email',
                'compiler' => true,
                'required' => array( 'welldone_header_top', '=', '1' ),
                'placeholder' => 'info@company.com'
            ),
            array(
                'id'     => 'section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
            array (
                'id' => 'header_show_auth',
                'type' => 'switch',
                'title' => __ ( 'Login / Register Buttons', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'required' => array( 'welldone_header_top', '=', '1' )
            ),
            array (
                'id' => 'header_fb_tw_login',
                'type' => 'switch',
                'title' => __ ( 'Facebook / Twitter Login', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'required' => array( 'welldone_header_top', '=', '1' )
            )
        ));
    //Header 
    $this->sections[] = array (
        'title'      => esc_html__( 'Header', 'welldone' ),
        'id'         => 'header-main',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_site_header',
                'type' => 'image_select',
                //'tiles' => true,
                'title' => __ ( 'Site Header Type', 'welldone' ),
                'compiler' => true,
                'options' => array (
                '1' => array (
                              'alt' => 'Header 1',
                              'img' => get_template_directory_uri () . '/images/headers/Header1.png'
                    ),
                '2' => array (
                        'alt' => 'Header 2',
                        'img' => get_template_directory_uri () . '/images/headers/Header2.png'
                    ),
                '3' => array (
                        'alt' => 'Header 3',
                        'img' => get_template_directory_uri () . '/images/headers/Header3.png'
                ),
                '4' => array (
                        'alt' => 'Header 4',
                        'img' => get_template_directory_uri () . '/images/headers/Header4.png'
                ),

           ),
               'default' => '1'
            ),
       
            array (
                'id' => 'sticky-header',
                'type' => 'switch',
                'title' => __('Enable Sticky Header','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'header-search',
                'type' => 'switch',
                'title' => __('Show Search','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'show-minicart',
                'type' => 'switch',
                'title' => __('Show Mini cart','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
           array (
                'id' => 'show-currency-switcher',
               'type' => 'switch',
               'title' => __('Show Currency Switcher','welldone'),
               'default' => '1',
               'compiler' => true,
               'on' => __('Yes','welldone'),
               'off' => __('No','welldone'),
           ),
           array (
                'id' => 'show-wpml-switcher',
                'type' => 'switch',
                'title' => __('Show WPML language Switcher','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
           ),
           array (
                'id' => 'show-woo-pages',
                'type' => 'switch',
                'title' => __('Show Woocommerce User Menu Pages','welldone'),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
           ),
        ));
   // Breadcrumbs And Tilte Settings
    $this->sections[] = array (
        'icon' => 'el-icon-minus',
        'icon_class' => 'icon',
        'title' => __('Title and Breadcrumbs','welldone'),
        'fields' => array (
            array (
                'id' => 'welldone_hide_breadcrumb',
                'type' => 'switch',
                'title' => __('Hide Breadcrumbs','welldone'),
                'compiler' => true,
                'default' => '0',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_hide_page_title',
                'type' => 'switch',
                'title' => __('Hide Title','welldone'),
                'compiler' => true,
                'default' => '0',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
        ));
    //Body Settings
    $this->sections[] = array (
        'title' => __ ( 'Body, Page', 'welldone' ),
        'icon' => 'el-icon-laptop',
        'fields' => array (
           array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Page Layout', 'welldone' ),
                'desc' => __ ( 'Select default page layout for the theme', 'welldone' ),
            ),
            array (
                'id' => 'welldone_page_layout',
                'type' => 'image_select',
                'title' => __ ( 'Page Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'no'
            ),
            array (
                'id' => 'welldone_page_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Page Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'select2' => array ( 'allowClear' => false ),
                'default' => 'page-sidebar-1',
                'required' => array ( "welldone_page_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_page_sidebar_right',
                'type' => 'select',
                'select2' => array ( 'allowClear' => false ),
                'title' => __ ( 'Page Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'page-sidebar-2',
                'required' => array ( "welldone_page_layout", "=", array ( "right", "both" ) )
            ),
         
           array (
               'id' => 'page-comment',
                'type' => 'switch',
                'title' => __('Show Comment Form on Page','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
        
           array (
               'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Social Share Plugin', 'welldone' ),
           ),
           array (
                'id' => 'welldone_social_share',
                'type' => 'select',
                'title' => __('Hide Social Share','welldone'),
                'desc' => __('select Post Type on which you want to hide social share plugin.Works only if the Font Awesome share Icons plugin is active','welldone'),
                'compiler' => true,
                'placeholder' => __('Select post Types','welldone'),
                'data' => 'post_types',
                'multi' => true
            ),
          
        ));
   //Footer Settings
    $this->sections[] = array (
        'title' => __ ( 'Footer Options', 'welldone' ),
        'id'    => 'footer-settings',
        'desc'  => esc_html__( '', 'welldone' ),
        'icon' => 'el-icon-website'
    );
    // Footer Top
    $this->sections[] = array (
        'title'      => esc_html__( 'Footer Top', 'welldone' ),
        'id'         => 'footer-top-set',
        'subsection' => true,
        'fields'     => array(
             array (
                'id' => 'welldone_show_footer_top',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Top Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
              array (
                'id' => 'welldone_show_footer_l_menu',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Top Left Menu', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
              array (
                'id' => 'welldone_show_footer_r_menu',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Top Right Menu', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
        ));
    //Header 
    $this->sections[] = array (
        'title'      => esc_html__( 'Footer Middle', 'welldone' ),
        'id'         => 'footer-middle',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_show_footer_mid',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Middle Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
            array (
                'id' => 'welldone_footer_widget_columns',
                'type' => 'image_select',
                'title' => __ ( 'Footer Widget Columns', 'welldone' ),
                'compiler' => true,
                'default' => '5',
                'options' => array (
                    '1' => array (
                        'alt' => __ ( '1 Column', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/1col.png'
                    ),
                    '2' => array (
                        'alt' => __ ( '2 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/2col.png'
                    ),
                    '3' => array (
                        'alt' => __ ( '3 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/3col.png'
                    ),
                    '4' => array (
                        'alt' => __ ( '4 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/4col.png'
                    ),
                    '5' => array (
                        'alt' => __ ( '4 Columns', 'welldone' ),
                        'img' => get_template_directory_uri () . '/images/default/5col.png'
                    )
                ),
            ),
            array (
                'id' => 'welldone_show_footer_top_widgets',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Middle Widgets', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
        ));
    $this->sections[] = array (
        'title'      => esc_html__( 'Footer Bottom', 'welldone' ),
        'id'         => 'footer-bottom',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_show_footer_bottom',
                'type' => 'switch',
                'title' => __ ( 'Show Footer Bottom Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
            
            array (
                'id' => 'footer_bottom_subscribe',
                'type' => 'switch',
                'title' => __ ( 'Show Subscribe Form', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
                'required' => array( 'welldone_show_footer_bottom', '=', '1' )
            ),
             array (
                'id' => 'footer_icons_info',
                'type' => 'section',
                'title' => __ ( 'Social Icons', 'welldone' ),
                'subtitle' => __ ( 'Icons that you want to hide, just leave their fields empty.', 'welldone' ),
                'indent'   => true, // Indent all options below until the next 'section' option is set.
                'required' => array( 'welldone_show_footer_bottom', '=', '1' )
            ),
            array (
                'id' => 'footer_icon_fb',
                'type' => 'text',
                'title' => __ ( 'Facebook', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'validate' => 'url',
                'placeholder' => 'https://www.facebook.com/'
            ),
            array (
                'id' => 'footer_icon_twitter',
                'type' => 'text',
                'title' => __ ( 'Twitter', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'https://www.twitter.com/'
            ),
            array (
                'id' => 'footer_icon_gmail',
                'type' => 'text',
                'validate' => 'url',
                'title' => __ ( 'Google+', 'welldone' ),
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'https://plus.google.com/'
            ),
            array (
                'id' => 'footer_icon_pin',
                'type' => 'text',
                'title' => __ ( 'Pinterest', 'welldone' ),
                'validate' => 'url',
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'http://pinterest.com/'
            ),
             array (
                'id' => 'footer_icon_mail',
                'type' => 'text',
                'title' => __ ( 'Email', 'welldone' ),
                'validate' => 'email',
                'compiler' => true,
                'required' => array( 'welldone_show_footer_bottom', '=', '1' ),
                'placeholder' => 'info@company.com'
            ),
            array(
                'id'     => 'footer-section-end',
                'type'   => 'section',
                'indent' => false, // Indent all options below until the next 'section' option is set.
            ),
        ));
    $this->sections[] = array (
        'title'      => esc_html__( 'Copyright Footer Section', 'welldone' ),
        'id'         => 'footer-copy',
        'subsection' => true,
        'fields'     => array(
            array (
                'id' => 'welldone_show_copyright_block',
                'type' => 'switch',
                'title' => __ ( 'Show Copyright Section', 'welldone' ),
                'default' => '1',
                'compiler' => true,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone')
            ),
            array (
                'id' => 'welldone_copyright',
                'type' => 'textarea',
                'title' => __ ( 'Copyright Text', 'welldone' ),
                'default' => '&copy; 2016 <a href="#">Welldone</a>. All right reserved',
                'compiler' => true,
                'validate' => 'html_custom',
                'required' => array( 'welldone_show_copyright_block', '=', '1' ),
                'allowed_html' => array (
                    'a' => array (
                        'href' => array (),
                        'title' => array ()
                    ),
                    'br' => array (),
                    'em' => array (),
                    'strong' => array (),
                    'span' => array ()
                ),
                'desc' => __ ( 'HTML allowed tags a, br, em, strong, span', 'welldone' ),
            ),
            array (
                'id' => 'welldone_footer_text',
                'type' => 'textarea',
                'title' => __ ( 'Text Info To The Right', 'welldone' ),
                'default' => 'with love <span class="icon-favorite color-heart"></span> from <a href="http://themeforest.net/user/etheme">etheme</a>',
                'compiler' => true,
                'validate' => 'html_custom',
                'required' => array( 'welldone_show_copyright_block', '=', '1' ),
                'allowed_html' => array (
                    'a' => array (
                        'href' => array (),
                        'title' => array ()
                    ),
                    'br' => array (),
                    'em' => array (),
                    'strong' => array (),
                    'span' => array (
                        'class' => array ()
                    )
                ),
                'desc' => __ ( 'HTML allowed tags a, br, em, strong, span', 'welldone' ),
            ),
        ));
   
    //blog and single post
    $this->sections[] = array (
        'icon' => 'el-icon-blogger',
        'title' => __ ( 'Blog & Single Post', 'welldone' ),
        'fields' => array (
            // blog options
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Blog Options', 'welldone' ),
            ),
            array (
                'id' => 'welldone_blog_layout',
                'type' => 'image_select',
                'title' => __ ( 'Blog Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'no'
            ),
            array (
                'id' => 'welldone_blog_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Blog Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'select2' => array ( 'allowClear' => false ),
                'default' => 'blog-sidebar-1',
                'required' => array ( "welldone_blog_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_blog_sidebar_right',
                'type' => 'select',
                'select2' => array ( 'allowClear' => false ),
                'title' => __ ( 'Blog Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'blog-sidebar-2',
                'required' => array ( "welldone_blog_layout", "=", array ( "right", "both" ) )
            ),
            array (
                'id' => 'welldone_blog_type',
                'type' => 'button_set',
                'title' => __('Blog Type','welldone'),
                'compiler' => true,
                'options' => array (
                    'classic' => __ ( 'Classic(Default)', 'welldone' ),
                    'blog-masonry' => __ ( "Blog Masonry", 'welldone' )
                ),
                'default' => 'classic'
            ),
            array (
                'id' => 'welldone_blog_masonry_columns',
                'type' => 'image_select',
                'title' => __ ( 'Select Blog Masonry Columns', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    '2' => array (
                        'alt' => '2 Columns',
                        'img' => get_template_directory_uri () . '/images/default/2col.png',
                    ),
                    "3" => array (
                        'alt' => '3 Columns',
                        'img' => get_template_directory_uri () . '/images/default/3col.png',
                    ),
                    "4" => array (
                        'alt' => '4 Columns',
                        'img' => get_template_directory_uri () . '/images/default/4col.png',
                    ),
                    "5" => array (
                        'alt' => '5 Columns',
                        'img' => get_template_directory_uri () . '/images/default/5col.png',
                    ),
                ),
                'default' => '4',
                'required' => array ( 'welldone_blog_type', '=', 'blog-masonry' ),
            ),
           //skip posts
            array (
                'id' => 'welldone_exclude_posts',
                'type' => 'text',
                'validate' => 'comma_numeric',
                'title' => __ ( 'Exclude Posts', 'welldone' ),
                'compiler' => true,
                'description' => __('input post ids comma seperated','welldone')
            ),
            // blog title
            array (
                'id' => 'welldone_blog_title',
                'type' => 'text',
                'validate' => 'no_html',
                'title' => __ ( 'Blog Page Title', 'welldone' ),
                'compiler' => true,
                'default' => 'Blog'
            ),
            // blog pagination type
            array (
                'id' => 'welldone_blog_pagination_type',
                'type' => 'select',
                'title' => __ ( 'Blog Classic Pagination Type', 'welldone' ),
                'compiler' => true,
                'select2' => array ( 'allowClear' => false ),
                // Must provide key => value pairs for select options
                'options' => array (
                    'pagination' => 'Pagination',
                    'more_button' => 'Ajax More Button',
                ),
                'default' => 'more_button'
            ),
            // hide blog post title
            array (
                'id' => 'welldone_hide_blog_post_title',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Title', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            // hide blog post author
            array (
                'id' => 'welldone_blog_excerpt',
                'type' => 'switch',
                'title' => __('Show Excerpt','welldone'),
                'default' => '0',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_blog_excerpt_length',
                'type' => 'text',
                'title' => __('Excerpt Length','welldone'),
                'desc' => __('The number of words','welldone'),
                'default' => '80',
            ),
            array (
                'id' => 'welldone_excerpt_type',
                'type' => 'button_set',
                'title' => __('Excerpt Type','welldone'),
                'options' => array ( 'text' => 'Text', 'html' => 'HTML' ),
                'default' => 'html'
            ),
            array (
                'id' => 'welldone_hide_blog_post_author',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Author Name', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_hide_blog_post_category',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Category', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_hide_blog_post_tags',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Tags', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'desc' => __ ( 'Single Post Options', 'welldone' ),
            ),
            array (
                'id' => 'welldone_post_layout',
                'type' => 'image_select',
                'title' => __ ( 'Post Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                   'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                   'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                   ),
                  'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    )
                ),
                'default' => 'left'
            ),
            array (
                'id' => 'welldone_post_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Post Left Sidebar', 'welldone' ),
                'compiler' => true,
                'select2' => array ( 'allowClear' => false ),
                'options' => $welldone_sidebar,
                'default' => 'single-post-sidebar',
                'required' => array ( "welldone_post_layout", "=", array ( "left", "both" ) )
            ),
            array (
               'id' => 'welldone_post_sidebar_right',
                'type' => 'select',
                'select2' => array ( 'allowClear' => false ),
                'title' => __ ( 'Post Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'single-post-sidebar',
                'required' => array ( "welldone_post_layout", "=", array ( "right", "both" ) )
            ),
            //Show Prev/Next Navigation
            array (
                'id' => 'welldone_post_page_nav',
                'type' => 'switch',
                'title' => __ ( 'Show Prev/Next Navigation', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            array(         
                'id'       => 'prev_post_block_background',
                'type'     => 'background',
                'title'    => __('Previous Post Block Navigation Background', 'welldone'),
                'background-position' => false,
                'transparent' => false,
                // 'preview_media' => true,
                'background-color' => true,
                'background-size' => false,
                'background-attachment' => false,
                // 'background-repeat' => false,
                'required' => array( 'welldone_post_page_nav', '=', '1' ),
                'default'  => array(
                    'background-color' => '#1e73be',
                    'background-image' => get_template_directory_uri () . '/images/bg-images/next_prev_post_bg.png',
                )
            ),

            array(         
                'id'       => 'next_post_block_background',
                'type'     => 'background',
                'title'    => __('Next Post Block Navigation Background', 'welldone'),
                'background-position' => false,
                'transparent' => false,
                // 'preview_media' => true,
                'background-color' => true,
                'background-size' => false,
                'background-attachment' => false,
                // 'background-repeat' => false,
                'required' => array( 'welldone_post_page_nav', '=', '1' ),
                'default'  => array(
                    'background-color' => '#1e73be',
                    'background-image' => get_template_directory_uri () . '/images/bg-images/next_prev_post_bg.png',
                )
            ),


            //Show Author Info
            array (
                'id' => 'welldone_hide_post_author',
                'type' => 'switch',
                'title' => __ ( 'Hide Blog Posts Author Name', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            array (
                'id' => 'welldone_hide_post_category',
                'type' => 'switch',
                'title' => __ ( 'Hide Posts Category', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            array (
                'id' => 'welldone_hide_post_tags',
                'type' => 'switch',
                'title' => __ ( 'Hide Posts Tags', 'welldone' ),
                'compiler' => true,
                'default' => 0,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            //Show Comments
            array (
                'id' => 'welldone_post_comments',
                'type' => 'switch',
                'title' => __ ( 'Show Comments', 'welldone' ),
                'compiler' => true,
                'default' => 1,
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),

            //Show Addthis Buttons above Content
        ));

    $this->sections[] = array (
       'title' => __ ( 'Woocommerce', 'welldone' ),
        'icon' => 'el-icon-gift',
        'fields' => array (
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'General Woocommerce Settings', 'welldone' )
            ),
          
            array (
                'id' => 'welldone_product_sale_label',
                'type' => 'switch',
                'title' => __ ( 'Show Sale Label', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                 'on' => __ ( 'Yes', 'welldone' ),
                 'off' => __ ( 'No', 'welldone' ),
            ),
           
            array (
                'id' => 'welldone_product_sale_label_text_type',
                'type' => 'button_set',
                'title' => __ ( 'Sale Label Text Type', 'welldone' ),
                'options' => array (
                    'custom-text' => __ ( 'Custom Text', 'welldone' ),
                    'per_sale_price' => __ ( 'Percentage saved', 'welldone' ),
                ),
                'default' => 'per_sale_price',
                'required' => array ( 'welldone_product_sale_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_sale_label_text',
                'type' => 'text',
                'title' => __ ( 'Sale Label Text', 'welldone' ),
                'default' => 'Sale',
                'required' => array ( 'welldone_product_sale_label_text_type', '=', 'custom-text' )
            ),
            array (
                'id' => 'welldone_product_new_label',
                'type' => 'switch',
                'title' => __('Show New Label','welldone'),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_product_new_label_time',
                'type' => 'text',
                'title' => __ ( 'New Label Time Period', 'welldone' ),
                'default' => '7',
                'validate' => 'numeric',
                'desc' => __ ( 'Number Of Days a product remains as new.Default is 7 days.', 'welldone' ),
                'required' => array ( 'welldone_product_new_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_product_new_label_text',
                'type' => 'text',
                'title' => __ ( 'New Label Text', 'welldone' ),
                'default' => 'New',
                'required' => array ( 'welldone_product_new_label', '=', '1' )
            ),
            array (
                'id' => 'welldone_show_add_to_cart_button',
                'type' => 'switch',
                'title' => __ ( 'Show Add To Cart', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
           array (
            'id' => 'welldone_show_quick_button',
            'type' => 'switch',
            'title' => __ ( 'Show Quick View', 'welldone' ),
            'compiler' => true,
            'default' => '1',
            'on' => __('Yes','welldone'),
            'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_price',
                'type' => 'switch',
                'title' => __ ( 'Show Product Price', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_wishlist_button',
                'type' => 'switch',
                'title' => __ ( 'Show Wishlist', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' => __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_compare_button',
                'type' => 'switch',
                'title' => __ ( 'Show Compare', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_product_category',
                'type' => 'switch',
                'title' => __ ( 'Show Category', 'welldone' ),
                'compiler' => true,
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'welldone_show_product_tags',
                'type' => 'switch',
                'title' => __ ( 'Show Tags', 'welldone' ),
                'compiler' => true,
                'desc' => __ ( 'Tags visible on single product page only', 'welldone' ),
                'default' => '1',
                'on' => __('Yes','welldone'),
                'off' =>  __('No','welldone'),
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Shop and Category Page', 'welldone' )
            ),
            
            array (
                'id' => 'welldone_products_grid_columns',
                'type' => 'select',
                'title' => __ ( 'Select Grid Columns', 'welldone' ),
                'options' => array (
                    'one-in-row' => "1",
                    'two-in-row' => "2",
                    'three-in-row' => "3",
                    'four-in-row' => "4",
                    'five-in-row' => "5",
                    'six-in-row' => "6",
                    'seven-in-row' => "7",
                    'eight-in-row' => "8",
                ),
                'default' => 'four-in-row'
            ),
            array (
                'id' => 'welldone_products_perpage',
                'type' => 'text',
                'title' => __ ( 'Products per Page', 'welldone' ),
                'compiler' => true,
                'validate' => 'comma_numeric',
                'description' => __ ( 'Comma-seperated. Default: 9,15,30', 'welldone' ),
                'default' => '9,15,30'
            ),
            array (
                'id' => 'welldone_shop_layout',
                'type' => 'image_select',
                'title' => __ ( 'Shop Page Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'left'
            ),
            array (
                'id' => 'welldone_shop_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Shop Page Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'select2' => array ( 'allowClear' => false ),
                'default' => 'shop-page-sidebar-1',
                'required' => array ( "welldone_shop_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_shop_sidebar_right',
                'type' => 'select',
                'title' => __ ( 'Shop Page Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'page-sidebar-2',
                'select2' => array ( 'allowClear' => false ),
                'required' => array ( "welldone_shop_layout", "=", array ( "right", "both" ) )
            ),
            array (
                'id' => 'info_warning',
                'type' => 'info',
                'title' => __ ( 'Product Page', 'welldone' )
            ),
            array (
                'id' => 'welldone_product_page_style',
                'type' => 'select',
                'title' => __ ( 'Product Page Style', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'v_1' => __ ( 'Style 1', 'welldone' ),
                    'v_2' => __ ( 'Style 2', 'welldone' ),
                ),
                'default' => 'v_1',
                'select2' => array ( 'allowClear' => false ),
                'desc' => __ ( 'Choose Style for the product page. Default is style 1', 'welldone' ),
            ),
            array (
                'id' => 'welldone_product_layout',
                'type' => 'image_select',
                'title' => __ ( 'Product Page Layout', 'welldone' ),
                'compiler' => true,
                'options' => array (
                    'no' => array (
                        'alt' => 'Full Width',
                        'img' => ReduxFramework::$_url . 'assets/img/1c.png'
                    ),
                    'left' => array (
                        'alt' => 'Left Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cl.png'
                    ),
                    'right' => array (
                        'alt' => 'Right Sidebar',
                        'img' => ReduxFramework::$_url . 'assets/img/2cr.png'
                    ),
                    'both' => array (
                        'alt' => 'Both Sidebars',
                        'img' => ReduxFramework::$_url . 'assets/img/3cm.png'
                    ),
                ),
                'default' => 'right'
            ),
            array (
                'id' => 'welldone_product_sidebar_left',
                'type' => 'select',
                'title' => __ ( 'Product Page Left Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'single-product-sidebar',
                'select2' => array ( 'allowClear' => false ),
                'required' => array ( "welldone_product_layout", "=", array ( "left", "both" ) )
            ),
            array (
                'id' => 'welldone_product_sidebar_right',
                'type' => 'select',
                'title' => __ ( 'Product Page Right Sidebar', 'welldone' ),
                'compiler' => true,
                'options' => $welldone_sidebar,
                'default' => 'single-product-sidebar',
                'select2' => array ( 'allowClear' => false ),
                'required' => array ( "welldone_product_layout", "=", array ( "right", "both" ) )
           ),
            array (
                'id' => 'welldone_product_bottom_products',
                'type' => 'sortable',
                'title' => __ ( 'Select Product Types To Show at the bottom of Product', 'welldone' ),
                'compiler' => true,
                'mode' => 'checkbox',
                'options' => array (
                    'Featured' => __ ( 'Featured Products', 'welldone' ),
                    'Popular' => __ ( 'Popular Products', 'welldone' ),
                    'TopRated' => __ ( 'Top Rated Products', 'welldone' ),
                    'Latest' => __ ( 'Latest Products', 'welldone' )
                ),
            ),
        ));
            $this->sections[] = array (
                'title' => __ ( 'Import / Export', 'welldone' ),
                'desc' => __ ( 'Import and Export your settings from file, text or URL.', 'welldone' ),
                'icon' => 'el-icon-refresh',
                'fields' => array (
                    array (
                        'id' => 'opt-import-export',
                        'type' => 'import_export',
                        'title' => __('Import Export','welldone'),
                        'subtitle' => __('Save and restore your Redux options','welldone'),
                        'full_width' => false,
                    ),
                ),
            );
            $this->sections[] = array (
                'type' => 'divide',
            );
            if ( file_exists ( trailingslashit ( get_template_directory () . '/inc/admin/README.html' ) ) ){
                $tabs[ 'docs' ] = array (
                    'icon' => 'el-icon-book',
                    'title' => __ ( 'Documentation', 'welldone' ),
                    //'content' => nl2br ( $wp_filesystem->get_contents(trailingslashit ( get_template_directory () . '/inc/admin/README.html') )
                    'content' => nl2br ( wp_remote_get ( trailingslashit ( get_template_directory () . '/inc/admin/README.html' ) ) )
                );
            }
        }

            public function setHelpTabs() {}

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                    'opt_name'           => 'welldone_settings',
                    // This is where your data is stored in the database and also becomes your global variable name.
                    'display_name'       => $theme->get ( 'Name' ) . ' ' . 'Settings',
                    // Name that appears at the top of your panel
                    'display_version'    => $theme->get( 'Version' ),
                    // Version that appears at the top of your panel
                    'menu_type'          => 'menu',
                    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                    'allow_sub_menu'     => true,
                    // Show the sections below the admin menu item or not
                    'menu_title'         => esc_html__( 'Theme Options', 'welldone' ),
                    'page_title'         => esc_html__( 'Theme Options', 'welldone' ),
                    // You will need to generate a Google API key to use this feature.
                    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                    'google_api_key'     => '',
                    // Must be defined to add google fonts to the typography module

                    'async_typography'   => false,
                    // Use a asynchronous font on the front end or font string
                    'admin_bar'          => true,
                    // Show the panel pages on the admin bar
                    'global_variable'    => '',
                    // Set a different name for your global variable other than the opt_name
                    'dev_mode'           => false,
                    // Show the time the page took to load, etc
                    'forced_dev_mode_off' => true,
                    'customizer'         => true,
                    // Enable basic customizer support

                    // OPTIONAL -> Give you extra features
                    'page_priority'      => null,
                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                    'page_parent'        => 'themes.php',
                    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                    'page_permissions'   => 'manage_options',
                    // Permissions needed to access the options panel.
                    'menu_icon'          => '',
                    // Specify a custom URL to an icon
                    'last_tab'           => '',
                    // Force your panel to always open to a specific tab (by id)
                    'page_icon'          => 'icon-themes',
                    // Icon displayed in the admin panel next to your menu_title
                    'page_slug'          => 'welldone_options',
                    // Page slug used to denote the panel
                    'save_defaults'      => true,
                    // On load save the defaults to DB before user clicks save or not
                    'default_show'       => false,
                    // If true, shows the default value next to each field that is not the default value.
                    'default_mark'       => '',
                    // What to print by the field's title if the value shown is default. Suggested: *
                    'show_import_export' => true,
                    // Shows the Import/Export panel when not used as a field.

                    // CAREFUL -> These options are for advanced use only
                    'transient_time'     => 60 * MINUTE_IN_SECONDS,
                    'output'             => true,
                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                    'output_tag'         => true,
                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                    'database'           => '',
                    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                    'system_info'        => false,
                    // REMOVE

                    // HINTS
                    'hints'              => array(
                        'icon'          => 'icon-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );

                // Panel Intro text -> before the form
                if ( ! isset( $this->args['global_variable'] ) || $this->args['global_variable'] !== false ) {
                    if ( ! empty( $this->args['global_variable'] ) ) {
                        $v = $this->args['global_variable'];
                    } else {
                        $v = str_replace( '-', '_', $this->args['opt_name'] );
                    }
                    $this->args['intro_text'] = sprintf( esc_html__( '', 'welldone' ), $v );
                } 

            
            }

        }

        global $reduxwelldoneSettings;
        $reduxwelldoneSettings = new Redux_Framework_welldone_settings();
    }
