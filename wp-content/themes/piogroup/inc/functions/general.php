<?php 
// **********************************************************************//
// ! Menus
// **********************************************************************//

if(!function_exists('welldone_register_menus')) {
    function welldone_register_menus() {
        register_nav_menus(array(
            'main-menu' => esc_html__('Main menu', 'welldone'),
            'sidebar-menu' => esc_html__('Sidebar menu', 'welldone'),
            'footer-menu-left' => esc_html__('Footer Top left menu', 'welldone'),
            'footer-menu-right' => esc_html__('Footer Top right menu', 'welldone'),
            'mobile-menu' => esc_html__('Mobile menu', 'welldone'),
            'account-menu' => esc_html__('Account menu', 'welldone')
        ));
    }
}
add_action('init', 'welldone_register_menus');

class Welldone_Walker_Nav_Primary extends Walker_Nav_Menu {
    function start_lvl( &$output, $depth = 0, $args = array() ){ //ul
        $indent = str_repeat("\t",$depth);
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        $output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";
    }
    
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){ //li a span
        
        $indent = ( $depth ) ? str_repeat("\t",$depth) : '';
        
        $li_attributes = '';
        $class_names = $value = '';
        
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        
        $classes[] = ($args->walker->has_children) ? 'dropdown' : '';
        $classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
        $classes[] = 'menu-item-' . $item->ID;
        if( $depth && $args->walker->has_children ){
            $classes[] = 'dropdown-submenu';
        }
        
        $class_names =  join(' ', apply_filters('nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = ' class="' . esc_attr($class_names) . '"';
        
        $id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
        
        $output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';
        
        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr($item->url) . '"' : '';
        
        $attributes .= ( $args->walker->has_children ) ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

        $prepend = '<span class="link-name">';
        $append = '</span>';
    
        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';

        $item_output .= $args->link_before;
        $item_output .= ( 0 == $depth) ? $prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append : apply_filters( 'the_title', $item->title, $item->ID );
        $item_output .= $args->link_after;

        // $item_output .= ( 0 == $depth) ? '<span class="arrow"></span>' : '';
        $item_output .= ( $depth == 0 && $args->walker->has_children ) ? ' <span class="caret caret--dots"></span></a>' : '</a>';
        $item_output .= $args->after;
        
        $output .= apply_filters ( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
        
    }
}



function welldone_current_page_id(){
    global $post, $wp_query;
    $current_page_id = '';
    // Get The Page ID You Need
    //wp_reset_postdata();
    if(class_exists("woocommerce")) {
        if( is_shop() ){ 
            $current_page_id  =  get_option ( 'woocommerce_shop_page_id' );
        }elseif(is_cart()) {
            $current_page_id  =  get_option ( 'woocommerce_cart_page_id' );
        }elseif(is_checkout()){
            $current_page_id  =  get_option ( 'woocommerce_checkout_page_id' );
        }elseif(is_account_page()){
            $current_page_id  =  get_option ( 'woocommerce_myaccount_page_id' );
        }elseif(is_view_order_page()){
            $current_page_id  = get_option ( 'woocommerce_view_order_page_id' );
        }
    }
    if($current_page_id=='') {
        if ( is_home () && is_front_page () ) {
            $current_page_id = '';
        } elseif ( is_home () ) {
            $current_page_id = get_option ( 'page_for_posts' );
        } elseif ( is_search () || is_category () || is_tag () || is_tax () ) {
            $current_page_id = '';
        } elseif ( !is_404 () ) {
            $id = $post->ID;
            if (isset($id)) {
                $current_page_id = $post->ID;
            }
        }
    }
    return $current_page_id;
}

function welldone_page_title_header () {
    global $welldone_settings,$post;
    $current_page_id = welldone_current_page_id();
    $id =  $current_page_id;
    $hide_page_title = ( get_post_meta (  $id, 'welldone_hide_page_title', true ) ) ? get_post_meta (  $id, 'welldone_hide_page_title', true ) : '';
    if(!$hide_page_title) {
        $hide_page_title = isset( $welldone_settings[ 'welldone_hide_page_title' ] ) ? $welldone_settings[ 'welldone_hide_page_title' ] : '';
    }
    $hide_page_breadcrumb = ( get_post_meta (  $id, 'welldone_hide_breadcrumb', true ) ) ? get_post_meta (  $id, 'welldone_hide_breadcrumb', true ) : '';
    if(!$hide_page_breadcrumb) {
        $hide_page_breadcrumb = isset( $welldone_settings[ 'welldone_hide_breadcrumb' ] ) ? $welldone_settings[ 'welldone_hide_breadcrumb' ] : '';
    }

    $position = ( get_post_meta (  $id, 'welldone_breadcrumb_title_position', true ) ) ? get_post_meta (  $id, 'welldone_breadcrumb_title_position', true ) : '';
    if(!$position) {
        $position = isset( $welldone_settings[ 'welldone_breadcrumb_title_position' ] ) ? $welldone_settings[ 'welldone_breadcrumb_title_position' ] : '';
    }
    $size = ( get_post_meta (  $id, 'welldone_bread_title_size', true ) ) ? get_post_meta (  $id, 'welldone_bread_title_size', true ) : '';
    if(!$size) {
        $size = isset( $welldone_settings[ 'welldone_bread_title_size' ] ) ? $welldone_settings[ 'welldone_bread_title_size' ] : '';
    }

    $bg_type = ( get_post_meta (  $id, 'welldone_bread_title_bg', true )  ) ? get_post_meta (  $id, 'welldone_bread_title_bg', true ) : '';
    if(!$bg_type) {
        $bg_type = isset( $welldone_settings[ 'welldone_bread_title_bg' ] ) ? $welldone_settings[ 'welldone_bread_title_bg' ] : '';
    }
    $parallax = false;
    //welldone_bread_title_color
    if($bg_type=='bg-img'){
        $parallax = ( get_post_meta (  $id, 'welldone_use_parallax', true )  ) ? get_post_meta (  $id, 'welldone_use_parallax', true ) : '';
        if(!$parallax) {
            $parallax = isset( $welldone_settings[ 'welldone_use_parallax' ] ) ? $welldone_settings[ 'welldone_use_parallax' ] : '';
        }
    }
    $use_full = false;
    $use_full = ( get_post_meta (  $id, 'welldone_breadcrumb_use_full', true )  ) ? get_post_meta (  $id, 'welldone_breadcrumb_use_full', true ) : '';
        if(!$use_full) {
            $use_full = isset( $welldone_settings[ 'welldone_breadcrumb_use_full' ] ) ? $welldone_settings[ 'welldone_breadcrumb_use_full' ] : '';
        }
    if($use_full =='no'){
        $use_full = '';
    }
    if(!$hide_page_title || !$hide_page_breadcrumb){ ?>
        <div class="page-top <?php echo esc_attr($position).' '.esc_attr($size).' '.( ($parallax and $bg_type=='bg-img' )?"parallax":"") ?>"
                <?php if($parallax and $bg_type=='bg-img'){ echo 'data-top="background-position:50% 0px;" data-bottom-top="background-position:50% -100%"'; } ?>>
            <?php if( !is_page('about-us') && !is_page('how-it-works') && !is_page('privacy-policy') && !is_front_page()) :    ?>
            <section class="breadcrumbs breadcrumbs-boxed hidden-xs">
                    <div class="container">
               <?php if(!$hide_page_breadcrumb){ ?>
                    <?php if($use_full){ ?> <div class="col-md-6"><?php } ?>
                            <?php welldone_breadcrumb($current_page_id) ?>
                    <?php if($use_full){ ?> </div><?php } ?>
                <?php   } ?>
                    </div>
                </section>
            <?php endif;?>
            <div class="container">
            <?php if($use_full){?> <div class="row"><?php } ?>
            <?php if(!$hide_page_title){ ?>
                    <?php if($use_full){ ?> <div class="col-md-6"><?php } ?>
                        <h1 class="page-title text-uppercase">
                            <?php echo welldone_page_title($current_page_id) ?>
                        </h1>
                    <?php if($use_full){ ?> </div><?php } ?>
            <?php   } ?>

            <?php if($use_full){ ?> </div><?php } ?>
            </div>
        </div>
 <?php   }
}

function welldone_page_title ($current_page_id) {
    global $welldone_settings, $post;
    
    $the_tax = get_taxonomy( get_query_var( 'taxonomy' ) );
    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    if($term){
        $cate = $term->name." ";
    }
    if ( is_front_page () && is_home ()  ) {
        return $welldone_settings[ 'welldone_blog_title' ];
    } elseif($the_tax ){
         $labels=$the_tax->labels->name;
         echo $labels." : ";
         echo $cate;  
    } elseif ( is_front_page () ) {
        return get_the_title ();
    } elseif ( is_category () ) {
        return esc_html__( 'Category Archives: ', 'welldone' ) . '"' . single_cat_title ( '', false ) . '"';
    } elseif ( is_tag () ) {
        return esc_html__( 'Tag Archives: ', 'welldone' ) . '"' . single_tag_title ( '', false ) . '"';
    } elseif ( is_year () ) {
        return esc_html__( 'Yearly Archives: ', 'welldone' ) . '"' . get_the_date ( "Y" ) . '"';
    } elseif ( is_author () ) {
        return esc_html__( 'Author Archives: ', 'welldone' ) . '"' . get_the_author () . '"';
    } elseif ( is_month () ) {
        return esc_html__( 'Monthly Archives: ', 'welldone' ) . '"' . get_the_date ( "F, Y" ) . '"';
    } elseif ( is_day () ) {
        return esc_html__( 'Daily Archives: ', 'welldone' ) . '"' . get_the_date ( "d F, Y" ) . '"';
    } elseif ( is_search () ) {
        return esc_html__( 'Search Result For: ', 'welldone' ) . '"' . get_search_query () . '"';
    }  elseif ( is_post_type_archive ( 'testimonial' ) ) {
        return esc_html__( 'Testimonials', 'welldone' );
    }elseif(is_post_type_archive ( 'clients' )){
        return esc_html__( 'Clients', 'welldone' );
    }elseif(is_404()){
        return esc_html__("Error 404","welldone");
    } elseif(is_single()){
        if(get_post_format($post->ID) == "aside"){
            //no post title for aside post
        }else {
            return get_the_title ($current_page_id);
        }
    }else{
        return get_the_title ($current_page_id);
    }
}


// Breadcrumb
function welldone_breadcrumb ($current_page_id) {
    // Settings
    global $welldone_settings, $post, $wp_query;
	$post_type = get_query_var( 'post_type' );

	if(class_exists("woocommerce") && is_woocommerce()){
        $args = array(
                'delimiter'   => '',
                'wrap_before' => '<ol class="woocommerce-breadcrumb breadcrumb breadcrumb--wd pull-left" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>',
                'wrap_after'  => '</ol>',
                'before'      => '<li>',
                'after'       => '</li>',
              );
        woocommerce_breadcrumb($args);
        return ;
    }
    $home_title = esc_html__('Home',"welldone");
    // Get the query & post information
    $category = get_the_category();
    // Build the breadcrums
    echo '<ol class="breadcrumb breadcrumb--wd pull-left">';
    if( get_option( 'page_for_posts' ) != 0 ) {
         $blog_page_id = get_option( 'page_for_posts' );
        $p = get_post( $blog_page_id );
         $page_title = $p->post_title;
         $blog_page_url =  "<li><a href ='" . esc_url(get_permalink( $blog_page_id )) . "'>" . esc_attr($page_title) . "</a></li>";
         $blog_page_crumb = "<li>".$page_title."</li>";
    } else {
        $theblog_url_query = new WP_Query( array(
            "post_type" => "page",
            'meta_key' => '_wp_page_template',
            'meta_value' => 'index.php'
        ) );
        if( $theblog_url_query->have_posts() ) {
            $theblog_url_query->the_post();
            $blog_page_url = "<li><a href ='" . esc_url(get_the_permalink()) . "'>" . esc_attr(get_the_title()) . "</a></li>";
            $blog_page_crumb = "<li>".esc_attr(get_the_title())."</li>";
            wp_reset_postdata();
        } else {
            $blog_page_url = '';
            $blog_page_crumb = "<li>".__("Blog","welldone")."</li>";
        }
    }
    // Do not display on the homepage
    if ( !is_front_page() ) {
        // Home page
        echo '<li><a href="' . esc_url(get_home_url()) . '" title="' . esc_attr($home_title) . '">' . esc_attr($home_title) . '</a></li>';
        if(is_home()){
            echo $blog_page_crumb;
        }elseif(is_singular("testimonial") ){
            $testimonials = new WP_Query( array(
                "post_type" => "page",
                'meta_key' => '_wp_page_template',
                'meta_value' => 'template-parts/testimonials.php'
            ) );
            if( $testimonials->have_posts() ) {
                $testimonials->the_post();
                echo  "<li><a href ='" . esc_url(get_the_permalink()) . "'>" . esc_html__("Testimonials","welldone") . "</a></li>";
                wp_reset_postdata();
            }else{
                echo  "<li>".__("Testimonials","welldone")."</li>";
            }
                echo "<li>".esc_attr(get_the_title())."</li>";
        }else if ( is_singular('post') ) {
            echo $blog_page_url;
            // Single post (Only display the first category)
            echo '<li><a href="' . esc_url(get_category_link($category[0]->term_id )) . '" title="' . esc_attr($category[0]->cat_name) . '">' . esc_attr($category[0]->cat_name) . '</a></li>';
            echo '<li>'.esc_attr($post->post_title).'</li>';
        } else if ( is_category() ) {
             echo $blog_page_url;
            // Category page
            echo '<li>' . esc_attr($category[0]->cat_name) . '</li>';

        } else if ( is_page() ) {
            // Standard page
            if( $post->post_parent ){
                // If child page, get parents
                $anc = get_post_ancestors( $post->ID );
                // Get parents in the right order
                $anc = array_reverse($anc);
                // Parent page loop
                $parents = '';
                foreach ( $anc as $ancestor ) {
                    $parents .= '<li><a href="'. esc_url(get_permalink($ancestor)) . '" title="' . esc_attr(get_the_title($ancestor)) . '">' . esc_attr(get_the_title($ancestor)) . '</a></li>';
                }
                // Display parent pages
                echo $parents;

                // Current page
                echo '<li>' . esc_attr(get_the_title()) . '</li>';

            } else {
                // Just display current page if not parents
                echo '<li>' . esc_attr(get_the_title()) . '</li>';
            }
        } else if ( is_tag() ) {
            // Tag page
            echo $blog_page_url;
            // Get tag information
            $term_id = get_query_var('tag_id');
            $taxonomy = 'post_tag';
            $args ='include=' . $term_id;
            $terms = get_terms( $taxonomy, $args );

            // Display the tag name
            echo '<li>'. esc_attr($terms[0]->name) . '</li>';

        } elseif ( is_day() ) {
            // Day archive
            echo $blog_page_url;
            // Year link
            echo '<li><a href="' . esc_url(get_year_link( get_the_time('Y') )) . '" title="' . esc_attr(get_the_time('Y')) . '">' . esc_attr(get_the_time('Y')).'</a></li>';
            // Month link
            echo '<li><a href="' . esc_url(get_month_link( get_the_time('Y'), get_the_time('m') )) . '" title="' . esc_attr(get_the_time('M')) . '">' .esc_attr( get_the_time('M')) .'</a></li>';
            // Day display
            echo '<li>' . esc_attr(get_the_time('jS')) . ' ' . esc_attr(get_the_time('M')).'</li>';
        } else if ( is_month() ) {
            echo $blog_page_url;
            // Month Archive
            // Year link
            echo '<li><a href="' . esc_url(get_year_link( get_the_time('Y') )) . '" title="' . esc_attr(get_the_time('Y')) . '">' . esc_attr(get_the_time('Y')) . '</a></li>';
            // Month display
            echo '<li title="' . esc_attr(get_the_time('M') ). '">' . esc_attr(get_the_time('M')) . '</li>';
        } else if ( is_year() ) {
            echo $blog_page_url;
            // Display year archive
            echo '<li>' . esc_attr(get_the_time('Y')) . '</li>';
        } else if ( is_author() ) {
            echo $blog_page_url;
            // Auhor archive
            // Get the author information
            global $author;
            $userdata = get_userdata( $author );
            // Display author name
            echo '<li>'.__("Author","welldone").': ' . esc_attr($userdata->display_name) . '</li>';
        }else if ( is_search() && get_query_var('post_type')=='post' ) {
            echo $blog_page_url;
            // Search results page
            echo '<li>'.__("Search results for","welldone").': ' . get_search_query() . '</li>';
        } elseif ( is_404() ) {
            // 404 page
            echo '<li>' . esc_html__("Error 404","welldone") . '</li>';
        }elseif ( is_post_type_archive( 'wppwp_projects' ) ) {
	        if($wp_query->is_search && $post_type == 'wppwp_projects'){
		        $obj = get_queried_object();
		        echo '<li>' . esc_html__("Search Page","welldone") . '</li>';
	        } else {
		        echo '<li>' . esc_html__( "Projects", "welldone" ) . '</li>';
	        }
        }
        elseif ( is_tax( 'projects_category' ) ) {
	        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	        echo '<li>' . esc_html__("Projects Category","welldone") . '</li>';
	        echo '<li>' . esc_html__($term->name,"welldone") . '</li>';
        }elseif ( is_tax( 'projects_charity' ) ) {
	        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	        $tax = get_taxonomy( 'projects_charity' ) ;
	        $link = get_term_link($term, 'projects_charity');
	        echo '<li>' . esc_html__("Projects Charity","welldone") . '</li>';
	        echo '<li>' . esc_html__($term->name,"welldone") . '</li>';
        } elseif(is_singular( 'wppwp_projects' )){
	        $obj = get_queried_object();
	        echo '<li>' . esc_html__("Projects","welldone") . '</li>';
        }
        if ( get_query_var('paged')  ) {
            // Paginated archives
            echo '<li>'.__('Page',"welldone") . ' ' . get_query_var('paged') .'</li>';
        }
    }

    echo '</ol>';
///	echo '<button class="custom-sign-up-button btn btn--wd">'.__('JOIN OUR NEWSLETTER','welldone').'</button>';
}

if ( !function_exists ( 'welldone_validate_url' ) ) {
    function welldone_validate_url () {
        global $post;
        $page_url = esc_url ( get_permalink ( $post->ID ) );
        $urlget = strpos ( $page_url, "?" );
        if ( $urlget === false ) {
            $concate = "?";
        } else {
            $concate = "&";
        }
        return $page_url . $concate;
    }
}

//pagination function
function welldone_pagination ($pages = '', $range = 3) {
    global $paged, $post, $welldone_settings, $wp_query;
    $current_page_id =  welldone_current_page_id();
    $showitems = ( $range * 2 ) + 1;
    if ( $pages == '' ) {
        $pages = $wp_query->max_num_pages;
        if ( !$pages ) {
            $pages = 1;
        }
    }
       echo '<nav class="pagination-container">';
            echo '<span class="pagination-info">';
    $paged    = max( 1, $wp_query->get( 'paged' ) );
    $per_page = $wp_query->get( 'posts_per_page' );
    $total    = $wp_query->found_posts;
    $first    = ( $per_page * $paged ) - $per_page + 1;
    $last     = min( $total, $wp_query->get( 'posts_per_page' ) * $paged );
    if ( 1 == $total ) {
        esc_html_e( 'Showing the single result', 'welldone' );
    } elseif ( $total <= $per_page || -1 == $per_page ) {
        printf( esc_html__( 'Showing all %d results', 'welldone' ), $total );
    } else {
        printf( _x( 'Showing %1$d-%2$d of %3$d', '%1$d = first, %2$d = last, %3$d = total', 'welldone' ), $first, $last, $total );
    }
            echo "</span>";
        if ( 1 != $pages ) {
            echo '<ul class="pagination">';
            if ( $paged > 1 + $range ) echo "<li><a href='" . esc_url(get_pagenum_link ( 1 )) . "' aria-label='First'><span aria-hidden='true'><i class='fa fa-angle-double-left'></i></span></a></li>";
            if ( $paged > 1 ) echo "<li><a href='" . esc_url(get_pagenum_link ( $paged - 1 ) ). "' aria-label='Previous'><span aria-hidden='true'><i class='fa fa-angle-left'></i></span></a></li>";
            for ( $i = 1; $i <= $pages; $i ++ ) {
                if ( 1 != $pages && ( !( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
                    echo "<li".( ( $paged == $i ) ? " class='active'" : "" ) ."><a href='" . get_pagenum_link ( $i ) . "'>" . $i . "</a></li>";
                }
            }
            if ( $paged < $pages ) echo "<li><a href='" .esc_url( get_pagenum_link ( $paged + 1 )) . "' aria-label='Next'><span aria-hidden='true'><i class='fa fa-angle-right'></i></span></a></li>";
            if ( $paged < $pages-$range ) echo "<li><a href='" . esc_url(get_pagenum_link ( $pages )) . "' aria-label='Last'><span aria-hidden='true'><i class='fa fa-angle-double-right'></i></span></a></li>";
        echo "</ul>";
        }
        echo "</nav>";
}
//overrite welldone columns, left sidebar and right sidebar for taxonomies
function welldone_taxonomy_page_layout_settings($key){
    global $welldone_settings;
    if(is_tax(array('product_cat','product_tag','faq-category')) || is_category() || is_tag()){
        $tax_id = get_queried_object_id();
        $tax_data =  get_option( 'tax_meta_'.$tax_id);
        if($tax_data){
            if(isset($tax_data[$key])){
                if($key=='welldone_taxonomy_layout' && $tax_data[$key]=='default'){
                    return '';
                }
                return $tax_data[$key];
            }
        }else{
            return "";
        }
    }
}

function welldone_posts_before( $query ) {
    global $welldone_settings;
  
    if(is_search() && get_query_var("post_type")=='product'){
        if(isset($_GET['product_cat_filter']) &&  $_GET['product_cat_filter'] !='0'){
            $taxquery = array(
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'id',
                    'terms' => array( $_GET['product_cat_filter'] ),
                )
            );
            $query->set( 'tax_query', $taxquery );
        }
    }
    if(is_search() && get_query_var("post_type")=='post'){
        if(isset($_GET['post_cat_filter']) &&  $_GET['post_cat_filter'] !='0'){
            $query->set( 'cat',$_GET['post_cat_filter'] );
        }
        if(isset($_GET['post_tag_filter']) &&  $_GET['post_tag_filter'] !='0'){
            $query->set( 'tag',$_GET['post_tag_filter'] );
        }
    }
}

//get columns
function welldone_page_columns(){
    global $welldone_settings, $post;
    $welldone_layout_columns = '';
    $welldone_layout_columns = welldone_taxonomy_page_layout_settings("welldone_taxonomy_layout");
    $current_page_id =  welldone_current_page_id();
    if(!$welldone_layout_columns) {
        $welldone_layout_columns = get_post_meta ( $current_page_id, 'welldone_page_layout', true ) ? get_post_meta ( $current_page_id, 'welldone_page_layout', true ) : '';
    }
    if(!$welldone_layout_columns) {

        if(is_page_template("templates/portfolio.php") || is_tax('portfolio-category') || (is_search() && get_query_var("post_type")=='portfolio') ){
            $welldone_layout_columns = ( isset( $welldone_settings[ 'welldone_portfolio_layout' ] ) ) ? $welldone_settings[ 'welldone_portfolio_layout' ] : '';
        }elseif(is_singular("portfolio")){
            $welldone_layout_columns = ( isset( $welldone_settings[ 'welldone_portfolio_single_layout' ] ) ) ? $welldone_settings[ 'welldone_portfolio_single_layout' ] : '';
        }elseif ( ( is_front_page() && is_home() ) || is_post_type_archive ( 'post' ) || is_search() ) {
            $welldone_layout_columns = ( isset( $welldone_settings[ 'welldone_blog_layout' ] ) ) ? $welldone_settings[ 'welldone_blog_layout' ] : '';
        } elseif ( is_single() ){
            $welldone_layout_columns = ( isset( $welldone_settings[ 'welldone_post_layout' ] ) ) ? $welldone_settings[ 'welldone_post_layout' ] : '';
        }
        if(class_exists("woocommerce")){
            if(is_shop() || is_product_category() || is_product_tag() || (is_search() && get_query_var("post_type")=='product')){
                $welldone_layout_columns = ( isset( $welldone_settings[ 'welldone_shop_layout' ] ) ) ? $welldone_settings[ 'welldone_shop_layout' ] : '';
            }elseif(is_product()){
                $welldone_layout_columns = ( isset( $welldone_settings[ 'welldone_product_layout' ] ) ) ? $welldone_settings[ 'welldone_product_layout' ] : '';
            }
        }

    }
    if(!$welldone_layout_columns){
        $welldone_layout_columns =  ( isset( $welldone_settings[ 'welldone_page_layout' ] ) ) ? $welldone_settings[ 'welldone_page_layout' ] : 'left';
    }
    return apply_filters("welldone_filter_page_columns",$welldone_layout_columns);
}
function welldone_left_sidebar(){
    global $welldone_settings, $post;
    $welldone_left_sidebar = '';
    $welldone_left_sidebar = welldone_taxonomy_page_layout_settings("welldone_taxonomy_left_sidebar");
    $current_page_id =  welldone_current_page_id();
    if ( !$welldone_left_sidebar ) {
        $welldone_left_sidebar = get_post_meta ( $current_page_id, 'welldone_page_sidebar_left', true ) ? get_post_meta ( $current_page_id, 'welldone_page_sidebar_left', true ) : '';
    }
        if(!$welldone_left_sidebar) {
            if(class_exists("woocommerce")){
                if(is_shop() || is_product_category() || is_product_tag() || is_tax( 'brand')){
                    $welldone_left_sidebar = ( isset( $welldone_settings[ 'welldone_shop_sidebar_left' ] ) ) ? $welldone_settings[ 'welldone_shop_sidebar_left' ] : '';
                }elseif(is_product()){
                    $welldone_left_sidebar = ( isset( $welldone_settings[ 'welldone_product_sidebar_left' ] ) ) ? $welldone_settings[ 'welldone_product_sidebar_left' ] : '';
                }
            }
            if ( (is_front_page () && is_home ()) || is_post_type_archive ( 'post' ) || is_search("post") ) {
                $welldone_left_sidebar = ( isset( $welldone_settings[ 'welldone_blog_sidebar_left' ] ) ) ? $welldone_settings[ 'welldone_blog_sidebar_left' ] : '';
            }if ( is_singular("post") ) {
                $welldone_left_sidebar = ( isset( $welldone_settings[ 'welldone_post_sidebar_left' ] ) ) ? $welldone_settings[ 'welldone_post_sidebar_left' ] : '';
            }
        }
        if(!$welldone_left_sidebar){
            $welldone_left_sidebar =  ( isset( $welldone_settings[ 'welldone_page_sidebar_left' ] ) ) ? $welldone_settings[ 'welldone_page_sidebar_left' ] : '';
        }
        return $welldone_left_sidebar;
}
function welldone_right_sidebar(){
    global $welldone_settings, $post;
    $welldone_right_sidebar = '';
    $welldone_right_sidebar  = welldone_taxonomy_page_layout_settings("welldone_taxonomy_right_sidebar");
    $current_page_id =  welldone_current_page_id();
    if (!$welldone_right_sidebar ) {
        $welldone_right_sidebar = get_post_meta ($current_page_id, 'welldone_page_sidebar_right', true ) ? get_post_meta ( $current_page_id, 'welldone_page_sidebar_right', true ) : '';
    }
    if(!$welldone_right_sidebar) {
            if(class_exists("woocommerce")){
                if(is_shop() || is_product_category() || is_product_tag()){
                    $welldone_right_sidebar = ( isset( $welldone_settings[ 'welldone_shop_sidebar_right' ] ) ) ? $welldone_settings[ 'welldone_shop_sidebar_right' ] : '';
                }elseif(is_product()){
                    $welldone_right_sidebar = ( isset( $welldone_settings[ 'welldone_product_sidebar_right' ] ) ) ? $welldone_settings[ 'welldone_product_sidebar_right' ] : '';
                }
            }
            if ( is_front_page () && is_home () || is_post_type_archive ( 'post' ) ) {
                $welldone_right_sidebar = ( isset( $welldone_settings[ 'welldone_blog_sidebar_right' ] ) ) ? $welldone_settings[ 'welldone_blog_sidebar_right' ] : '';
            }elseif ( is_singular ("post") ) {
                $welldone_right_sidebar = ( isset( $welldone_settings[ 'welldone_post_sidebar_right' ] ) ) ? $welldone_settings[ 'welldone_post_sidebar_right' ] : '';
            }
        }
        if(!$welldone_right_sidebar){
            $welldone_right_sidebar =  ( isset( $welldone_settings[ 'welldone_page_sidebar_right' ] ) ) ? $welldone_settings[ 'welldone_page_sidebar_right' ] : '';
        }
        return $welldone_right_sidebar;
}
function welldone_wrapper_class(){
    global $welldone_settings, $post;
    $id = welldone_current_page_id();
    $wrapper =  get_post_meta ( $id, 'welldone_theme_wrapper', true ) ? get_post_meta ( $id, 'welldone_theme_wrapper', true ) : '';
  if(!$wrapper){
      $wrapper = ( isset( $welldone_settings[ 'welldone_theme_wrapper' ] ) ) ? $welldone_settings[ 'welldone_theme_wrapper' ] : 'wide';
  }
    return $wrapper;
}

function welldone_current_header(){

    global $post, $welldone_settings;
    if(isset($post)) {
        $header_hide_id = $post->ID;
    }else{
        $header_hide_id = 0;
    }
    
    $welldone_header_hide = get_post_meta ( $header_hide_id, 'welldone_header_hide', true ) ? get_post_meta ( $header_hide_id, 'welldone_header_hide', true ) : '';

    
    if($welldone_header_hide==1){
       return;
    }
    else{
    
    $id = welldone_current_page_id();
    $header = get_post_meta ( $id, 'welldone_page_header', true ) ? get_post_meta ( $id, 'welldone_page_header', true ) : '';
    if(!$header){
        $header = isset($welldone_settings["welldone_site_header"])?$welldone_settings["welldone_site_header"]:"1";
    }
    return $header;
    }
}
function welldone_current_footer(){
   
       global $post, $welldone_settings,$current_page;
    
    if(isset($post)) {
        $footer_hide_id = $post->ID;
    }else{
        $footer_hide_id = 0;
    }
    $id = welldone_current_page_id();
    $footer = get_post_meta ( $id, 'welldone_footer_type', true ) ? get_post_meta ( $id, 'welldone_footer_type', true ) : '';
    if(!$footer){
        $footer = isset($welldone_settings["welldone_footer_type"])?$welldone_settings["welldone_footer_type"]:"1";
    }
    
    return $footer;
}

add_action( 'vc_before_init', 'welldone_set_vc_as_theme' );
function welldone_set_vc_as_theme() {
    vc_set_as_theme();
}

add_filter( 'oembed_dataparse','welldone_oembed_filter',10,1);
function welldone_oembed_filter( $return ){
    $add_class = str_replace( 'allowfullscreen', 'class="embed-responsive-item" allowfullscreen', $return );
    return $add_class;
}
function welldone_class_name(){
    global $welldone_layout_columns;
    $class = "col-md-";
        if ( $welldone_layout_columns == 'left' ) {
            $class .=  '9 col-md-push-3';
        } elseif ( $welldone_layout_columns == 'no' ) {
            $class .= '12';
        } elseif ( $welldone_layout_columns == 'right' ) {
            $class .= '9';
        } else {
            $class .= '6 col-md-push-3';
        }
    return $class;
}

function welldone_add_social_share() {
    global $post, $welldone_settings;
    $current_page_id  = welldone_current_page_id();
    $show = true;
    $show = ( get_post_meta (  $current_page_id, 'welldone_page_social_share', true ) ) ? get_post_meta (  $current_page_id, 'welldone_page_social_share', true ) : true;
    if($show==='hide'){
        return ;
    }
    if($show===true) {
        if ( isset($welldone_settings[ 'welldone_social_share' ]) && is_array ( $welldone_settings[ 'welldone_social_share' ] ) && in_array ( $post->post_type, $welldone_settings[ 'welldone_social_share' ] )  ) {
            return;
        }
    }
    if ( function_exists ( "wpfai_social" ) && get_option ( 'wpfai' ) != '' && $show) {
            if ( $post->post_type == 'portfolio' ) { ?><li><?php } ?>
            <div class="share-box">
                <span
                    class="share-label"><?php echo htmlspecialchars_decode ( esc_textarea ( $welldone_settings[ 'welldone_social_share_label' ] ) ); ?></span>

                <div class="social-icons">
                    <?php $content = wpfai_social ();
                    echo str_replace ( "wpfai-link", "social-icon", $content );
                    ?>
                </div>
            </div>
            <?php if ( $post->post_type == 'portfolio' ) { ?></li><?php } ?>
        <?php
    }
}

function welldone_main_container_class(){
    global $welldone_settings;
    $current_page_id  = welldone_current_page_id();
    $container =  get_post_meta ( $current_page_id, 'welldone_container_size', true ) ? get_post_meta ( $current_page_id, 'welldone_container_size', true ) : '';
    if($container==''){
        $container = $welldone_settings['welldone_container_size']?$welldone_settings['welldone_container_size']:"no";
    }
    if($container=='no'){
        return "container ";
    }else{
        return "container-fluid ";
    }
}

function welldone_page_banner(){
    global $welldone_settings;
    $current_page_id = welldone_current_page_id();
    ob_start ();
    
    $banner_type = ( get_post_meta($current_page_id, 'welldone_banner_type', true) ) ? get_post_meta($current_page_id, 'welldone_banner_type', true) : '';
    
    if(!empty($banner_type)){
    $banner_type = ( get_post_meta($current_page_id, 'welldone_banner_type', true) ) ? get_post_meta($current_page_id, 'welldone_banner_type', true) : '';
    }else{
        $current_header = welldone_current_header();
    if($current_header=='9' ||$current_header=='17'){
        $banner_type='cus_option';
        $welldone_settings['header_background_nine17'];
        }
    }
    if($banner_type){
        echo "<div class='welldone_banner'>";
        if( $banner_type=='video' ){
            $video_embed = get_post_meta($current_page_id, 'welldone_banner_video_embed', true);
            if( $video_embed ) {
               // entry-media
                echo '<div class="embed-responsive embed-responsive-16by9">';
                echo wp_oembed_get ( $video_embed );
                echo '</div>';
            }
        }elseif( $banner_type=='image' ){
            $app_gallery = get_post_meta($current_page_id, 'welldone_banner_image', true);
            if( $app_gallery ) {
                $img_src = wp_get_attachment_image_src( $app_gallery, 'full' ) ;
                echo '<img src="'. esc_url($img_src[0]).'" class="img-responsive" alt="">';
            }
        }elseif($banner_type=='rev_slider'){
            if(shortcode_exists('rev_slider') || function_exists('rev_slider')){
                $rev_slider = get_post_meta($current_page_id, 'welldone_banner_rev_slider', true);
                if($rev_slider){
                    echo do_shortcode('[rev_slider "'. $rev_slider .'"]');
                }
            }
        }elseif($banner_type='cus_option'){
            if(isset($welldone_settings['header_background_nine17']['url'])){
                $welldone_image=$welldone_settings['header_background_nine17']['url'];
                if($welldone_image){
                 echo '<img src="'. esc_url($welldone_image).'" class="img-responsive" alt="">';    
                }
            }
        }
        
        echo "</div>";
    }
    echo ob_get_clean();
    //wrap the banner in a div with class welldone_banner
}

function welldone_taxonomy_banner(){
    if(is_tax(array('product_cat','product_tag','portfolio-category','faq-category')) || is_category() || is_tag()) {
        $tax_id = get_queried_object_id ();
        $tax_data = get_option ( 'tax_meta_' . $tax_id );
        ob_start ();
        $tax_data =  get_option( 'tax_meta_'.$tax_id);
        $banner_type =(isset($tax_data['welldone_taxonomy_banner_type']) && $tax_data['welldone_taxonomy_banner_type'])?$tax_data['welldone_taxonomy_banner_type']:'';
        if($banner_type){
            echo "<div class='welldone_banner'>";
            if( $banner_type == 'video' ){
                $video_embed = (isset($tax_data['welldone_taxonomy_banner_video']) && $tax_data['welldone_taxonomy_banner_video'])?$tax_data['welldone_taxonomy_banner_video']:'';
                if( $video_embed ) {
//                    entry-media
                    echo '<div class="embed-responsive embed-responsive-16by9">';
                    echo wp_oembed_get ( $video_embed );
                    echo '</div>';
                }
            }elseif( $banner_type=='image' ){
                $app_gallery = (isset($tax_data['welldone_taxonomy_banner_image']) && $tax_data['welldone_taxonomy_banner_image'])?$tax_data['welldone_taxonomy_banner_image']:'';
                if( $app_gallery ) {
                    $img_src = wp_get_attachment_image_src( $app_gallery['id'], 'full' ) ;
                    echo '<img src="'. esc_url($img_src[0]).'" class="img-responsive" alt="">';

                }
            }elseif($banner_type=='rev_slider'){
                if(shortcode_exists('rev_slider') || function_exists('rev_slider')){
                    $rev_slider = (isset($tax_data['welldone_taxonomy_banner_rev_slider']) && $tax_data['welldone_taxonomy_banner_rev_slider'])?$tax_data['welldone_taxonomy_banner_rev_slider']:'';
                    if($rev_slider && RevSlider::isAliasExists($rev_slider)){
                        echo do_shortcode('[rev_slider "'. $rev_slider .'"]');
                    }
                }
            }elseif($banner_type=='custom_banner'){
                $custom_banner = (isset($tax_data['welldone_taxonomy_banner_custom']) && $tax_data['welldone_taxonomy_banner_custom'])?$tax_data['welldone_taxonomy_banner_custom']:'';
                if($custom_banner){
                    echo do_shortcode( $custom_banner );
                }
            }
            echo "</div>";
        }
        echo ob_get_clean();
        //wrap the banner in a div with class welldone_banner
    }
}

function welldone_comment( $comment, $args, $depth ) { ?>
    <?php $add_below = ''; ?>
   <li  <?php comment_class( 'comments__comment' ); ?>  id="comment-<?php comment_ID() ?>" >
    <div class="comments__comment__userpic">
        <?php echo get_avatar( $comment, 40 ); ?>
      <!--  <img class="media-object" src="images/blog/christopher.jpg" alt="christopher"> -->
    </div>
    <div class="comments__comment__text">
    <h5 class="comments__comment__text__username text-uppercase"><?php echo get_comment_author_link() ?></h5>
    <div class="comments__comment__text__date"><?php echo get_comment_date('M j, Y') . ' at ' . get_comment_time(); ?></div>
        <?php if( $comment->comment_approved == '0' ) : ?>
            <p><em><?php echo esc_html__( 'Your comment is awaiting moderation.', 'welldone' ) ?></em></p>
            <br/>
        <?php endif; ?>
        <?php comment_text() ?>
        <span class="btn btn--wd btn--xs"><?php comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'welldone' ), 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args[ 'max_depth' ] ) ) ) ?></span>
        <span class="comment-edit">
            <?php edit_comment_link( esc_html__( ' Edit', 'welldone' ), '  ', '' ) ?>
        </span>
    </div>
</li>
<?php }

function welldone_search_form ( $form ) {
    get_template_part(ABSPATH . 'wp-admin/includes/plugin');
    if( isset( $welldone_settings[ 'welldone_search_type' ] ) && $welldone_settings[ 'welldone_search_type' ] === 'product' && is_plugin_active( 'yith-woocommerce-ajax-search/init.php' ) && class_exists( 'WooCommerce' ) ) {
        global $con_class, $search_button_class;
        $search_button_class = " btn-custom";
        $con_class = "header-search-container sm-margin"; // header-simple-search
        $wc_get_template = function_exists( 'wc_get_template' ) ? 'wc_get_template' : 'woocommerce_get_template';
        ob_start();
        $wc_get_template( 'yith-woocommerce-ajax-search.php', array(), '', YITH_WCAS_DIR . 'templates/' );
        return ob_get_clean();
    }
    ob_start ();
    ?>
    <div class="header-search-container header-simple-search">
        <form action="<?php echo esc_url ( home_url ( '/' ) ); ?>">
            <div class="form-group">
                <input type="search" required="required" class="form-control" value="<?php echo get_search_query () ?>" name="s" id="s"/>
                        <?php
                         $type = isset( $welldone_settings[ 'welldone_search_type' ] ) ? $welldone_settings[ 'welldone_search_type' ] : 'post';
                        if(! post_type_exists( $type )){
                                $type = 'post';
                        }  ?>
                <input type="hidden" name="post_type" value="<?php echo esc_attr($type); ?>"/>
                <?php $lang_code = explode ( '-', get_bloginfo ( "language" ) ); ?>
                <input type="hidden" name="lang" value="<?php echo esc_attr($lang_code[ 0 ]); ?>"/>
                <button class="btn" type="submit" title="<?php esc_html__("Search",'welldone') ?>"><i class="fa fa-search"></i></button>
            </div>
        </form>
    </div>
    <?php
    return ob_get_clean ();
}

add_filter ( 'get_search_form', 'welldone_search_form');

function welldone_custom_tag_cloud_widget($args) {
    $args['format'] = 'list'; //ul with a class of wp-tag-cloud

    return $args;
}
add_filter( 'widget_tag_cloud_args', 'welldone_custom_tag_cloud_widget' );



function welldone_crumbs_tax( $term_id, $tax, $sep, $linkpatt ){
    $termlink = array();
    while( (int) $term_id ){
        $term2      = get_term( $term_id, $tax );
        $termlink[] = sprintf( $linkpatt, get_term_link( (int) $term2->term_id, $term2->taxonomy ), $term2->name ). $sep;
        $term_id    = (int) $term2->parent;
    }

    $termlinks = array_reverse( $termlink );

    return implode('', $termlinks );
}


if ( !function_exists('welldone_prev_next_product') ) {
    global $post, $wp_query, $wp_post_types;
function welldone_prev_next_product() {
    ?>
    <?php $prevPost = get_previous_post(); $prevthumbnail = get_the_post_thumbnail($prevPost->ID, "welldone-thumb-60"); ?>
    <?php $nextPost = get_next_post(); $nextthumbnail = get_the_post_thumbnail($nextPost->ID, "welldone-thumb-60"); ?>


    <ul id="productOther" class="product-other pull-right hidden-xs">
        <li class="product-other__link product-prev">

            <?php previous_post_link( '%link'); ?>
            <span class="product-other__link__image"><?php echo $prevthumbnail; ?></span>

        </li>
        <?php if( !empty( $nextthumbnail ) ) { ?>
        <li class="product-other__link product-next">

            <?php next_post_link('%link'); ?>  
            <span class="product-other__link__image"><?php echo $nextthumbnail; ?></span>

        </li>
        <?php } ?>
    </ul>


<?php
}
}
add_action('woocommerce_after_breadcrumbs', 'welldone_prev_next_product', 25);






if ( !function_exists('welldone_blog_slider_image_filter') ) {
    function welldone_blog_slider_image_filter($html, $post_id, $post_thumbnail_id, $size, $attr) {
        $html = str_replace('class="', 'class="img-responsive animate scale ', $html);
        return $html;
    }
}



/**
 * Welldone_Product_Brands_List_Walker class.
 *
 * @extends     Walker
 * @class       Welldone_Product_Brands_List_Walker
 * @version     1.0.0
 * @package     WooCommerce/Classes/Walkers
 * @author      Web Flash Templates
 */

class Welldone_Product_Brands_List_Walker extends Walker {

    var $tree_type = 'brand';
    var $db_fields = array ( 'parent' => 'parent', 'id' => 'term_id', 'slug' => 'slug' );

    /**
     * @see Walker::start_lvl()
     * @since 2.1.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int $depth Depth of brand. Used for tab indentation.
     * @param array $args Will only append content if style argument value is 'list'.
     */
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        if ( 'list' != $args['style'] )
            return;

        $level = $depth + 1;

        $indent = str_repeat("\t", $depth);

        $output .= "$indent". '<ul class="level'.$level.' children">'."\n";
    }

    /**
     * @see Walker::end_lvl()
     * @since 2.1.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int $depth Depth of brand. Used for tab indentation.
     * @param array $args Will only append content if style argument value is 'list'.
     */
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        if ( 'list' != $args['style'] )
            return;

        $indent = str_repeat("\t", $depth);

        $output .= "$indent</ul>\n";
    }

    /**
     * @see Walker::start_el()
     * @since 2.1.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $object Brand data object.
     * @param int $depth Depth of brand in reference to parents.
     * @param array $args
     * @param int $current_object_id
     */
    function start_el( &$output, $object, $depth = 0, $args = array(), $current_object_id = 0 ) {

        $level = $depth + 1;

        $output .= '<li class="level'. $level .' brand-item brand-item-' . $object->term_id;

        if ( $args['current_brand'] == $object->term_id )
            $output .= ' current-brand';

        if ( $args['current_brand_ancestors'] && $args['current_brand'] && in_array( $object->term_id, $args['current_brand_ancestors'] ) )
            $output .= ' current-brand-parent';

        $output .=  '"><a href="' . esc_url(get_term_link( (int) $object->term_id, 'brand' )) . '"></a>';

        if ( $args['show_count'] )
            $output .= ' <span class="count">(' . $object->count . ')</span>';

    }

    /**
     * @see Walker::end_el()
     * @since 2.1.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $object Not used.
     * @param int $depth Depth of category. Not used.
     * @param array $args Only uses 'list' for whether should append to output.
     */
    function end_el( &$output, $object, $depth = 0, $args = array() ) {

        $output .= "</li>\n";

    }

    /**
     * Traverse elements to create list from elements.
     *
     * Display one element if the element doesn't have any children otherwise,
     * display the element and its children. Will only traverse up to the max
     * depth and no ignore elements under that depth. It is possible to set the
     * max depth to include all depths, see walk() method.
     *
     * This method shouldn't be called directly, use the walk() method instead.
     *
     * @since 2.5.0
     *
     * @param object $element Data object
     * @param array $children_elements List of elements to continue traversing.
     * @param int $max_depth Max depth to traverse.
     * @param int $depth Depth of current element.
     * @param array $args
     * @param string $output Passed by reference. Used to append additional content.
     * @return null Null on failure with no changes to parameters.
     */
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

        if ( !$element )
            return;

        if ( ! $args[0]['show_children_only'] || ( $args[0]['show_children_only'] && ( $element->parent == 0 || $args[0]['current_brand'] == $element->parent || in_array( $element->parent, $args[0]['current_brand_ancestors'] ) ) ) ) {

            $id_field = $this->db_fields['id'];

            //display this element
            if ( is_array( $args[0] ) )
                $args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
            $cb_args = array_merge( array(&$output, $element, $depth), $args);
            @call_user_func_array(array(&$this, 'start_el'), $cb_args);

            $id = $element->$id_field;

            // descend only when the depth is right and there are children for this element
            if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

                foreach( $children_elements[ $id ] as $child ){

                    if ( !isset($newlevel) ) {
                        $newlevel = true;
                        //start the child delimiter
                        $cb_args = array_merge( array(&$output, $depth), $args);
                        @call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
                    }
                    $this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
                }
                unset( $children_elements[ $id ] );
            }

            if ( isset($newlevel) && $newlevel ){
                //end the child delimiter
                $cb_args = array_merge( array(&$output, $depth), $args);
                @call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
            }

            //end this element
            $cb_args = array_merge( array(&$output, $element, $depth), $args);
            @call_user_func_array(array(&$this, 'end_el'), $cb_args);

        }
    }

}

if ( !function_exists('welldone_get_product_brands') ) {
    function welldone_get_product_brands($args = '') {

        $defaults = array( 'taxonomy' => 'brand' );
        $args = wp_parse_args( $args, $defaults );

        $taxonomy = $args['taxonomy'];
        /**
         * Filter the taxonomy used to retrieve terms when calling get_categories().
         *
         * @since 2.7.0
         *
         * @param string $taxonomy Taxonomy to retrieve terms from.
         * @param array  $args     An array of arguments. @see get_terms()
         */
        $taxonomy = apply_filters( 'get_brands_taxonomy', $taxonomy, $args );

        // Back compat
        if ( isset($args['type']) && 'link' == $args['type'] ) {
            _deprecated_argument( __FUNCTION__, '3.0', '' );
            $taxonomy = $args['taxonomy'] = 'link_brand';
        }

        $brands = (array) get_terms( $taxonomy, $args );

        foreach ( array_keys( $brands ) as $k )
            _make_cat_compat( $brands[$k] );

        return $brands;

    }
}

if ( !function_exists('welldone_list_brands') ) {
    /**
     * Display or retrieve the HTML list of brands.
     *
     * The list of arguments is below:
     *     'show_option_all' (string) - Text to display for showing all brands.
     *     'orderby' (string) default is 'ID' - What column to use for ordering the
     * brands.
     *     'order' (string) default is 'ASC' - What direction to order brands.
     *     'show_count' (bool|int) default is 0 - Whether to show how many posts are
     * in the brand.
     *     'hide_empty' (bool|int) default is 1 - Whether to hide brands that
     * don't have any posts attached to them.
     *     'use_desc_for_title' (bool|int) default is 1 - Whether to use the
     * description instead of the brand title.
     *     'feed' - See {@link welldone_get_product_brands()}.
     *     'feed_type' - See {@link welldone_get_product_brands()}.
     *     'feed_image' - See {@link welldone_get_product_brands()}.
     *     'child_of' (int) default is 0 - See {@link welldone_get_product_brands()}.
     *     'exclude' (string) - See {@link welldone_get_product_brands()}.
     *     'exclude_tree' (string) - See {@link welldone_get_product_brands()}.
     *     'echo' (bool|int) default is 1 - Whether to display or retrieve content.
     *     'current_category' (int) - See {@link welldone_get_product_brands()}.
     *     'hierarchical' (bool) - See {@link welldone_get_product_brands()}.
     *     'title_li' (string) - See {@link welldone_get_product_brands()}.
     *     'depth' (int) - The max depth.
     *
     * @since 2.1.0
     *
     * @param string|array $args Optional. Override default arguments.
     * @return string HTML content only if 'echo' argument is 0.
     */
    function welldone_list_brands($args = '') {
        $defaults = array(
            'show_option_all' => '',
            'show_option_none' => esc_html__('No brands', "welldone"),
            'orderby' => 'name',
            'order' => 'ASC',
            'style' => 'list',
            'show_count' => 0,
            'hide_empty' => 1,
            'use_desc_for_title' => 1,
            'child_of' => 0,
            'feed' => '',
            'feed_type' => '',
            'feed_image' => '',
            'exclude' => '',
            'exclude_tree' => '',
            'current_brand' => 0,
            'hierarchical' => true,
            'title_li' => esc_html__( 'Brands', "welldone" ),
            'echo' => 0,
            'depth' => 0,
            'taxonomy' => 'brand',
            'walker' => new Welldone_Product_Brands_List_Walker
        );

        $r = wp_parse_args( $args, $defaults );

        if ( !isset( $r['pad_counts'] ) && $r['show_count'] && $r['hierarchical'] )
            $r['pad_counts'] = true;

        if ( true == $r['hierarchical'] ) {
            $r['exclude_tree'] = $r['exclude'];
            $r['exclude'] = '';
        }

        if ( !isset( $r['class'] ) )
            $r['class'] = ( 'category' == $r['taxonomy'] ) ? 'categories' : $r['taxonomy'];

        extract( $r );

        if ( !taxonomy_exists($taxonomy) )
            return false;

        $brands = welldone_get_product_brands( $r );

        $output = '';
        if ( $title_li && 'list' == $style )
            $output = '<li class="' . esc_attr( $class ) . '">' . $title_li . '<ul>';

        if ( empty( $brands ) ) {
            if ( ! empty( $show_option_none ) ) {
                if ( 'list' == $style )
                    $output .= '<li>' . $show_option_none . '</li>';
                else
                    $output .= $show_option_none;
            }
        } else {
            if ( ! empty( $show_option_all ) ) {
                $posts_page = ( 'page' == get_option( 'show_on_front' ) && get_option( 'page_for_posts' ) ) ? get_permalink( get_option( 'page_for_posts' ) ) : home_url( '/' );
                $posts_page = esc_url( $posts_page );
                if ( 'list' == $style )
                    $output .= "<li><a href='$posts_page'>$show_option_all</a></li>";
                else
                    $output .= "<a href='$posts_page'>$show_option_all</a>";
            }

            if ( empty( $r['current_brand'] ) && ( is_category() || is_tax() || is_tag() ) ) {
                $current_term_object = get_queried_object();
                if ( $current_term_object && $r['taxonomy'] === $current_term_object->taxonomy )
                    $r['current_brand'] = get_queried_object_id();
            }

            if ( $hierarchical )
                $depth = $r['depth'];
            else
                $depth = -1; // Flat.

            $output .= walk_category_tree( $brands, $depth, $r );
        }

        if ( $title_li && 'list' == $style )
            $output .= '</ul></li>';

        $output = apply_filters( 'welldone_list_brands', $output, $args );

        if ( $echo )
            echo "".$output;
        else
            return $output;
    }
}

if ( !function_exists('welldone_get_shop_by_brand') ) {
    function welldone_get_shop_by_brand($args = array()) {

        global $wp_query, $post, $woocommerce;

        extract( $args );

        $count = $count ? '1' : '0';
        $hierarchical = $hierarchical ? true : false;
        $show_children_only = (isset($show_children_only) && $show_children_only) ? '1' : '0';
        $orderby = isset($orderby) ? $orderby : 'order';
        $taxonomy = $taxonomy ? $taxonomy : 'brand';

        $brand_args = array( 'show_count' => $count, 'hierarchical' => $hierarchical, 'taxonomy' => $taxonomy );

        $brand_args['menu_order'] = false;

        if ( $orderby == 'order' ) {

            $brand_args['menu_order'] = 'asc';

        } else {

            $brand_args['orderby'] = 'title';

        }

        $current_brand = false;
        $brand_ancestors = array();

        if ( is_tax($taxonomy) ) {

            $current_brand = $wp_query->queried_object;
            $brand_ancestors = get_ancestors( $current_brand->term_id, $taxonomy );

        } elseif ( is_singular('product') ) {

            $product_brand = wc_get_product_terms( $post->ID, $taxonomy, array( 'orderby' => 'parent' ) );

            if ( $product_brand ) {
                $current_brand   = end( $product_brand );
                $brand_ancestors = get_ancestors( $current_brand->term_id, $taxonomy );
            }

        }

        include_once( $woocommerce->plugin_path() . '/includes/walkers/class-product-cat-list-walker.php' );

        $brand_args['title_li']             = '';
        $brand_args['show_children_only']   = $show_children_only;
        $brand_args['pad_counts']       = 1;
        $brand_args['show_option_none']     = esc_html__('No product categories exist.', 'welldone' );
        $brand_args['current_brand']    = ( $current_brand ) ? $current_brand->term_id : '';
        $brand_args['current_brand_ancestors']  = $brand_ancestors;
        $brand_args['echo'] = false;

        $html = '<li class="level0 nav-2 level-top first parent">
                    <a class="level-top"><span>' . esc_html__('Shop by Brand', 'welldone') . '</span></a>
                    <ul class="level0">
                        <li>
                            <ul class="shadow">
                                <li class="list_column">
                                    <ul class="list_in_column">';

        $html .= welldone_list_brands( apply_filters( 'woocommerce_product_brands_widget_args', $brand_args ) );

        $html .= '</ul></li></ul></li></ul></li>';

        return $html;
    }
}



// **********************************************************************// 
// ! Function to get post image
// **********************************************************************//  

function welldone_get_image( $attachment_id = 0, $width = null, $height = null, $crop = true, $post_id = null ) {
    global $post;
    if (!$attachment_id) {
        if (!$post_id) {
            $post_id = $post->ID;
        }
        if ( has_post_thumbnail( $post_id ) ) {
            $attachment_id = get_post_thumbnail_id( $post_id );
        } 
        else {
            $attached_images = (array)get_posts( array(
                'post_type'   => 'attachment',
                'numberposts' => 1,
                'post_status' => null,
                'post_parent' => $post_id,
                'orderby'     => 'menu_order',
                'order'       => 'ASC'
            ) );
            if ( !empty( $attached_images ) )
                $attachment_id = $attached_images[0]->ID;
        }
    }
    
    if (!$attachment_id)
        return;

    $image_url = welldone_get_resized_url($attachment_id,$width, $height, $crop);
    
    return apply_filters( 'blanco_product_image', $image_url );
}

// **********************************************************************// 
// ! Get all images uploaded to posts
// **********************************************************************//  

function welldone_get_images($width = null, $height = null, $crop = true, $post_id = null ) {
    global $post;
    
    if (!$post_id) {
        $post_id = $post->ID;
    }   
    
    if ( has_post_thumbnail( $post_id ) ) {
        $attachment_id = get_post_thumbnail_id( $post_id );
    } 
    
    $args = array(
        'post_type' => 'attachment',
        'post_status' => null,
        'post_parent' => $post_id,
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'exclude' => get_post_thumbnail_id( $post_id )
    );
    
    $attachments = get_posts($args);
    
    if (empty( $attachments) && empty($attachment_id))
        return;
        
    $image_urls = array();
    
    if(!empty($attachment_id))
        $image_urls[] = welldone_get_resized_url($attachment_id,$width, $height, $crop);
        
    foreach($attachments as $one) {
        $image_urls[] = welldone_get_resized_url($one->ID,$width, $height, $crop);
    }

    return apply_filters( 'blanco_attachment_image', $image_urls );
}

function welldone_get_resized_url($id,$width, $height, $crop) {
    if ( function_exists("gd_info") && (($width >= 10) && ($height >= 10)) && (($width <= 1024) && ($height <= 1024)) ) {
        $vt_image = welldone_vt_resize( $id, '', $width, $height, $crop );
        if ($vt_image) 
            $image_url = $vt_image['url'];
        else
            $image_url = false;
    }
    else {
        $full_image = wp_get_attachment_image_src( $id, 'full');
        if (!empty($full_image[0]))
            $image_url = $full_image[0];
        else
            $image_url = false;
    }
    
    if( is_ssl() && !strstr(  $image_url, 'https' ) ) str_replace('http', 'https', $image_url);
    
    return $image_url;
}

if ( !function_exists('welldone_vt_resize') ) {
    function welldone_vt_resize( $attach_id = null, $img_url = null, $width, $height, $crop = false ) {
    
        // this is an attachment, so we have the ID
        if ( $attach_id ) {
        
            $image_src = wp_get_attachment_image_src( $attach_id, 'full' );
            $file_path = get_attached_file( $attach_id );
        
        // this is not an attachment, let's use the image url
        } else if ( $img_url ) {
            
            $file_path = parse_url( $img_url );
            $file_path = $_SERVER['DOCUMENT_ROOT'] . $file_path['path'];
            
            $orig_size = getimagesize( $file_path );
            
            $image_src[0] = $img_url;
            $image_src[1] = $orig_size[0];
            $image_src[2] = $orig_size[1];
        }
        
        $file_info = pathinfo( $file_path );

        $file_info['dirname'] = isset($file_info['dirname'])? $file_info['dirname'] : '';
        $file_info['filename'] = isset($file_info['filename']) ? $file_info['filename'] : '';
        $file_info['extension'] = isset($file_info['extension']) ? $file_info['extension'] : '';

        // check if file exists
        $base_file = $file_info['dirname'].'/'.$file_info['filename'].'.'.$file_info['extension'];
        if ( !file_exists($base_file) )
            return;
         
        $extension = '.'. $file_info['extension'];
    
        // the image path without the extension
        $no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];
        
        // checking if the file size is larger than the target size
        // if it is smaller or the same size, stop right here and return
        if ( $image_src[1] > $width || $image_src[2] > $height ) {
    
            if ( $crop == true ) {
            
                $cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.$extension;
                
                // the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
                if ( file_exists( $cropped_img_path ) ) {
        
                    $cropped_img_url = str_replace( get_template_directory() . $image_src[0], get_template_directory() . $cropped_img_path, $image_src[0] );
                    
                    $vt_image = array (
                        'url' => $cropped_img_url,
                        'width' => $width,
                        'height' => $height
                    );
                    
                    return $vt_image;
                }
            }
            elseif ( $crop == false ) {
            
                // calculate the size proportionaly
                $proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
                $resized_img_path = $no_ext_path.'-'.$proportional_size[0].'x'.$proportional_size[1].$extension;            

                // checking if the file already exists
                if ( file_exists( $resized_img_path ) ) {
                
                    $resized_img_url = str_replace( get_template_directory() . $image_src[0],get_template_directory() . $resized_img_path, $image_src[0] );
    
                    $vt_image = array (
                        'url' => $resized_img_url,
                        'width' => $proportional_size[0],
                        'height' => $proportional_size[1]
                    );
                    
                    return $vt_image;
                }
            }
    
            // check if image width is smaller than set width
            $img_size = getimagesize( $file_path );
            if ( $img_size[0] <= $width ) $width = $img_size[0];        
    
            // no cache files - let's finally resize it
            //$new_img_path = image_resize( $file_path, $width, $height, $crop );

            $image = wp_get_image_editor( $file_path );
            if ( ! is_wp_error( $image ) ) {
                $image->resize( $width, $height, $crop );
                $new_img_path = $image->save();
                $new_img_path = $new_img_path['path'];
            } else{
                $new_img_path = $file_path;
            }

            $new_img_size = getimagesize( $new_img_path );
            $new_img = str_replace( get_template_directory() . $image_src[0], get_template_directory() . $new_img_path, $image_src[0] );
    
            // resized output
            $vt_image = array (
                'url' => $new_img,
                'width' => $new_img_size[0],
                'height' => $new_img_size[1]
            );
            
            return $vt_image;
        }
    
        // default output - without resizing
        $vt_image = array (
            'url' => $image_src[0],
            'width' => $image_src[1],
            'height' => $image_src[2]
        );
        
        return $vt_image;
    }
}

if ( !function_exists('welldone_vt_resize2') ) {
    function welldone_vt_resize2( $img_name, $dir_url, $dir_path, $width, $height, $crop = false ) {
        
        $file_path = trailingslashit($dir_path).$img_name;
        
        $orig_size = getimagesize( $file_path );
        
        $image_src[0] = trailingslashit($dir_url).$img_name;
        $image_src[1] = $orig_size[0];
        $image_src[2] = $orig_size[1];
        
        $file_info = pathinfo( $file_path );
    
        // check if file exists
        $base_file = $file_info['dirname'].'/'.$file_info['filename'].'.'.$file_info['extension'];
        if ( !file_exists($base_file) )
            return;
         
        $extension = '.'. $file_info['extension'];
    
        // the image path without the extension
        $no_ext_path = $file_info['dirname'].'/'.$file_info['filename'];
        
        // checking if the file size is larger than the target size
        // if it is smaller or the same size, stop right here and return
        if ( $image_src[1] > $width || $image_src[2] > $height ) {
    
            if ( $crop == true ) {
            
                $cropped_img_path = $no_ext_path.'-'.$width.'x'.$height.$extension;
                
                // the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
                if ( file_exists( $cropped_img_path ) ) {
        
                    $cropped_img_url = str_replace( get_template_directory() . $image_src[0], get_template_directory() . $cropped_img_path, $image_src[0] );
                    
                    $vt_image = array (
                        'url' => $cropped_img_url,
                        'width' => $width,
                        'height' => $height
                    );
                    
                    return $vt_image;
                }
            }
            elseif ( $crop == false ) {
            
                // calculate the size proportionaly
                $proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
                $resized_img_path = $no_ext_path.'-'.$proportional_size[0].'x'.$proportional_size[1].$extension;            
    
                // checking if the file already exists
                if ( file_exists( $resized_img_path ) ) {
                
                    $resized_img_url = str_replace( get_template_directory() . $image_src[0],get_template_directory() . $resized_img_path, $image_src[0] );
    
                    $vt_image = array (
                        'url' => $resized_img_url,
                        'width' => $proportional_size[0],
                        'height' => $proportional_size[1]
                    );
                    
                    return $vt_image;
                }
            }
    
            // check if image width is smaller than set width
            $img_size = getimagesize( $file_path );
            if ( $img_size[0] <= $width ) $width = $img_size[0];        
    
            // no cache files - let's finally resize it
            $image = wp_get_image_editor( $file_path );
            if ( ! is_wp_error( $image ) ) {
                $image->resize( $width, $height, $crop );
                $new_img_path = $image->save();
                $new_img_path = $new_img_path['path'];
            } else{
                $image = $file_path;
            }

            $new_img_size = getimagesize( $image );
            $new_img = str_replace( get_template_directory() . $image_src[0], get_template_directory() . $new_img_path, $image_src[0] );
    
            // resized output
            $vt_image = array (
                'url' => $new_img,
                'width' => $new_img_size[0],
                'height' => $new_img_size[1]
            );
            
            return $vt_image;
        }
    
        // default output - without resizing
        $vt_image = array (
            'url' => $image_src[0],
            'width' => $image_src[1],
            'height' => $image_src[2]
        );
        
        return $vt_image;
    }
}





// **********************************************************************// 
// ! Product brand label
// **********************************************************************//

add_action( 'admin_enqueue_scripts', 'welldone_et_brand_admin_scripts' );
if(!function_exists('welldone_et_brand_admin_scripts')) {
    function welldone_et_brand_admin_scripts() {
        $screen = get_current_screen();
        if ( in_array( $screen->id, array('edit-brand') ) )
          wp_enqueue_media();
    }
}
if(!function_exists('welldone_et_product_brand_image')) {
    function welldone_et_product_brand_image() {
        global $post, $wpdb, $product;
        $terms = wp_get_post_terms( $post->ID, 'brand' );

        if(count($terms)>0) {
            ?>
            <div class="product-brands">
                <?php
                    foreach($terms as $brand) {
                        $image          = '';
                        $thumbnail_id   = absint( get_woocommerce_term_meta( $brand->term_id, 'thumbnail_id', true ) );
                        if ($thumbnail_id) :
                            $image = welldone_get_image( $thumbnail_id );
                            ?>
                                <img src="<?php echo esc_url($image); ?>" title="<?php echo esc_attr($brand->name); ?>" alt="<?php echo esc_attr($brand->name); ?>" class="brand-image" />
                            <?php
                        endif;
                    }
                ?>
            </div>
            <?php
        }
        

        
    }
}

add_action( 'init', 'welldone_et_create_brand_taxonomies', 0 );
if(!function_exists('welldone_et_create_brand_taxonomies')) {
    function welldone_et_create_brand_taxonomies() {
        $labels = array(
            'name'              => esc_html__( 'Brands', "welldone" ),
            'singular_name'     => esc_html__( 'Brand', "welldone" ),
            'search_items'      => esc_html__( 'Search Brands', "welldone" ),
            'all_items'         => esc_html__( 'All Brands', "welldone" ),
            'parent_item'       => esc_html__( 'Parent Brand', "welldone" ),
            'parent_item_colon' => esc_html__( 'Parent Brand:', "welldone" ),
            'edit_item'         => esc_html__( 'Edit Brand', "welldone" ),
            'update_item'       => esc_html__( 'Update Brand', "welldone" ),
            'add_new_item'      => esc_html__( 'Add New Brand', "welldone" ),
            'new_item_name'     => esc_html__( 'New Brand Name', "welldone" ),
            'menu_name'         => esc_html__( 'Brands', "welldone" ),
        );

        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'capabilities'          => array(
                'manage_terms'      => 'manage_product_terms',
                'edit_terms'        => 'edit_product_terms',
                'delete_terms'      => 'delete_product_terms',
                'assign_terms'      => 'assign_product_terms',
            ),
            'rewrite'           => array( 'slug' => 'brand' ),
        );

        @register_taxonomy( 'brand', array( 'product' ), $args );
    }
}

add_action( 'brand_add_form_fields', 'welldone_et_brand_fields' );
if(!function_exists('welldone_et_brand_fields')) {
    function welldone_et_brand_fields() {
        global $woocommerce;
        ?>
        <div class="form-field">
            <label><?php esc_html_e( 'Thumbnail', 'welldone' ); ?></label>
            <div id="brand_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo woocommerce_placeholder_img_src(); ?>" width="60px" height="60px" /></div>
            <div style="line-height:60px;">
                <input type="hidden" id="brand_thumbnail_id" name="brand_thumbnail_id" />
                <button type="submit" class="upload_image_button button"><?php esc_html_e( 'Upload/Add image', 'welldone' ); ?></button>
                <button type="submit" class="remove_image_button button"><?php esc_html_e( 'Remove image', 'welldone' ); ?></button>
            </div>
            <script type="text/javascript">

                 // Only show the "remove image" button when needed
                 if ( ! jQuery('#brand_thumbnail_id').val() )
                     jQuery('.remove_image_button').hide();

                // Uploading files
                var file_frame;

                jQuery(document).on( 'click', '.upload_image_button', function( event ){

                    event.preventDefault();

                    // If the media frame already exists, reopen it.
                    if ( file_frame ) {
                        file_frame.open();
                        return;
                    }

                    // Create the media frame.
                    file_frame = wp.media.frames.downloadable_file = wp.media({
                        title: '<?php esc_html_e( 'Choose an image', 'welldone' ); ?>',
                        button: {
                            text: '<?php esc_html_e( 'Use image', 'welldone' ); ?>',
                        },
                        multiple: false
                    });

                    // When an image is selected, run a callback.
                    file_frame.on( 'select', function() {
                        attachment = file_frame.state().get('selection').first().toJSON();

                        jQuery('#brand_thumbnail_id').val( attachment.id );
                        jQuery('#brand_thumbnail img').attr('src', attachment.url );
                        jQuery('.remove_image_button').show();
                    });

                    // Finally, open the modal.
                    file_frame.open();
                });

                jQuery(document).on( 'click', '.remove_image_button', function( event ){
                    jQuery('#brand_thumbnail img').attr('src', '<?php echo woocommerce_placeholder_img_src(); ?>');
                    jQuery('#brand_thumbnail_id').val('');
                    jQuery('.remove_image_button').hide();
                    return false;
                });

            </script>
            <div class="clear"></div>
        </div>
        <?php
    }
}


add_action( 'brand_edit_form_fields', 'welldone_et_edit_brand_fields', 10,2 );
if(!function_exists('welldone_et_edit_brand_fields')) {
    function welldone_et_edit_brand_fields( $term, $taxonomy ) {
        global $woocommerce;
    
        $image          = '';
        $thumbnail_id   = absint( get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ) );
        if ($thumbnail_id) :
            $image = wp_get_attachment_thumb_url( $thumbnail_id );
        else :
            $image = woocommerce_placeholder_img_src();
        endif;
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php esc_html_e( 'Thumbnail', 'welldone' ); ?></label></th>
            <td>
                <div id="brand_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo esc_url($image); ?>" width="60px" height="60px" /></div>
                <div style="line-height:60px;">
                    <input type="hidden" id="brand_thumbnail_id" name="brand_thumbnail_id" value="<?php echo esc_attr($thumbnail_id); ?>" />
                    <button type="submit" class="upload_image_button button"><?php esc_html_e( 'Upload/Add image', 'welldone' ); ?></button>
                    <button type="submit" class="remove_image_button button"><?php esc_html_e( 'Remove image', 'welldone' ); ?></button>
                </div>
                <script type="text/javascript">
    
                    // Uploading files
                    var file_frame;
    
                    jQuery(document).on( 'click', '.upload_image_button', function( event ){
    
                        event.preventDefault();
    
                        // If the media frame already exists, reopen it.
                        if ( file_frame ) {
                            file_frame.open();
                            return;
                        }
    
                        // Create the media frame.
                        file_frame = wp.media.frames.downloadable_file = wp.media({
                            title: '<?php esc_html_e( 'Choose an image', 'welldone' ); ?>',
                            button: {
                                text: '<?php esc_html_e( 'Use image', 'welldone' ); ?>',
                            },
                            multiple: false
                        });
    
                        // When an image is selected, run a callback.
                        file_frame.on( 'select', function() {
                            attachment = file_frame.state().get('selection').first().toJSON();
    
                            jQuery('#brand_thumbnail_id').val( attachment.id );
                            jQuery('#brand_thumbnail img').attr('src', attachment.url );
                            jQuery('.remove_image_button').show();
                        });
    
                        // Finally, open the modal.
                        file_frame.open();
                    });
    
                    jQuery(document).on( 'click', '.remove_image_button', function( event ){
                        jQuery('#brand_thumbnail img').attr('src', '<?php echo woocommerce_placeholder_img_src(); ?>');
                        jQuery('#brand_thumbnail_id').val('');
                        jQuery('.remove_image_button').hide();
                        return false;
                    });
    
                </script>
                <div class="clear"></div>
            </td>
        </tr>
        <?php
    }
}

if(!function_exists('welldone_et_brands_fields_save')) {
    function welldone_et_brands_fields_save( $term_id, $tt_id, $taxonomy ) {
        
        if ( isset( $_POST['brand_thumbnail_id'] ) )
            update_woocommerce_term_meta( $term_id, 'thumbnail_id', absint( $_POST['brand_thumbnail_id'] ) );
    
        delete_transient( 'wc_term_counts' );
    }
}

add_action( 'created_term', 'welldone_et_brands_fields_save', 10,3 );
add_action( 'edit_term', 'welldone_et_brands_fields_save', 10,3 );



/**
 * Add the field to the checkout
 */
add_action( 'woocommerce_form_field', 'welldone_custom_checkout_field' );
 
function welldone_custom_checkout_field( $checkout ) {
 
    echo '<div id="welldone_custom_checkout_field"><h2>' . esc_html__('Extra Information', 'welldone') . '</h2>';
 
    woocommerce_form_field( 'my_field_name', array(
        'type'          => 'text',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => esc_html__('Fill in this field', 'welldone'),
        'placeholder'   => esc_html__('Enter something', 'welldone'),
        ), $checkout->get_value( 'my_field_name', 'welldone' ));
 
    echo '</div>';
 
}


global $product;

function welldone_override_mce_options($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
}
add_filter('tiny_mce_before_init', 'welldone_override_mce_options');


// Add OSD Subscribe filter
add_filter('osd_subscribe', 'filter_osd_subscribe', 10, 6);

// OSD Subscribe filter callback
function filter_osd_subscribe($default, $pre_content, $email_input, $message_div, $submit_input, $post_content) {
    return $pre_content.$email_input.$submit_input.$message_div.$post_content;
}

// Add video field to Total metabox for WooCommerce products
function welldone_add_woo_to_product_settings( $array ) {

    // Create your own tab - cool!
    $array['woo_video_tab'] = array(
        'title'     => esc_html__( 'Video', 'welldone' ), // Tab title
        'post_type' => array( 'product' ), // Used to limit by post type, to display on all don't add this param
        'settings'  => array(
            'wpex_post_video' => array(
                'title'         => esc_html__( 'Video', 'welldone' ), // Custom field title
                'description'   => esc_html__( 'Product video', 'welldone' ), // Custom field description
                'id'            => 'wpex_post_video', // Custom field ID used to retrive via get_post_meta
                'type'          => 'text', // Type to display in the admin
            ),
        ),
    );

    // Return fields
    return $array;

}

function welldone_woocommerce_image_dimensions() {
    global $pagenow;
 
    if ( ! isset( $_GET['activated'] ) || $pagenow != 'themes.php' ) {
        return;
    }
    $catalog = array(
        'width'     => '526',   // px
        'height'    => '660',   // px
        'crop'      => 1        // true
    );
    $single = array(
        'width'     => '457',   // px
        'height'    => '574',   // px
        'crop'      => 1        // true
    );
    $thumbnail = array(
        'width'     => '148',   // px
        'height'    => '184',   // px
        'crop'      => 0        // false
    );
    // Image sizes
    update_option( 'shop_catalog_image_size', $catalog );       // Product category thumbs
    update_option( 'shop_single_image_size', $single );         // Single product image
    update_option( 'shop_thumbnail_image_size', $thumbnail );   // Image gallery thumbs
}
add_action( 'after_switch_theme', 'welldone_woocommerce_image_dimensions', 1 );

add_filter( 'wpex_metabox_array', 'welldone_add_woo_to_product_settings' );


if ( !function_exists('welldone_product_countdown') ) {
    function welldone_product_countdown() {

        $html = '';
        global $args;
        $product_countdown = rwmb_meta( 'welldone_product_countdown', $args, get_the_ID() );
        // $product_countdown = date('Y/m/d', $product_countdown);

        if ( !empty($product_countdown) ) {

            // if ( empty($product_countdown['end_date']) || $product_countdown['end_date'] == 'yyyy-mm-dd' ) {
            //     return false;
            // }

            $today = time();

            $date = new DateTime($product_countdown);
            $date_time = $date->getTimestamp();

            if ( $date_time <= $today ) {
                return false;
            }

            $diff = $date_time - $today;

            $diff_items = array();

            $d = floor($diff/86400);

            if ( $d > 0 ) {
                $diff_items['days'] = $d;
            }

            $h = floor(($diff-$d*86400)/3600);

            if ( $h > 0 ) {
                $diff_items['hours'] = $h;
            }

            $m = floor(($diff-($d*86400+$h*3600))/60);

            if ( $m > 0 ) {
                $diff_items['minutes'] = $m;
            }

            $s = $diff-($d*86400+$h*3600+$m*60);
            if ( $s > 0 ) {
                $diff_items['seconds'] = $s;
            }

            $html .= '<div class="countdown_box"><div class="countdown_inner">';


            $html .= '<div class="title">' . esc_html__('special price valid:', "welldone") . '</div>';
            

            $sections = count($diff_items);

            $html .= '<div class="hasCountdown is-countdown"><span class="countdown-row countdown-show' . $sections . '">';

            if ( $diff_items['days'] ) {
                $html .= '<span class="countdown-section">
                    <span class="countdown-amount">' . $diff_items['days'] . '</span>
                    <span class="countdown-period">' . esc_html__('Days', "welldone") . '</span>
                </span>';
            }
            if ( $diff_items['hours'] ) {
                $html .= '<span class="countdown-section">
                    <span class="countdown-amount">' . $diff_items['hours'] . '</span>
                    <span class="countdown-period">' . esc_html__('Hrs', "welldone") . '</span>
                </span>';
            }
            if ( $diff_items['minutes'] ) {
                $html .= '<span class="countdown-section">
                    <span class="countdown-amount">' . $diff_items['minutes'] . '</span>
                    <span class="countdown-period">' . esc_html__('Min', "welldone") . '</span>
                </span>';
            }
            if ( $diff_items['seconds'] ) {
                $html .= '<span class="countdown-section">
                    <span class="countdown-amount">' . $diff_items['seconds'] . '</span>
                    <span class="countdown-period">' . esc_html__('Sec', "welldone") . '</span>
                </span>';
            }
            $html .= '</span></div>';
            $html .= '</div></div>';
            
            $year = date("Y", $date_time);
            $month = date("m", $date_time)-1;
            $day = date("d", $date_time);
            
            $html .= '<script type="text/javascript">
                        jQuery(function () {
                            jQuery(".countdown-show'. $sections .'").countdown({until: new Date('.$year.', '.$month.', '.$day.')});
                        });
                    </script>';

        }

        return $html;
    }
}

function welldone_echo_product_countdown() {
    echo welldone_product_countdown();
}
add_action('wft_after_shop_loop_item_image', 'welldone_echo_product_countdown', 10);



///////////////////////////////
if ( !function_exists('welldone_get_sale_products') ) {

    function welldone_get_sale_products($per_page = 12, $orderby='title', $order='asc') {

        global $woocommerce;

        // Get products on sale
        $product_ids_on_sale = woocommerce_get_product_ids_on_sale();

        $meta_query = array();
        $meta_query[] = WC()->query->visibility_meta_query();
        $meta_query[] = WC()->query->stock_status_meta_query();
        $meta_query   = array_filter( $meta_query );

        $args = array(
            'posts_per_page'=> $per_page,
            'orderby'       => $orderby,
            'order'         => $order,
            'no_found_rows' => 1,
            'post_status'   => 'publish',
            'post_type'     => 'product',
            'meta_query'    => $meta_query,
            'post__in'      => array_merge( array( 0 ), $product_ids_on_sale )
        );

        return new WP_Query( $args );
    }
}

if ( !function_exists('welldone_get_best_selling_products') ) {

    function welldone_get_best_selling_products($per_page = 12) {

        global $woocommerce_loop;

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page' => $per_page,
            'meta_key'       => 'total_sales',
            'orderby'        => 'meta_value_num',
            'meta_query' => array(
                array(
                    'key' => '_visibility',
                    'value' => array( 'catalog', 'visible' ),
                    'compare' => 'IN'
                )
            )
        );

        ob_start();

        return new WP_Query( $args );
    }
}

if ( !function_exists('welldone_get_top_rated_products') ) {

    function welldone_get_top_rated_products($per_page = 12, $orderby = 'title', $order = 'asc', $p_id=array()) {

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'ignore_sticky_posts'   => 1,
            'orderby' => $orderby,
            'order' => $order,
            'posts_per_page' => $per_page,
            'meta_query' => array(
                array(
                    'key' => '_visibility',
                    'value' => array('catalog', 'visible'),
                    'compare' => 'IN'
                )
            ),
            'post__in' => $p_id
        );

        add_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );

        $products = new WP_Query( $args );

        remove_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );

        return $products;

    }
}

if ( !function_exists('welldone_get_featured_products') ) {

    function welldone_get_featured_products($per_page = 12, $orderby = 'date', $order='desc') {

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page' => $per_page,
            'orderby' => $orderby,
            'order' => $order,
            'meta_query' => array(
                array(
                    'key' => '_visibility',
                    'value' => array('catalog', 'visible'),
                    'compare' => 'IN'
                ),
                array(
                    'key' => '_featured',
                    'value' => 'yes'
                )
            )
        );

        return new WP_Query( $args );

    }
}

if ( !function_exists('welldone_get_recent_products') ) {

    function welldone_get_recent_products($per_page = 12, $orderby='date', $order='desc', $category='', $p_id=array()) {

        $meta_query = WC()->query->get_meta_query();

        $args = array(
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'ignore_sticky_posts' => 1,
            'posts_per_page'      => $per_page,
            'orderby'             => $orderby,
            'order'               => $order,
            'meta_query'          => $meta_query,
            'slug'                => $category,
            'post__in'            => $p_id
        );

        return new WP_Query( $args );

    }

}

if ( !function_exists('welldone_get_related_products') ) {

    function welldone_get_related_products($per_page = 12, $orderby='date', $order='desc') {

        global $product;

        $related = $product->get_related( $per_page );

        if ( sizeof( $related ) == 0 ) return;

        $args = apply_filters('woocommerce_related_products_args', array(
            'post_type'             => 'product',
            'ignore_sticky_posts'   => 1,
            'no_found_rows'         => 1,
            'posts_per_page'        => $per_page,
            'orderby'               => $orderby,
            'post__in'              => $related,
            'post__not_in'          => array($product->id)
        ) );

        return new WP_Query( $args );

    }

}

if ( !function_exists('welldone_get_out_of_stock_products') ) {
    function welldone_get_out_of_stock_products($per_page = 12, $orderby='date', $order='desc') {

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'ignore_sticky_posts'   => 1,
            'posts_per_page' => $per_page,
            'orderby' => $orderby,
            'order' => $order,
            'meta_query' => array(
                array(
                    'key'       => '_stock_status',
                    'value'     => 'outofstock',
                    'compare'   => '='
                )
            )
        );

        return new WP_Query( $args );
    }
}

///////////////////////////////




function welldone_true_load_posts(){
    $args = unserialize(stripslashes($_POST['query']));
    $args['paged'] = $_POST['page'] + 1; // next page
    $args['post_status'] = 'publish';
    $q = new WP_Query($args);
    global $post;
    if( $q->have_posts() ):
        while($q->have_posts()): $q->the_post();
            
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class($class);?>>

                  <?php  if( ('video'==$post_format || 'gallery'==$post_format || 'audio'==$post_format) && !post_password_required()  ){ ?>
                        
                       <?php }if(has_post_thumbnail() && !post_password_required()){ ?>
                          <div class="entry-media">
                    <div class="list-blog-img">
                        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                            <span class="trim">
                            <?php 
                            if(isset($thumbnail_size)){
                                $url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $thumbnail_size );
                            ?>
                            
                            <img src="<?php echo esc_url($url[0]); ?>" title="<?php the_title(); ?> " alt="<?php  esc_attr(the_title()); ?>"/>
                            <?php 
                                
                            }else{
                                the_post_thumbnail();
                            }
                             ?>
                            </span>
                            <time class="img-circle" datetime=""><span><?php echo get_the_date('j'); ?></span><?php echo get_the_date('M'); ?></time>
                        </a>
                    </div>
                </div>
                                
                        <?php }
                  ?>
                  
                       

                            <h2 class="entry-title post__title text-uppercase"><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>

                           
                          <?php get_template_part('content',$post_format); ?> 
                            

                         <?php get_template_part("content","post"); ?>

                            <div class="entry-meta post__meta">
                            <?php if($post_format != 'aside') { ?>
                            <span class="post__meta__item"><span class="icon icon-clock"></span><?php the_time('F j, Y') ?></span>
                            <?php } elseif($post_format == 'aside'){ ?>
                                   <span class="post__meta__item"><span class="icon icon-chat"></span><?php esc_html_e( "Status on ", "welldone" ); ?><?php the_time('F j, Y') ?></span>
                            <?php }?>
                          
                            <?php if(!$blog_settings['hide_blog_post_author']){ ?>
                            <span class="post__meta__item"><span class="icon icon-user-circle"></span>
                                <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="post-author" title="<?php the_author(); ?>"><?php the_author(); ?></a>
                            </span>
                            <?php } ?>
                           <?php if(!$blog_settings['hide_blog_post_category'] && has_category()){ ?>
                                <span class="post__meta__item"><span class="icon icon-folder"></span><?php the_category(", "); ?></span>
                            <?php } ?>
                            <?php if(!$blog_settings['hide_blog_post_tags'] && !is_single() && has_tag()){ ?>
                                <span class="post__meta__item"><span class="icon icon-shop-label"></span><?php the_tags("", ", "); ?></span>
                            <?php } ?>
                        </div>

                                                                      
                        
                </article>
            <?php
        endwhile;
    endif;
    wp_reset_postdata();
    die();
}
 
 
add_action('wp_ajax_loadmore', 'welldone_true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'welldone_true_load_posts');

?>