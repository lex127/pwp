<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 */
add_filter ( 'rwmb_meta_boxes', 'welldone_register_meta_boxes' );
/**
 * Register meta boxes
 * @return void
 */
function welldone_register_meta_boxes ( $meta_boxes ) {
    /**
     * prefix of meta keys (optional)
     * Use underscore (_) at the beginning to make keys hidden
     * Alt.: You also can make prefix empty to disable it
     */
    // Better has an underscore as last sign
    $prefix = 'welldone_';
    $welldone_sidebar_nosidebar = array ();
    $welldone_sidebar = array ();
    $welldone_sidebar_nosidebar[ '' ] = "No Sidebar";
    global $post;
    $post_id = ( isset( $_GET[ 'post' ] ) && $_GET[ 'post' ] ) ? $_GET[ 'post' ] : '';
    $post_id = ( isset( $_REQUEST[ 'post' ] ) && $_REQUEST[ 'post' ] ) ? $_REQUEST[ 'post' ] : '';
    $template_file = get_post_meta ( $post_id, '_wp_page_template', TRUE );
    $categories = get_terms ( 'portfolio-category', 'orderby=count&hide_empty=0' );
    $welldone_portfolio_categories[ 'all' ] = __ ( 'All Categories', 'welldone' );
    if ( $categories && !is_wp_error ( $categories ) ) {
        foreach ( $categories as $category ) {
            $welldone_portfolio_categories[ $category->name ] =  $category->name;
        }
    }
    foreach ( $welldone_portfolio_categories as $key => $value ) {
        if ( $key == 'uncategorized' ) continue;
        $welldone_portfolio_default[ ] = $key;
    }
    $wp_registered_sidebars = wp_get_sidebars_widgets ();
    foreach ( $wp_registered_sidebars as $sidebar => $sidebar_info ) {
        if ( $sidebar == 'wp_inactive_widgets' ) continue;
        $welldone_sidebar[ $sidebar ] = ucwords ( str_replace ( array ( '_', '-' ), ' ', $sidebar ) );
        $welldone_sidebar_nosidebar[ $sidebar ] = ucwords ( str_replace ( array ( '_', '-' ), ' ', $sidebar ) );
    }
// Page
    require_once ( get_template_directory().'/inc/functions/metaboxes/meta-page.php' );
// Post
    require_once ( get_template_directory().'/inc/functions/metaboxes/meta-post.php' );
// testimonials
    require_once( get_template_directory().'/inc/functions/metaboxes/meta-testimonial.php');
// Product
    require_once( get_template_directory().'/inc/functions/metaboxes/meta-product.php');
    //general
    require_once ( get_template_directory().'/inc/functions/metaboxes/meta-general.php' );
    return $meta_boxes;
}