<?php
//global $welldone_sidebar;
$meta_boxes[ ] = array (
// Meta box id, UNIQUE per meta box. Optional since 4.1.5
    'id' => 'client_options',
// Meta box title - Will appear at the drag and drop handle bar. Required.
    'title' => esc_html__( 'Clients Options', 'welldone' ),
// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
    'pages' => array ( 'clients' ),
// Where the meta box appear: normal (default), advanced, side. Optional.
    'context' => 'normal',
// Order of meta box: high (default), low. Optional.
    'priority' => 'high',
// Auto save: true, false (default). Optional.
    'autosave' => true,
// List of meta fields
    'fields' => array (
//Blog Template Options
// HEADING
        array (
            'type' => 'heading',
            'name' => esc_html__( 'Client\'s Options', 'welldone' ),
            'id' => 'clients_page_heading',
        ),
        array (
            'name' => esc_html__( 'Client\'s URL', 'welldone' ),
            'id' => "{$prefix}client_url",
            'type' => 'url',
            'desc' => esc_html__('Client\'s URL','welldone')
        )
    )
);
?>