<?php
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id' => 'blog_post_options',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title' => esc_html__( 'Post Options', 'welldone' ),
		// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
		'pages' => array( 'post' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context' => 'advanced',
		// Order of meta box: high (default), low. Optional.
		'priority' => 'high',
		// Auto save: true, false (default). Optional.
		'autosave' => true,
		// List of meta fields
        'fields' => array(
            array(
                'type' => 'heading',
                'name' => esc_html__( 'Post Meta Options', 'welldone' ),
                'id'   => 'format_gallery_heading', // Not used but needed for plugin
            ),
            array (
                'name' =>  esc_html__( 'Posts Author Name', 'welldone' ),
                'id' => "{$prefix}hide_post_author",
                'type' => 'select_advanced',
                'options' => array (
                    '' =>  esc_html__( 'Use Default', 'welldone' ),
                    '1' =>  esc_html__( 'Hide Posts Author Name', 'welldone' ),
                ),
            ),
            array (
                'name' =>  esc_html__( 'Posts Category', 'welldone' ),
                'id' => "{$prefix}hide_post_category",
                'type' => 'select_advanced',
                'options' => array (
                    '' =>  esc_html__( 'Use Default', 'welldone' ),
                    '1' =>  esc_html__( 'Hide Posts Category', 'welldone' ),
                ),
            ),
            array (
                'name' =>  esc_html__( 'Posts Tags', 'welldone' ),
                'id' => "{$prefix}hide_post_tags",
                'type' => 'select_advanced',
                'options' => array (
                    '' =>  esc_html__( 'Use Default', 'welldone' ),
                    '1' =>  esc_html__( 'Hide Posts Tags', 'welldone' ),
                ),
            ),
            array(
                'type' => 'heading',
                'name' => esc_html__( 'Gallery Fields (Use in Gallery Format)', 'welldone' ),
                'id'   => 'format_gallery_heading', // Not used but needed for plugin
            ),
            array(
                'name'             => esc_html__( 'Slider Images', 'welldone' ),
                'id'               => "{$prefix}option_image",
                'type'             => 'image_advanced',
                'max_file_uploads' => 6,
            ),
            array(
                'type' => 'heading',
                'name' => esc_html__( 'Video Fields (Use in Video Format)', 'welldone' ),
                'id'   => 'format_video_heading', // Not used but needed for plugin
            ),
            array(
                'name' => esc_html__( 'Video URL', 'welldone' ),
                'id'   => "{$prefix}video_embed",
                'type' => 'oembed',
                'desc' => 'Paste the URL of the Flash (YouTube or Vimeo etc). Only necessary when the post format is video.',
                //class="embed-responsive-item"
            ),
            array(
                'type' => 'heading',
                'name' => esc_html__( 'Audio Fields (Use in Audio Format)', 'welldone' ),
                'id'   => 'format_audio_heading', // Not used but needed for plugin
            ),
            array(
                'name' => esc_html__( 'Audio file upload', 'welldone' ),
                'id'   => "{$prefix}file_audio",
                'type' => 'file_advanced',
                'max_file_uploads' => 1,
                'mime_type' => 'audio', // Leave blank for all file types
            ),
            array(
                'type' => 'heading',
                'name' => esc_html__( 'Quote Fields (Use in Quote Format)', 'welldone' ),
                'id'   => 'format_quote_heading', // Not used but needed for plugin
            ),
            array(
                'name' => esc_html__( 'Author', 'welldone' ),
                'id'   => "{$prefix}quote_author",
                'type' => 'text',
            ),
            array(
                'name' => esc_html__( 'Quote', 'welldone' ),
                'id'   => "{$prefix}quote_content",
                'type' => 'textarea',
            ),
            array(
                'name' => esc_html__( 'Quote Icon', 'welldone' ),
                'id'   => "{$prefix}quote_icon",
                'type' => 'checkbox_list',
                'options' => array (
                    '1' => esc_html__( 'Use Blockquote Icon', 'welldone' ),
                ),

            ),
            array(
                'type' => 'heading',
                'name' => esc_html__( 'Link Field (Use in Link Format)', 'welldone' ),
                'id'   => 'format_link_heading', // Not used but needed for plugin
            ),

            array(
                'name' => esc_html__( 'Link', 'welldone' ),
                'id'   => "{$prefix}post_external_link",
                'type' => 'url',
                'desc' => esc_html__('Enter URL for link format post.','welldone')
            ),
        )
	);
?>