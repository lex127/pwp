<?php
add_action( 'widgets_init', 'welldone_widgets_init' );

function welldone_widgets_init() {
    register_sidebar ( array (
        'name' => esc_html__( 'Blog Sidebar 1', 'welldone' ),
        'id' => 'blog-sidebar-1',
        'before_widget' => '<div id="%1$s" class="page-sidebar  %2$s">',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'after_widget' => '</div>'
    ) );
//Page SIdebars
    register_sidebar ( array (
        'name' => esc_html__( 'Page Sidebar 1', 'welldone' ),
        'id' => 'page-sidebar-1',
        'before_widget' => '<div id="%1$s" class="page-sidebar %2$s">',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'after_widget' => '</div>'
    ) );

      register_sidebar ( array (
        'name' => esc_html__( 'Page Sidebar 2', 'welldone' ),
        'id' => 'page-sidebar-2',
        'before_widget' => '<div id="%1$s" class="page-sidebar %2$s">',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'after_widget' => '</div>'
    ) );

    //post SIdebars
    register_sidebar ( array (
        'name' => esc_html__( 'Single Post Sidebar', 'welldone' ),
        'id' => 'single-post-sidebar',
        'before_widget' => '<div id="%1$s" class="page-sidebar %2$s">',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'after_widget' => '</div>'
    ) );
//shop page sidebar
//Page SIdebars
    register_sidebar ( array (
        'name' => esc_html__( 'Shop Page Sidebar 1', 'welldone' ),
        'id' => 'shop-page-sidebar-1',
        'before_widget' => '<div id="%1$s" class="filters-col__collapse open page-sidebar %2$s">',
        'before_title' => '<h4 class="filters-col__collapse__title text-uppercase">',
        'after_title' => '</h4>',
        'after_widget' => '</div>'
    ) );
    register_sidebar ( array (
        'name' => esc_html__( 'Shop Page Sidebar 2', 'welldone' ),
        'id' => 'shop-page-sidebar-2',
        'before_widget' => '<div id="%1$s" class="page-sidebar %2$s">',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'after_widget' => '</div>'
    ) );
      //product sidebar
    register_sidebar ( array (
        'name' => esc_html__( 'Single Product Sidebar', 'welldone' ),
        'id' => 'single-product-sidebar',
        'before_widget' => '<div id="%1$s" class="page-sidebar %2$s">',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
        'after_widget' => '</div>'
    ) );
//Footer Widgets
    register_sidebar ( array (
        'name' => esc_html__( 'Footer Widget 1', 'welldone' ),
        'id' => 'footer_1',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'before_title' => '<h5 class="title text-uppercase mobile-collapse__title">',
        'after_title' => '</h5>',
        'after_widget' => '</div>'
    ) );
    register_sidebar ( array (
        'name' => esc_html__( 'Footer Widget 2', 'welldone' ),
        'id' => 'footer_2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'before_title' => '<h5 class="title text-uppercase mobile-collapse__title">',
        'after_title' => '</h5>',
        'after_widget' => '</div>'
    ) );
    register_sidebar ( array (
        'name' => esc_html__( 'Footer Widget 3', 'welldone' ),
        'id' => 'footer_3',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'before_title' => '<h5 class="title text-uppercase mobile-collapse__title">',
        'after_title' => '</h5>',
        'after_widget' => '</div>'
    ) );
    register_sidebar ( array (
        'name' => esc_html__( 'Footer Widget 4', 'welldone' ),
        'id' => 'footer_4',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'before_title' => '<h5 class="title text-uppercase mobile-collapse__title">',
        'after_title' => '</h5>',
        'after_widget' => '</div>'
    ) );
    register_sidebar ( array (
        'name' => esc_html__( 'Footer Widget 5', 'welldone' ),
        'id' => 'footer_5',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'before_title' => '<h5 class="title text-uppercase mobile-collapse__title">',
        'after_title' => '</h5>',
        'after_widget' => '</div>'
    ) );
}

/**
 * Contact Information Widget Class
 */
class Welldone_WP_Widget_Contact_Info extends WP_Widget {
    function __construct() {
        $widget_ops = array( 
            'classname' => 'widget_contact_info', 
            'description' => esc_html__('Your contact information', 'welldone') 
        );
        $control_ops = array( 
            'width' => 600 
        );
        
        parent::__construct('custom-contact-info', '&nbsp;' . esc_html__('Welldone - Contact Information', 'welldone'), $widget_ops, $control_ops);
    }
    
    function widget($args, $instance) {
        extract($args);
        
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        $address = isset($instance['address']) ? $instance['address'] : '';
        $phone = isset($instance['phone']) ? $instance['phone'] : '';
        $email = isset($instance['email']) ? $instance['email'] : '';
        $skype = isset($instance['skype']) ? $instance['skype'] : '';
        
        echo '<div class="v-links-list mobile-collapse__content">' . 
            $before_widget;
        
        if ($title) { 
            echo $before_title . $title . $after_title;
        }
        echo '<div class="v-links-list mobile-collapse__content"><ul>';
        if ($address != '') { 
            echo '<li class="icon icon-home">' . $address . '</li>';
        }
        
        if ($phone != '') { 
            echo '<li class="icon icon-telephone">' . $phone . '</li>';
        }
        
        if ($email != '') { 
            echo '<li class="icon icon-mail">' . 
                '<a href="mailto:' . $email . '">' . $email . '</a>' . 
            '</li>';
        }

        if ($skype != '') { 
            echo '<li class="icon icon-skype">' . $skype . '</li>';
        }
        echo '</ul></div>';
        
        echo $after_widget . 
        '</div>';
    }
    
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['address'] = strip_tags($new_instance['address']);
        $instance['phone'] = strip_tags($new_instance['phone']);
        $instance['email'] = strip_tags($new_instance['email']);
        $instance['skype'] = strip_tags($new_instance['skype']);
        
        return $instance;
    }
    
    function form($instance) {
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $address = isset($instance['address']) ? esc_attr($instance['address']) : '';
        $phone = isset($instance['phone']) ? esc_attr($instance['phone']) : '';
        $email = isset($instance['email']) ? esc_attr($instance['email']) : '';
        $skype = isset($instance['skype']) ? esc_attr($instance['skype']) : '';
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title', 'welldone'); ?>:<br />
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('address'); ?>"><?php esc_html_e('Address', 'welldone'); ?>:<br />
                <input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo $address; ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php esc_html_e('Phone', 'welldone'); ?>:<br />
                <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo $phone; ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php esc_html_e('Email', 'welldone'); ?>:<br />
                <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" />
            </label>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('skype'); ?>"><?php esc_html_e('Skype', 'welldone'); ?>:<br />
                <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('skype'); ?>" type="text" value="<?php echo $skype; ?>" />
            </label>
        </p>
        <div class="cl"></div>
        <?php
    }
}

add_action("widgets_init", function () {
    register_widget("Welldone_WP_Widget_Contact_Info");
});