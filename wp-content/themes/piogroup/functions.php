<?php
/**
 * welldone functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package welldone
 */


// Image Size
//add image sizes

add_image_size( 'welldone-shop-image', 200, 276, false );
add_image_size( "welldone-single-product", 470, 630, false );
add_image_size( 'welldone-shop-widget', 58, 72, false );
add_image_size( 'welldone-shop-widget-2', 74, 92, true );
add_image_size( 'welldone-blog-slider', 215, 215, true );
add_image_size( 'welldone-custom-size', 215, 215, true );
add_image_size( 'welldone-wish-list-thumb', 160, 200, true );
add_image_size( "welldone-blog-other-style", 870, 420, false );
add_image_size( "welldone-large", 870, 420, false );
add_image_size( "welldone-blog-image", 570, 390, false );
add_image_size( "welldone-thumb-60", 60, 70, false );
add_image_size( "welldone-search-page-img", 350, 310, true );
add_image_size( "welldone-projects-archive-img", 400, 310, true );
add_image_size( "welldone-projects-slide-in-single", 260, 310, true );


require get_template_directory() . '/inc/plugins/sidebar-generator/sidebar_generator.php';


if ( ! function_exists( 'welldone_setup' ) ) :

	function welldone_setup() {

		load_theme_textdomain( 'welldone', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/* Enable support for Post Formats. */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'gallery',
			'video',
			'audio',
			'quote',
			'link',
		) );

		// Woocommerce Support
		add_theme_support( "woocommerce" );
		if ( defined( "WOOCOMMERCE_VERSION" ) ) {
			if ( version_compare( WOOCOMMERCE_VERSION, "2.1" ) >= 0 ) {
				add_filter( "woocommerce_enqueue_styles", "__return_false" );
			} else {
				define( "WOOCOMMERCE_USE_CSS", false );
			}
		}

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'etheme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
	}
endif; // welldone_setup
add_action( 'after_setup_theme', 'welldone_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function welldone_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'welldone_content_width', 640 );
}

add_action( 'after_setup_theme', 'welldone_content_width', 0 );


if ( ! function_exists( 'welldone_enqueue_scripts' ) ) {
	function welldone_enqueue_scripts() {

		global $etheme_theme_data;
		$etheme_theme_data = wp_get_theme( 'welldone' );

		if ( ! is_admin() ) {

			wp_enqueue_style( 'style-colors', get_template_directory_uri() . '/css/style-colors.css', array(), '1.0.0', 'screen' );

			wp_enqueue_style( 'waves', get_template_directory_uri() . '/css/waves.css', array(), '1.0.0', 'screen' );

			wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick/slick.css', array(), '1.0.0', 'screen' );

			wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick/slick-theme.css', array(), '1.0.0', 'screen' );

			wp_enqueue_style( 'nouislider', get_template_directory_uri() . '/css/nouislider.css', array(), '1.0.0', 'screen' );

			wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(), '1.0.0', 'screen' );

			wp_enqueue_style( 'bootstrap-select', get_template_directory_uri() . '/css/bootstrap-select.css', array(), '1.0.0', 'screen' );

			wp_enqueue_style( 'style-fonts', get_template_directory_uri() . '/font/style-fonts.css', array(), '1.0.0', 'screen' );

			wp_register_style( "countdown-styles", get_template_directory_uri() . '/css/jquery.countdown.css' );

			wp_enqueue_style( "style", get_stylesheet_directory_uri() . '/style.css', array(), $etheme_theme_data->Version );

			wp_enqueue_style( "welldone-override", get_stylesheet_directory_uri() . '/css/welldone-override.css', array(), $etheme_theme_data->Version );
			wp_enqueue_style( 'jquery-confirm', get_template_directory_uri() . '/inc/confirm/jquery-confirm.min.css', array(), '1.0.0', 'screen' );

			wp_register_style( "rtl", get_template_directory_uri() . '/css/rtl.css' );

			$filename = get_template_directory() . '/_config/settings_' . get_current_blog_id() . '.css';
			if ( file_exists( $filename ) ) {
				wp_register_style( 'settings_', get_template_directory_uri() . '/_config/settings_' . get_current_blog_id() . '.css' );
				wp_enqueue_style( 'settings_' );
			}
			if ( is_rtl() ) {
				wp_enqueue_style( "rtl", get_template_directory_uri() . '/css/rtl.css' );
			}
			if ( is_page( 'privacy-policy' ) ) {
				wp_enqueue_style( "privacy-policy-page", get_template_directory_uri() . '/css/privacy-policy-page.css' );
			}

			//scripts
			wp_enqueue_script( "bootstrap-js", get_template_directory_uri() . '/js/bootstrap.min.js', array(), false, true );
			wp_enqueue_script( "bootstrap-select-min-js", get_template_directory_uri() . '/js/bootstrap-select.min.js', array(), false, true );
			wp_enqueue_script( "slick-min-js", get_template_directory_uri() . '/js/slick.min.js', array(), false, true );
			wp_enqueue_script( "waves-min-js", get_template_directory_uri() . '/js/waves.min.js', array(), false, true );
			wp_enqueue_script( "jquery-parallax", get_template_directory_uri() . '/js/jquery.parallax.js', array(), false, true );
			wp_enqueue_script( "isotope-pkgd", get_template_directory_uri() . '/js/isotope/isotope.pkgd.min.js', array(), false, true );
			wp_enqueue_script( "elevateZoom", get_template_directory_uri() . '/js/jquery.elevateZoom.min.js', array(), false, true );
			wp_enqueue_script( "doubletaptogo", get_template_directory_uri() . '/js/doubletaptogo.js', array(), false, true );
			wp_enqueue_script( "imagesloaded-pkgd", get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array(), false, true );
			wp_enqueue_script( "jquery-plugin", get_template_directory_uri() . '/js/jquery.plugin.js', array(), false, true );
			wp_enqueue_script( "jquery-countdown", get_template_directory_uri() . '/js/jquery.countdown.js', array(), false, true );
			wp_enqueue_script( "countTo", get_template_directory_uri() . '/js/jquery.countTo.js', array(), false, true );
			wp_enqueue_script( "nouislider-min", get_template_directory_uri() . '/js/nouislider.min.js', array(), false, true );

			wp_enqueue_script( "app_waypoints_1", get_template_directory_uri() . '/js/jquery.waypoints.min.js', array(), false, true );
			wp_enqueue_script( "sticky_header", get_template_directory_uri() . '/js/sticky.min.js', array(), false, true );
			wp_enqueue_script( "jquery-inview", get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), false, true );
			wp_enqueue_script( "mousewheel", get_template_directory_uri() . '/js/jquery.mousewheel.min.js', array(), false, true );
			wp_enqueue_script( 'addthis', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57081e34b9649297', array(), false, true );

			wp_enqueue_script( "jquery-confirm", get_template_directory_uri() . '/inc/confirm/jquery-confirm.min.js', array(), false, true );

			wp_enqueue_script( "custom-setting", get_template_directory_uri() . '/js/custom-setting.js', array(), false, true );

			if ( ( ! is_admin() ) && is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}

			// Modernizr
			wp_enqueue_script( "modernizr", get_template_directory_uri() . '/js/modernizr.js', array(), false, true );

			wp_enqueue_script( "welldone-landing-banners", get_template_directory_uri() . '/js/welldone-landing-banners.js', array(), false, true );
			wp_enqueue_script( "welldone-custom", get_template_directory_uri() . '/js/welldone-custom.js', array(), false, true );
			wp_enqueue_script( "wc-country-select", get_template_directory_uri() . '/woocommerce/js/country-select.js', array(), false, true );

		}
	}
}

add_action( 'wp_enqueue_scripts', 'welldone_enqueue_scripts', 30 );

// add_action('wp_enqueue_scripts', 'override_woo_frontend_scripts');
// function override_woo_frontend_scripts() {
//     wp_deregister_script('wc-country-select');
//     wp_enqueue_script('wc-country-select', get_template_directory_uri() . '/woocommerce/js/country-select.js', array('jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'), null, true);
// }

// function load_my_scripts() {
//   wp_dequeue_script('wc-country-select');
//   wp_enqueue_script('wc-country-select', get_template_directory_uri() . '/woocommerce/js/country-select.js' , array(), false, true);
// }

// add_action('wp_enqueue_scripts', 'load_my_scripts');

/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
	add_editor_style( 'custom-editor-style.css' );
}

add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );
require_once( get_template_directory() . '/inc/custom/shortcode.php' );

require_once( get_template_directory() . '/inc/functions/general.php' );

require_once( get_template_directory() . '/inc/functions/post.php' );

require_once( get_template_directory() . '/inc/functions/widgets.php' );

require_once( get_template_directory() . '/inc/functions/theme-widgets.php' );

require_once( get_template_directory() . '/inc/functions/metaboxes.php' );

require_once( get_template_directory() . '/inc/functions/meta-taxonomy.php' );

require_once( get_template_directory() . '/inc/functions/woocommerce.php' );

require_once( get_template_directory() . '/inc/menu/menu-option.php' );

if ( file_exists( get_template_directory() . '/inc/admin/welldone/functions.php' ) ) {
	require_once( get_template_directory() . '/inc/admin/welldone/functions.php' );
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


require get_template_directory() . '/inc/plugin-activator.php';


/**
 * Embedded Redux Framework
 */

if ( ! class_exists( 'ReduxFramework' ) && file_exists( get_template_directory() . '/inc/admin/ReduxCore/framework.php' ) ) {
	require_once( get_template_directory() . '/inc/admin/ReduxCore/framework.php' );
}
if ( ! isset( $welldone_settings ) && file_exists( get_template_directory() . '/inc/admin/theme-options.php' ) ) {
	require_once( get_template_directory() . '/inc/admin/theme-options.php' );
}
function removeDemoModeLink() { // Be sure to rename this function to something more unique
	if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
		remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks' ), null, 2 );
	}
	if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
		remove_action( 'admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
	}
}

add_action( 'init', 'removeDemoModeLink' );

require get_template_directory() . '/inc/piogroup.php';

require_once( get_template_directory() . '/inc/ajax-auth/custom-ajax-auth.php' );

add_filter( 'nav_menu_css_class', 'add_active_class', 10, 2 );

function add_active_class( $classes, $item ) {
	if ( $item->menu_item_parent == 0 && in_array( 'current-menu-item', $classes ) ) {
		$classes[] = "active-menu";
	}

	return $classes;
}

function custom_email_replace( $old_replacements, $user_id ) {
	$user = get_userdata( $user_id );
	// Generate the new url we need for this user

	// An array of our replacement variables and their values
	$new_replacements = array(
		'%my_first_name%' => $user->first_name,
	);
	// Merge our two arrays together
	$replacements = array_merge( $old_replacements, $new_replacements );

	// Return the array of all the replacements
	return $replacements;
}

add_filter( 'tml_replace_vars', 'custom_email_replace', 99, 2 );