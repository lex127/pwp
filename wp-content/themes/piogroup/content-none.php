<?php
/**
 * The template for displaying a "No posts found" message
 *
 * @package WordPress
 * @subpackage welldone
 * @since welldone 1.0
 */
?>

		<h2><?php esc_html_e( 'Nothing Found', 'welldone' ); ?></h2>
		<section>
			<div>
				<?php if ( is_home() && current_user_can( 'publish_posts' ) ) { ?>
					<p><?php printf( esc_html__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'welldone' ), admin_url( 'post-new.php' ) ); ?></p>

				<?php }elseif ( is_search() ) { ?>
				<p><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'welldone'); ?></p>
				<div class="entry"><?php get_search_form(); ?></div>

			<?php 	}else { ?>
    <p><?php esc_html_e( 'It seems we can not find what you are looking for. Perhaps searching can help.', 'welldone' ); ?></p>
    <div class="entry"><?php get_search_form(); ?></div>
<?php } ?>
    </div>
    </section>