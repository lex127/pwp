<?php

class AMPHTML_Update {

	public $current_version;

	public function __construct() {
		$this->current_version = get_option( 'amphtml_version' );
		$this->update_template_options();
	}

	public function update_template_options() {
		$update_ver        = '6.6';
		$disabled_elements = array(
			'archive_title',
			'post_title',
			'post_content',
			'page_title',
			'page_content',
			'search_page_title',
			'archive_content_block',
			'blog_content_block',
			'search_page_content_block',
		);

		if ( $this->current_version < $update_ver ) {
			do_action( 'update_6_6' );
			foreach ( $disabled_elements as $element ) {
				update_option( 'amphtml_' . $element, 1 );
			}
		}
	}
}