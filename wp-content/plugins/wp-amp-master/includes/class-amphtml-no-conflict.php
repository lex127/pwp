<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class AMPHTML_No_Conflict {

	public $fixed_doc_title = '';

	public function __construct() {
		$this->no_conflict();
	}

	public function no_conflict() {
		if ( AMPHTML()->is_amp() ) {
			remove_filter( 'template_redirect', 'redirect_canonical' );
			add_action( 'wp', array( $this, 'rocket_disable_options_on_amp' ) );
			add_action( 'wp', array( $this, 'disable_aisop_ga' ) );
			add_filter( 'bj_lazy_load_run_filter', '__return_false' );
			$this->WPML();
			$this->hyper_cache_compatibility();
			$this->jetpack_compatibility();
			add_filter( 'amphtml_illegal_tags', array( $this, 'sanitize_goodlayer_content' ) );
			if ( AMPHTML::instance()->is_yoast_seo() ) {
				add_action( 'pre_get_document_title', array ( $this, 'fix_yoast_seo' ), 10 );
			}
			if ( AMPHTML::instance()->is_aiosp() ) {
				add_action( 'wp', array ( $this, 'fix_aiosp_title' ), 999 );
				add_action( 'wp', array ( $this, 'fix_aiosp_desc' ), 999 );
			}
			$this->disable_w3_total_cache();
			
			// WP Ultimate Recipe support
			if ( in_array( 'wp-ultimate-recipe/wp-ultimate-recipe.php', get_option( 'active_plugins' ) ) ) {
				add_filter( 'amphtml_single_content', array( $this, 'amphtml_single_content_urecipe') );
			}
			
		}
	}

	/**
	 * Remove Minification, DNS Prefetch, LazyLoad, Defer JS when on an AMP version of a post
	 */
	function rocket_disable_options_on_amp() {
		if ( function_exists( 'wp_resource_hints' ) ) {
			remove_filter( 'wp_resource_hints', 'rocket_dns_prefetch', 10 );
		} else {
			remove_filter( 'rocket_buffer', '__rocket_dns_prefetch_buffer', 12 );
		}
		remove_filter( 'rocket_buffer', 'rocket_exclude_deferred_js', 11 );
		remove_filter( 'rocket_buffer', 'rocket_dns_prefetch', 12 );
		remove_filter( 'rocket_buffer', 'rocket_minify_process', 13 );
		add_filter( 'do_rocket_lazyload', '__return_false' );
	}

	/**
	 * all-in-one-seo-pack
	 */
	public function disable_aisop_ga() {
		if ( AMPHTML()->is_aiosp() ) {
			global $aiosp;
			remove_action( 'aioseop_modules_wp_head', array( $aiosp, 'aiosp_google_analytics' ) );
			remove_action( 'wp_head', array( $aiosp, 'aiosp_google_analytics' ) );
			add_filter( 'aiosp_google_analytics', '__return_false' );
		}
	}

	/**
	 * WPML
	 */
	public function WPML() {
		if ( class_exists( 'SitePress' ) ) {
			define( 'ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true );
		}
	}

	/*
	 * Hyper Cache
	 */
	public function hyper_cache_compatibility() {
		global $cache_stop;
		if ( AMPHTML()->is_amp() !== false ) {
			$cache_stop = true;
		}
	}

	/*
	 * Jetpack
	 */
	public function jetpack_compatibility() {
		//remove sharing buttons
		add_filter( 'sharing_show', '__return_false' );
		//remove related posts
		add_filter( 'wp', array ( $this, 'jetpackme_remove_rp' ), 20 );
		//remove like buttons
		add_action( 'wp', array ( $this, 'jetpackme_remove_likes' ) );
		//add jetpack stats
		add_action( 'wp', array ( $this, 'jetpackme_add_stats' ) );
	}

	public function jetpackme_remove_rp() {
		if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
			$jprp     = Jetpack_RelatedPosts::init();
			$callback = array ( $jprp, 'filter_add_target_to_dom' );
			remove_filter( 'the_content', $callback, 40 );
		}
	}

	public function jetpackme_remove_likes() {
		remove_filter( 'the_content', 'sharing_display', 19 );
		remove_filter( 'the_excerpt', 'sharing_display', 19 );
		if ( class_exists( 'Jetpack_Likes' ) ) {
			remove_filter( 'the_content', array( Jetpack_Likes::init(), 'post_likes' ), 30, 1 );
		}
	}

	public function jetpackme_add_stats() {
		if ( class_exists( 'Jetpack' ) AND method_exists( 'Jetpack', 'is_module_active' ) ) {
			if( Jetpack::is_module_active( 'stats' ) ) {
				if ( function_exists( 'stats_build_view_data' ) ) { //support only for latest versions
					add_filter( 'amphtml_template_footer_content', array( $this, 'jetpackme_add_stats_pixel' ) );
				}
			}
		}
	}

	public function jetpackme_add_stats_pixel( $footer_content ) {
		$footer_content .= '<amp-pixel src="' . $this->jetpackme_get_stats_pixel_url() . '"></amp-pixel>'; 

		return $footer_content;
	}

	function jetpackme_get_stats_pixel_url() {
		$data         = stats_build_view_data();
		$data['host'] = rawurlencode( $_SERVER['HTTP_HOST'] );
		$data['rand'] = 'RANDOM';
		$data['ref']  = 'DOCUMENT_REFERRER';
		$data         = array_map( 'rawurlencode' , $data );

		return add_query_arg( $data, 'https://pixel.wp.com/g.gif' );
	}

	public function sanitize_goodlayer_content( $tags ) {
		$tags[] = 'pre[class=gdlr_rp]';
		return $tags;
	}

	public function fix_yoast_seo( $title ) {
		global $wp_query;

		if ( function_exists( 'is_shop' ) && is_shop() ) {
			$wp_query->queried_object->ID = wc_get_page_id( 'shop' );
			add_filter( 'wpseo_metadesc', array( $this, 'wpseo_metadesc_filter_fix' ) );
		}

		$this->fixed_doc_title = WPSEO_Frontend::get_instance()->title( '' );

		add_filter( 'amphtml_template_load_after', array( $this, 'get_yoast_seo_title' ) );

		return $title;
	}

	public function get_yoast_seo_title( $template ) {
		global $wp_query;

		if ( $this->fixed_doc_title ) {
			$template->doc_title = $this->fixed_doc_title;
		}

		return $template;
	}

	public function wpseo_metadesc_filter_fix( $metadesc ) {
		$current_post_id = get_option( 'page_on_front' );
		$wpseo_metadesc  = WPSEO_Meta::get_value( 'metadesc', $current_post_id );

		return $wpseo_metadesc;
	}

	public function fix_aiosp_title() {
		global $wp_query;

		if ( ( $wp_query->is_posts_page != false ) || ( is_home() && ( $wp_query->is_posts_page == false ) ) ) {
			add_filter( 'amphtml_template_load_after', array ( $this, 'aiosp_fix_title' ) );
		}
	}

	public function aiosp_fix_title( $template ) {
		global $aioseop_options, $wp_query;

		if ( $wp_query->is_posts_page ) {
			$page_id             = get_option( 'page_for_posts' );
			$title_format        = $aioseop_options['aiosp_page_title_format'];
			$title_origin        = get_the_title( $page_id );
			$title_aoiseo        = get_post_meta( $page_id, '_aioseop_title', true );
			$title               = ! empty( $title_aoiseo ) ? $title_aoiseo : $title_origin;
			$title_format        = str_replace( '%page_title%', $title, $title_format );
			$title               = str_replace( '%blog_title%', get_bloginfo( 'name' ), $title_format );

			$template->doc_title = $title;

		}

		if ( is_home() && ! ( $wp_query->is_posts_page ) ) {
			$title_format = $aioseop_options['aiosp_home_page_title_format'];
			$title        = $aioseop_options['aiosp_home_title'];
			$title        = str_replace( '%page_title%', $title, $title_format );

			$template->doc_title = $title;

		}

		if ( ! empty( $aioseop_options['aiosp_use_static_home_info'] ) ) {

			if ( is_home() && ! ( $wp_query->is_posts_page ) ) {
				$page_id      = get_option( 'page_on_front' );
				$title        = get_post_meta( $page_id, '_aioseop_title', true );
				$title        = ! empty( $title ) ? $title : get_the_title( $page_id );
				$title_format = $aioseop_options['aiosp_home_page_title_format'];
				$title        = str_replace( '%page_title%', $title, $title_format );

				$template->doc_title = $title;

				return $template;
			}

		}

		return $template;
	}

	public function fix_aiosp_desc() {
		global $wp_query;
		if ( is_home() && ( $wp_query->is_posts_page == false ) ) {
			add_filter( 'aioseop_description', array ( $this, 'aiosp_fix_descriptions' ) );
		}
	}

	public function aiosp_fix_descriptions() {
		global $aioseop_options;
		if ( ! empty( $aioseop_options['aiosp_use_static_home_info'] ) ) {
			$current_post_id = get_option( 'page_on_front' );

			return get_post_meta( $current_post_id, '_aioseop_description', true );
		}

		return $aioseop_options['aiosp_home_description'];
	}

	public function disable_w3_total_cache() {
		if ( is_plugin_active( 'w3-total-cache/w3-total-cache.php' ) ) {
			//Disables page caching for a given page.
			define('DONOTCACHEPAGE', true);

			//Disables database caching for given page.
			define('DONOTCACHEDB', true);

			//Disables minify for a given page.
			define('DONOTMINIFY', true);

			//Disables content delivery network for a given page.
	        define('DONOTCDN', true);

			//Disables object cache for a given page.
			define('DONOTCACHEOBJECT', true);
		}
	}

	public function amphtml_single_content_urecipe( $content ) {
		if ( class_exists( 'WPURP_Recipe' ) ) {
			$recipe     = new WPURP_Recipe( get_post() );
			$recipe_box = apply_filters( 'wpurp_output_recipe', $recipe->output_string( 'amp' ), $recipe );
			if ( strpos( $content, '[recipe]' ) !== false ) {
				$content = str_replace( '[recipe]', $recipe_box, $content );
			} else {
				$content .= $recipe_box;
			}
			// Remove searchable part
			$content = preg_replace( "/\[wpurp-searchable-recipe\][^\[]*\[\/wpurp-searchable-recipe\]/", "", $content );
		}

		return $content;
	}

}