<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

include_once( ABSPATH . 'wp-admin/includes/media.php' );

// TODO: class AMPHTML_Template extends AMPHTML_Template_Abstract {
class AMPHTML_Template extends AMPHTML_Template_Abstract {

	const TEMPLATE_DIR = 'templates';
	const TEMPLATE_PART_DIR = 'templates/parts';
	const TEMPLATE_CART_DIR = 'templates/add-to-cart';
	const STYLE_DIR = 'css';
	const SITE_ICON_SIZE = 32;
	const SCHEMA_IMG_MIN_WIDTH = 696;

	public $properties;
	protected $sanitizer;
	protected $embedded_elements;

	protected $template = 'base';
	public $template_content = '';
	protected $fonts;
	protected $blocks;

	public $thumbnail_size = array( 75, 75 );

	/**
	 * @var AMPHTML_Options
	 */
	protected $options;

	public function __construct( $options ) {

		if ( true === AMPHTML()->is_amp() ) {
			remove_shortcode( 'gallery' );
			add_shortcode( 'gallery', array( $this, 'gallery_shortcode' ) );

		}

		$this->sanitizer    = new AMPHTML_Sanitize( $this );
		$this->properties   = array();
		$this->options      = $options;
		$this->doc_title    = function_exists( 'wp_get_document_title' ) ? wp_get_document_title() : wp_title( '', false );
		$this->base_url     = home_url() . '/';
		$this->blog_name    = $this->options->get( 'logo_text' );
		$this->logo         = $this->options->get( 'logo' );
		$this->default_logo = $this->options->get( 'default_logo' );
		$this->favicon      = $this->get_favicon();

		add_action( 'amphtml_template_head', array( $this, 'page_fonts' ) );
		add_action( 'amphtml_template_css', array( $this, 'get_custom_css' ) );
		add_filter( 'get_pagenum_link', array( $this, 'update_pagination_link' ) );

		/*
		 * Yoast SEO integration issues
		 */
		if ( AMPHTML::instance()->is_yoast_seo() && is_home() ) {
			add_filter( 'wpseo_canonical', array( $this, 'fix_canonical_url' ) );
			add_filter( 'wpseo_opengraph_title', array( $this, 'fix_og_title' ) );
		}

		/*
		 * Add extra css
		 */
		add_action( 'amphtml_template_css', array( $this, 'set_extra_css' ), 100 );

		$this->embedded_elements = array(
			array( 'slug' => 'amp-form', 'src' => 'https://cdn.ampproject.org/v0/amp-form-0.1.js' ),
			array( 'slug' => 'amp-ad', 'src' => 'https://cdn.ampproject.org/v0/amp-ad-0.1.js' ),
			//array( 'slug' => 'amp-carousel', 'src' => 'https://cdn.ampproject.org/v0/amp-carousel-0.1.js' )
		);

		if ( $this->options->get( 'header_menu_type' ) == 'sidebar' ) {
			$menu_handler = array(
				'slug' => 'amp-sidebar',
				'src'  => 'https://cdn.ampproject.org/v0/amp-sidebar-0.1.js'
			);
		} else {
			$menu_handler = array(
				'slug' => 'amp-accordion',
				'src'  => 'https://cdn.ampproject.org/v0/amp-accordion-0.1.js'
			);
		}

		$this->add_embedded_element( $menu_handler );
		$this->google_analytics = $this->get_google_analitycs();

		add_action( 'amphtml_after_footer', array( $this, 'render_analytics' ) );
	}
	
	protected function get_favicon(){
		$icon = $this->options->get( 'favicon' );
		if( $img_obj = json_decode( $icon ) ) {
			return $img_obj->url;
		}
		return $icon;
	}

	/**
	 * Multi page content render - <!--nextpage-->
	 *
	 * @global type $page
	 *
	 * @param object $content
	 *
	 * @return string content
	 */
	public function multipage_content( $content ) {
		global $page;
		$page    = $page ? $page : 1;
		$content = $content->save();

		if ( false !== strpos( $content, '<!--nextpage-->' ) ) {
			$content = str_replace( "\n<!--nextpage-->\n", '<!--nextpage-->', $content );
			$content = str_replace( "\n<!--nextpage-->", '<!--nextpage-->', $content );
			$content = str_replace( "<!--nextpage-->\n", '<!--nextpage-->', $content );

			// Ignore nextpage at the beginning of the content.
			if ( 0 === strpos( $content, '<!--nextpage-->' ) ) {
				$content = substr( $content, 15 );
			}

			$pages   = explode( '<!--nextpage-->', $content );
			$content = $pages[ $page - 1 ];
			add_filter( 'wp_link_pages_link', array( $this, 'wpamp_link_pages' ) );

			return $content;
		}

		return $content;
	}

	/**
	 * Make page pagination for amp
	 *
	 * @param string $link
	 *
	 * @return string amp-link
	 */
	public function wpamp_link_pages( $link ) {
		// The Regular Expression filter
		$reg_exUrl = "#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#";
		// Check if there is a url
		if ( preg_match( $reg_exUrl, $link, $url ) ) {
			$amp_link = str_replace( $url[0], $this->get_amphtml_link( $url[0] ), $link );

			return $amp_link;
		}

		return $link;
	}

	protected function get_google_analitycs() {
		if ( $this->options->get( 'google_analytic' ) ) {
			$this->add_embedded_element(
				array( 'slug' => 'amp-analytics', 'src' => 'https://cdn.ampproject.org/v0/amp-analytics-0.1.js' )
			);
			$analytics = array(
				'vars'     => array(
					'account' => $this->options->get( 'google_analytic' )
				),
				'triggers' => array(
					'trackPageview' => array(
						'on'      => 'visible',
						'request' => 'pageview',
					),
				),
			);

			return $analytics;
		}
	}

	/**
	 * @deprecated
	 */
	public function set_default_embedded_elements() {
		$default_elemets = array(
			array( 'slug' => 'amp-carousel', 'src' => 'https://cdn.ampproject.org/v0/amp-carousel-0.1.js' ),
			array( 'slug' => 'amp-iframe', 'src' => 'https://cdn.ampproject.org/v0/amp-iframe-0.1.js' ),
			array( 'slug' => 'amp-youtube', 'src' => 'https://cdn.ampproject.org/v0/amp-youtube-0.1.js' ),
			array( 'slug' => 'amp-audio', 'src' => 'https://cdn.ampproject.org/v0/amp-audio-0.1.js' ),
			array( 'slug' => 'amp-vimeo', 'src' => 'https://cdn.ampproject.org/v0/amp-vimeo-0.1.js' )
		);

		$this->embedded_elements = array_merge( $this->embedded_elements, $default_elemets );

		return $this;
	}

	public function get_embedded_elements() {
		return apply_filters( AMPHTML::TEXT_DOMAIN . '_embedded_elements', $this->embedded_elements );
	}

	public function add_embedded_element( $new_element ) {
		$slugs = array();

		foreach ( $this->embedded_elements as $element ) {
			$slugs[] = $element['slug'];
		}

		if ( ! in_array( $new_element['slug'], $slugs ) ) {
			$this->embedded_elements[] = $new_element;
		}

		return $this;
	}

	public function page_fonts() {
		$used_fonts = array();
		foreach ( $this->options->get_tabs()->get( 'appearance' )->get_font_fields( 'fonts' ) as $font ) {
			$font_name = $this->options->get( $font['id'] );
			if ( ! in_array( $font_name, $used_fonts ) ) {
				$additional_styles = apply_filters( 'amphtml_font_styles', ':400,700,400italic,500,500italic' );
				echo '<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=' . $font_name . $additional_styles . '">' . PHP_EOL;
			}
			$used_fonts[]               = $font_name;
			$this->fonts[ $font['id'] ] = str_replace( '+', ' ', $font_name );
		}
	}

	public function get_element_fonts() {

		return ".logo { Font-Family: {$this->fonts['logo_font']}, serif }"
		       . ".main-navigation, .hamburger { Font-Family: {$this->fonts['menu_font']}, serif }"
		       . "#menu-amp-menu { Font-Family: {$this->fonts['menu_font']}, serif }"
		       . ".amp-menu-sidebar { Font-Family: {$this->fonts['menu_font']}, serif }"
		       . "amp-sidebar .menu { Font-Family: {$this->fonts['menu_font']}, serif }"
		       . ".amphtml-title { Font-Family: {$this->fonts['title_font']}, serif }"
		       . ".amphtml-meta-author { Font-Family: {$this->fonts['post_meta_font']}, serif }"
		       . ".amphtml-content { Font-Family: {$this->fonts['content_font']}, serif }"
		       . ".footer { Font-Family: {$this->fonts['footer_font']}, serif }";

	}

	public function get_element_colors() {
		$add_to_cart_button = $this->options->get( 'add_to_cart_button_color' );

		return 'nav.amphtml-title,'
		       . 'section[expanded] span.expanded,'
		       . 'section[expanded] span.collapsed,'
		       . 'section:not([expanded]) span.collapsed,'
		       . 'section .accordion-header'
		       . "{ background: {$this->options->get( 'header_color' )}; }"
		       . ".footer { background: {$this->options->get( 'footer_color' )}; }"
		       . "a, #pagination .next a, #pagination .prev a { color: {$this->options->get( 'link_color' )}; }"
		       . "body { background: {$this->options->get( 'background_color' )} }"
		       . "body, .amphtml-content, .amphtml-sku, .amphtml-stock-status,"
		       . " .price, .amphtml-posted-in, .amphtml-tagged-as, .amphtml-meta { color: {$this->options->get( 'main_text_color' )} }"
		       . "h1.amphtml-title { color: {$this->options->get( 'main_title_color' )} }"
		       . "nav.amphtml-title a, nav.amphtml-title div, div.main-navigation a { color: {$this->options->get( 'header_text_color' )} }"
		       . ".amphtml-add-to a { background-color: {$add_to_cart_button}; border-color: {$add_to_cart_button} }"
		       . ".amphtml-add-to a:hover, .amphtml-add-to a:active { background-color: {$add_to_cart_button}; border-color: {$add_to_cart_button}; }"
		       . "amp-sidebar { background-color: {$this->options->get( 'sidebar_menu_color' )} }"
		       . "amp-sidebar a { color: {$this->options->get( 'main_text_color' )} }"
		       . "div.footer, div.footer a { color: {$this->options->get( 'footer_text_color' )} }";
	}


	public function get_custom_css() {
		$content_width      = absint( $this->options->get( 'content_width' ) );
		$main_content_width = $content_width + 32;

		echo PHP_EOL
		     . ".amphtml-title div, .footer .inner { max-width: {$content_width}px; margin: 0 auto;}"
		     . "#main .inner { max-width: {$main_content_width}px; } "
		     . $this->get_element_fonts()
		     . $this->get_element_colors();
	}

	public function __get( $key ) {
		if ( isset( $this->properties[ $key ] ) ) {
			return $this->properties[ $key ];
		}

		return '';
	}

	public function __set( $key, $value ) {
		$this->properties[ $key ] = $value;
	}

	public function the_template_content() {
		echo $this->render( $this->template_content );
	}

	public function render( $filename = '' ) {
		if ( ! $filename ) {
			$filename = $this->template;
		}

		$template_path = $this->get_template_path( $filename );

		if ( file_exists( $template_path ) ) {
			ob_start();
			include( $template_path );

			return ob_get_clean();
		}
	}

	public function get_template_path( $filename ) {
		//theme templates
		$path[] = locate_template( array(
			AMPHTML()->get_plugin_folder_name() . DIRECTORY_SEPARATOR . $filename
			. '.php'
		), false );
		$path[] = locate_template( array(
			AMPHTML()->get_plugin_folder_name() . DIRECTORY_SEPARATOR . 'parts'
			. DIRECTORY_SEPARATOR . $filename . '.php'
		), false );
		$path[] = locate_template( array(
			AMPHTML()->get_plugin_folder_name() . DIRECTORY_SEPARATOR . 'add-to-cart'
			. DIRECTORY_SEPARATOR . $filename . '.php'
		), false );
		//plugin templates
		$path[] = $this->get_dir_path( self::TEMPLATE_DIR ) . DIRECTORY_SEPARATOR . $filename . '.php';
		$path[] = $this->get_dir_path( self::TEMPLATE_PART_DIR ) . DIRECTORY_SEPARATOR . $filename . '.php';
		$path[] = $this->get_dir_path( self::TEMPLATE_CART_DIR ) . DIRECTORY_SEPARATOR . $filename . '.php';

		foreach ( $path as $template ) {
			if ( file_exists( $template ) ) {
				return $template;
			}
		}
	}

	public function get_option( $option ) {
		return $this->options->get( $option );
	}

	protected function get_dir_path( $sub_dir ) {
		$amphtml_dir = AMPHTML::instance()->get_amphtml_path();

		if ( is_dir( $amphtml_dir . $sub_dir ) ) {
			return $amphtml_dir . $sub_dir;
		}

		return false;
	}

	public function set_template_content( $template ) {
		$this->template_content = $template;
		add_action( 'amphtml_template_content', array( $this, 'the_template_content' ) );

		return $this;
	}

	public function get_style( $filename ) {
		$styles = '';
		$path   = $this->get_dir_path( self::STYLE_DIR ) . DIRECTORY_SEPARATOR . $filename . '.css';

		if ( file_exists( $path ) ) {
			$styles = file_get_contents( $path );
		}

		return apply_filters( 'amphtml_style', $styles, $this );

	}

	public function get_title( $id ) {
		return ( get_post_meta( $id, "amphtml-override-title", true ) ) ?
			get_post_meta( $id, "amphtml-custom-title", true ) : get_the_title( $id );
	}

	public function get_content( $post ) {
		$content = $post->post_content;
		if ( get_post_meta( $post->ID, "amphtml-override-content", true ) ) {
			$content = get_post_meta( $post->ID, "amphtml-custom-content", true );
		}
		if ( $this->options->get( 'default_the_content' ) ) {
			$this->remove_custom_the_content_hooks();
		}

		return apply_filters( 'the_content', $content );
	}

	public function remove_custom_the_content_hooks() {
		global $wp_filter;

		$hooks    = $wp_filter['the_content'];
		$defaults = $this->get_default_the_content_hooks();

		if ( class_exists( 'WP_Hook' ) ) {
			$hooks = $hooks->callbacks;
		}

		foreach ( $hooks as $priority => $functions ) {

			foreach ( $functions as $name => $function ) {

				$function_name = ( is_array( $function['function'] ) ) ? $function['function'][1] : $function['function'];

				if ( ! isset( $defaults[ $priority ] ) || ! in_array( $function_name, $defaults[ $priority ] ) ) {
					if ( isset( $wp_filter['the_content'] ) ) {
						if ( class_exists( 'WP_Hook' ) ) {
							unset( $wp_filter['the_content']->callbacks[ $priority ][ $name ] );
						} else {
							unset( $wp_filter['the_content'][ $priority ][ $name ] );
						}
					}
				}

			}

			if ( ! count( $wp_filter['the_content'][ $priority ] ) ) {
				unset( $wp_filter['the_content'][ $priority ] );
			}

		}
	}

	public function get_default_the_content_hooks() {
		return apply_filters( 'amphtml_the_content', array(
			'11' => array( 'capital_P_dangit', 'do_shortcode' ),
			'10' => array(
				'wptexturize',
				'convert_smilies',
				'wpautop',
				'shortcode_unautop',
				'prepend_attachment',
				'wp_make_content_images_responsive',
			),
			'8'  => array( 'run_shortcode', 'autoembed' ),
		) );
	}

	public function set_post( $id ) {

		$this->post               = get_post( $id );
		$this->ID                 = $this->post->ID;
		$this->title              = $this->get_title( $this->ID );
		$this->publish_timestamp  = get_the_date( 'U', $this->ID );
		$this->modified_timestamp = get_post_modified_time( 'U', false, $this->post );
		$this->author             = get_userdata( $this->post->post_author );
		$this->content            = $this->get_content( $this->post );
		$this->content            = apply_filters( 'amphtml_single_content', $this->content );
		$this->content            = $this->sanitizer->sanitize_content( $this->content );
		$this->content            = $this->multipage_content( $this->content );
		$this->featured_image     = $this->get_featured_image();
		$this->metadata           = $this->get_schema_metadata( $this->post, $this->get_post_excerpt_by_id( $id ) );

	}

	public function set_schema_metadata( $description = '' ) {
		global $post;
		$this->metadata = $this->get_schema_metadata( $post, $description );
	}

	public function get_schema_metadata( $post, $description = '' ) {
		if( $post ){
			$author        = get_userdata( $post->post_author );
			$post_image_id = $this->get_post_image_id( $post->ID );
		}
		$logo          = $this->default_logo;

		if ( empty( $logo ) ) {
			$logo = $this->logo;
		}

		$metadata = array(
			'@context'         => 'http://schema.org',
			'@type'            => apply_filters( 'amphtml_schema_type', $this->options->get( 'schema_type' ), $this ),
			'mainEntityOfPage' => array(
				'@type' => 'WebPage',
				'@id'   => get_permalink() ? get_permalink() : get_bloginfo('url'),
			),
			'publisher'        => array(
				'@type' => 'Organization',
				'name'  => $this->blog_name,
			),
			'headline'         => $this->title,
			'datePublished'    => $post ? date( 'c', get_the_date( 'U', $post->ID ) ) : date('c'),
			'dateModified'     => $post ? date( 'c', get_post_modified_time( 'U', false, $post ) ) : date('c'),
			'author'           => array(
				'@type' => 'Person',
				'name'  => $post ? $author->display_name : 'admin',
			),
			'image'            => $post ? $this->get_schema_images( $post_image_id ) : 
				array(
					"@type" => "ImageObject",
					"url" => plugin_dir_url(__FILE__). 'img/teamdev.png',
					'height' => '210',
					'width'  => '211'
				)
		);

		if ( $description ) {
			$metadata['description'] = $this->remove_html_comments( $description );
		}

		if ( $logo ) {
			if( $img_obj = json_decode($logo) ) {
				$metadata['publisher']['logo'] = array(
					'@type'  => 'ImageObject',
					'url'    => $img_obj->url,
					'height' => $img_obj->height,
					'width'  => $img_obj->width,
				);
			} else {
				$attachment_id                 = $this->get_attachment_id_from_url( $logo );
				$logo_arr                      = wp_get_attachment_metadata( $attachment_id );
				$metadata['publisher']['logo'] = array(
					'@type'  => 'ImageObject',
					'url'    => $logo,
					'height' => $logo_arr['height'],
					'width'  => $logo_arr['width'],
				);
			}
		}

		return apply_filters( 'amphtml_metadata', $metadata, $this );
	}


	/**
	 * Get attachment ID from URL
	 *
	 * @global type $wpdb
	 *
	 * @param string $attachment_url
	 *
	 * @return int
	 */
	function get_attachment_id_from_url( $attachment_url = '' ) {
		global $wpdb;
		$attachment_id = false;
		if ( '' == $attachment_url ) {
			return;
		}
		$upload_dir_paths = wp_upload_dir();
		// Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
		if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {
			$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );
			$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
			$attachment_id  = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );
		}

		return $attachment_id;
	}


	public function get_attachment_id_from_src( $image_src ) {
		global $wpdb;
		$query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";

		return $wpdb->get_var( $query );
	}

	public function remove_html_comments( $html ) {
		return preg_replace( '/<!--(.*?)-->/', '', $html );
	}

	private function get_post_image_id( $post_id ) {

		$post_image_id = $this->get_post_thumbnail_id( $post_id );

		if ( $post_image_id ) {
			return $post_image_id;
		}

		$image_ids = get_posts( array(
			'post_parent'      => $post_id,
			'post_type'        => 'attachment',
			'post_mime_type'   => 'image',
			'posts_per_page'   => 1,
			'orderby'          => 'menu_order',
			'order'            => 'ASC',
			'fields'           => 'ids',
			'suppress_filters' => false,
		) );

		if ( count( $image_ids ) ) {
			$post_image_id = current( $image_ids );
		} else {
			// default image 
			$logo = $this->options->get( 'default_image' );
			if( $img_obj = json_decode( $logo ) ) {
				$post_image_id = $img_obj->id;
			} else {
				$post_image_id = $this->get_attachment_id_from_src( $logo );
			}
		}

		return $post_image_id;
	}

	public function get_post_thumbnail_id( $post_id ) {
		$thumbnail_id = get_post_meta( $post_id, 'amphtml_featured_image_id', true );
		if ( ! $thumbnail_id ) {
			$thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
		}

		return $thumbnail_id;
	}

	public function get_schema_images( $post_image_id ) {
		$post_image_src = wp_get_attachment_image_src( $post_image_id, 'full' );

		if ( is_array( $post_image_src ) ) {
			return array(
				'@type'  => 'ImageObject',
				'url'    => $post_image_src[0],
				'width'  => ( $post_image_src[1] > self::SCHEMA_IMG_MIN_WIDTH ) ? $post_image_src[1] : self::SCHEMA_IMG_MIN_WIDTH,
				'height' => $post_image_src[2],
			);
		}

		return array(
			'@type' => 'ImageObject'
		);
	}

	public function get_post_excerpt_by_id( $post_id ) {
		global $post;
		$post = get_post( $post_id );
		setup_postdata( $post );
		$the_excerpt = get_the_excerpt();
		wp_reset_postdata();

		return $the_excerpt;
	}

	public function get_featured_image() {
		$featured_iamge    = '';
		$image_id          = get_post_meta( $this->ID, 'amphtml_featured_image_id', true );
		$post_thumbnail_id = ( $image_id ) ? $image_id : get_post_thumbnail_id( $this->ID );
		if ( $post_thumbnail_id ) {
			$featured_iamge = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
			$featured_iamge['alt'] = get_post_meta( $post_thumbnail_id, '_wp_attachment_image_alt', true);
		}
		
		return $featured_iamge;
	}

	public function nav_menu() {
		$nav_menu = wp_nav_menu( array(
				'theme_location' => $this->options->get( 'amphtml_menu' ),
				'echo'           => false
			)
		);
		$nav_menu = $this->update_content_links( $nav_menu );

		return apply_filters( 'amphtml_nav_menu', $nav_menu );
	}

	public function update_content_links( $content ) {
		$dom_model       = $this->sanitizer->get_dom_model();
		$avoid_amp_class = apply_filters( 'amphtml_no_amp_menu_link', 'no-amp' );
		$content         = $dom_model->load( $content );
		foreach ( $content->find( 'a' ) as $anchor ) {
			if ( false === strpos( $anchor->parent->class, $avoid_amp_class ) ) {
				$id = $this->get_postid_from_nav_el( $anchor );
				$anchor->href = $this->get_amphtml_link( $anchor->href, $id );
			}
		}

		return $content;
	}

	public function get_postid_from_nav_el( $anchor ) {
		$id      = explode( '-', $anchor->parent->id );
		$id      = array_pop( $id );
		$post_id = get_post_meta( $id, '_menu_item_object_id', true );

		return $post_id != $id ? $post_id : '';
	}

	public function get_amphtml_link( $link, $id = '' ) {
		return $this->options->get_amphtml_link( $link, $id );
	}

	public function get_logo_link() {
		$arg = apply_filters( 'amphtml_logo_link', false );
		if ( $arg == false ) {
			echo esc_url( $this->get_amphtml_link( $this->base_url ) );
		} else {
			echo $arg;
		}
	}

	public function gallery_shortcode( $attr ) {
		$size = $this->get_default_image_size();

		add_image_size( 'amphtml-size', $size['width'], $size['height'] );

		$sanitizer = $this->sanitizer;
		$gallery   = gallery_shortcode( $attr );

		$attr = shortcode_atts( array(
			'size' => 'amphtml-size'
		), $attr );

		$sanitizer->load_content( $gallery );

		$image_size      = $this->get_image_size( $attr['size'] );
		$gallery_content = $sanitizer->get_content();
		$gallery_images  = $sanitizer->get_amp_images( $image_size );

		$gallery_content = $this->render_element( 'carousel', array(
			'width'  => $image_size['width'],
			'height' => $image_size['height'],
			'images' => $gallery_images
		) );

		return $gallery_content;
	}

	public function get_default_image_size() {
		$size           = array();
		$size['width']  = $this->options->get( 'content_width' );
		$size['height'] = round( $size['width'] / ( 16 / 9 ), 0 );

		return $size;
	}

	public function get_image_size( $size ) {
		$sizes = $this->get_image_sizes();

		if ( isset( $sizes[ $size ] ) ) {
			return $sizes[ $size ];
		}

		return false;
	}

	public function get_image_sizes() {
		global $_wp_additional_image_sizes;

		$sizes = array();

		foreach ( get_intermediate_image_sizes() as $_size ) {
			if ( in_array( $_size, array( 'thumbnail', 'medium', 'medium_large', 'large' ) ) ) {
				$sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
				$sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
				$sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
			} elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
				$sizes[ $_size ] = array(
					'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
					'height' => $_wp_additional_image_sizes[ $_size ]['height'],
					'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
				);
			}
		}

		return $sizes;
	}

	public function render_element( $template, $element ) {

		$template_path = $this->get_template_path( $template );

		if ( file_exists( $template_path ) ) {
			ob_start();
			include( $template_path );

			return ob_get_clean();
		}
	}

	public function get_post_meta() {
		$meta = ( $this->options->get( 'post_meta_author' ) ) ? $this->render( 'meta-author' ) : '';
		$meta .= ( $this->options->get( 'post_meta_categories' ) ) ? $this->render( 'meta-cats' ) : '';
		$meta .= ( $this->options->get( 'post_meta_tags' ) ) ? $this->render( 'meta-tags' ) : '';
		$meta .= $this->render( 'meta-time' );
		$meta = $this->update_content_links( $meta );

		return apply_filters( 'amphtml_meta', $meta, $this );
	}

	public function is_featured_image() {
		global $wp_query;

		if ( ! $this->featured_image ) {
			return false;
		}

		return ( ( is_archive() && $this->options->get( 'archive_featured_image' ) )
		         || ( is_page() && $this->options->get( 'page_featured_image' ) )
		         || ( is_home() && ! is_front_page() && ! $wp_query->is_posts_page && $this->options->get( 'page_featured_image' ) )
		         || ( is_single() && $this->options->get( 'post_featured_image' ) )
		         || ( is_search() && $this->options->get( 'search_page_post_featured_image' ) )
		         || ( is_home() && is_front_page() && $this->options->get( 'blog_page_post_featured_image' ) )
		         || ( $wp_query->is_posts_page && $this->options->get( 'blog_page_post_featured_image' ) )
		);
	}

	public function is_enabled_meta() {
		global $wp_query;

		return ( ( is_search() && $this->options->get( 'search_page_post_meta' ) )
		         || ( is_archive() && $this->options->get( 'archive_meta' ) )
		         || ( is_home() && is_front_page() && $this->options->get( 'blog_page_post_meta' ) )
		         || ( $wp_query->is_posts_page && $this->options->get( 'blog_page_post_meta' ) )
		);
	}

	public function update_pagination_link( $pagination_link ) {
		$endpoint = AMPHTML()->get_endpoint();
		$pattern  = "/(\/page\/\d+)?\/($endpoint)(\/page\/\d+)?\/?$/";

		preg_match( $pattern, $pagination_link, $matches );

		if ( false === isset( $matches[2] ) ) {
			return $pagination_link;
		}

		$mached_endpoint = $matches[2];

		$pagination_link = str_replace( $matches[0], '', $pagination_link );

		$page = isset( $matches[3] ) ? $matches[3] : '';

		$pagination_link = $pagination_link . $page . '/' . $matches[2] . '/';

		return $pagination_link;
	}

	public function get_canonical_url() {

		return AMPHTML()->get_canonical_url();
	}

	public function get_permalink() {
		global $wp;

		return home_url( add_query_arg( array(), $wp->request ) );
	}

	public function fix_canonical_url() {
		return $this->get_canonical_url();
	}

	public function fix_og_title() {
		return $this->doc_title;
	}

	public function set_blocks( $type, $default = true ) {
		$this->blocks = $this->options->get_template_elements( $type, $default );
	}

	public function get_blocks() {
		return $this->blocks;
	}

	/**
	 * Block posts title
	 *
	 * @param string $type block type
	 *
	 * @return string
	 */
	public function get_posts_title( $type ) {
		return __( $this->get_option( 'post_' . $type . '_title' ) );
	}

	/**
	 * Get posts count option
	 *
	 * @param string $type option type
	 *
	 * @return int posts count to render
	 */
	public function get_posts_count( $type ) {
		return $this->get_option( 'post_' . $type . '_count' );
	}

	/**
	 * Is show amp post thumbnail
	 *
	 * @param string $type
	 *
	 * @return bolean
	 */
	public function get_posts_thumbnail( $type ) {
		return $this->get_option( 'post_' . $type . '_thumbnail' ) ? true : false;
	}

	/**
	 * Render amp post thumbnail in list
	 *
	 * @param int $post_id
	 *
	 * @return empty string if no any thumbnail
	 */
	public function the_post_thumbnail_tpl( $post_id ) {
		$thumb_id = (int) get_post_meta( $post_id, 'amphtml_featured_image_id', true );
		$thumb_id = $thumb_id ? $thumb_id : (int) get_post_thumbnail_id( $post_id );
		if ( $thumb_id ) {
			$img = wp_get_attachment_image_src( $thumb_id, $this->thumbnail_size );
			echo "<div class='wp-amp-recent-thumb'>"
			     . "<amp-img src='{$img[0]}' width='{$img[1]}' height='{$img[2]}' layout='responsive'>"
			     . "</amp-img></div>";
		}

		return '';
	}

	public function get_related_posts( $post, $count = 2 ) {
		$taxs = get_object_taxonomies( $post );
		if ( ! $taxs ) {
			return '';
		}

		// ignoring post formats
		if ( ( $key = array_search( 'post_format', $taxs ) ) !== false ) {
			unset( $taxs[ $key ] );
		}

		// try tags first
		if ( ( $tag_key = array_search( 'post_tag', $taxs ) ) !== false ) {

			$tax          = 'post_tag';
			$tax_term_ids = wp_get_object_terms( $post->ID, $tax, array( 'fields' => 'ids' ) );
		}

		// if no tags, then by cat or custom tax
		if ( empty( $tax_term_ids ) ) {
			// remove post_tag to leave only the category or custom tax
			if ( $tag_key !== false ) {
				unset( $taxs[ $tag_key ] );
				$taxs = array_values( $taxs );
			}

			$tax          = $taxs[0];
			$tax_term_ids = wp_get_object_terms( $post->ID, $tax, array( 'fields' => 'ids' ) );

		}

		if ( $tax_term_ids ) {
			$args    = array(
				'post_type'      => $post->post_type,
				'posts_per_page' => $count,
				'orderby'        => 'rand',
				'post_status'    => 'publish',
				'tax_query'      => array(
					array(
						'taxonomy' => $tax,
						'field'    => 'id',
						'terms'    => $tax_term_ids
					)
				),
				'post__not_in'   => array( $post->ID ),
			);
			$related = new WP_Query( $args );

			return $related;
		}
	}

	public function get_recent_posts( $count ) {
		return new WP_Query(
			array(
				'orderby'             => 'date',
				'posts_per_page'      => $count,
				'no_found_rows'       => true,
				'post_status'         => 'publish',
				'ignore_sticky_posts' => true
			)
		);
	}

	public function get_doc_title() {
		return $this->doc_title;
	}

	public function set_extra_css() {
		$extra_css = $this->options->get( 'extra_css_amp' );
		if ( ! empty( $extra_css ) ) {
			echo $extra_css;
		}
	}

	public function get_footer() {
		$footer_content = apply_filters( 'amphtml_template_footer', $this->options->get( 'footer_content' ) );
		if ( $footer_content ) {
			$footer_content = do_shortcode( $footer_content );
			$footer_content = $this->sanitizer->sanitize_content( $footer_content )->save();
		}

		return apply_filters( 'amphtml_template_footer_content', $footer_content );;
	}

	public function load() {
		global $wp_query;
		$social_share_script = array(
			'slug' => 'amp-social-share',
			'src'  => 'https://cdn.ampproject.org/v0/amp-social-share-0.1.js'
		);

		switch ( true ) {
			case is_front_page():
			case ( $wp_query->is_posts_page ):
				$this->set_template_content( 'archive' );
				$this->doc_title = $this->title = $this->options->get( 'blog_page_title' );
				add_filter( 'aioseop_title', array( $this, 'get_doc_title' ) );
				$this->set_blocks( 'blog' );
				$this->set_schema_metadata();
				break;
			case is_home():
				$this->set_template_content( 'single-content' );
				$current_post_id = get_option( 'page_on_front' );
				$this->set_post( $current_post_id );
				$this->doc_title = $this->post->post_title;
				add_filter( 'aioseop_title', array( $this, 'get_doc_title' ) );
				$this->set_blocks( 'pages' );
				if ( $this->options->get( 'page_social_share' ) ) {
					$this->add_embedded_element( $social_share_script );
				}
				break;
			case is_single():
				$this->set_template_content( 'single-content' );
				$current_post_id = get_the_ID();
				$this->set_post( $current_post_id );
				$this->set_blocks( 'posts' );
				if ( $this->options->get( 'post_social_share' ) ) {
					$this->add_embedded_element( $social_share_script );
				}
				break;
			case is_page():
				$this->set_template_content( 'single-content' );
				$current_post_id = get_the_ID();
				$this->set_post( $current_post_id );
				$this->set_blocks( 'pages' );
				if ( $this->options->get( 'page_social_share' ) ) {
					$this->add_embedded_element( $social_share_script );
				}
				break;
			case is_archive():
				$this->set_template_content( 'archive' );
				$this->set_blocks( 'archives' );
				$this->title = get_the_archive_title();
				$this->set_schema_metadata( get_the_archive_description() );
				break;
			case is_404():
				$this->set_template_content( 'single-content' );
				$this->set_blocks( '404' );
				break;
			case is_search():
				$this->set_template_content( 'archive' );
				$this->set_blocks( 'search' );
				$this->title = __( 'Search Results', 'amphtml' );
				$this->set_schema_metadata();
				break;
		}
	}

	public function get_image_size_from_url( $url ) {
		$image = new FastImage( $url );
		list( $size['width'], $size['height'] ) = $image->getSize();

		return $size;
	}

	public function get_sanitize_obj() {
		return $this->sanitizer;
	}

	public function render_analytics() {
		echo $this->render( 'facebook-pixel' );
		if ( $this->google_analytics ) {
			echo $this->render( 'google-analytics' );
		}
	}

	public function get_template_name( $element ) {
		$name = '';
		if ( $this->options->get( $element ) ) {
			switch ( $element ) {
				case false !== strpos( $element, '_ad_' ):
					$name = $this->set_ad_data( $element ) ? 'ad' : '';
					break;
				case false !== strpos( $element, 'custom_html' ):
					$name = $this->set_custom_html( $element ) ? 'custom_html' : '';
					break;
				default:
					$template_name = $this->options->get( $element, 'template_name' );
					$name = $template_name ? $template_name : $element;
					break;
			}
		}

		return apply_filters( 'amphtml_template_name', $name, $element, $this );
	}

	public function set_ad_data( $element ) {

		if ( ! $this->options->get( 'ad_enable' ) ) {
			return false;
		}

		if ( false !== strpos( $element, '_ad_top' ) ) {
			$this->ad = array(
				'data_client'  => $this->options->get( 'ad_data_id_client_top' ),
				'data_ad_slot' => $this->options->get( 'ad_adsense_data_slot_top' ),
				'type'         => $this->options->get( 'ad_type_top' ),
				'width'        => $this->options->get( 'ad_top_width' ),
				'height'       => $this->options->get( 'ad_top_height' ),
				'data_slot'    => $this->options->get( 'ad_doubleclick_data_slot_top' )
			);
		} elseif ( false !== strpos( $element, '_ad_bottom' ) ) {
			$this->ad = array(
				'data_client'  => $this->options->get( 'ad_data_id_client_bottom' ),
				'data_ad_slot' => $this->options->get( 'ad_adsense_data_slot_bottom' ),
				'type'         => $this->options->get( 'ad_type_bottom' ),
				'width'        => $this->options->get( 'ad_bottom_width' ),
				'height'       => $this->options->get( 'ad_bottom_height' ),
				'data_slot'    => $this->options->get( 'ad_doubleclick_data_slot_bottom' )
			);
		} else {
			$ad_num   = substr( $element, - 1 );
			$this->ad = array(
				'data_client'  => $this->options->get( "ad_data_id_client_$ad_num" ),
				'data_ad_slot' => $this->options->get( "ad_adsense_data_slot_$ad_num" ),
				'type'         => $this->options->get( "ad_type_$ad_num" ),
				'width'        => $this->options->get( "ad_width_$ad_num" ),
				'height'       => $this->options->get( "ad_height_$ad_num" ),
				'data_slot'    => $this->options->get( "ad_doubleclick_data_slot_$ad_num" )
			);
		}

		return ( $this->ad['data_ad_slot'] || $this->ad['data_slot'] );
	}

	public function set_custom_html( $element ) {
		return $this->custom_html = $this->options->get( $element );
	}
}
