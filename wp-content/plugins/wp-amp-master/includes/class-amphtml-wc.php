<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class AMPHTML_WC {
	
	private static $instance = null;
	
	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new AMPHTML_WC();
		}
		return self::$instance;
	}

	private function __clone() {}
	
	private function __construct() {
		add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ), 9 );
		add_action( 'before_load_amphtml', array( $this, 'exclude_pages' ) );
		add_filter( 'amphtml_is_mobile_get_redirect_url', array( $this, 'add_to_cart_redirect' ) );
		add_filter( 'amphtml_admin_tab_list', array( $this, 'add_wc_options_tab' ), 10, 1 );
		add_filter( 'amphtml_template_load_callback', array( $this, 'template_load' ) );
		add_filter( 'amphtml_color_fields', array( $this, 'add_colors' ), 10, 3 );
		add_filter( 'amphtml_schemaorg_tab_fields', array( $this, 'add_schema_org_type' ), 10, 2 );
		add_filter( 'amphtml_schema_type', array( $this, 'set_wc_schema_type' ), 10, 2 );
//		add_filter( 'amphtml_allow_get_template', array( $this, 'allow_add_to_cart_block' ), 10, 2 ); //todo remove 
		add_action( 'update_6_6', array( $this, 'update_content_block_fields' ) );
	}
	
	public function pre_get_posts( $q ) {

		if ( ! $q->is_main_query() ) {
			return '';
		}

		if ( AMPHTML()->is_amp() && $q->is_home() && 'page' === get_option( 'show_on_front' ) && absint( get_option( 'page_on_front' ) ) === wc_get_page_id( 'shop' ) && ! isset( $q->query['pagename'] ) ) {

			$q->is_page              = true;
			$q->is_home              = false;
			$q->is_post_type_archive = true;
			$q->set( 'post_type', 'product' );

			global $wp_post_types;

			$shop_page = get_post( wc_get_page_id( 'shop' ) );

			$wp_post_types['product']->ID         = '';
			$wp_post_types['product']->post_title = $shop_page->post_title;
			$wp_post_types['product']->post_name  = $shop_page->post_name;
			$wp_post_types['product']->post_type  = $shop_page->post_type;
			$wp_post_types['product']->ancestors  = get_ancestors( $shop_page->ID, $shop_page->post_type );
		}
	}

	public function exclude_pages( $queried_object_id ) {

		//exclude woocommerce form based pages
		if ( is_cart() || is_checkout() || is_account_page() ) {
			update_post_meta( $queried_object_id, 'amphtml-exclude', "true" );
		}
	}

	public function add_to_cart_redirect( $is_mobile ) {
		return $is_mobile && false == isset( $_GET['add-to-cart-redirect'] );
	}

	public function add_wc_options_tab($tab_list) {
		$tab_list['wc'] = __( 'WooCommerce', 'amphtml' );
		return $tab_list;
	}

	public function template_load( $callback ) {
		return array( $this, 'load_template' );
	}

	public function add_colors( $fields, $tab, $section ) {
		$fields[] = array(
			'id'                    => 'add_to_cart_button_color',
			'title'                 => __( 'Add to Cart Button', 'amphtml' ),
			'default'               => '#0087be',
			'display_callback'      => array( $tab, 'display_color_field' ),
			'display_callback_args' => array( 'id' => 'add_to_cart_button_color' ),
			'sanitize_callback'     => array( $tab, 'sanitize_color' ),
			'section'               => $section
		);
		return $fields;
	}

	public function is_home_shop_page() {
		return  AMPHTML()->is_posts_page() && absint( get_option( 'page_on_front' ) ) === wc_get_page_id( 'shop' ) && absint (AMPHTML()->get_queried_object_id() ) === wc_get_page_id( 'shop' );
	}

	public function add_schema_org_type( $fields, $schema_tab ) {
		$fields[] = array(
			'id'                    => 'wc_schema_type',
			'title'                 => __( 'WooCommerce Content Type', 'amphtml' ),
			'display_callback'      => array( $schema_tab, 'display_select' ),
			'default'               => 'Product',
			'display_callback_args' => array(
				'id'             => 'wc_schema_type',
				'select_options' => array(
					'NewsArticle' => 'NewsArticle',
					'BlogPosting' => 'BlogPosting',
					'Product'     => 'Product'
				)
			),
			'description'           => '',
		);

		return $fields;
	}

	public function set_wc_schema_type( $type, $template ) { //todo refactoring
		if ( is_product() || is_shop() || $this->is_home_shop_page() || is_product_taxonomy() ) {
			$type = $template->get_option( 'wc_schema_type' );
			add_filter( 'amphtml_metadata', array( $this, 'update_product_schema' ) );
		}
		return $type;
	}

	/**
	 * @var $template AMPHTML_Template
	 */
	public function load_template( $template ) {
		global $wp_query;

		$social_share_script = array(
			'slug' => 'amp-social-share',
			'src'  => 'https://cdn.ampproject.org/v0/amp-social-share-0.1.js'
		);

		switch ( true ) {
			case $this->is_home_shop_page() :

				$template->set_template_content( 'wc-product-shop' );
				$template->set_blocks( 'shop' );
				$shop_page_id    = wc_get_page_id( 'shop' );
				$template->title = woocommerce_page_title( false );
				$template->set_schema_metadata();
				break;
			case is_front_page():

			case ( $wp_query->is_posts_page ):
				$template->set_template_content( 'archive' );
				$template->doc_title = $template->title = $template->get_option( 'blog_page_title' );
				add_filter( 'aioseop_title', array( $template, 'get_doc_title' ) );
				$template->set_blocks( 'blog' );
				$template->set_schema_metadata();
				break;
			case is_home():

				$template->set_template_content( 'single-content' );
				$current_post_id = get_option( 'page_on_front' );
				$template->set_post( $current_post_id );
				$template->doc_title = $template->post->post_title;
				add_filter( 'aioseop_title', array( $template, 'get_doc_title' ) );
				$template->set_blocks( 'pages' );
				if ( $template->get_option( 'page_social_share' ) ) {
					$template->add_embedded_element( $social_share_script );
				}
				break;
			case is_product():
				$template->set_template_content( 'single-content' );
				$template->set_blocks( 'product' );
				$current_post_id   = get_the_ID();
				$product_factory   = new WC_Product_Factory();
				$template->product = $product_factory->get_product( $current_post_id );
				$template->set_post( $current_post_id );
				if ( $template->get_option( 'product_social_share' ) ) {
					$template->add_embedded_element( $social_share_script );
				}

				if ( $this->is_product_carousel( $template->product ) ) {
					$template->add_embedded_element(
						array(
							'slug' => 'amp-carousel',
							'src'  => 'https://cdn.ampproject.org/v0/amp-carousel-0.1.js'
						)
					);
				}

				break;
			case is_shop():
				$template->set_template_content( 'wc-product-shop' );
				$template->set_blocks( 'shop' );
				$shop_page_id    = wc_get_page_id( 'shop' );
				$template->title = woocommerce_page_title( false );
				$template->set_schema_metadata();
				break;
			case is_product_taxonomy():
				$template->set_template_content( 'wc-product-archive' );
				$template->set_blocks( 'wc_archives' );
				$template->title = woocommerce_page_title( false );
				$template->set_schema_metadata();
				break;
			case is_single():
				$template->set_template_content( 'single-content' );
				$current_post_id = get_the_ID();
				$template->set_post( $current_post_id );
				$template->set_blocks( 'posts' );
				if ( $template->get_option( 'post_social_share' ) ) {
					$template->add_embedded_element( $social_share_script );
				}
				break;
			case is_page():
				$template->set_template_content( 'single-content' );
				$current_post_id = get_the_ID();
				$template->set_post( $current_post_id );
				$template->set_blocks( 'pages' );
				if ( $template->get_option( 'page_social_share' ) ) {
					$template->add_embedded_element( $social_share_script );
				}
				break;
			case is_archive():
				$template->set_template_content( 'archive' );
				$template->set_blocks( 'archives' );
				$template->title = get_the_archive_title();
				$template->set_schema_metadata( get_the_archive_description() );
				break;
			case is_404():
				$template->set_template_content( 'single-content' );
				$template->set_blocks( '404' );
				break;
			case is_search():
				$template->set_template_content( 'archive' );
				$template->set_blocks( 'search' );
				$template->title = __( 'Search Results', 'amphtml' );
				$template->set_schema_metadata();
				break;
		}
	}

	public function update_product_schema( $metadata ) {
		$prohibited_attributes = array( 'publisher', 'author', 'headline', 'datePublished', 'dateModified', 'author' );
		$metadata['name']      = $metadata['headline'];
		foreach ( $prohibited_attributes as $attr ) {
			unset( $metadata[ $attr ] );
		}

		return $metadata;
	}

	public function allow_add_to_cart_block( $is_enabled, $element ) {
		switch( $element ) {
			case 'product_add_to_cart_block':
			case 'wc_archives_add_to_cart_block':
			case 'shop_add_to_cart_block':
				$is_enabled = true;
				break;
		}
		return $is_enabled;
	}

	public function get_add_to_cart_button() {
		global $product;

		if ( $product->product_type === 'external' ) {

			return include( $this->get_template()->get_template_path( $product->product_type ) );
		}

		return include( $this->get_template()->get_template_path( 'simple' ) );

	}

	public function get_product_image_links( WC_Product $product ) {

		$attachment_ids = $product->get_gallery_attachment_ids();

		if ( count( $attachment_ids ) ) {
			$attachment_ids[]        = $product->get_image_id();
			$this->get_template()->product_image_ids = $attachment_ids;
			$sanitizer               = $this->get_template()->get_sanitize_obj();

			$sanitizer->load_content( $this->get_template()->render( 'wc-product-images' ) );

			$image_size = $this->get_template()->get_default_image_size();

			$gallery_content = $sanitizer->get_content();
			$gallery_images  = $sanitizer->get_amp_images( $image_size );

			$gallery_content = $this->get_template()->render_element( 'carousel', array(
				'width'  => $image_size['width'],
				'height' => $image_size['height'],
				'images' => $gallery_images
			) );

			return $gallery_content;
		}

		return $this->get_template()->render_element( 'image', $this->get_template()->featured_image );

	}

	public function is_product_carousel( WC_Product $product ) {
		$attachment_ids = $product->get_gallery_attachment_ids();
		return count( $attachment_ids );
	}

	public function get_template() {
		return AMPHTML()->get_template();
	}

	public function update_content_block_fields() {
		$disabled_elements = array(
			'product_add_to_cart_block',
			'shop_add_to_cart_block',
			'wc_archives_add_to_cart_block'
		);
		foreach ( $disabled_elements as $element ) {
			update_option( 'amphtml_' . $element, 1 );
		}

	}

}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	AMPHTML_WC::get_instance();

	function AMPHTML_WC() {
		return AMPHTML_WC::get_instance();
	}
}