<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( function_exists( 'AMPHTML' ) ) {
	function esc_amphtml( $content ) {
		$amphtml = AMPHTML()->get_template()
		                    ->get_sanitize_obj()
		                    ->sanitize_content( $content );

		return apply_filters( 'esc_amphtml', $amphtml );
	}
}

add_shortcode( 'no-amp', 'amphtml_no_amp' );

function amphtml_no_amp( $atts, $content ) {
	if ( is_wp_amp() ) {
		$content  = '';
	}
	return $content;
}