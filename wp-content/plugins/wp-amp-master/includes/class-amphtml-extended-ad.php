<?php

class AMPHTML_Extended_Ad {

	const DEFAULT_NUM_OF_SECTIONS = '2';

	const OPTION = 'amphtml_ad_sections';

	private $ad_sections;

	function __construct() {
		add_action( 'amphtml_init_tab', array( $this, 'init' ) );
		add_filter( 'amphtml_ad_tab_fields', array( $this, 'update_ad_fields' ), 10, 2 );
		$this->ad_sections = get_option( self::OPTION )
			? explode( ',', get_option( self::OPTION ) ) : array();
		$this->add_blocks_to_template();

	}

	public function init( $tab ) {
		if ( 'ad' == $tab->get_name() && $tab->is_current() ) {
			add_filter( 'amphtml_ad_tab_sections', array( $this, 'update_ad_section' ), 10, 2 );
			add_action( 'get_tab_submit_button', array( $this, 'update_submit_button' ) );
			add_action( 'amphtml_proceed_settings_form', array( $this, 'add_new_ad_block' ) );
			add_action( 'amphtml_proceed_settings_form', array( $this, 'delete_ad_block' ) );
		}
	}

	public function update_ad_section( $sections, $tab ) {
		foreach ( $this->ad_sections as $section ) {
			$sections[ $section ] = __( "Ad Block #$section", 'amphtml' );
		}

		return $sections;
	}

	public function update_ad_fields( $fields, $tab ) {
		foreach ( $this->ad_sections as $section ) {
			$section_fields = $this->get_section_fields( $section, $tab );
			$fields         = array_merge( $fields, $section_fields );
		}

		return $fields;
	}

	public function update_submit_button( $tab ) {
		?>
		<input type="submit"
		       name="add_new_ad" id="add-new-ad"
		       class="button"
		       value="<?php echo __( 'Add New', 'amphtml' ); ?>"
		       style="margin-left: 10px;">
		<?php if ( ! in_array( $tab->get_current_section(), array( 'top', 'bottom' ) ) ): ?>
			<input type="submit"
			       name="delete_ad" id="delete-ad"
			       class="button"
			       value="<?php echo __( 'Delete current', 'amphtml' ); ?>"
			       style="margin-left: 10px;">
		<?php endif;
	}

	public function add_new_ad_block( AMPHTML_Options $opt ) {
		if ( $opt->get_request_var( 'action' ) == 'add_new_ad_block' ) {
			$new_section_id      = end( $this->ad_sections )
				? end( $this->ad_sections ) : self::DEFAULT_NUM_OF_SECTIONS;
			$this->ad_sections[] = ++ $new_section_id;
			update_option( self::OPTION, implode( ',', $this->ad_sections ) );

			$new_section_url = add_query_arg( array(
				'page'    => 'amphtml-options',
				'tab'     => 'ad',
				'section' => $new_section_id
			), get_admin_url( null, 'options-general.php' ) );

			wp_redirect( $new_section_url );
		}
	}

	public function delete_ad_block( AMPHTML_Options $opt ) {
		$current_section = isset( $_POST['section'] ) ? $_POST['section'] : '';
		if ( $opt->get_request_var( 'action' ) == 'delete_ad_block' && $current_section ) {
			$block_opts = array(
				"ad_height_$current_section",
				"ad_width_$current_section",
				"ad_type_$current_section",
				"ad_doubleclick_data_slot_$current_section",
				"ad_data_id_client_$current_section",
				"ad_adsense_data_slot_$current_section"
			);
			if ( ( $key = array_search( $current_section, $this->ad_sections ) ) !== false ) {
				unset( $this->ad_sections[ $key ] );
				update_option( self::OPTION, implode( ',', $this->ad_sections ) );
				foreach ( $block_opts as $opt ) {
					delete_option( $opt );
				}
			}

			$new_section_url = add_query_arg( array(
				'page'    => 'amphtml-options',
				'tab'     => 'ad',
				'section' => 'top'
			), get_admin_url( null, 'options-general.php' ) );

			wp_redirect( $new_section_url );
		}
	}

	public function get_section_fields( $section, $tab ) {
		return array(
			array(
				'id'                    => "ad_height_$section",
				'title'                 => __( 'Height', 'amphtml' ),
				'section'               => $section,
				'default'               => '50',
				'display_callback'      => array( $tab, 'display_text_field' ),
				'display_callback_args' => array( "ad_height_$section", true ),
				'sanitize_callback'     => array( $tab, 'sanitize_ad_top_height' ),
				'description'           => __( 'Ad block height (in pixels)', 'amphtml' ),
			),
			array(
				'id'                    => "ad_width_$section",
				'title'                 => __( 'Width', 'amphtml' ),
				'section'               => $section,
				'default'               => '200',
				'display_callback'      => array( $tab, 'display_text_field' ),
				'display_callback_args' => array( "ad_width_$section", true ),
				'sanitize_callback'     => array( $tab, 'sanitize_ad_top_width' ),
				'description'           => __( 'Ad block width (in pixels)', 'amphtml' ),
			),
			array(
				'id'                    => "ad_type_$section",
				'title'                 => __( 'Type', 'amphtml' ),
				'default'               => 'adsense',
				'section'               => $section,
				'display_callback'      => array( $tab, 'display_ad_type' ),
				'display_callback_args' => array( "ad_type_$section" ),
				'description'           => __( 'Ad network', 'amphtml' ),
			),
			array(
				'id'                    => "ad_doubleclick_data_slot_$section",
				'title'                 => __( 'Data Slot', 'amphtml' ),
				'section'               => $section,
				'default'               => '',
				'display_callback'      => array( $tab, 'display_text_field' ),
				'display_callback_args' => array( "ad_doubleclick_data_slot_$section", true ),
				'sanitize_callback'     => array( $tab, 'sanitize_ad_doubleclick_data_slot_top' ),
				'description'           => __( 'data-slot', 'amphtml' ),
			),
			array(
				'id'                    => "ad_data_id_client_$section",
				'title'                 => __( 'AdSense Client', 'amphtml' ),
				'section'               => $section,
				'default'               => '',
				'display_callback'      => array( $tab, 'display_text_field' ),
				'display_callback_args' => array( "ad_data_id_client_$section", true ),
				'description'           => __( 'data-ad-client', 'amphtml' ),
			),
			array(
				'id'                    => "ad_adsense_data_slot_$section",
				'title'                 => __( 'Data Slot', 'amphtml' ),
				'section'               => $section,
				'default'               => '',
				'display_callback'      => array( $tab, 'display_text_field' ),
				'display_callback_args' => array( "ad_adsense_data_slot_$section", true ),
				'description'           => __( 'data-ad-slot', 'amphtml' ),
			),
		);
	}

	public function add_blocks_to_template() {
		$template_filters = array(
			'posts'    => 'amphtml_template_posts_fields',
			'pages'    => 'amphtml_template_page_fields',
			'search'   => 'amphtml_template_search_fields',
			'blog'     => 'amphtml_template_blog_fields',
			'archives' => 'amphtml_template_archive_fields'
		);
		foreach ( $template_filters as $section => $filter ) {
			add_filter( $filter, array( $this, 'get_blocks_for_templates' ), 10, 3 );
		}
	}

	public function get_blocks_for_templates( $fields, $section, $tab ) {
		$blocks = array();
		foreach ( $this->ad_sections as $block ) {
			$blocks[] = array(
				'id'                    => "post_ad_$block",
				'title'                 => __( "Ad Block #$block", 'amphtml' ),
				'default'               => 0,
				'section'               => $section,
				'display_callback'      => array( $tab, 'display_checkbox_field' ),
				'display_callback_args' => array( "post_ad_$block" ),
				'description'           => __( "Show ad block #$block", 'amphtml' ),
			);
		}

		return array_merge( $fields, $blocks );
	}
}