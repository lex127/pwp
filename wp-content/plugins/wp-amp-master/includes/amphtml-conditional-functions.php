<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( function_exists( 'AMPHTML' ) ) {

	/**
	 * @return bool
	 */
	function is_wp_amp() {
		return AMPHTML()->is_amp();
	}

	function is_wp_amp_search() {
		return isset( $_GET['is_amp'] ) && sanitize_text_field( $_GET['is_amp'] );
	}
}