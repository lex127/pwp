<?php
/**
 * The Base Template for displaying AMP HTML page.
 *
 * This template can be overridden by copying it to yourtheme/wp-amp/base.php.
 *
 * @var $this AMPHTML_Template
 */
?>
<!doctype html>
<html amp>
<head>
	<?php echo $this->render( 'header' ) ?>
</head>
<?php $rtl = $this->options->get('rtl_enable'); ?>
<body <?php echo $rtl ? 'class="rtl"' : ''; ?>>
<!-- Sidebar Nav, look wp-amp/templates/nav.php -->
<?php if ( $this->options->get( 'header_menu' ) ): ?>
	<?php if ( $this->options->get( 'header_menu_type' ) == 'sidebar' ): ?>
		<amp-sidebar id='amp-sidebar' layout="nodisplay" side="right">
			<amp-img class='amp-close-image' 
					 src="<?php echo plugins_url( 'img/ic_close_black_18dp_2x.png', dirname(__FILE__)); ?>" 
					 width="20"
					 height="20"
					 alt="close sidebar"
					 on="tap:amp-sidebar.close"
					 role="button"
					 tabindex="0">
			</amp-img>
			<?php echo $this->nav_menu(); ?>
		</amp-sidebar>
	<?php endif; ?>
<?php endif; ?>	

<div class="wrapper">
	<nav class="amphtml-title">
		<?php echo $this->render( 'nav' ) ?>
	</nav>
	<div id="main">
		<div class="inner">
			<?php do_action( AMPHTML::TEXT_DOMAIN . '_template_content' ); ?>
		</div>
	</div>
	<?php $footer = $this->get_footer(); ?>
	<div class="footer">
		<?php do_action( AMPHTML::TEXT_DOMAIN . '_footer_logo' ); ?>
		<?php if ( $footer ): ?>
			<div class="inner">
				<?php echo $footer; ?>
			</div>
		<?php endif; ?>
		<?php do_action( AMPHTML::TEXT_DOMAIN . '_footer_bottom' ); ?>
	</div>
	<?php do_action( 'amphtml_after_footer' ); ?>
</div>

</body>
</html>