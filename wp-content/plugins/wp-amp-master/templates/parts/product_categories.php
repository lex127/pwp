<?php
$cat_count  = sizeof( get_the_terms( $this->post->ID, 'product_cat' ) );
echo $this->update_content_links( $this->product->get_categories( ', ',
	'<p class="amphtml-posted-in">' . _n( 'Category:', 'Categories:', $cat_count, 'amphtml' ) . ' ', '</p>' ) );
