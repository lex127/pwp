<?php if ( $this->options->get( 'page_comments_btn' ) && comments_open() ): ?>
<a href="<?php echo $this->get_canonical_url(); ?>#comments">
		<button><?php _e('Comments', 'amphtml' );?></button>
	</a>
<?php endif;

