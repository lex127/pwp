<div class="amp-button-holder">
	<a href="<?php echo $this->get_canonical_url(); ?>" class="amp-button">
		<?php _e( $this->options->get( 'post_original_btn_text' ), 'amphtml' );?>
	</a>
</div>