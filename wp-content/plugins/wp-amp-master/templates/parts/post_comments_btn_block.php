<div class="amp-button-holder">
	<a href="<?php echo $this->get_canonical_url(); ?>#comments" class="amp-button">
		<?php _e( $this->options->get( 'post_comments_btn_text' ), 'amphtml' );?>
	</a>
</div>