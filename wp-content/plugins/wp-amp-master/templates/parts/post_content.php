<?php
echo $this->content;
wp_link_pages(
	array(
		'next_or_number' => 'next',
		'before' => '<p class="amp-multipage-holder">',
		'after' => '</p><div class="clear"></div>',
		'nextpagelink'     => '<span class="alignright">' . __( 'Next »' ) . '</span>',
		'previouspagelink' => '<span class="alignleft">' . __( '« Prev' ) . '</span>',
	)
);