<?php if ( $fb_pixel_id = $this->options->get( 'facebook_pixel' ) ): ?>
	<?php $src = "https://www.facebook.com/tr?id=$fb_pixel_id&ev=PageView&noscript=1" ?>
	<amp-pixel src="<?php echo $src ?>"></amp-pixel>
<?php endif; ?>