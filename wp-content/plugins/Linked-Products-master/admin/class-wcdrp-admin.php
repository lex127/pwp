<?php
/**
 * Admin Class
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * WC_Admin class.
 */
class WCDRP_Admin {

	const SECTION_NAME = 'wcdrp';

	/**
	 * @var WCDRP_Post_type
	 */
	protected $post_type;

	public function __construct( $post_type ) {
		$this->post_type = $post_type;
		add_filter( 'woocommerce_get_sections_products', array ( $this, 'add_section' ) );
		add_filter( 'woocommerce_get_settings_products', array ( $this, 'add_settings' ), 10, 2 );

		add_filter( 'manage_edit-wcdrp_rules_columns', array ( $this, 'colums' ) );
		add_action( 'manage_wcdrp_rules_posts_custom_column', array ( $this, 'wcdrp_manage_columns' ), 10, 2 );
		add_filter( 'manage_edit-wcdrp_rules_sortable_columns', array ( $this, 'sortable_columns' ) );
		add_filter( 'request', array ( $this, 'request_query' ) );
		add_filter( 'post_row_actions', array ( $this, 'remove_quick_button' ), 10, 1 );
		add_filter( 'post_updated_messages', array ( $this, 'wcdrp_post_published' ), 99 );
		add_action( 'admin_menu', array ( $this, 'wcdrp_remove_meta_boxes' ) );
	}

	public function add_section( $sections ) {

		$sections[ self::SECTION_NAME ] = __( 'Auto Linked Products', 'wc-alp' );

		return $sections;

	}

	public function add_settings( $settings, $current_setction ) {
		if ( self::SECTION_NAME !== $current_setction ) {
			return $settings;
		}
		$settings = array ( 'type' => 'sectionend', 'id' => 'wcdrp' );

		return $settings;
	}


	/*
	* Start modified display table
	*/
	public function get_option_value( $option, $value ) {

		$options = $this->post_type->get_post_type_options();

		$positions = isset( $options[ $option ] ) ? $options[ $option ] : array ();

		if ( count( $positions ) ) {
			$value = $positions['options'][ $value ];
		}

		return $value;
	}

	public function colums( $columns ) {
		$columns = array (
			'cb'       => '<input type="checkbox" />',
			'title'    => __( 'Rule', 'wc-alp' ),
			'id'       => __( 'Id', 'wc-alp' ),
			'name'     => __( 'Title', 'wc-alp' ),
			'position' => __( 'Position', 'wc-alp' ),
			'priority' => __( 'Priority', 'wc-alp' ),
			'status'   => __( 'Status', 'wc-alp' ),
		);

		return $columns;
	}

	public function sortable_columns( $columns ) {
		$custom = array (
			'name'     => 'name',
			'id'       => 'id',
			'tite'     => 'title',
			'priority' => 'priority',
			'status'   => array ( 'status', 1 ),
			'position' => 'position',
		);

		return wp_parse_args( $custom, $columns );
	}

	public function wcdrp_manage_columns( $column, $post_id ) {
		global $post;

		$post_meta = get_post_meta( $post_id, 'wcdrp_rules' );

		switch ( $column ) {
			case 'id':

				echo $post_id;

				break;
			case 'name':

				foreach ( $post_meta as $key => $value ) { // ?
					$value = $value["wcdrp-appearance"]["block_title"];
					echo $value;
				}

				break;
			case 'position':

				foreach ( $post_meta as $key => $value ) {
					$position = $value["wcdrp-general"]["position"];
					$value    = $value["wcdrp-general"]["position"];
					echo $this->get_option_value( 'position', $value );
				}

				if ( empty( $position ) ) {
					echo __( 'None' );
				}

				break;
			case 'priority':

				foreach ( $post_meta as $key => $value ) {
					$priority = $value["wcdrp-general"]["priority"];
					echo $priority;
				}

				if ( empty( $priority ) ) {
					echo __( 'None' );
				}
				break;
			case 'status':

				foreach ( $post_meta as $key => $value ) {
					$value = $value["wcdrp-general"]["status"];
					echo $this->get_option_value( 'status', $value );
				}

				break;
			default :
				break;
		}

	}

	/**
	 * Filters and sorting handler.
	 *
	 * @param  array $vars
	 *
	 * @return array
	 */
	public function request_query( $vars ) {
		global $typenow, $wp_query, $wp_post_statuses;

		if ( 'wcdrp_rules' !== $typenow ) {
			return $vars;
		}

		if ( ! isset( $vars['orderby'] ) ) {
			return $vars;
		}

		// Sorting
		if ( 'priority' == $vars['orderby'] ) {
			$vars = array_merge( $vars, array (
				'meta_key' => 'wcdrp_general_priority',
				'orderby'  => 'meta_value_num'
			) );
		}
		if ( 'name' == $vars['orderby'] ) {
			$vars = array_merge( $vars, array (
				'meta_key' => 'wcdrp_appearance_block_title',
				'orderby'  => 'meta_value'
			) );
		}
		if ( 'position' == $vars['orderby'] ) {
			$vars = array_merge( $vars, array (
				'meta_key' => 'wcdrp_general_position',
				'orderby'  => 'meta_value'
			) );
		}
		if ( 'status' == $vars['orderby'] ) {
			$vars = array_merge( $vars, array (
				'meta_key' => 'wcdrp_general_position',
				'orderby'  => 'meta_value_num'
			) );
		}

		return $vars;
	}

	/*
	 * Changes display view functions
	 */
	public function remove_quick_button( $actions ) {
		global $post;

		if ( $post->post_type == 'wcdrp_rules' ) {
			unset( $actions['view'] );
			unset( $actions['inline hide-if-no-js'] );
		}

		return $actions;
	}

	public function wcdrp_post_published( $messages ) {
		global $post;

		if ( $post->post_type == 'wcdrp_rules' ) {
			$messages['post'][1] = __( 'Rule updated.', 'wc-alp' );
			$messages['post'][6] = __( 'Rule published.', 'wc-alp' );
		}

		return $messages;
	}

	function wcdrp_remove_meta_boxes() {

		remove_meta_box( 'submitdiv', 'wcdrp_rules', 'core' );

		add_meta_box( 'submitdiv', __( 'Publish Rule', 'wc-alp' ), array (
			$this, 'wcdrp_submit_meta_box' ), 'wcdrp_rules', 'side', 'low' );
	}


	function wcdrp_submit_meta_box() {
		global $action, $post;

		$post_type        = $post->post_type;
		$post_type_object = get_post_type_object( $post_type );
		$can_publish      = current_user_can( $post_type_object->cap->publish_posts );
		$item             = 'wcdrp_rules';
		?>
		<div class="submitbox" id="submitpost">
			<div id="major-publishing-actions">
				<?php do_action( 'post_submitbox_start' ); ?>
				<div id="delete-action">
					<?php
					if ( current_user_can( "delete_post", $post->ID ) ) {
						if ( ! EMPTY_TRASH_DAYS ) {
							$delete_text = __( 'Delete Permanently', 'wc-alp' );
						} else {
							$delete_text = __( 'Move to Trash', 'wc-alp' );
						}
						?>
						<a class="submitdelete deletion"
						   href="<?php echo get_delete_post_link( $post->ID ); ?>"><?php echo $delete_text; ?></a><?php
					} ?>
				</div>
				<div id="publishing-action">
					<span class="spinner"></span>
					<?php
					if ( ! in_array( $post->post_status, array (
							'publish',
							'future',
							'private'
						) ) || 0 == $post->ID
					) {
						if ( $can_publish ) : ?>
							<input name="original_publish" type="hidden" id="original_publish"
							       value="<?php esc_attr_e( 'Publish', 'wc-alp' ) ?>"/>
							<?php submit_button( sprintf( __( 'Publish', 'wc-alp' ), $item ), 'primary button-large', 'publish', false, array ( 'accesskey' => 'p' ) ); ?>
							<?php
						endif;
					} else { ?>
						<input name="original_publish" type="hidden" id="original_publish"
						       value="<?php esc_attr_e( 'Update ', 'wcdrp_rules' ) . $item; ?>"/>
						<input name="save" type="submit" class="button button-primary button-large" id="publish"
						       accesskey="p" value="<?php esc_attr_e( 'Update ', 'wcdrp_rules' ) . $item; ?>"/>
						<?php
					} ?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<?php
	}

}