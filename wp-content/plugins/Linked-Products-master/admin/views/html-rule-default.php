<?php
/**
 * @var $this WCDRP_Template
 */
?>
<table class="form-table">
	<?php if( isset( $this->args['description'] ) ): ?>
		<p><?php echo $this->args['description']  ?></p>
	<?php endif; ?>
	<?php $this->render_fields() ?>
</table>