<?php
/**
 * @var $this WCDRP_Template
 */
$option_name = sprintf( "%s[%s]", $this->post_type_slug, $this->metabox_id );
$params      = $this->get_product_atts();
$operators   = $this->get_rule_operators();
$values      = array(); // todo maybe set default rule ( optional )

?>
<?php if ( isset( $this->args['description'] ) ): ?>
	<p><?php echo $this->args['description'] ?></p>
<?php endif; ?>
<div class="repeater">
	<div data-repeater-list="<?php echo $option_name ?>" class="sortable">
		<div data-repeater-item class="alp-select2-drop" style="display: none">
			<input type="hidden" name="template" value="1">
			<label class="select2-offscreen"><?php _e( 'Rule', 'wc-alp' ) ?>:</label>
			<select name="param" class="rule-param" required>
				<?php foreach ( $params as $value => $label ): ?>
					<option value="<?php echo $value ?>"><?php echo $label ?></option>
				<?php endforeach; ?>
			</select>

			<select name="operator" class="rule-operator" style="width: 15%" required> <?php //todo move width to css ?>
				<?php foreach ( $operators as $value => $label ): ?>
					<option value="<?php echo $value ?>"><?php echo $label ?></option>
				<?php endforeach; ?>
			</select>

			<select name="value" class="rule-value" style="width: 30%"> <?php //todo move width to css ?>
				<?php foreach ( $values as $value => $label ): ?>
					<option value="<?php echo $value ?>"><?php echo $label ?></option>
				<?php endforeach; ?>
			</select>
			<input data-repeater-delete type="button" class="button button-secondary"
			       value="<?php _e( 'Delete', 'wc-alp' ) ?>">

		</div>
		<?php if ( count( $this->option_values ) ): ?>
			<?php foreach ( $this->option_values as $index => $rule ): ?>
				<?php
				$this->current_rule_index = $index;
				$operators                = $this->get_rule_operators( $rule['param'] );
				?>
				<div data-repeater-item class="alp-select2-drop">
					<label class="select2-offscreen"><?php _e( 'Rule', 'wc-alp' ) ?>:</label>
					<select name="param" class="rule-param" required>
						<?php foreach ( $params as $value => $label ): ?>
							<option
								value="<?php echo $value ?>" <?php selected( $value, $rule['param'] ) ?>>
								<?php echo $label ?>
							</option>
						<?php endforeach; ?>
					</select>

					<select name="operator" class="rule-operator" style="width: 15%"
					        required> <?php //todo move width to css ?>
						<?php foreach ( $operators as $value => $label ): ?>
							<option
								value="<?php echo $value ?>" <?php selected( $value, $rule['operator'] ) ?>>
								<?php echo $label ?>
							</option>
						<?php endforeach; ?>
					</select>

					<?php echo $this->get_param_values_view( $rule['param'] ) ?>

					<input data-repeater-delete type="button" class="button button-secondary"
					       value="<?php _e( 'Delete', 'wc-alp' ) ?>">
				</div>
			<?php endforeach; ?>
		<?php endif; ?>
	</div>
	<input data-repeater-create type="button" class="button button-secondary"
	       value="<?php _e( 'Add', 'wc-alp' ) ?>">
</div>

<script>
	jQuery(document).ready(function ($) {

		$('.sortable').sortable();

		function updateValues(obj) {
			var self = obj;
			var choosers = ['product_cat', 'name'];
			var data = {
				'action': wcdrp.action,
				'name': $(obj).find('option:selected').val(),
				field_name: $(self).attr('name')
			};
			$.post(wcdrp.ajax_url, data, function (response) {
				$(['.chosen-container', '.rule-operator', '.rule-value']).each(function (i, val) {
					$(self).siblings(val).remove();
				});
				$(response).insertAfter(self);
				if (choosers.indexOf(data.name) !== -1) {
					$(self).siblings('.rule-value').chosen();
				}
			});
		}

		$('.rule-param').change(function () {
			updateValues(this);
		});

		function addRepeaterFields() {
			$(this).slideDown(400, 'swing', function () {
				$(this).find('.rule-param').change(function () {
					updateValues(this);
				});
			});
		}

		function removeFields() {
			$(this).slideUp(function () {
				$(this).remove();
			});
		}

		var metaboxId = '<?php echo $this->metabox_id ?>';

		$('#' + metaboxId).find('.repeater').repeater({

			show: function () {
				addRepeaterFields.apply(this);
				$('.sortable').sortable();
			},
			hide: function (deleteElement) {
				removeFields.apply(this);
			},
			ready: function (setIndexes) {
			}

		});

	});
</script>