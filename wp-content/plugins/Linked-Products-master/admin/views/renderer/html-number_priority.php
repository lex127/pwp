<?php
$field = $this->current_option;
$name  = esc_attr( $field['name'] );
$id    = esc_attr( $field['id'] );
$title = esc_attr( $field['title'] );
$desc  = esc_html( $field['description'] );
$value = esc_attr__( $field['value'] );
?>
<tr>
	<th>
		<label for="<?php echo $name ?>" class="<?php echo $name ?>_label"><?php echo $title ?></label>
	</th>
	<td>
		<input
			type="number"
			step="1" min="10"
			id="<?php echo $id ?>"
			name="<?php echo $name ?>"
			class="<?php echo $id ?>"
			<?php if ( ! empty( $field['required'] ) )
				echo $field['required'] ?>
			value="<?php echo $value ?>"
		>
		<p class="description"><?php echo $desc ?></p>
	</td>
</tr>
