<?php
$field = $this->current_option;
$name  = esc_attr( $field['name'] );
$title = esc_attr( $field['title'] );
$desc  = esc_html( $field['description'] );
$value = esc_attr__( $field['value'] );
?>
<tr>
	<th>
		<label for="<?php echo $name ?>" class="<?php echo $name ?>_label"><?php echo $title ?></label>
	</th>
	<td>
		<input
			type="text"
			id="<?php echo $name ?>"
			name="<?php echo $name ?>"
			class="<?php echo $name ?>_field"
			<?php if ( ! empty( $field['required'] ) )
				echo $field['required'] ?>
			value="<?php echo $value ?>"
		>
		<p class="description"><?php echo $desc ?></p>
	</td>
</tr>
