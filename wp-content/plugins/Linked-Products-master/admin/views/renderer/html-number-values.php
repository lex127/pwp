<?php
/**
 * @var $this WCDRP_Template
 */
$name  = $this->get_metabox_field_name();
$value =$current_value = isset( $this->option_values[ $this->current_rule_index ]['value'] )
	? $this->option_values[ $this->current_rule_index ]['value'] : '';
?>
<input type="number" step="1" min="1"
       name="<?php echo $name ?>"
       class="rule-value" required
       style="width: 35%"
       value="<?php echo $value ?>"
>