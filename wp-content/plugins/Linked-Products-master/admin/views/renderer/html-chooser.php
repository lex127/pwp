<?php
/**
 * @var $this WCDRP_Template
 */
$current_value = isset( $this->option_values[ $this->current_rule_index ]['value'] )
	? ( array ) $this->option_values[ $this->current_rule_index ]['value'] : array();
$collection    = $this->collection;
$name          = $this->get_metabox_field_name();
if ( WC_DRP()->is_request( 'ajax' ) ) {
	$name .= '[]';
}
?>
<select
	name="<?php echo $name ?>"
	class="rule-value chosen-select"
	data-placeholder="Choose a product..."
	style="width: 35%" <?php //todo move width to css ?>
	multiple>
	<?php foreach ( $collection as $value => $label ): ?>
		<option
			value="<?php echo $value ?>" <?php selected( in_array( $value, $current_value ) ) ?> ><?php echo $label ?></option>
	<?php endforeach; ?>
</select>
<script type="text/javascript">
	jQuery(function ($) {
		$('.rule-value').each(function () {
			if ($(this).is('select') && true === $(this).hasClass('chosen-select')) {
				$(this).chosen();
			}
		});
	})
</script>
