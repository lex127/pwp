<?php
$field = $this->current_option;
$name  = esc_attr( $field['name'] );
$title = esc_attr( $field['title'] );
$desc  = esc_html( $field['description'] );
$value = esc_attr__( $field['value'] );
?>
<tr>
	<th>
		<label for="<?php echo $name ?>" class="<?php echo $name ?>_label"><?php echo $title ?></label>
	</th>
	<td>
		<select  name="<?php echo $name ?>">
			<?php foreach ( $field['options'] as $name => $title ): ?>
				<option value="<?php echo $name ?>" <?php selected( $value, $name ) ?>><?php echo $title ?></option>
			<?php endforeach; ?>
		</select>
		<p class="description"><?php echo $desc ?></p>
	</td>
</tr>