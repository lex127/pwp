<?php
/**
 * @var $this WCDRP_Template
 */

$name  = $this->get_metabox_field_name();
$value =$current_value = isset( $this->option_values[ $this->current_rule_index ]['value'] )
	? $this->option_values[ $this->current_rule_index ]['value'] : '';
?>
<select name="<?php echo $name ?>" style="width: 35%" class="rule-value" required> <?php //todo move width to css ?>
		<option value="instock" <?php selected( $value, "instock" ) ?>><?php echo __( 'In stock', 'wc-alp' ) ?></option>
		<option value="outofstock" <?php selected( $value, "outofstock" ) ?>><?php echo __( 'Out of stock', 'wc-alp' ) ?></option>
</select>