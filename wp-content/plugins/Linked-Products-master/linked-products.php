<?php
/**
 * Plugin Name: Linked Products
 * Description: Dynamic Rules for WooCommerce Up-Sells and Related Products. Define the rules for related products and offer people alternatives to the current product automatically.
 * Version: 1.2
 * Author: TeamDev Ltd
 * Author URI: https://www.teamdev.com/
 * Text Domain: wc-related-products
 * Domain Path: /languages/
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'WC_DRP' ) ) :

	/**
	 * Main Class.
	 */
	final class WC_DRP {

		const INCLUDE_PATH = 'includes';
		const ADMIN_PATH   = 'admin';
		const VERSION      = '1.0.0';
		const PREFIX       = 'wcdrp_';

		/**
		 * The single instance of the class.
		 */
		protected static $_instance = null;

		protected $loader;

		protected $post_type;

		/**
		 * Main Instance.
		 *
		 * Ensures only one instance of WooCommerce is loaded or can be loaded.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}

		/**
		 * Cloning is forbidden.
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wc-alp' ), '2.1' );
		}

		/**
		 * Unserializing instances of this class is forbidden.
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wc-alp' ), '2.1' );
		}

		/**
		 * WooCommerce Constructor.
		 */
		public function __construct() {
			$this->add_hooks();
			$this->includes();
			$this->load();
			$this->init();
			add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );
		}

		/**
		 * Include required core files used in admin and on the frontend.
		 */
		public function load() {

			$this->loader = new TeamDev_Wp_Autoloader( $this->get_path() );
			if ( $this->is_request( 'admin' ) ) {
				$this->loader->autoload( self::ADMIN_PATH );
			}
			$this->loader->autoload( self::INCLUDE_PATH );

		}

		public function init() {
			$this->post_type = new WCDRP_Post_type();
			if ( $this->is_request( 'admin' ) ) {
				new WCDRP_Admin( $this->post_type );
			}
			add_action( 'after_switch_theme', array( $this, 'activation' ) );
		}

		public function includes() {
			include_once( 'vendor/teamdev/teamdev-wp-autoloader.php' );
			include_once( 'vendor/teamdev/teamdev-wp-template.php' );
			include_once( 'vendor/teamdev/teamdev-wp-cpt.php' );
		}

		public function add_hooks() {
			add_action( 'wp_enqueue_scripts', array( $this, 'load_resources' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'load_resources' ) );
		}

		public function activation() {
			global $wp_filter;
			update_option( self::PREFIX . 'hooks', array() );
		}

		/**
		 * What type of request is this?
		 *
		 * @param  string $type admin, ajax, cron or frontend.
		 *
		 * @return bool
		 */
		public function is_request( $type ) {
			switch ( $type ) {
				case 'admin' :
					return is_admin();
				case 'ajax' :
					return defined( 'DOING_AJAX' );
				case 'frontend' :
					return ( ! is_admin() || defined( 'DOING_AJAX' ) );
			}
		}

		/**
		 * Load localization files.
		 */
		public function load_plugin_textdomain() {
			load_plugin_textdomain( 'wc-alp', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
		}


		/**
		 * Get the plugin url.
		 * @return string
		 */
		public function get_url() {
			return untrailingslashit( plugins_url( '/', __FILE__ ) );
		}

		/**
		 * Get the plugin path.
		 * @return string
		 */
		public function get_path() {
			return untrailingslashit( plugin_dir_path( __FILE__ ) );
		}

		/**
		 * Get the template path.
		 * @return string
		 */
		public function get_template_path() {
			return apply_filters( 'woocommerce_template_path', 'wc_related_products/' );
		}

		/**
		 * Get Ajax URL.
		 * @return string
		 */
		public function ajax_url() {
			return admin_url( 'admin-ajax.php', 'relative' );
		}

		public static function load_resources() {

			wp_register_script(
				self::PREFIX . 'wcdrp-carousel',
				plugins_url( 'assets/js/owl.carousel.js', __FILE__ ),
				array( 'jquery' ),
				self::VERSION,
				true
			);

			wp_register_script(
				self::PREFIX . 'wc-auto-linked-products',
				plugins_url( 'assets/js/wc-auto-linked-products.js', __FILE__ ),
				array( 'jquery' ),
				self::VERSION,
				true
			);

			wp_register_style(
				self::PREFIX . 'admin-style',
				plugins_url( 'assets/css/admin.css', __FILE__ ),
				array(),
				self::VERSION,
				'all'
			);

			wp_register_style(
				self::PREFIX . 'admin-style-chosen',
				plugins_url( 'assets/css/chosen.css', __FILE__ ),
				array(),
				self::VERSION,
				'all'
			);

			wp_register_style(
				self::PREFIX . 'wc-auto-linked-products',
				plugins_url( 'assets/css/style.css', __FILE__ ),
				array(),
				self::VERSION,
				'all'
			);

			wp_register_style(
				self::PREFIX . 'wcdrp-carousel',
				plugins_url( 'assets/css/owl.carousel.css', __FILE__ ),
				array(),
				self::VERSION,
				'all'
			);

			wp_register_style(
				self::PREFIX . 'wcdrp-carousel-default',
				plugins_url( 'assets/css/owl.theme.default.css', __FILE__ ),
				array(),
				self::VERSION,
				'all'
			);

			if ( is_admin() ) {
				wp_enqueue_style( self::PREFIX . 'admin-style' );
				wp_enqueue_style( self::PREFIX . 'admin-style-chosen' );


				wp_enqueue_script( self::PREFIX . 'repeater',
					plugins_url( 'assets/js/jquery.repeater.js', __FILE__ ), array( 'jquery' ), self::VERSION, true
				);
				wp_enqueue_script( self::PREFIX . 'chosen',
					plugins_url( 'assets/js/chosen.jquery.js', __FILE__ ), array( 'jquery' ), self::VERSION, true
				);
				wp_localize_script( self::PREFIX . 'chosen', 'wcdrp',
					array(
						'ajax_url' => admin_url( 'admin-ajax.php' ),
						'action'   => 'wcdrp_additional_settings'
					) );
			} else {
				wp_enqueue_style( self::PREFIX . 'wc-auto-linked-products' );
				wp_enqueue_style( self::PREFIX . 'wcdrp-carousel' );
				wp_enqueue_style( self::PREFIX . 'wcdrp-carousel-default' );
				wp_enqueue_script( self::PREFIX . 'wcdrp-carousel' );
				wp_enqueue_script( self::PREFIX . 'wc-auto-linked-products' );
			}
		}

		public function get_wc_filter_data( $match ) {
			global $wp_filter;

			$prefix      = 'woocommerce';
			$filter_data = array();

			foreach ( $wp_filter as $tag => $priorities ) {
				if ( false !== strpos( $tag, $prefix ) ) {
					foreach ( $priorities as $priority => $hooks ) {
						$hook = current( preg_grep( "/_$match$/i", array_keys( $hooks ) ) );
						if ( $hook ) {
							$filter_data = array( $tag, $hook, $priority );
						}
					}
				}
			}

			return $filter_data;
		}

	}

endif;

/**
 * Main instance of Linked Products.
 *
 * Returns the main instance of WC_Related_Products to prevent the need to use globals.
 *
 * @return WC_DRP
 */
function WC_DRP() {
	return WC_DRP::instance();
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	WC_DRP();
	register_activation_hook( __FILE__, array( WC_DRP(), 'activation' ) );
}