<?php
/**
 * Post Types
 *
 * Registers post types and taxonomies.
 *
 * @class     WCDRP_Post_types
 * @category  Class
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Post_types Class.
 */
class WCDRP_Post_type extends TeamDev_Wp_CPT {

	const METABOX_VIEW_DIR = 'admin/views';

	public function __construct() {
		parent::__construct();
		if ( ! is_admin() || defined( 'DOING_AJAX' ) ) {
			add_action( 'wp', array( $this, 'setup_linked_products' ) );
		}
	}

	public function init() {
		parent::init();
		new WCDRP_Shortcode( $this );
	}

	public function get_template_path( $template_dir ) {
		return WC_DRP()->get_path() . DIRECTORY_SEPARATOR . $template_dir;
	}

	public function set_post_type_slug() {
		$this->post_type_slug = 'wcdrp_rules';
	}

	public function setup_linked_products() {
		global $post;
		if ( ! $post ) {
			return '';
		}

		$rule_ids = $this->match_rule( $post->ID );

		if ( count( $rule_ids ) ) {
			$rules = array();
			foreach ( $rule_ids as $rule_id ) {
				$rule_options                                 = $this->get_post_meta( $rule_id );
				$rules[ $rule_id ]['options'] = $rule_options;
				if ( isset( $rule_options['wcdrp-products'] ) ) {
					$rules[ $rule_id ]['products'] = $this->get_linked_products( $rule_options );
				}
			}
			$this->template->rules = $rules;
		}
	}

	public function get_linked_products( $options ) {
		$products = array();
		$args = array();

		if ( isset( $options['wcdrp-products'] ) ) {
			$args = $this->get_wp_query_args( $options['wcdrp-products'] );
		}

		$order_args = $this->get_order_args( $options['wcdrp-appearance']['sort_by'] );

		$args = array_merge( $args, $order_args, array(
			'posts_per_page' => $options['wcdrp-appearance']['max_products'],
		) );

		$products = new WP_Query( $args );

		return $products;
	}

	public function get_order_args( $value ) {
		$args = array();
		switch ( $value ) {
			case 1:
				$args['orderby'] = 'rand';
				break;
			case 2:
				$args = array(
					'meta_key' => 'total_sales',
					'orderby'  => 'meta_value_num',
				);
				break;
			case 3:
				$args = array(
					'orderby' => 'date',
					'order'   => 'DESC'
				);
				break;
			case 4:
				$args = array(
					'meta_key' => '_price',
					'orderby'  => 'meta_value_num',
					'order'    => 'DESC'
				);
				break;
			case 5:
				$args = array(
					'meta_key' => '_price',
					'orderby'  => 'meta_value_num',
					'order'    => 'ASC'
				);
		}

		return $args;
	}

	public function match_rule( $product_id ) {
		//get all rules
		$rules    = array();
		$rule_ids = new WP_Query( array(
			'post_status'    => 'publish',
			'post_type'      => $this->get_post_type_slug(),
			'posts_per_page' => - 1,
			'fields'         => 'ids'
		) );

		$rule_ids = $rule_ids->posts;

		if ( ! count( $rule_ids ) ) {
			return '';
		}

		//get options of each rule
		foreach ( $rule_ids as $id ) {

			$rule_data = $this->get_post_meta( $id );

			if ( ! $rule_data || ! $rule_data['wcdrp-general']['status'] ) {
				continue;
			}

			$rule_option_name = 'wcdrp-location';

			if ( isset( $rule_data[ $rule_option_name ] ) ) {
				$rules[ $id ]['params']   = $rule_data[ $rule_option_name ];
				$rules[ $id ]['priority'] = $rule_data['wcdrp-general']['priority'];
				$rules[ $id ]['id']       = $id;
			}

			// split method start
			if ( count( $rules[ $id ]['params'] ) ) {
				$args = $this->get_wp_query_args( $rules[ $id ]['params'] );
				$args['fields'] = 'ids';

				$products = new WP_Query( $args );

				$product_ids = $products->posts;

				if ( ! count( $product_ids ) || ! in_array( $product_id, $product_ids ) ) {
					unset( $rules[ $id ] );
				}
			}

		}

		$matched_rule_ids = array();

		if ( count( $rules ) ) {
			usort( $rules, array( $this, 'priority_sorting' ) );
			foreach ( $rules as $rule ) {
				$matched_rule_ids[] = $rule['id'];
			}
		}

		return $matched_rule_ids;
	}

	public function get_wp_query_args( $params ) {

		$conditions = array();

		foreach ( $params as $condition ) {
			$conditions[ $condition['param'] ] = $condition;
		}

		$args = array(
			'post_type'      => 'product',
			'post_status'    => 'published',
			'posts_per_page' => - 1,
		);

		if ( isset( $conditions['name']['value'] ) ) {

			$ids = ( array ) $conditions['name']['value'];

			if ( 'IN' == $conditions['name']['operator'] ) {
				$args['post__in'] = $ids;
			} else {
				$args['post__not_in'] = $ids;
			}

		}

		if ( isset( $conditions['product_cat'] ) ) {
			$terms    = ! empty( $conditions['product_cat']['value'] ) ? $conditions['product_cat']['value'] : '';
			$operator = ! empty( $conditions['product_cat']['operator'] ) ? $conditions['product_cat']['operator'] : '';

			$args['tax_query'][] = array(
				'taxonomy'         => 'product_cat',
				'field'            => 'id',
				'include_children' => true,
				'terms'            => $terms,
				'operator'         => $operator
			);
		}

		if ( isset( $conditions['product_tag'] ) ) {
			$terms    = ! empty( $conditions['product_tag']['value'] ) ? $conditions['product_tag']['value'] : '';
			$operator = ! empty( $conditions['product_tag']['operator'] ) ? $conditions['product_tag']['operator'] : '';

			$args['tax_query'][] = array(
				'taxonomy'         => 'product_tag',
				'field'            => 'id',
				'include_children' => true,
				'terms'            => $terms,
				'operator'         => $operator
			);

			if ( isset( $conditions['product_cat'] ) ) {
				$args['tax_query']['relation'] = 'AND';
			}
		}

		$meta_query_keys = $this->get_meta_query_keys();

		foreach ( $meta_query_keys as $key ) {
			if ( isset( $conditions[ $key ] ) ) {
				$args['meta_query'][] = array(
					'key'     => $key,
					'value'   => $conditions[ $key ]['value'],
					'compare' => $conditions[ $key ]['operator'],
					'type'    => $this->get_meta_query_type( $key )
				);
			}
		}

		if ( isset( $args['meta_query'] ) && count( $args['meta_query'] ) ) {
			$args['meta_query']['relation'] = 'AND';
		}

		return $args;
	}

	public function get_meta_query_keys() {
		$keys = array( '_price', '_sku', '_weight', '_stock_status' );
		return $keys;
	}

	public function get_meta_query_type( $key ) { //todo move to main field definition
		$type = 'CHAR';
		switch ( $key ) {
			case '_price':
				$type = 'NUMERIC';
				break;
			default:
				$type = 'CHAR';
		}

		return $type;
	}

	public function priority_sorting( $a, $b ) {
		$a = $a['priority'];
		$b = $b['priority'];
		if ( $a == $b ) {
			return 0;
		}

		return ( $a < $b ) ? - 1 : 1;
	}

	public function set_template() {
		$this->template = new WCDRP_Template( $this->get_template_path( self::METABOX_VIEW_DIR ) );
		$this->template->post_type = $this;
	}

	protected function get_post_type_params() {
		$labels = array(
			'name'                  => __( 'Linked Product Rules', 'wc-alp' ),
			'singular_name'         => __( 'Linked Product Rule', 'wc-alp' ),
			'menu_name'             => _x( 'Linked Product Rules', 'Admin menu name', 'wc-alp' ),
			'add_new'               => __( 'Add Rule', 'wc-alp' ),
			'add_new_item'          => __( 'Add New Rule', 'wc-alp' ),
			'edit'                  => __( 'Edit', 'wc-alp' ),
			'edit_item'             => __( 'Edit Rule', 'wc-alp' ),
			'new_item'              => __( 'New Rule', 'wc-alp' ),
			'view'                  => __( 'View Rules', 'wc-alp' ),
			'view_item'             => __( 'View Rule', 'wc-alp' ),
			'search_items'          => __( 'Search Rules', 'wc-alp' ),
			'not_found'             => __( 'No Rules found', 'wc-alp' ),
			'not_found_in_trash'    => __( 'No Rules found in trash', 'wc-alp' ),
			'parent'                => __( 'Parent Rule', 'wc-alp' ),
			'filter_items_list'     => __( 'Filter rules', 'wc-alp' ),
			'items_list_navigation' => __( 'Rules navigation', 'wc-alp' ),
			'items_list'            => __( 'Rules list', 'wc-alp' ),
		);
		$params = array(
			'labels'              => $labels,
			'description'         => __( 'This is where you can add new  related product rules.', 'wc-alp' ),
			'public'              => false,
			'show_ui'             => true,
			'capability_type'     => 'post',
			'map_meta_cap'        => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => true,
			'show_in_menu'        => current_user_can( 'manage_woocommerce' ) ? 'woocommerce' : true,
			'hierarchical'        => false,
			'rewrite'             => false,
			'query_var'           => false,
			'supports'            => array( 'title' ),
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
		);

		return apply_filters( 'wcdrp_post_type_params', $params );
	}

	protected function get_metaboxes_data() {
		return array(
			'general'            => array(
				'id'            => 'wcdrp-general',
				'title'         => __( 'Rule Information', 'wc-alp' ),
				'callback'      => array( $this, 'get_metabox_view' ),
				'callback_args' => array( 'template' => 'rule-default' ),
			),
			'display-location'   => array(
				'id'            => 'wcdrp-location',
				'title'         => __( 'Where to Display', 'wc-alp' ),
				'callback'      => array( $this, 'get_metabox_view' ),
				'callback_args' => array(
					'template'    => 'rule-location',
					'description' => __( 'Choose products the related items will be displayed for. Conditions and condition combinations allow you to narrow selection down to a specific range of products', 'wc-alp' )
				),

			),
			'display-products'   => array(
				'id'            => 'wcdrp-products',
				'title'         => __( 'What to Display', 'wc-alp' ),
				'callback'      => array( $this, 'get_metabox_view' ),
				'callback_args' => array(
					'template'    => 'rule-location',
					'description' => __( 'Choose products that will be displayed in the block. Conditions and condition combinations allow you to narrow selection down to a specific range of products', 'wc-alp' )
				)
			),
			'display-appearance' => array(
				'id'            => 'wcdrp-appearance',
				'title'         => __( 'How to Display', 'wc-alp' ),
				'callback'      => array( $this, 'get_metabox_view' ),
				'callback_args' => array( 'template' => 'rule-default' )
			),
		);
	}

	public function get_post_type_options() {
		return array(
			'status'       => array(
				'title'           => __( 'Status', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_select' ),
				'default'         => 1,
				'metabox_id'      => 'wcdrp-general',
				'required'        => 'required',
				'options'         => array(
					1 => __( 'Enabled', 'wc-alp' ),
					0 => __( 'Disabled', 'wc-alp' )
				)
			),
			'priority'     => array(
				'title'           => __( 'Priority', 'wc-alp' ),
				'description'     => __( 'Some products might fall under several rules. Priority is here to decide which rule will be executed in such case. Rules with higher priority (e.g. 10 is higher than 20) will be executed first.', 'wc-alp' ),
				'render_callback' => array( $this->template, 'render_admin_priority' ),
				'metabox_id'      => 'wcdrp-general',
				'default'         => 10,

			),
			'position'     => array(
				'title'           => __( 'Block position', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_select' ),
				'metabox_id'      => 'wcdrp-general',
				'default'         => 2,
				'options'         => array(
					1 => __( 'Before single product summary', 'wc-alp' ),
					2 => __( 'After single product summary', 'wc-alp' ),
					3 => __( 'Replace related products', 'wc-alp' ),
					4 => __( 'Replace up-sell products', 'wc-alp' )
				)
			),
			'block_title'  => array(
				'title'           => __( 'Block Title', 'wc-alp' ),
				'description'     => __( 'Name the block in a way that reflects what kind of products it displays. e.g. Frequently bought together, you might be also interested in, featured products, more in this collection, etc.', 'wc-alp' ),
				'render_callback' => array( $this->template, 'render_admin_text' ),
				'metabox_id'      => 'wcdrp-appearance',
				'default'         => __( 'More in this collection', 'wc-alp' ),
			),
			'block_layout' => array(
				'title'           => __( 'Block Layout', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_select' ),
				'metabox_id'      => 'wcdrp-appearance',
				'default'         => 1,
				'options'         => array(
					1 => __( 'Slider', 'wc-alp' ),
					2 => __( 'Grid', 'wc-alp' ),
				)
			),
			'add_to_cart'  => array(
				'title'           => __( 'Display Add To Cart', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_select' ),
				'metabox_id'      => 'wcdrp-appearance',
				'default'         => 0,
				'options'         => array(
					0 => __( 'No', 'wc-alp' ),
					1 => __( 'Yes', 'wc-alp' ),
				)
			),
			'ratings'  => array(
				'title'           => __( 'Display Ratings', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_select' ),
				'metabox_id'      => 'wcdrp-appearance',
				'default'         => 1,
				'options'         => array(
					0 => __( 'No', 'wc-alp' ),
					1 => __( 'Yes', 'wc-alp' ),
				)
			),
			'price'  => array(
				'title'           => __( 'Display Price', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_select' ),
				'metabox_id'      => 'wcdrp-appearance',
				'default'         => 1,
				'options'         => array(
					0 => __( 'No', 'wc-alp' ),
					1 => __( 'Yes', 'wc-alp' ),
				)
			),
			'max_products' => array(
				'title'           => __( 'Max products to display', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_number' ),
				'metabox_id'      => 'wcdrp-appearance',
				'default'         => 5,

			),
			'sort_by'      => array(
				'title'           => __( 'Sort products by', 'wc-alp' ),
				'description'     => '',
				'render_callback' => array( $this->template, 'render_admin_select' ),
				'metabox_id'      => 'wcdrp-appearance',
				'default'         => 1,
				'options'         => array(
					1 => __( 'Random', 'wc-alp' ),
					2 => __( 'Bestsellers', 'wc-alp' ),
					3 => __( 'Newest', 'wc-alp' ),
					4 => __( 'Price: high to low', 'wc-alp' ),
					5 => __( 'Price: low to high', 'wc-alp' )
				)

			)

		);
	}

	protected function save_custom_fields( $post_id, $new_values ) {
		$custom_fields = array( 'wcdrp-location', 'wcdrp-products' );

		foreach ( $custom_fields as $field ) {
			if ( isset( $new_values['wcdrp_rules'][ $field ] ) ) {
				foreach ( $new_values['wcdrp_rules'][ $field ] as $key => $rule ) {
					if ( isset( $rule['template'] ) && $rule['template'] ) {
						unset( $new_values['wcdrp_rules'][ $field ][ $key ] );
					}
				}
			}
		}

		parent::save_custom_fields( $post_id, $new_values );
	}

}