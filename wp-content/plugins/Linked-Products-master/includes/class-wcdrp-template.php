<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCDRP_Template extends TeamDev_Wp_Template {

	public function render_admin_select() {
		echo $this->render_view( 'select', 'renderer', false );
	}

	public function render_admin_text() {
		echo $this->render_view( 'text', 'renderer', false );
	}

	public function render_admin_number() {
		echo $this->render_view( 'number', 'renderer', false );
	}

	public function render_admin_priority() {
		echo $this->render_view( 'number_priority', 'renderer', false );
	}

	public function render_fields() {
		foreach ( $this->options as $name => $option ) {
			$id              = $this->current_post->ID;
			$option['name']  = sprintf( "%s[%s][%s]", $this->post_type_slug, $option['metabox_id'], $name );
			$option['id']    = sprintf( "%s_%s_%s", $this->post_type_slug, $option['metabox_id'], $name );
			$option['value'] = ( isset( $this->option_values[ $name ] ) ) ? $this->option_values[ $name ] : $option['default'];

			$this->current_option = $option;
			call_user_func( $option['render_callback'] );
		}
	}

	public function get_rule_operators( $type = '' ) {
		$disallowed_operators = array();
		$operators            = array(
			'='        => __( 'is', 'wc-alp' ),
			'!='       => __( 'is not', 'wc-alp' ),
			'>'        => __( 'greater than', 'wc-alp' ),
			'<'        => __( 'less than', 'wc-alp' ),
			'IN'       => __( 'is one of', 'wc-alp' ),
			'NOT IN'   => __( 'is not one of', 'wc-alp' ),
			'LIKE'     => __( 'contains', 'wc-alp' ),
			'NOT LIKE' => __( 'does not contain', 'wc-alp' )
		);

		if ( $type ) {
			switch ( $type ) {
				case 'name':
				case 'product_cat':
				case 'product_tag':
					$disallowed_operators = array( '=', '!=', '<', '>', 'LIKE', 'NOT LIKE' );
					break;
				case '_weight':
				case '_price':
					$disallowed_operators = array( 'IN', 'NOT IN', 'LIKE', 'NOT LIKE' );
					break;
				case '_sku':
					$disallowed_operators = array( '<', '>', 'IN', 'NOT IN' );
					break;
				case '_stock_status':
					$disallowed_operators = array( '>', '<', 'IN', 'NOT IN', 'LIKE', 'NOT LIKE' );
					break;

			}
		}

		if ( count( $disallowed_operators ) ) {
			foreach ( $disallowed_operators as $operator ) {
				unset( $operators[ $operator ] );
			}
		}

		return $operators;
	}

	public function get_product_atts() {
		return array( // todo add product tags and attributes
			'name'          => __( 'Products', 'wc-alp' ),
			'product_cat'   => __( 'Categories', 'wc-alp' ),
			'product_tag'   => __( 'Tags', 'wc-alp' ),
			'_price'        => __( 'Price', 'wc-alp' ),
			'_sku'          => __( 'SKU', 'wc-alp' ),
			'_stock_status' => __( 'Stock status', 'wc-alp' ),
			'_weight'       => __( 'Weight', 'wc-alp' )
		);
	}

	protected function init_hooks() {
		add_action( 'wp_ajax_wcdrp_additional_settings', array( $this, 'get_additional_settings' ) );
		if ( WC_DRP()->is_request( 'frontend' ) ) {
			add_action( 'wp', array( $this, 'add_linked_products' ) );
		}
		add_action( 'wcdrp_before_display_linked_products', array( $this, 'before_display_linked_products' ) );
		add_action( 'wcdrp_after_display_linked_products', array( $this, 'after_display_linked_products' ) );
	}

	public function get_block_position( $value, $priority = 10 ) {

		$position = array();
		$hooks    = get_option( 'wcdrp_hooks' );
		switch ( $value ) {
			case '1':
				$position['action'] = 'woocommerce_before_single_product_summary';
				$position['priority'] = $priority;
				break;
			case '2':
				$position['action'] = 'woocommerce_after_single_product_summary';
				$position['priority'] = $priority;
				break;
			case '3':
				if ( ! isset( $hooks['related'] ) ) {
					$hooks['related'] = WC_DRP()->get_wc_filter_data( 'related_products' );
					update_option( 'wcdrp_hooks', $hooks );
				}
				if ( count( $hooks['related'] ) ) {
					remove_action( $hooks['related'][0], $hooks['related'][1], $hooks['related'][2] );
					$position['action']   = $hooks['related'][0];
					$position['priority'] = $hooks['related'][2];
				}
				break;
			case '4':
				if ( ! isset( $hooks['upsell'] ) ) {
					$hooks['upsell'] = WC_DRP()->get_wc_filter_data( 'upsell_display' );
					update_option( 'wcdrp_hooks', $hooks );
				}
				if ( count( $hooks['upsell'] ) ) {
					remove_action( $hooks['upsell'][0], $hooks['upsell'][1], $hooks['upsell'][2] );
					$position['action']   = $hooks['upsell'][0];
					$position['priority'] = $hooks['upsell'][2];
				}
				break;
		}

		return $position;

	}

	public function add_linked_products() {
		if ( ! is_woocommerce() || ! $this->rules ) {
			return '';
		}
		foreach ( $this->rules as $id => $rule ) {
			$position = $this->get_block_position( $rule['options']['wcdrp-general']['position'] );
			if ( count( $position ) ) {
				add_action( $position['action'], array( $this, 'display_products' ), $position['priority'] );
			}
		}
	}

	public function display_products() {
		global $wp_current_filter;
		$rules = $this->rules;
		foreach ( $rules as $id => $rule ) {
			$position = $this->get_block_position( $rule['options']['wcdrp-general']['position'] );
			if ( ! isset( $position['action'] ) || current_action() !== $position['action'] ) {
				continue;
			}
			$products = $rule['products'];
			if ( $products ) {
				$this->rule_id      = $id;
				$this->options      = $rule['options'];
				$this->products     = $rule['products'];
				$this->template_dir = $this->post_type->get_template_path( 'templates' );
				echo $this->render_view( 'linked-products', '', false );
			}
			unset( $rules[ $id ] );
		}
		$this->rules = $rules;
	}

	public function get_all_product_list() {
		$result = array( '' => '' );

		$posts_raw = get_posts( array(
			'posts_per_page' => - 1,
			'post_type'      => 'product',
			'post_status'    => array( 'publish', 'pending', 'draft', 'future', 'private', 'inherit' ),
			'fields'         => 'ids',
		) );

		foreach ( $posts_raw as $post_id ) {
			$result[ $post_id ] = '#' . $post_id . ' ' . get_the_title( $post_id );
		}

		return $result;
	}

	public function get_taxonomy_terms( $taxonomy ) {
		$result = array( '' => '' );

		$post_categories_raw       = get_terms( array( $taxonomy ), array( 'hide_empty' => 0 ) );
		$post_categories_raw_count = count( $post_categories_raw );

		foreach ( $post_categories_raw as $post_cat_key => $post_cat ) {
			$category_name = $post_cat->name;

			if ( $post_cat->parent ) {
				$parent_id  = $post_cat->parent;
				$has_parent = true;

				$found = false;
				$i     = 0;

				while ( $has_parent && ( $i < $post_categories_raw_count || $found ) ) {

					// Reset each time
					$found = false;
					$i     = 0;

					foreach ( $post_categories_raw as $parent_post_cat_key => $parent_post_cat ) {

						$i ++;

						if ( $parent_post_cat->term_id == $parent_id ) {
							$category_name = $parent_post_cat->name . ' &rarr; ' . $category_name;
							$found         = true;

							if ( $parent_post_cat->parent ) {
								$parent_id = $parent_post_cat->parent;
							} else {
								$has_parent = false;
							}

							break;
						}
					}
				}
			}

			$result[ $post_cat->term_id ] = $category_name;
		}

		return $result;
	}

	public function get_param_values_view( $param ) {
		$view = '';
		switch ( $param ) {
			case 'name':
				$this->collection = $this->get_all_product_list();
				$view             = $this->render_view( 'chooser', 'renderer', false );
				break;
			case 'product_cat':
				$this->collection = $this->get_taxonomy_terms( 'product_cat' );
				$view             = $this->render_view( 'chooser-cat', 'renderer', false );
				break;
			case 'product_tag':
				$this->collection = $this->get_taxonomy_terms( 'product_tag' );
				$view             = $this->render_view( 'chooser-cat', 'renderer', false );
				break;
			case '_weight':
			case '_price':
				$view = $this->render_view( 'number-values', 'renderer', false );
				break;
			case '_sku':
				$view = $this->render_view( 'text-values', 'renderer', false );
				break;
			case '_stock_status':
				$view = $this->render_view( 'stockstatus-values', 'renderer', false );
				break;
		}

		return $view;

	}

	public function get_additional_settings() {
		if ( isset( $_REQUEST['name'] ) && false === empty( $_REQUEST['name'] ) ) {
			?>

			<select name="<?php echo str_replace( 'param', 'operator', $_REQUEST['field_name'] ) ?>"
			        class="rule-operator" required>
				<?php foreach ( $this->get_rule_operators( $_REQUEST['name'] ) as $value => $label ): ?>
					<option value="<?php echo $value ?>"><?php echo $label ?></option>
				<?php endforeach; ?>
			</select>

			<?php
			echo $this->get_param_values_view( $_REQUEST['name'] );
			exit();
		}
	}

	public function get_metabox_field_name() {
		return ( isset( $_REQUEST['field_name'] ) ) ? str_replace( 'param', 'value', $_REQUEST['field_name'] ) : 'value';
	}

	//Apply display options
	public function before_display_linked_products() {
		if ( ! $this->options['wcdrp-appearance']['add_to_cart'] ) {
			$status = remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
		}

		if ( $this->is_slider ) {
			add_filter( 'post_class', array( $this, 'update_classes' ), 999 );
		}

		if ( isset ( $this->options['wcdrp-appearance']['ratings'] ) && ! $this->options['wcdrp-appearance']['ratings'] ) {
			$status = remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
		}

		if ( isset( $this->options['wcdrp-appearance']['price'] ) && ! $this->options['wcdrp-appearance']['price'] ) {
			add_filter( 'woocommerce_get_price_html', '__return_empty_string' );
			add_filter( 'woocommerce_get_variation_price_html', '__return_empty_string' );
		}
	}

	//Disable display options
	public function after_display_linked_products() {

		if ( ! has_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' ) ) {
			add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
		}

		if ( ! has_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating' ) ) {
			add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
		}

		$status = remove_filter( 'post_class', array( $this, 'update_classes' ), 999 );

		remove_filter( 'woocommerce_get_price_html', '__return_empty_string' );
		remove_filter( 'woocommerce_get_variation_price_html', '__return_empty_string' );

	}

	public function update_classes( $classes ) {
		$classes[] = 'wcdrp-product';

		return $classes;
	}

}