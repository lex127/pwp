<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCDRP_Shortcode {

	/**
	 * @var WCDRP_Template
	 */
	protected $template;

	/**
	 * @var WCDRP_Post_type
	 */
	protected $post_type;

	public function __construct( $post_type ) {
		$this->post_type = $post_type;
		$this->template = $this->post_type->get_template();
		add_shortcode( 'linked-products', array( $this, 'linked_products' ) );
	}

	// [linked-products id=1234]
	public function linked_products( $atts ) {

		$rule_id = isset( $atts['id'] ) ? (int)$atts['id'] : '';

		if ( ! $rule_id ) {
			return '';
		}

		$this->template->options  = current( get_post_meta( $rule_id, 'wcdrp_rules' ) );
		$this->template->products = $this->post_type->get_linked_products( $this->template->options );
		$this->template->path     =   $this->post_type->get_template_path( 'templates' );

		return $this->template->render_view( 'linked-products', '', false, $this->template->path );
	}

}