### WooCommerce Linked Products
Create rules for assigning linked products automatically
## Features:
- Dynamic rules for linked products
- Custom linked product block with title, and dynamic rules
- Custom appearance for linked blocks
- Separate blocks for category, and product pages
- Ability to show several blocks on one page
- Sort blocks order by priority
- Ability to sort products in block
- Ability to display out-of-stock products in block
- Four predefined positions: 
    - before content
    - instead of related products
    - Instead of up-sell products
    - After content
- Flexible positioning - you can insert related products block as shortcode
