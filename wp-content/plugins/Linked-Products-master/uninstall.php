<?php
/*
* Plugin uninstall
*/

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

if ( ! current_user_can( 'activate_plugins' ) ) {
	exit;
}

// here we go

global $wpdb;

$post_type = 'wcdrp_rules';
$prefix = WC_DRP()::PREFIX;

// Delete posts + data
$wpdb->query( "DELETE FROM {$wpdb->posts} WHERE post_type IN ( '$post_type' );" );
$wpdb->query( "DELETE meta FROM {$wpdb->postmeta} meta LEFT JOIN {$wpdb->posts} posts ON posts.ID = meta.post_id WHERE posts.ID IS NULL;" );
$wpdb->query( "DELETE FROM $wpdb->options WHERE option_name LIKE '{$prefix}%';" );