<?php
/**
 * @var $this WCDRP_Template
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $woocommerce_loop;
$woocommerce_loop['loop'] = 0;

$options         = $this->options;
$products        = $this->products;
$this->is_slider = ( '1' == $options['wcdrp-appearance']['block_layout'] ) ? 1 : 0;
$id              = "wcdrp-{$this->rule_id}";

do_action( 'wcdrp_before_display_linked_products' );
if ( $products->have_posts() ) : ?>
	<div class="products">
		<h2><?php echo $options['wcdrp-appearance']['block_title'] ?></h2>
		<ul id="<?php echo $id ?>"
		    class="products <?php if ( $this->is_slider ): echo 'owl-carousel owl-theme'; endif; ?>">
			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
				<?php wc_get_template_part( 'content', 'product' ); ?>
			<?php endwhile; ?>
		</ul>
	</div>
<?php endif;
do_action( 'wcdrp_after_display_linked_products' );
wp_reset_postdata();