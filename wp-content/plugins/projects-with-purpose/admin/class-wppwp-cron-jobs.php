<?php

class WPPWP_Cron_Jobs {

	protected static $count_expired_campaings = 0;
	protected static $count_disabled_campaings = 0;
	protected static $count_active_campaings = 0;
	protected $wp_filesystem;


	public function __construct() {
		if ( ! wp_next_scheduled( 'dailyops' ) ) {
			wp_schedule_event( time(), 'twicedaily', 'dailyops' );
		}
		add_action( 'dailyops', array( $this, 'get_all_projects' ) );
	}

	public function get_all_projects() {
		$path         = dirname( __FILE__ ) . '/log.txt';
		$agent        = $_SERVER['HTTP_USER_AGENT'];
		$time         = date( 'Y-m-d H:i:s', time() );
		$memory_usege = memory_get_peak_usage();
		$mystring     = $time . ' ' . $agent . PHP_EOL . 'memory:' . $memory_usege . " Peak mem. usage: <b>" . round( $memory_usege / 1024 / 10124, 2 ) . "</b> MB";
		if ( ( $h = fopen( $path, "w" ) ) !== false ) {
			$mystring .= '  End /n';
			fwrite( $h, $mystring );

		}
		$last_error = error_get_last();

		if ( $last_error ) {
			$error = implode( ' ', $last_error );
			error_log( $error, 3, $path );
		}

		$first_page = 1;

		$page = ! empty( $first_page ) ? $first_page : 1;

		$options_limit = 100;

		$limit = ! empty( $options_limit ) ? $options_limit : 50;

		$url = 'https://everydayhero.com/api/v2/campaigns?limit=' . $limit . '&page=' . $page;

		$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );

		if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			$respons = json_decode( $request['body'] );

			if ( ! empty( get_option( 'wppwp_download_time_first_page' ) ) ) {
				update_option( 'wppwp_download_time_first_page', $time );
			} else {
				$deprecated = '';
				$autoload   = 'no';
				add_option( 'wppwp_download_time_first_page', $time, $deprecated, $autoload );
			}

			$total_pages     = $respons->meta->total_pages;
			$last_page_admin = $total_pages;
			$last_page       = ! empty( $last_page_admin ) ? $last_page_admin : $total_pages;

			for ( $i = $page; $i ++ <= $last_page; ) {
				$this->write_all_company_to_DB( $limit, $page ++, $h );
			}
		} else {
			$mystring = 'is wp error: ' . is_wp_error( $request );
			$mystring .= 'is wp_remote_retrieve_response_code' . wp_remote_retrieve_response_code( $request );
			fwrite( $h, $mystring );
		}


		$return = [
			'last_update' => current_time( 'mysql' ),

		];

		if ( ! empty( get_option( 'wppwp_global_last_update' ) ) ) {
			update_option( 'wppwp_global_last_update', current_time( 'mysql' ) );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_last_update', current_time( 'mysql' ), $deprecated, $autoload );
		}


		if ( ! empty( get_option( 'wppwp_global_expired_campaings' ) ) ) {
			update_option( 'wppwp_global_expired_campaings', $this::$count_expired_campaings );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_expired_campaings', $this::$count_expired_campaings, $deprecated, $autoload );
		}

		if ( ! empty( get_option( 'wppwp_global_count_disabled_campaings' ) ) ) {
			update_option( 'wppwp_global_count_disabled_campaings', $this::$count_disabled_campaings );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_count_disabled_campaings', $this::$count_disabled_campaings, $deprecated, $autoload );
		}

		if ( ! empty( get_option( 'wppwp_global_active_campaings' ) ) ) {
			update_option( 'wppwp_global_active_campaings', $this::$count_active_campaings );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_active_campaings', $this::$count_active_campaings, $deprecated, $autoload );
		}

		fwrite( $h, $mystring );

		fclose( $h );
		unlink( $path );

	}

	public function write_all_company_to_DB( $limit, $page, $h ) {
		global $wpdb;
		$agent        = $_SERVER['HTTP_USER_AGENT'];
		$time         = date( 'Y-m-d H:i:s', time() );
		$memory_usege = memory_get_peak_usage();
		$mystring     = 'function write_all_company_to_DB: ' . $time . ' ' . $agent . PHP_EOL . 'memory:' . $memory_usege . " Peak mem. usage: <b>" . round( $memory_usege / 1024 / 10124, 2 ) . "</b> MB";
		static $count_cycle = 1;
		static $added_compaings = 0;
		$user_id = get_current_user_id();
		$url     = 'https://everydayhero.com/api/v2/campaigns?limit=' . $limit . '&page=' . $page;

		$option_name = 'wppwp_last_update_page';
		if ( ! empty( get_option( 'wppwp_last_update_page' ) ) ) {
			update_option( 'wppwp_last_update_page', $page );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_last_update_page', $page, $deprecated, $autoload );
		}

		$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );
		if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			$campanys = json_decode( $request['body'] );

			foreach ( $campanys->campaigns as $campany ) {
				$post_data = null;
				$status    = null;
				$api_ID    = null;

				$json_campany      = serialize( $campany );
				$post_title        = $campany->name;
				$post_name         = $campany->slug;
				$display_start_at  = $campany->display_start_at;
				$display_finish_at = $campany->display_finish_at;
				$start_at          = $campany->start_at;
				$start_date        = new DateTime( $start_at );
				$finish_at         = $campany->finish_at;
				$finish_date       = new DateTime( $finish_at );
				(string) $api_ID = $campany->id;
				$ID = preg_replace( '/[^0-9]/', '', $campany->id );

				$country_code = $campany->country_code;
				$banner_url   = $campany->banner_url;
				//	$status       = $campany->status;
				$donate_url   = ! empty( $campany->donate_url ) ? $campany->donate_url : '';
				$funds_raised = $campany->funds_raised;
				$banner_url   = $campany->banner_url;
				$post_content = ! empty( $campany->description ) ? $campany->description : '';


				if ( strcasecmp( $campany->status, 'expired' ) == 0 ) {
					$this::$count_expired_campaings ++;
					$status = 'expired';
				}

				if ( strcasecmp( $campany->status, 'disabled' ) == 0 ) {
					$this::$count_disabled_campaings ++;
					$status = 'disabled';
				}

				if ( strcasecmp( $campany->status, 'active' ) == 0 ) {
					$this::$count_active_campaings ++;
					$status = 'active';
				}
				$values  = $wpdb->get_results( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key ='%s' AND meta_value = '%s'", '_wppwp_api_id', $api_ID ) );
				$post_id = ! empty( $values[0]->post_id ) ? $values[0]->post_id : false;

				if ( $post_id ) {
					$curent_post_status = get_post_status( $post_id );
				}
				if ( ( $post_id !== false ) && ! ( strcasecmp( $status, 'active' ) === 0 ) ) {

					$post_data = [
						'post_title'   => $post_title,
						'post_content' => get_post_field('post_content', $post_id),
						'post_type'    => 'wppwp_projects',
						'post_name'    => $post_name,
						'ID'           => $post_id,
						'post_status'  => $curent_post_status,

					];

				}


				if ( ( $post_id !== false ) && ( strcasecmp( $status, 'active' ) == 0 ) ) {

					$post_data = [
						'post_title'   => $post_title,
						'post_content' => get_post_field('post_content', $post_id),
						'post_type'    => 'wppwp_projects',
						'post_name'    => $post_name,
						'ID'           => $post_id,
						'post_status'  => $curent_post_status,

					];

				}

				if ( ( $post_id === false ) && strcasecmp( $status, 'active' ) == 0 ) {
					$post_data = [
						'post_title'   => $post_title,
						'post_content' => $post_content,
						'post_type'    => 'wppwp_projects',
						'post_name'    => $post_name,
						'post_status'  => 'draft',
					];

				}


				if ( ! is_null( $post_data ) ) {

					$page_id = wp_insert_post( $post_data );

					if ( ! empty( $page_id ) ) {

						update_post_meta( $page_id, '_wppwp_json_campany', $json_campany );
						update_post_meta( $page_id, '_wppwp_api_id', $api_ID );

						update_post_meta( $page_id, '_wppwp_display_start_at', $display_start_at );

						update_post_meta( $page_id, '_wppwp_id', $ID );

						update_post_meta( $page_id, '_wppwp_display_finish_at', $display_finish_at );

						update_post_meta( $page_id, '_wppwp_start_at', $start_at );
						update_post_meta( $page_id, '_wppwp_start_at_formated', $start_date->format( "Y-m-d" ) );

						update_post_meta( $page_id, '_wppwp_finish_at', $finish_at );
						update_post_meta( $page_id, '_wppwp_finish_at_formated', $finish_date->format( "Y-m-d" ) );

						update_post_meta( $page_id, '_wppwp_country_code', $country_code );

						update_post_meta( $page_id, '_wppwp_banner_url', $banner_url );

						update_post_meta( $page_id, '_wppwp_status', $status );

						update_post_meta( $page_id, '_wppwp_donate_url', $donate_url );

						update_post_meta( $page_id, '_wppwp_funds_raised', $funds_raised );

						update_post_meta( $page_id, '_wppwp_banner_url', $banner_url );
						$blogtime = current_time( 'mysql' );
						update_post_meta( $page_id, '_wppwp_last_time_update', $blogtime );

					}

				}

				$added_compaings ++;

				if ( ! empty( get_option( 'wppwp_count_added_compaigns' ) ) ) {
					update_option( 'wppwp_count_added_compaigns', $added_compaings );
				} else {
					$deprecated = '';
					$autoload   = 'no';
					add_option( 'wppwp_count_added_compaigns', $added_compaings, $deprecated, $autoload );
				}

			}


			if ( $h ) {
				$mystring .= "  End Cycle $count_cycle /n";
				fwrite( $h, $mystring );
				$count_cycle ++;
			}

			if ( ! empty( get_option( 'wppwp_download_time_last_page' ) ) ) {
				update_option( 'wppwp_download_time_last_page', $time );
			} else {
				$deprecated = '';
				$autoload   = 'no';
				add_option( 'wppwp_download_time_last_page', $time, $deprecated, $autoload );
			}
		}
	}
}

