<?php
/**
 * Project settings
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPPWP_Project_Setting {

	protected static $count_expired_campaings = 0;
	protected static $count_disabled_campaings = 0;
	protected static $count_active_campaings = 0;
	protected $wp_filesystem;


	public function __construct() {
		add_action( 'admin_head-edit.php', array( $this, 'addCustomImportButton' ) );
		add_action( 'admin_menu', array( $this, 'add_to_cpt_menu' ) );
		add_action( 'wp_ajax_wppwp_get_all_company_callback', array( $this, 'wppwp_get_all_company_callback' ) );
		add_action( 'wp_ajax_wppwp_get_all_company_callback', array( $this, 'wppwp_get_all_company_callback' ) );
		//SaveFirstPage
		add_action( 'wp_ajax_wppwp_save_first_page_callback', array( $this, 'wppwp_save_first_page_callback' ) );
		add_action( 'wp_ajax_wppwp_save_first_page_callback', array( $this, 'wppwp_save_first_page_callback' ) );
		//SaveLimit Company on the query
		add_action( 'wp_ajax_wppwp_wsave_limit_count_callback', array( $this, 'wppwp_wsave_limit_count_callback' ) );
		add_action( 'wp_ajax_wppwp_wsave_limit_count_callback', array( $this, 'wppwp_wsave_limit_count_callback' ) );
		//DropAllCompanies
		add_action( 'wp_ajax_wppwp_delet_all_companies_callback', array(
			$this,
			'wppwp_delet_all_companies_callback',
		) );
		add_action( 'wp_ajax_wppwp_delet_all_companies_callback', array(
			$this,
			'wppwp_delet_all_companies_callback',
		) );
		//SaveLastPage
		add_action( 'wp_ajax_wppwp_last_page_admin_callback', array( $this, 'wppwp_last_page_admin_callback' ) );
		add_action( 'wp_ajax_wppwp_last_page_admin_callback', array( $this, 'wppwp_last_page_admin_callback' ) );
		//SaveGlobalMeta
		add_action( 'wp_ajax_wppwp_save_project_meta_callback', array( $this, 'wppwp_save_project_meta_callback' ) );
		add_action( 'wp_ajax_wppwp_save_project_meta_callback', array( $this, 'wppwp_save_project_meta_callback' ) );
		//ResetLogFile

		add_action( 'wp_ajax_wppwp_drop_log_file_callback', array( $this, 'wppwp_drop_log_file_callback' ) );
		add_action( 'wp_ajax_wppwp_drop_log_file_callback', array( $this, 'wppwp_drop_log_file_callback' ) );


		add_action( 'admin_footer', array( $this, 'media_selector_print_scripts' ) );
		//todo add method if need
		add_action( 'wp_ajax_wppwp_update_meta_company_callback', array(
			$this,
			'wppwp_update_meta_company_callback',
		) );
		add_action( 'wp_ajax_wppwp_update_meta_company_callback', array(
			$this,
			'wppwp_update_meta_company_callback',
		) );
	}


	public function wppwp_save_first_page_callback() {

		$first_page = $_POST['firstPageNumber'];

		$first_page  = ! empty( $first_page ) ? $first_page : 1;
		$option_name = 'wppwp_first_page';

		if ( get_option( $option_name ) != $first_page ) {
			update_option( $option_name, $first_page );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( $option_name, $first_page, $deprecated, $autoload );
		}
		$return = array(
			'message' => 'Saved',

		);
		wp_die( wp_send_json_success( $return ) );
	}

	public function wppwp_wsave_limit_count_callback() {


		$limit_count = $_POST['firstPageNumber'];
		$limit_count = ! empty( $limit_count ) ? $limit_count : 50;
		$option_name = 'wppwp_limit_company_per_query';

		if ( get_option( $option_name ) != $limit_count ) {
			update_option( $option_name, $limit_count );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( $option_name, $limit_count, $deprecated, $autoload );
		}

		$first_page = get_option( 'wppwp_first_page' );

		$page = ! empty( $first_page ) ? $first_page : 646;

		$limit = get_option( 'wppwp_limit_company_per_query' );

		$url = 'https://everydayhero.com/api/v2/campaigns?limit=' . $limit . '&page=' . $page;

		$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );

		if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			$respons = json_decode( $request['body'] );

			$total_pages = $respons->meta->total_pages;

			if ( get_option( 'wppwp_total_pages_with_current_limit' ) != $total_pages ) {
				update_option( 'wppwp_total_pages_with_current_limit', $total_pages );
			} else {
				$deprecated = '';
				$autoload   = 'no';
				add_option( 'wppwp_total_pages_with_current_limit', $total_pages, $deprecated, $autoload );
			}

		}

		$return = array(
			'message'     => 'Saved',
			'total_pages' => $total_pages,
		);
		wp_die( wp_send_json_success( $return ) );
	}


	public function wppwp_delet_all_companies_callback() {

		global $wpdb;
		$sql = $wpdb->prepare( "DELETE p, pm FROM $wpdb->posts  p, $wpdb->postmeta  pm WHERE p.post_type = 'wppwp_projects' AND p.ID = pm.post_id" );

		$do_query = $wpdb->query( $sql );


		$sql      = $wpdb->prepare( "DELETE  FROM $wpdb->postmeta  WHERE meta_key LIKE '%_wppwp_%'" );
		$do_query .= ' and_options: ' . $wpdb->query( $sql );

		$return = array(
			'delete'           => 1,
			'companies_delete' => $do_query,
		);
		wp_die( wp_send_json_success( $return ) );

	}

	public function wppwp_last_page_admin_callback() {

		$last_page = $_POST['lastPageNumber'];

		$last_page = ! empty( $last_page ) ? $last_page : 0;

		$option_name = 'wppwp_first_page';

		if ( get_option( 'wppwp_last_page_admin' ) != $last_page ) {
			update_option( 'wppwp_last_page_admin', $last_page );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_last_page_admin', $last_page, $deprecated, $autoload );
		}
		$return = array(
			'message' => 'Saved',

		);
		wp_die( wp_send_json_success( $return ) );


	}

	public function wppwp_save_project_meta_callback() {

		$wppwpGlobalHeader       = $_POST['wppwpGlobalHeader'];
		$wppwpGlobalDescriptions = $_POST['wppwpGlobalDescriptions'];
		// if no have image save false and view text "No image" in admin
		$wppwpGlobalimageSRC = ! empty( $_POST['wppwpGlobalimage'] ) ? $_POST['wppwpGlobalimage'] : false;
		$image               = false;
		if ( false !== $wppwpGlobalimageSRC ) {

			$image_attachment_id    = $_POST['image_attachment_id'];
			$wppwpGlobalTextinImage = $_POST['wppwpGlobalTextinImage'];
			$image['src']           = $wppwpGlobalimageSRC;
			$image['ID']            = $image_attachment_id;

		}

		if ( get_option( 'wppwp_global_header_admin' ) != $wppwpGlobalHeader ) {
			update_option( 'wppwp_global_header_admin', $wppwpGlobalHeader );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_header_admin', $wppwpGlobalHeader, $deprecated, $autoload );
		}

		if ( get_option( 'wppwp_global_descriptions_admin' ) != $wppwpGlobalDescriptions ) {
			update_option( 'wppwp_global_descriptions_admin', $wppwpGlobalDescriptions );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_descriptions_admin', $wppwpGlobalDescriptions, $deprecated, $autoload );
		}
		if ( ! empty( $wppwpGlobalimageSRC ) || $wppwpGlobalimageSRC == false ) {
			if ( get_option( 'wppwp_global_image_admin' ) != $image ) {
				update_option( 'wppwp_global_image_admin', $image );
			} else {
				$deprecated = '';
				$autoload   = 'no';
				add_option( 'wppwp_global_image_admin', $image, $deprecated, $autoload );
			}
		}
		if ( get_option( 'wppwp_global_text_in_image' ) != $wppwpGlobalTextinImage ) {
			update_option( 'wppwp_global_text_in_image', $wppwpGlobalTextinImage );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_text_in_image', $wppwpGlobalTextinImage, $deprecated, $autoload );
		}
		$return = array(
			'message' => 'Saved',

		);
		wp_die( wp_send_json_success( $return ) );

	}

	public function wppwp_drop_log_file_callback() {
		$path = dirname( __FILE__ ) . '/log.txt';

		if ( $_POST['drop'] == 1 ) {
			unlink( $path );
		}

		wp_die( wp_send_json_success() );

	}

	public function addCustomImportButton() {
		global $current_screen;

		// Not our post type, exit earlier
		// You can remove this if condition if you don't have any specific post type to restrict to.
		if ( 'wppwp_projects' != $current_screen->post_type ) {
			return;
		}

		?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                jQuery(jQuery(".wrap h2")[0]).append("<a  id='doc_popup' class='add-new-h2'>Import</a>");
            });
        </script>
		<?php
	}

	public function add_to_cpt_menu() {
		add_submenu_page( 'edit.php?post_type=wppwp_projects', 'Custom Post Type Admin', 'Settings', 'edit_posts', 'wp-pwp-submenu-page', array(
			$this,
			'wppwp_submenu_page_callback',
		) );
		add_settings_section( 'section-1', __( 'General settings', 'textdomain' ), array(
			$this,
			'wppwp_section_1_callback',
		), 'wp-pwp-submenu-page' );
	}

	public function wppwp_section_1_callback() {
		ob_start();
		?>
        <label class="pwp-admin-label" for="wppwpSaveFirstPage">Start Page:</label>
        <input type='number' step="1" min="1" name="wppwpSaveFirstPageNumber"
               value="<?php echo get_option( 'wppwp_first_page' ); ?>"/>


		<?php echo ob_get_clean();

	}

	function section_1_callback() {
		_e( 'Some help text regarding Section One goes here.', 'textdomain' );
	}

	function section_2_callback() {
		_e( 'Some help text regarding Section Two goes here.', 'textdomain' );
	}


	public function wppwp_submenu_page_callback() {

		echo '<div class="wrap">';
		echo '<h2>Plugin Settings</h2>'; ?>
        <br>
		<?php
		$path = dirname( __FILE__ ) . '/log.txt';
		if ( ! file_exists( $path ) ) {
			echo '<div class="wppwp-admin-status">STATUS DOWNLOADS: <span class="wppwp_circle_downloads_red"></span></div>'; ?>


            <button id="wppwpGiveAll" type="submit" class="button-primary">Download/Update CAMPAIGNS</button>

		<?php } else {
			echo '<div class="wppwp-admin-status">STATUS DOWNLOADS: <span class="wppwp_circle_downloads_green"></span></div>';
			echo 'CAMPAIGNS are being downloaded at this time, please wait!';
		}
		?>

        <br>    <br>
        <br>

		<?php settings_fields( 'wp-pwp-submenu-page' ); ?>
		<?php do_settings_sections( 'wp-pwp-submenu-page' ); ?>
        <button type="submit" class="button-primary" id="wppwpSaveFirstPage">SAVE!</button>
        <br><br> <label class="pwp-admin-label" for="wppwp_last_page_admin">Last Page:</label>
        <input type='number' step="1" min="0" name="LastPageAdmin"
               value="<?php echo get_option( 'wppwp_last_page_admin' ); ?>"/>
        <button type="submit" class="button-primary" id="LastPageAdmin"><?php _e( 'SAVE!', 'welldone' ); ?></button>
        <code> <?php _e( 'If get all pages - 0', 'welldone' ); ?></code>

        <br><br>
		<?php $ajax_loader = WP_PWP::get_default_thumbail( 3 ); ?>
        <h1 class="wppwpAJAXhiddenElemepnts"
            style="font-weight:bold;z-index:1;display:none;position: absolute;margin: auto;top:35%;left:40%"><span
                    class="wppwpAdminTextSave"></span></h1>
        <img class="wppwpAJAXhiddenElemepnts"
             style="z-index:1; display:none;position: absolute;margin: auto;top:40%;left:45%"
             src="<?php echo $ajax_loader; ?>"/>

        <hr>
        <label class="pwp-admin-label" for="wppwpSaveFirstPage"><?php _e( 'Limit count:', 'welldone' ); ?></label>
        <input type='number' step="1" min="10" name="wppwpLimitCount"
               value="<?php echo get_option( 'wppwp_limit_company_per_query' ); ?>"/>
        <button type="submit" class="button-primary" id="SaveLimitCount"><?php _e( 'SAVE!', 'welldone' ); ?></button>
        <code><?php _e( '- CAMPAIGNS per
            query', 'welldone' ); ?></code>

        <hr>

        <h2>STATUS:</h2>
        <label for="last_update_page">Last Updated Page: </label>
        <input name="last_update_page" disabled value="<?php echo get_option( 'wppwp_last_update_page' ); ?>"/>

        <label for="all_pages">All Pages: </label>
        <input name="all_pages" disabled value="<?php echo get_option( 'wppwp_total_pages_with_current_limit' ); ?>"/>
        <code>- It depends on the limit!</code>
        <br><br>
        <br>
        <p>Last GLOBAL update: <?php echo get_option( ' wppwp_global_last_update' ); ?></p>
        <!--        <p>CAMPAIGNS status active: --><?php //echo get_option( ' wppwp_global_active_campaings' ); ?><!--</p>-->
        <!--        <p>CAMPAIGNS status expired: --><?php //echo get_option( ' wppwp_global_expired_campaings' ); ?><!--</p>-->
        <!--        <p>CAMPAIGNS status disabled: --><?php //echo get_option( ' wppwp_global_count_disabled_campaings' ); ?><!--</p>-->
        <p>Download time of the first page: <?php echo get_option( 'wppwp_download_time_first_page' ); ?></p>
        <p>Download time of the last page: <?php echo get_option( 'wppwp_download_time_last_page' ); ?></p>
        <p>Campaigns Verified: <?php echo get_option( 'wppwp_count_added_compaigns' ); ?></p>

        <p>Current server time: <?php echo date( 'Y-m-d H:i:s', time() ); ?></p>

        <br><br>
        <br>
        <button type="submit" class="button-primary" style="background-color: red" id="DropAllCompany">DROP ALL
            CAMPAIGNS
        </button><br/><br/>
		<?php if ( file_exists( $path ) ): ?>
            <a target="_blank"
               href="<?php echo get_site_url(); ?>/wp-content/plugins/projects-with-purpose/admin/log.txt" type="submit"
               class="button-primary" style="background-color: red">OPEN Log
                File
            </a>
            <br/>        <br/>

            <button type="submit" class="button-primary" style="background-color: red" id="DropLogFile">RESET Log File
            </button> <code> - If Download button does not appear for a long time</code>
		<?php endif; ?>
        <hr>
        <label class="pwp-admin-label" for="wppwpGlobalHeader"><?php _e( 'Global Header:', 'welldone' ); ?> </label>

        <input class="pwp-input-220" name="wppwpGlobalHeader"
               value="<?php echo get_option( 'wppwp_global_header_admin' ); ?>"/><br/><br/>
        <label class="pwp-admin-label"
               for="wppwpGlobalTextinImage"><?php _e( 'Text in image:', 'welldone' ); ?> </label>

        <input class="pwp-input-220" name="wppwpGlobalTextinImage"
               value="<?php echo get_option( 'wppwp_global_text_in_image' ); ?>"/>
		<?php wp_enqueue_media(); ?>
        <br/><br/>
        <div class='image-preview-wrapper'>
			<?php
			$get_image_array = get_option( 'wppwp_global_image_admin' );
			$images          = wp_get_attachment_image_src( $get_image_array['ID'], 'medium' );
			?>
			<?php _e( 'Current image:', 'welldone' ); ?><br/> <?php ///echo $images; ?>

            <img id='image-preview' src='
			<?php echo $images[0]; ?>' width='300' height='160'
                 style='max-height: 100px; width: 300px; '>

        </div>
        <input id="upload_image_button" type="button" class="button" value="<?php _e( 'Upload image' ); ?>"/>
        <input id="delete_logo_button" name="theme_wptuts_options[delete_logo]" type="submit" class="button"
               value="<?php _e( 'Delete Logo', 'wptuts' ); ?>"/>
        <input type='hidden' name='image_attachment_id' id='image_attachment_id'
               value='<?php echo $get_image_array['ID']; ?>'>
        <!--        <br/><br/>Global Descriptions:<br/>-->
        <!--        <textarea name="wppwpGlobalDescriptions" cols="50"-->
        <!--                  rows="4">--><?php //echo get_option( 'wppwp_global_descriptions_admin' ); ?><!--</textarea>-->
        <br/><br/><br/>

        <button type="submit" class="button-primary" id="SaveProjectMeta">SAVE!</button>
		<?php echo '</div>';

	}

	public function media_selector_print_scripts() {

		$my_saved_attachment_post_id = get_option( 'media_selector_attachment_id', 0 );

		?>
        <script type='text/javascript'>
            jQuery(document).ready(function ($) {
                // Uploading files
                var file_frame;
                var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
                var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; // Set this
                jQuery('#upload_image_button').on('click', function (event) {
                    event.preventDefault();
                    // If the media frame already exists, reopen it.
                    if (file_frame) {
                        // Set the post ID to what we want
                        file_frame.uploader.uploader.param('post_id', set_to_post_id);
                        // Open frame
                        file_frame.open();
                        return;
                    } else {
                        // Set the wp.media post id so the uploader grabs the ID we want when initialised
                        wp.media.model.settings.post.id = set_to_post_id;
                    }
                    // Create the media frame.
                    file_frame = wp.media.frames.file_frame = wp.media({
                        title: 'Select a image to upload',
                        button: {
                            text: 'Use this image',
                        },
                        multiple: false	// Set to true to allow multiple files to be selected
                    });
                    // When an image is selected, run a callback.
                    file_frame.on('select', function () {
                        // We set multiple to false so only get one image from the uploader
                        attachment = file_frame.state().get('selection').first().toJSON();
                        // Do something with attachment.id and/or attachment.url here
                        $('#image-preview').attr('src', attachment.url).css('width', 'auto');
                        $('#image_attachment_id').val(attachment.id);
                        // Restore the main post ID
                        wp.media.model.settings.post.id = wp_media_post_id;
                    });
                    // Finally, open the modal
                    file_frame.open();
                });
                // Restore the main ID when the add media button is pressed
                jQuery('a.add_media').on('click', function () {
                    wp.media.model.settings.post.id = wp_media_post_id;
                });
            });
        </script><?php
	}

	public function wppwp_get_all_company_callback() {
		$path         = dirname( __FILE__ ) . '/log.txt';
		$agent        = $_SERVER['HTTP_USER_AGENT'];
		$time         = date( 'Y-m-d H:i:s', time() );
		$memory_usege = memory_get_peak_usage();
		$mystring     = $time . ' ' . $agent . PHP_EOL . 'memory:' . $memory_usege . " Peak mem. usage: <b>" . round( $memory_usege / 1024 / 10124, 2 ) . "</b> MB";
		if ( ( $h = fopen( $path, "w" ) ) !== false ) {
			$mystring .= '  End /n';
			fwrite( $h, $mystring );

		}
		$last_error = error_get_last();

		if ( $last_error ) {
			$error = implode( ' ', $last_error );
			error_log( $error, 3, $path );
		}

		$first_page = get_option( 'wppwp_first_page' );

		$page = ! empty( $first_page ) ? $first_page : 1;

		$options_limit = get_option( 'wppwp_limit_company_per_query' );

		$limit = ! empty( $options_limit ) ? $options_limit : 50;

		$url = 'https://everydayhero.com/api/v2/campaigns?limit=' . $limit . '&page=' . $page;

		$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );

		if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			$respons = json_decode( $request['body'] );

			if ( ! empty( get_option( 'wppwp_download_time_first_page' ) ) ) {
				update_option( 'wppwp_download_time_first_page', $time );
			} else {
				$deprecated = '';
				$autoload   = 'no';
				add_option( 'wppwp_download_time_first_page', $time, $deprecated, $autoload );
			}

			$total_pages     = $respons->meta->total_pages;
			$last_page_admin = get_option( 'wppwp_last_page_admin' );
			$last_page       = ! empty( $last_page_admin ) ? $last_page_admin : $total_pages;

			for ( $i = $page; $i ++ <= $last_page; ) {
				$this->write_all_company_to_DB( $limit, $page ++, $h );
			}
		} else {
			$mystring = 'is wp error: ' . is_wp_error( $request );
			$mystring .= 'is wp_remote_retrieve_response_code' . wp_remote_retrieve_response_code( $request );
			fwrite( $h, $mystring );
		}


		$return = [
			'last_update' => current_time( 'mysql' ),

		];

		if ( ! empty( get_option( 'wppwp_global_last_update' ) ) ) {
			update_option( 'wppwp_global_last_update', current_time( 'mysql' ) );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_last_update', current_time( 'mysql' ), $deprecated, $autoload );
		}


		if ( ! empty( get_option( 'wppwp_global_expired_campaings' ) ) ) {
			update_option( 'wppwp_global_expired_campaings', $this::$count_expired_campaings );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_expired_campaings', $this::$count_expired_campaings, $deprecated, $autoload );
		}

		if ( ! empty( get_option( 'wppwp_global_count_disabled_campaings' ) ) ) {
			update_option( 'wppwp_global_count_disabled_campaings', $this::$count_disabled_campaings );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_count_disabled_campaings', $this::$count_disabled_campaings, $deprecated, $autoload );
		}

		if ( ! empty( get_option( 'wppwp_global_active_campaings' ) ) ) {
			update_option( 'wppwp_global_active_campaings', $this::$count_active_campaings );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_global_active_campaings', $this::$count_active_campaings, $deprecated, $autoload );
		}

		fwrite( $h, $mystring );

		fclose( $h );
		unlink( $path );
		wp_die( wp_send_json_success( $return ) );

	}

	public function write_all_company_to_DB( $limit, $page, $h ) {
		global $wpdb;
		$agent        = $_SERVER['HTTP_USER_AGENT'];
		$time         = date( 'Y-m-d H:i:s', time() );
		$memory_usege = memory_get_peak_usage();
		$mystring     = 'function write_all_company_to_DB: ' . $time . ' ' . $agent . PHP_EOL . 'memory:' . $memory_usege . " Peak mem. usage: <b>" . round( $memory_usege / 1024 / 10124, 2 ) . "</b> MB";
		static $count_cycle = 1;
		static $added_compaings = 0;
		$user_id = get_current_user_id();
		$url     = 'https://everydayhero.com/api/v2/campaigns?limit=' . $limit . '&page=' . $page;

		$option_name = 'wppwp_last_update_page';
		if ( ! empty( get_option( 'wppwp_last_update_page' ) ) ) {
			update_option( 'wppwp_last_update_page', $page );
		} else {
			$deprecated = '';
			$autoload   = 'no';
			add_option( 'wppwp_last_update_page', $page, $deprecated, $autoload );
		}


		$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );
		if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
			$campanys = json_decode( $request['body'] );

			foreach ( $campanys->campaigns as $campany ) {
				$post_data = null;
				$status    = null;
				$api_ID    = null;

				$json_campany      = serialize( $campany );
				$post_title        = $campany->name;
				$post_name         = $campany->slug;
				$display_start_at  = $campany->display_start_at;
				$display_finish_at = $campany->display_finish_at;
				$start_at          = $campany->start_at;
				$start_date        = new DateTime( $start_at );
				$finish_at         = $campany->finish_at;
				$finish_date       = new DateTime( $finish_at );
				(string) $api_ID = $campany->id;
				$ID = preg_replace( '/[^0-9]/', '', $campany->id );

				$country_code = $campany->country_code;
				$banner_url   = $campany->banner_url;
				//	$status       = $campany->status;
				$donate_url   = ! empty( $campany->donate_url ) ? $campany->donate_url : '';
				$funds_raised = $campany->funds_raised;
				$banner_url   = $campany->banner_url;
				$post_content = ! empty( $campany->description ) ? $campany->description : '';


				if ( strcasecmp( $campany->status, 'expired' ) == 0 ) {
					$this::$count_expired_campaings ++;
					$status = 'expired';
				}

				if ( strcasecmp( $campany->status, 'disabled' ) == 0 ) {
					$this::$count_disabled_campaings ++;
					$status = 'disabled';
				}

				if ( strcasecmp( $campany->status, 'active' ) == 0 ) {
					$this::$count_active_campaings ++;
					$status = 'active';
				}
				$values  = $wpdb->get_results( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key ='%s' AND meta_value = '%s'", '_wppwp_api_id', $api_ID ) );
				$post_id = ! empty( $values[0]->post_id ) ? $values[0]->post_id : false;

				if ( $post_id ) {
					$curent_post_status = get_post_status( $post_id );
				}
				if ( ( $post_id !== false ) && ! ( strcasecmp( $status, 'active' ) === 0 ) ) {

				    $post_data = [
						'post_title'   => $post_title,
						'post_content' => get_post_field('post_content', $post_id),
						'post_type'    => 'wppwp_projects',
						'post_name'    => $post_name,
						'ID'           => $post_id,
						'post_status'  => $curent_post_status,

					];

				}


				if ( ( $post_id !== false ) && ( strcasecmp( $status, 'active' ) == 0 ) ) {

					$post_data = [
						'post_title'   => $post_title,
						'post_content' => get_post_field('post_content', $post_id),
						'post_type'    => 'wppwp_projects',
						'post_name'    => $post_name,
						'ID'           => $post_id,
						'post_status'  => $curent_post_status,

					];

				}

				if ( ( $post_id === false ) && strcasecmp( $status, 'active' ) == 0 ) {
					$post_data = [
						'post_title'   => $post_title,
						'post_content' => $post_content,
						'post_type'    => 'wppwp_projects',
						'post_name'    => $post_name,
						'post_status'  => 'draft',
					];

				}


				if ( ! is_null( $post_data ) ) {

					$page_id = wp_insert_post( $post_data );

					if ( ! empty( $page_id ) ) {

						update_post_meta( $page_id, '_wppwp_json_campany', $json_campany );
						update_post_meta( $page_id, '_wppwp_api_id', $api_ID );

						update_post_meta( $page_id, '_wppwp_display_start_at', $display_start_at );

						update_post_meta( $page_id, '_wppwp_id', $ID );

						update_post_meta( $page_id, '_wppwp_display_finish_at', $display_finish_at );

						update_post_meta( $page_id, '_wppwp_start_at', $start_at );
						update_post_meta( $page_id, '_wppwp_start_at_formated', $start_date->format( "Y-m-d" ) );

						update_post_meta( $page_id, '_wppwp_finish_at', $finish_at );
						update_post_meta( $page_id, '_wppwp_finish_at_formated', $finish_date->format( "Y-m-d" ) );

						update_post_meta( $page_id, '_wppwp_country_code', $country_code );

						update_post_meta( $page_id, '_wppwp_banner_url', $banner_url );

						update_post_meta( $page_id, '_wppwp_status', $status );

						update_post_meta( $page_id, '_wppwp_donate_url', $donate_url );

						update_post_meta( $page_id, '_wppwp_funds_raised', $funds_raised );

						update_post_meta( $page_id, '_wppwp_banner_url', $banner_url );
						$blogtime = current_time( 'mysql' );
						update_post_meta( $page_id, '_wppwp_last_time_update', $blogtime );

					}

				}

				$added_compaings ++;

				if ( ! empty( get_option( 'wppwp_count_added_compaigns' ) ) ) {
					update_option( 'wppwp_count_added_compaigns', $added_compaings );
				} else {
					$deprecated = '';
					$autoload   = 'no';
					add_option( 'wppwp_count_added_compaigns', $added_compaings, $deprecated, $autoload );
				}

			}


			if ( $h ) {
				$mystring .= "  End Cycle $count_cycle /n";
				fwrite( $h, $mystring );
				$count_cycle ++;
			}

			if ( ! empty( get_option( 'wppwp_download_time_last_page' ) ) ) {
				update_option( 'wppwp_download_time_last_page', $time );
			} else {
				$deprecated = '';
				$autoload   = 'no';
				add_option( 'wppwp_download_time_last_page', $time, $deprecated, $autoload );
			}
		}

	}


}