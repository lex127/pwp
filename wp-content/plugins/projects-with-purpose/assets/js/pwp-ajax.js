var pwpAJAXFundsRaised = function () {
};
pwpAJAXFundsRaised.prototype = {
    updatePrice: function () {
        jQuery('#founds-raised').addClass('blink_animation');
        jQuery.ajax({
            url: projectPage.ajax_url,
            data: {
                action: 'do_update_funds_raised',
                nonce: projectPage.nonce,
                params: projectPage
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {
                jQuery('#founds-raised span').html(data.content);
                jQuery('#founds-raised').removeClass('blink_animation');
            },
            error: function (MLHttpRequest, textStatus, errorThrown) {
                console.log(textStatus); // TODO: removetextStatus);
            }
        })
    }
};

var pwpAJAXFundsRaisedObject = new pwpAJAXFundsRaised();
pwpAJAXFundsRaisedObject.updatePrice();
