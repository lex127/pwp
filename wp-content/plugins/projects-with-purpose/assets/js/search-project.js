(function ($) {

    $(".form-search-page select ").change(function () {
        var str = "";
        $(".terms_category option:selected").each(function () {
            str += $(this).val();
        });
        $("input[name='category_term_id']").val(str);

    }).trigger("change");

    $(".form-search-page select ").change(function () {
        var str = "";
        $(".terms_charity option:selected").each(function () {
            str += $(this).val();
        });
        $("input[name='charity_term_id']").val(str);

    }).trigger("change");

})(jQuery);