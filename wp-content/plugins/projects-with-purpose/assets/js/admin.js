jQuery(document).ready(function () { // wait for page to finish loading

    jQuery("#wppwpGiveAll").click(function () {
        jQuery("#wppwpGiveAll").prop('disabled', true);
        jQuery(".wppwp-admin-status span").removeClass('wppwp_circle_downloads_red').addClass('wppwp_circle_downloads_green');

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_get_all_company_callback',
                variable: 45 // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {
                jQuery("#wppwpGiveAll").prop('disabled', false);
                jQuery(".wppwp-admin-status span").removeClass('wppwp_circle_downloads_green').addClass('wppwp_circle_downloads_red');
                jQuery(".wppwp-admin-status").append('<p class="wppwp-done-element"><strong>DONE!</strong></p>');
                setTimeout(function () {
                    jQuery(".wppwp-done-element").hide('slow');
                }, 2000);
            }
        });

    });
    jQuery("#delete_logo_button").click(function () {
        jQuery("#image-preview").attr('src', '' );
        jQuery("#image-preview").hide();
    });
    jQuery("#upload_image_button").click(function () {

        jQuery("#image-preview").show();
    });
    jQuery("#wppwpUpdateMeta").click(function () {

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_update_meta_company_callback',
                variable: 45 // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {
                console.log(data);
            }
        });

    });

    jQuery("#wppwp_update_single_company").click(function () {

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_update_single_company_callback',
                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                params: options
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {

            }
        });

    });

    jQuery(document).on("click", "#wppwpSaveFirstPage", function (e) {

        wppwpSaveFirstPageNumber = jQuery('[name=wppwpSaveFirstPageNumber]').val();
        // console.log(wppwpSaveFirstPageNumber);
        //  settings['wppwpSaveFirstPageNumber'] = wppwpSaveFirstPageNumber;

        jQuery('#wpwrap').append('<div class="wppwpOpacityOptions"></div>');
        jQuery('.wppwpAdminTextSave').text('SAVING, WAIT PLEASE!');
        jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'block').css('opacity', '1').css('z-index', '9');

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_save_first_page_callback',
                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                //params: wppwpSaveFirstPageNumber,
                firstPageNumber: wppwpSaveFirstPageNumber
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {
                jQuery('#wpwrap').css('opacity', '1');
                jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'none');
                jQuery(".wppwpOpacityOptions").remove();
            }
        });

    });

    jQuery(document).on("click", "#SaveLimitCount", function (e) {
        wppwpLimitCount = jQuery('[name=wppwpLimitCount]').val();
        console.log(1);
        jQuery('#wpwrap').append('<div class="wppwpOpacityOptions"></div>');
        jQuery('.wppwpAdminTextSave').text('WAIT FOR ANSWER FROM API...');

        jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'block').css('opacity', '1').css('z-index', '9');

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_wsave_limit_count_callback',
                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                //params: wppwpSaveFirstPageNumber,
                firstPageNumber: wppwpLimitCount
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {

                jQuery('#wpwrap').css('opacity', '1');
                jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'none');
                jQuery(".wppwpOpacityOptions").remove();
                jQuery('[name=all_pages]').val(data.data.total_pages)

            }
        });

    });


    jQuery(document).on("click", "#LastPageAdmin", function (e) {
        lastPageNumber = jQuery('[name=LastPageAdmin]').val();
        jQuery('#wpwrap').append('<div class="wppwpOpacityOptions"></div>');
        jQuery('.wppwpAdminTextSave').text('WAIT FOR ANSWER FROM API...');

        jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'block').css('opacity', '1').css('z-index', '9');

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_last_page_admin_callback',
                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                //params: wppwpSaveFirstPageNumber,
                lastPageNumber: lastPageNumber
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {

                jQuery('#wpwrap').css('opacity', '1');
                jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'none');
                jQuery(".wppwpOpacityOptions").remove();
                jQuery('[name=all_pages]').val(data.data.total_pages)

            }
        });

    });

    jQuery(document).on("click", "#SaveProjectMeta", function (e) {
        wppwpGlobalHeader = jQuery('[name=wppwpGlobalHeader]').val();
        wppwpGlobalDescriptions = jQuery('[name=wppwpGlobalDescriptions]').val();
        wppwpGlobalimage = jQuery('#image-preview').attr('src');
        image_attachment_id = jQuery('#image_attachment_id').val();
        wppwpGlobalTextinImage =jQuery('[name=wppwpGlobalTextinImage]').val();
        jQuery('#wpwrap').append('<div class="wppwpOpacityOptions"></div>');
        jQuery('.wppwpAdminTextSave').text('SAVING, WAIT PLEASE...');

        jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'block').css('opacity', '1').css('z-index', '9');

        jQuery.ajax({
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'wppwp_save_project_meta_callback',
                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                //params: wppwpSaveFirstPageNumber,
                wppwpGlobalHeader : wppwpGlobalHeader,
                wppwpGlobalDescriptions : wppwpGlobalDescriptions,
                wppwpGlobalimage : wppwpGlobalimage,
                image_attachment_id : image_attachment_id,
                wppwpGlobalTextinImage : wppwpGlobalTextinImage,
            },
            type: 'post',
            dataType: 'json',
            success: function (data, textStatus, XMLHttpRequest) {
                jQuery('#wpwrap').css('opacity', '1');
                jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'none');
                jQuery(".wppwpOpacityOptions").remove();
                jQuery(".attachment-thumbnail").attr('src', wppwpGlobalimage );
            }
        });

    });


    jQuery(document).on("click", "#DropAllCompany", function (e) {


        jQuery.confirm({
            theme: 'supervan', // 'material', 'bootstrap'
            title: 'DROP ALL COMPANY ?',
            content: 'You are sure that delete all the companies, they can not be returned?',
            confirmButton: 'Yes',
            cancelButton: 'No',
            buttons: {
                dropTable: {
                    text: 'DROP',
                    btnClass: 'btn-red',
                    action: function () {

                        jQuery('#wpwrap').append('<div class="wppwpOpacityOptions"></div>');
                        jQuery('.wppwpAdminTextSave').text('DELETING ALL COMPANIES, WAIT PLEASE!');
                        jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'block').css('opacity', '1').css('z-index', '9');

                        jQuery.ajax({
                            url: "/wp-admin/admin-ajax.php",
                            data: {
                                action: 'wppwp_delet_all_companies_callback',
                                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                                //params: wppwpSaveFirstPageNumber,
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (data, textStatus, XMLHttpRequest) {
                                console.log(data);
                                jQuery('#wpwrap').css('opacity', '1');
                                jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'none');
                                jQuery(".wppwpOpacityOptions").remove();
                                jQuery('[name=all_pages]').val(data.data.total_pages)
                                // jQuery('#DropAllCompany').append('<p>Companies deleted: ' + data.data.companies_delete + '</p>');
                                jQuery('#DropAllCompany').append('<p>Deleted string informations: ' + data.data.companies_delete + '</p>');


                            }
                        });


                    }
                },
                close: function () {

                    console.log('Closed');

                }
            }
        });

    });

    jQuery(document).on("click", "#DropLogFile", function (e) {


        jQuery.confirm({
            theme: 'supervan', // 'material', 'bootstrap'
            title: 'RESET ?',
            content: 'If Download button does not appear for a long time',
            confirmButton: 'Yes',
            cancelButton: 'No',
            buttons: {
                dropTable: {
                    text: 'RESET',
                    btnClass: 'btn-red',
                    action: function () {

                        jQuery('#wpwrap').append('<div class="wppwpOpacityOptions"></div>');
                        jQuery('.wppwpAdminTextSave').text('RESET, WAIT PLEASE!');
                        jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'block').css('opacity', '1').css('z-index', '9');

                        jQuery.ajax({
                            url: "/wp-admin/admin-ajax.php",
                            data: {
                                action: 'wppwp_drop_log_file_callback',
                                variable: 45, // enter in anyname here instead of variable, you will need to catch this value using $_POST['variable'] in your php function.
                               drop:1,
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function (data, textStatus, XMLHttpRequest) {
                                console.log(data);
                                jQuery('#wpwrap').css('opacity', '1');
                                jQuery('.wppwpAJAXhiddenElemepnts').css('display', 'none');
                                jQuery(".wppwpOpacityOptions").remove();
                                // jQuery('#DropAllCompany').append('<p>Companies deleted: ' + data.data.companies_delete + '</p>');
                                location.reload();

                            }
                        });


                    }
                },
                close: function () {

                    console.log('Closed');

                }
            }
        });

    });



});