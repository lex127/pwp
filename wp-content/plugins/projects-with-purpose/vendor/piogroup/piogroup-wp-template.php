<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

abstract class PioGroup_Wp_Template {

	/**
	 * @var wpdb
	 */
	protected $wpdb;

	protected $data = array();

	protected $template_dir;

	public function __construct( $dir ) {
		$this->template_dir = $dir;
		$this->wpdb = $this->get_wpdb();
		$this->init_hooks();
	}

	protected function get_wpdb() {
		return $GLOBALS['wpdb'];
	}

	protected function init_hooks() {}

	public function get_request_var( $var, $default = "" ) {
		return isset( $_REQUEST[ $var ] ) ? sanitize_text_field( $_REQUEST[ $var ] ) : $default;
	}

	public function __get( $name ) {
		if ( array_key_exists( $name, $this->data ) ) {
			return $this->data[ $name ];
		}
	}

	public function __set( $name, $value ) {
		$this->data[ $name ] = $value;
	}

	public function render_view( $slug, $sub_dir ='', $require_once = true, $template_dir = '' ) {
		$tab_data_func_name = str_replace( '-', '_', $slug ) . '_get_data';
		if ( method_exists( $this, $tab_data_func_name ) ) {
			call_user_func( array( $this, $tab_data_func_name ) );
		}
		if ( method_exists( $this, 'localize_script' ) ) {
			add_action( 'admin_footer', array( $this, 'localize_script' ) );
		}
		ob_start();
		$template_dir = ( $template_dir ) ? $template_dir : $this->template_dir;
		$path = $template_dir. DIRECTORY_SEPARATOR. "html-{$slug}.php";
		if( $sub_dir ) {
			$path = $template_dir. DIRECTORY_SEPARATOR. $sub_dir. DIRECTORY_SEPARATOR. "html-{$slug}.php";
		}
		$this->load_template( $path, $require_once );
		return ob_get_clean();
	}

	public function load_template( $_template_file, $require_once = true ) {
		if ( ! file_exists( $_template_file ) ) {
			return '';
		}
		if ( $require_once ) {
			require_once( $_template_file );
		} else {
			require( $_template_file );
		}
	}

}