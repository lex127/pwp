<?php
/**
 * Post Types
 *
 * Registers post types and taxonomies.
 *
 * @class     WCDRP_Post_types
 * @category  Class
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_Post_types Class.
 */
abstract class PioGroup_Wp_CPT {

	/**
	 * @var PioGroup_Wp_Template
	 */
	protected $template;

	protected $post_type_slug;

	public function __construct() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'save_post', array( $this, 'save_post' ) );

	}

	abstract function get_template_path( $template_dir );

	abstract protected function get_post_type_params();

	abstract protected function get_metaboxes_data();

	abstract public function set_template();

	abstract protected function set_post_type_slug();

	abstract protected function get_post_type_options();

	/**
	 * Hook in methods.
	 */
	public function init() {
		$this->set_post_type_slug();
		$this->set_template();
		$this->create_post_type();
	}

	public function create_post_type() {
		if ( ! post_type_exists( $this->get_post_type_slug() ) ) {
			$post_type_params                         = $this->get_post_type_params();
			$post_type_params['register_meta_box_cb'] = array( $this, 'add_meta_boxes' );
			$post_type                                = register_post_type( $this->get_post_type_slug(), $post_type_params );
			if ( is_wp_error( $post_type ) ) {
				echo __METHOD__ . ' error: ' . $post_type->get_error_message(); // todo replace by _doing_it_wrong
			}
		}
	}

	public function get_template() {
		return $this->template;
	}

	public function get_post_type_slug() {
		return $this->post_type_slug;
	}

	public function add_meta_boxes() {
//		foreach ( $this->get_metaboxes_data() as $metabox ) {
//			add_meta_box(
//				$metabox['id'],
//				$metabox['title'],
//				array( $this, 'get_metabox_view' ),
//				$this->get_post_type_slug(),
//				'normal',
//				'default',
//				$metabox['callback_args']
//			);
//		}
	}

	public function get_metabox_view( $post, $metabox ) {
		$template                 = $this->get_template();
		$template->args           = $metabox['args'];
		$template->metabox_id     = $metabox['id'];
		$template->current_post   = $post;
		$template->post_type_slug = $this->get_post_type_slug();
		$template->options        = $this->get_metabox_options( $metabox['id'] );
		$template->option_values  = $this->get_metabox_values( $post->ID, $metabox['id'] );
		echo $template->render_view( $metabox['args']['template'], '', false );
	}

	public function get_post_meta( $post_id ) {
		return get_post_meta( $post_id, $this->get_post_type_slug(), true );
	}

	public function get_metabox_values( $post_id, $metabox_id ) {
		$values = $this->get_post_meta( $post_id );

		return ( isset( $values[ $metabox_id ] ) ) ? $values[ $metabox_id ] : array();
	}

	public function get_metabox_options( $metabox_id ) {
		$options = $this->get_post_type_options();
		foreach ( $options as $name => $data ) {
			if ( isset( $data['metabox_id'] ) && $metabox_id !== $data['metabox_id'] ) {
				unset( $options[ $name ] );
			}
		}

		return $options;
	}

	public function save_post( $post_id ) {
		global $post;
		$ignored_actions = array( 'trash', 'untrash', 'restore' );
		if ( isset( $_GET['action'] ) && in_array( $_GET['action'], $ignored_actions ) ) {
			return;
		}
		if ( ! $post || $post->post_type != $this->get_post_type_slug() || ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
		$this->save_custom_fields( $post_id, $_POST );
	}

	protected function is_allowed( $options ) {
		return true;
	}

	protected function save_custom_fields( $post_id, $new_values ) {
		if ( isset( $new_values[ $this->get_post_type_slug() ] ) ) {
			if ( $this->is_allowed( $new_values[ $this->get_post_type_slug() ] ) ) {
				update_post_meta( $post_id, $this->get_post_type_slug(), $new_values[ $this->get_post_type_slug() ] );

				foreach ( $new_values[ $this->get_post_type_slug() ] as $names => $values ) {
					if ( count( $values ) ) {
						foreach ( $values as $name => $value ) {
							update_post_meta( $post_id, str_replace( '-', '_', $names ) . '_' . $name, $value );
						}
					}
				}

			} else {
				_e( 'Example of failing validation', 'wc-alp' );
			}
		}
	}

}

add_action( 'wp_ajax_do_update_funds_raised', 'update_funds_raised_from_api' );
add_action( 'wp_ajax_nopriv_do_update_funds_raised', 'update_funds_raised_from_api' );

function update_funds_raised_from_api() {

	$wppwp_api_id = $_POST['params']['api-id'];
	$page_id      = $_POST['params']['page-id'];

	$url     = 'https://everydayhero.com/api/v2/campaigns/' . $wppwp_api_id;
	$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );
	if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
		$respons      = json_decode( $request['body'] );
		$funds_raised = $respons->campaign->funds_raised;
		update_post_meta( $page_id, '_wppwp_funds_raised', $funds_raised );

	}
	$res['content'] = $funds_raised->currency->symbol . number_format( $funds_raised->cents / 100, 2, '.', ' ' );
;

	wp_send_json($res);
}