<?php
/**
 * Add Shortcode
 *
 *
 * @class     WPPWP Shortcode
 * @category  Class
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPPWP_Shortcode {

	public function __construct() {

		add_shortcode( 'last_added_campaigns_slider', array( $this, 'last_added_campaigns_slider' ) );

		add_shortcode( 'wppwp_projects_search_page', array( $this, 'wppwp_projects_search_page' ) );
	}

	public function last_added_campaigns_slider( $atts ) {

		$atts = shortcode_atts( array(
			'per_page' => 8,
		), $atts );


		$args      = [
			'posts_per_page' => (int) $atts['per_page'],
			'post_type'      => 'wppwp_projects',
			'post_status'    => 'publish',
			'orderby'        => 'post_date',
			'order'          => 'DESC',
		];
		$loop      = new WP_Query( $args );
		$classes   = array();
		$classes[] = 'welldone_product product product-preview-wrapper';
		$classes[] = '';

		?>
        <section class="content product-ups">
            <div class="container">
                <!-- Modal -->
                <div class="modal quick-view zoom" id="quickView" style=" opacity: 1">
                    <div class="modal-dialog">
                        <div class="modal-content"></div>
                    </div>
                </div>
				<?php
				if ( $loop->have_posts() ) { ?>
                    <h2 class="text-center text-uppercase product-slider-title">FEATURED Campaigns</h2>
                    <div class="row product-carousel mobile-special-arrows animated-arrows product-grid four-in-row">
						<?php
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php $image = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'wppwp-single-project' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'wppwp-single-project' ) : WP_PWP::get_default_thumbail( 2 ); ?>
							<?php

							$donate_url   = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );
							$current_user = wp_get_current_user();
							$donate_url .= "?first_name=$current_user->user_firstname&last_name=$current_user->user_lastname&email=$current_user->user_email";

							?>

                            <div <?php post_class( $classes ); ?>>

                                <div class="product-preview">
                                    <div class="product-preview__image" style="min-height: 300px">
                                        <figure class="shop_catalog owl-carousel product-slider">
                                            <a href="<?php the_permalink(); ?>">
                                                <img height="200" class="product-image" src="<?php echo $image; ?>"/>
                                            </a>
                                        </figure>

                                    </div>
                                    <div class="product-preview__info text-center">
                                        <a href="<?php the_permalink(); ?>">
                                            <div class="product-preview__info__title">
                                                <h2><?php the_title(); ?></h2>
                                            </div>
                                        </a>
                                        <div>
	                                        <?php if ( is_user_logged_in() ): ?>
                                                <a href="<?php echo $donate_url; ?>"
                                                   class="btn btn--wd buy-link donate-button-in-slider"
                                                   target="_blank">Donate Now</a>
	                                        <?php else: ?>
                                                <a href="#" link="<?php echo get_the_permalink(); ?>" class="btn btn--wd buy-link donate-button-global popmake-register"
                                                   title="<?php echo get_the_title(); ?>" style="padding: 10px 10px;">Donate now</a>
	                                        <?php endif; ?>


                                        </div>
                                    </div>

                                </div>

                            </div>

						<?php endwhile; ?>
                    </div>
				<?php } ?>
            </div>
        </section>

		<?php


	}

	public function wppwp_projects_search_page( $atts ) { ?>
		<?php

		include WP_PWP::get_path() . '/templates/search/search-projects.php';
	}

}