<?php
/**
 * Post Types
 *
 * Registers post types and taxonomies.
 *
 * @class     WPPWP_Post_types
 * @category  Class
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPPWP_Post_type extends PioGroup_Wp_CPT {

	const METABOX_VIEW_DIR = 'templates';

	public function __construct() {
		parent::__construct();
		if ( ! is_admin() || defined( 'DOING_AJAX' ) ) {
			//	add_action( 'wp', array( $this, 'setup_linked_products' ) );
		}

		add_action( 'add_meta_boxes', array( $this, 'wppwp_add_meta_box' ) );
		add_action( 'save_post', array( $this, 'wpse_save_meta_fields' ) );
		add_action( 'new_to_publish', array( $this, 'wpse_save_meta_fields' ) );
		//todo refactoring

		add_action( 'projects_category_add_form_fields', array( $this, 'projects_category_add_meta_fields' ), 10, 2 );

		add_action( 'projects_category_edit_form_fields', array( $this, 'projects_category_edit_meta_fields' ), 10, 2 );

		add_action( 'created_projects_category', array( $this, 'projects_category_save_taxonomy_meta' ), 10, 2 );

		add_action( 'edited_projects_category', array( $this, 'projects_category_save_taxonomy_meta' ), 10, 2 );

		add_action( 'projects_charity_add_form_fields', array( $this, 'projects_charity_add_meta_fields' ), 10, 2 );

		add_action( 'projects_charity_edit_form_fields', array( $this, 'projects_charity_edit_meta_fields' ), 10, 2 );

		add_action( 'created_projects_charity', array( $this, 'projects_charity_save_taxonomy_meta' ), 10, 2 );

		add_action( 'edited_projects_charity', array( $this, 'projects_charity_save_taxonomy_meta' ), 10, 2 );

		add_action( 'init', array( $this, 'project_type_settings' ), 10, 2 );

	}

	public function init() {

		parent::init();
	}

	public function project_type_settings() {


		$labels = array(
			'name'               => __( 'Projects Categories' ), //Categories of programs
			'singular_name'      => __( 'Category' ),
			'add_new'            => __( 'Add New' ),
			'add_new_item'       => __( 'Add New Category' ),
			'edit_item'          => __( 'Edit Category' ),
			'new_item'           => __( 'New Category' ),
			'all_items'          => __( 'All Categories' ),
			'view_item'          => __( 'View Category' ),
			'search_items'       => __( 'Search Category' ),
			'not_found'          => __( 'No Categories Found' ),
			'not_found_in_trash' => __( 'No Categories Found in Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Categories', 'blackman' ),
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => _x( 'projects_category', 'URL slug' ), 'with_front' => false ),
			'has_archive'        => true,
			'hierarchical'       => true,
			'supports'           => array( 'title', 'editor' ),
		);

		register_taxonomy( 'projects_category', 'wppwp_projects', $args );

		$labels = array(
			'name'               => __( 'Charities' ), //Categories of programs
			'singular_name'      => __( 'Charity' ),
			'add_new'            => __( 'Add New' ),
			'add_new_item'       => __( 'Add New Charity' ),
			'edit_item'          => __( 'Edit Charity' ),
			'new_item'           => __( 'New Charity' ),
			'all_items'          => __( 'All Charities' ),
			'view_item'          => __( 'View Category' ),
			'search_items'       => __( 'Search Category' ),
			'not_found'          => __( 'No Categories Found' ),
			'not_found_in_trash' => __( 'No Categories Found in Trash' ),
			'parent_item_colon'  => '',
			'menu_name'          => __( 'Charities', 'blackman' ),
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => _x( 'charities', 'URL slug' ), 'with_front' => false ),
			'has_archive'        => true,
			'hierarchical'       => true,
			'supports'           => array( 'title', 'editor' ),
		);

		register_taxonomy( 'projects_charity', 'wppwp_projects', $args );
	}

	public function wppwp_add_meta_box() {
		add_meta_box(
			'custom_meta_box-2'
			, __( 'Campaign Information', 'myplugin_textdomain' )
			, array( $this, 'wppwp_show_custom_meta_box' )
			, 'wppwp_projects'
			, 'normal'
			, 'high'
		);
	}

	//showing custom form fields
	public function wppwp_show_custom_meta_box() {
		global $post;

		$custom_title        = get_post_meta( get_the_ID(), '_wppwp_custom_title', true );
		$custom_descriptions = get_post_meta( get_the_ID(), '_wppwp_custom_descriptions', true );
		$wppwp_start         = get_post_meta( get_the_ID(), '_wppwp_start_at', true );
		$start_date          = new DateTime( $wppwp_start );
		$wppwp_finish        = get_post_meta( get_the_ID(), '_wppwp_finish_at', true );
		$finish_date         = new DateTime( $wppwp_finish );
		$funds_raised        = get_post_meta( get_the_ID(), '_wppwp_funds_raised', true );
		$goal                = get_post_meta( get_the_ID(), '_wppwp_goal', true );
		$status              = get_post_meta( get_the_ID(), '_wppwp_status', true );
		$donate_url          = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );
		$blog_time           = get_post_meta( get_the_ID(), '_wppwp_last_time_update', true );
		$api_id              = get_post_meta( get_the_ID(), '_wppwp_api_id', true );
		$fb_page             = get_post_meta( get_the_ID(), '_wppwp_facebook_page', true );
		$lin_page            = get_post_meta( get_the_ID(), '_wppwp_linkdin_page', true );


		wp_nonce_field( basename( __FILE__ ), 'wpse_our_nonce' );
		?>
        <!-- my custom value input -->
        <label for="wppwp_start">Title: </label>
        <input type="text" name="_wppwp_custom_title" value="<?php echo $custom_title; ?>">

        <!--    <label for="wppwp_start">Custom DESC: </label>-->
        <!--    <input type="text" name="_wppwp_custom_descriptions" value="--><?php //echo $custom_descriptions; ?><!--">-->

        <label for="wppwp_start">Start: </label>
        <input disabled type="date" name="_wppwp_start" value="<?php echo $start_date->format( "Y-m-d" ); ?>">

        <label for="wppwp_finish">Finish: </label>
        <input disabled type="date" name="_wppwp_finish" value="<?php echo $finish_date->format( "Y-m-d" ); ?>">

        <label for="wppwp_fund_raised"> Fund Raised: </label>
        <input disabled type="number" min="0.00" step="0.01" name="_wppwp_fund_raised"
               value="<?php echo number_format( $funds_raised->cents / 100, 2, '.', ' ' ); ?>">

        <label for="wppwp_goal">Goal: </label>
        <input type="number" min="0.00" step="0.01" name="_wppwp_goal" value="<?php echo $goal; ?>"
               id="wppwp_goal">

        </br>
        <hr>
        <label for="wppwp_status">Status:
            <code><?php echo $status; ?></code></label>
        </br>
        <hr>

        <label for="wppwp_donate_url">Donate URL:
            <code type="text" name="_wppwp_donate_url" value=""
                  id="wppwp_donate_url"><?php echo $donate_url; ?></code></label></br>
        <hr>
        <label for="wppwp_facebook_page">Facebook Page:
            <input type="text" name="_wppwp_facebook_page" value="<?php echo $fb_page; ?>"
                   id="wppwp_facebook_page"></input></label><code>- without "http://"</code></br>
        <hr>
        <label for="wppwp_linkdin_page">LinkedIn Page:
            <input type="text" name="_wppwp_linkdin_page" value="<?php echo $lin_page; ?>"
                   id="wppwp_linkdin_page"></input></label><code>- without "http://"</code></br>
        <hr>
        <p><b> Money INFO: </b></p>
        <p>Name: <code><?php echo $funds_raised->currency->name; ?></code></p>
        <p>ISO code: <code><?php echo $funds_raised->currency->iso_code; ?></code></p>
        <p>Symbol: <code><?php echo $funds_raised->currency->symbol; ?></code></p>


        </br>
        <input id="wppwp_update_single_company" type="submit" name="wppwp_update_single_company"
               class="button button-primary button-large"
               value="UPDATE COMPANY"/>
        <hr>
        <label for="last_update">Last Update Time:
            <code><?php echo $blog_time; ?></code></label>
        <label for="last_update">api id:
            <code><?php echo $api_id ?></code></label>
		<?php
	}

	public
	function wpse_save_meta_fields(
		$post_id
	) {

		// verify nonce
		if ( ! isset( $_POST['wpse_our_nonce'] ) || ! wp_verify_nonce( $_POST['wpse_our_nonce'], basename( __FILE__ ) ) ) {
			return 'nonce not verified';
		}
		if ( isset( $_POST['wppwp_update_single_company'] ) ) {

			$get_company_id = get_post_meta( $post_id, '_wppwp_api_id', true );

			$url = 'https://everydayhero.com/api/v2/campaigns/' . $get_company_id;

			$request = wp_remote_get( $url, array( 'timeout' => 120, 'httpversion' => '1.1' ) );

			if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
				$respons = json_decode( $request['body'] );
				$campany = $respons->campaign;

				(int) $current_id = $post_id;

				$post_title        = $campany->name;
				$post_name         = $campany->slug;
				$display_start_at  = $campany->display_start_at;
				$display_finish_at = $campany->display_finish_at;
				$start_at          = $campany->start_at;
				$start_date        = new DateTime( $start_at );
				$finish_at         = $campany->finish_at;
				$finish_date       = new DateTime( $finish_at );

				$country_code = $campany->country_code;
				$banner_url   = ! empty( $campany->banner_url ) ? $campany->banner_url : '';
				$status       = $campany->status;
				$donate_url   = ! empty( $campany->donate_url ) ? $campany->donate_url : '';
				$funds_raised = ! empty( $campany->funds_raised ) ? $campany->funds_raised : '';
				$blogtime     = current_time( 'mysql' );

				update_post_meta( $current_id, '_wppwp_display_start_at', $display_start_at );
				update_post_meta( $current_id, '_wppwp_display_finish_at', $display_finish_at );
				update_post_meta( $current_id, '_wppwp_start_at', $start_at );
				update_post_meta( $current_id, '_wppwp_start_at_formated', $start_date->format( "Y-m-d" ) );
				update_post_meta( $current_id, '_wppwp_finish_at', $finish_at );
				update_post_meta( $current_id, '_wppwp_finish_at_formated', $finish_date->format( "Y-m-d" ) );
				update_post_meta( $current_id, '_wppwp_country_code', $country_code );
				update_post_meta( $current_id, '_wppwp_banner_url', $banner_url );

				update_post_meta( $current_id, '_wppwp_status', (string) $campany->status );

				update_post_meta( $current_id, '_wppwp_donate_url', $donate_url );
				update_post_meta( $current_id, '_wppwp_funds_raised', $funds_raised );
				update_post_meta( $current_id, '_wppwp_banner_url', $banner_url );
				update_post_meta( $current_id, '_wppwp_last_time_update', $blogtime );


			} else {
				echo 'error :';
				var_dump( $request );
			}


		}
		// check autosave
		if ( wp_is_post_autosave( $post_id ) ) {
			return 'autosave';
		}

		//check post revision
		if ( wp_is_post_revision( $post_id ) ) {
			return 'revision';
		}

		// check permissions
		if ( 'project' == $_POST['post_type'] ) {
			if ( ! current_user_can( 'edit_page', $post_id ) ) {
				return 'cannot edit page';
			}
		} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
			return 'cannot edit post';
		}

		//so our basic checking is done, now we can grab what we've passed from our newly created form
		$custom_title        = ! empty( $_POST['_wppwp_custom_title'] ) ? $_POST['_wppwp_custom_title'] : '';
		$custom_descriptions = ! empty( $_POST['_wppwp_custom_descriptions'] ) ? $_POST['_wppwp_custom_descriptions'] : '';
		$wppwp_start         = ! empty( $_POST['_wppwp_start'] ) ? $_POST['_wppwp_start'] : '';
		$wppwp_finish        = ! empty( $_POST['_wppwp_finish'] ) ? $_POST['_wppwp_finish'] : '';
		$wppwp_fund_raised   = ! empty( $_POST['_wppwp_fund_raised'] ) ? $_POST['_wppwp_fund_raised'] : '';
		$wppwp_goal          = ! empty( $_POST['_wppwp_goal'] ) ? $_POST['_wppwp_goal'] : '';
		$status              = ! empty( $_POST['_wppwp_status'] ) ? $_POST['_wppwp_status'] : '';
		$donate_url          = ! empty( $_POST['_wppwp_donate_url'] ) ? $_POST['_wppwp_donate_url'] : '';
		$fb_page             = ! empty( $_POST['_wppwp_facebook_page'] ) ? $_POST['_wppwp_facebook_page'] : '';
		$lin_page            = ! empty( $_POST['_wppwp_linkdin_page'] ) ? $_POST['_wppwp_linkdin_page'] : '';

		if ( ! add_post_meta( $post_id, '_wppwp_custom_title', $custom_title, true ) ) {
			update_post_meta( $post_id, '_wppwp_custom_title', $custom_title );
		}
		if ( ! add_post_meta( $post_id, '_wppwp_custom_descriptions', $custom_descriptions, true ) ) {
			update_post_meta( $post_id, '_wppwp_custom_descriptions', $custom_descriptions );
		}

		if ( ! add_post_meta( $post_id, '_wppwp_goal', $wppwp_goal, true ) ) {
			update_post_meta( $post_id, '_wppwp_goal', $wppwp_goal );
		}

		if ( ! add_post_meta( $post_id, '_wppwp_facebook_page', $fb_page, true ) ) {
			update_post_meta( $post_id, '_wppwp_facebook_page', $fb_page );
		}
		if ( ! add_post_meta( $post_id, '_wppwp_linkdin_page', $lin_page, true ) ) {
			update_post_meta( $post_id, '_wppwp_linkdin_page', $lin_page );
		}


	}

	public function set_post_type_slug() {
		$this->post_type_slug = 'wppwp_projects';
	}

	public function get_template_path( $template_dir ) {
		return WP_PWP()->get_path() . DIRECTORY_SEPARATOR . $template_dir;
	}

	protected function get_metaboxes_data() {
	}

	public function set_template() {
		$this->template            = new WPPWP_Template( $this->get_template_path( self::METABOX_VIEW_DIR ) );
		$this->template->post_type = $this;

	}

	public function get_post_type_options() {

	}

	protected function get_post_type_params() {

		$labels = [
			'name'          => __( 'Projects' ),
			'singular_name' => __( 'Project' ),
			'all_items'     => __( 'All Projects' ),
			'archives'      => __( 'Archives Location' ),
		];

		$params = [
			'labels'      => $labels,
			'public'      => true,
			'rewrite'     => array( 'slug' => _x( 'projects', 'URL slug' ), 'with_front' => false ),
			'has_archive' => true,
			'menu_icon'   => 'dashicons-list-view',
			'supports'    => [ 'title', 'editor', 'author', 'thumbnail', 'excerpt', ],

		];

		return apply_filters( 'wcdrp_post_type_params', $params );

	}

	public function projects_category_add_meta_fields( $taxonomy ) {
		?>
        <div class="form-field term-group">
            <label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
            <input type="text" id="alternative_heading" name="alternative_heading"/>
        </div>
		<?php
	}

	public function projects_category_edit_meta_fields( $term, $taxonomy ) {
		$alternative_heading = get_term_meta( $term->term_id, 'alternative_heading', true );
		?>
        <tr class="form-field term-group-wrap">
            <th scope="row">
                <label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
            </th>
            <td>
                <input type="text" id="alternative_heading" name="alternative_heading"
                       value="<?php echo $alternative_heading; ?>"/>
            </td>
        </tr>
		<?php
	}

	public function projects_category_save_taxonomy_meta( $term_id, $tag_id ) {
		if ( isset( $_POST['alternative_heading'] ) ) {
			update_term_meta( $term_id, 'alternative_heading', esc_attr( $_POST['alternative_heading'] ) );
		}
	}

	function projects_charity_add_meta_fields( $taxonomy ) {
		?>
        <div class="form-field term-group">
            <label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
            <input type="text" id="alternative_heading" name="alternative_heading"/>
        </div>
		<?php
	}

	public function projects_charity_edit_meta_fields( $term, $taxonomy ) {
		$alternative_heading = get_term_meta( $term->term_id, 'alternative_heading', true );
		?>
        <tr class="form-field term-group-wrap">
            <th scope="row">
                <label for="alternative_heading"><?php _e( 'Alternative Heading', 'my-plugin' ); ?></label>
            </th>
            <td>
                <input type="text" id="alternative_heading" name="alternative_heading"
                       value="<?php echo $alternative_heading; ?>"/>
            </td>
        </tr>
		<?php
	}

	public function projects_charity_save_taxonomy_meta( $term_id, $tag_id ) {
		if ( isset( $_POST['alternative_heading'] ) ) {
			update_term_meta( $term_id, 'alternative_heading', esc_attr( $_POST['alternative_heading'] ) );
		}
	}

}
