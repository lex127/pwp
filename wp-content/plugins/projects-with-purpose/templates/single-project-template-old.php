<?php
$title   = get_the_title();
$content = get_the_content();

$image               = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'wppwp-single-project' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'wppwp-single-project' ) : WP_PWP::get_default_thumbail( 1 );
$custom_title        = get_post_meta( get_the_ID(), '_wppwp_custom_title', true );
$custom_descriptions = get_post_meta( get_the_ID(), '_wppwp_custom_descriptions', true );
$wppwp_start         = get_post_meta( get_the_ID(), '_wppwp_start_at_formated', true );

$wppwp_finish = get_post_meta( get_the_ID(), '_wppwp_finish_at_formated', true );

$funds_raised = get_post_meta( get_the_ID(), '_wppwp_funds_raised', true );
$goal         = get_post_meta( get_the_ID(), '_wppwp_goal', true );
$status       = get_post_meta( get_the_ID(), '_wppwp_status', true );
$donate_url   = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );
$current_user = wp_get_current_user();
$donate_url .= "?first_name=$current_user->user_firstname&last_name=$current_user->user_lastname&email=$current_user->user_email";
$json_campany    = get_post_meta( get_the_ID(), '_wppwp_json_campany', true );
$content         = apply_filters( 'post_content', $post->post_content );
$trimmed_content = wp_trim_words( $content, 40 );
$home_title      = esc_html__( 'Home', "welldone" );
$fb_page  = get_post_meta( get_the_ID(), '_wppwp_facebook_page', true );
$lin_page = get_post_meta( get_the_ID(), '_wppwp_linkdin_page', true );
?>

<?php get_header(); ?>
    <div class="page-top   ">
        <section class="breadcrumbs breadcrumbs-boxed hidden-xs">
            <div class="container">
                <ol class="breadcrumb breadcrumb--wd pull-left">
					<?php echo '<li><a href="' . esc_url( get_home_url() ) . '" title="' . esc_attr( $home_title ) . '">' . esc_attr( $home_title ) . '</a></li>'; ?>
                    <li>
                        <a href="<?php echo get_post_type_archive_link( 'wppwp_projects' ); ?>"><?php echo esc_html__( "Projects", "welldone" ); ?> </a>
                    </li>
                    <li><?php echo $title; ?></li>
                </ol>
<!--                <button class="custom-sign-up-button btn btn--wd">--><?php //echo __( 'JOIN OUR NEWSLETTER', 'welldone' ); ?><!--</button>-->
            </div>
        </section>
        <div class="container">

        </div>
    </div>
    <style>
        body {
            font-size: 1.4em !important;
        }
    </style>

    <section class="content project-details"><!-- section.content -->
        <div class="container"><!-- section.content > .container -->
            <div class="row wrapper-start"><!-- .container > .row.wrapper-start -->

                <div class="col-md-9"><!-- .row.wrapper-start > .products_container -->
                    <div id="project-<?php the_ID(); ?>">
                        <div class="col-sm-5 col-md-5 col-lg-6" style="padding-left: 0px;">
                            <div class="images product-gallery-container product-main-image">
                                <div class="product-top product-main-image__item">
									<?php if ( ! empty( $image ) ) : ?>
                                        <img src="<?php echo $image; ?> ">
									<?php else : echo 'No image upload'; ?>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="product-info col-sm-7 col-md-7 col-lg-6">
                            <div class="single-project-summary summary">
                                <div class="product-details text-left">
                                    <div class="product-info__title">
                                        <h2><?php echo $title; ?></h2>
                                    </div>
									<?php if ( ! empty( $content ) ): ?>
                                        <div itemprop="description">
                                            <p><?php echo apply_filters( 'the_content', $content ); ?></p>
                                        </div>
									<?php endif; ?>
									<?php if ( is_user_logged_in() ): ?>
                                        <a target="_blank"
                                           class="single-project-button-donate btn btn--wd text-uppercase button alt"
                                           href="<?php echo $donate_url; ?>"> <?php _e( 'Donate now', 'welldone' ); ?>
                                        </a>
									<?php else: ?>
                                        <a title="<?php echo get_the_title(); ?>" link="<?php echo get_the_permalink(); ?>"
                                           class="popmake-login single-project-button-donate btn btn--wd text-uppercase button alt"
                                           href="#"> <?php _e( 'Donate now', 'welldone' ); ?>
                                        </a>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>

				<?php include plugin_dir_path( __FILE__ ) . 'single-project/right-sidebar.php'; ?>


            </div>

        </div>
    </section>

<?php include plugin_dir_path( __FILE__ ) . 'single-project/social-sections.php'; ?>

<?php include plugin_dir_path( __FILE__ ) . 'single-project/slider-sections.php'; ?>

<?php get_footer() ?>