<?php
get_header();
$class                = welldone_main_container_class();
$quantity_terms       = get_queried_object_id();
$quantity_term_object = get_term_by( 'id', $quantity_terms, 'projects_category' );
$quantity_term_name   = ! empty( $quantity_term_object->name ) ? $quantity_term_object->name : 0;
$alternative_heading  = get_term_meta( $quantity_terms, 'alternative_heading', true );
$header               = ! empty( $alternative_heading ) ? $alternative_heading : $quantity_term_name;

//get in themes
$columns  = '';
$pr_class = apply_filters( "welldone_filter_product_container_class", "row-view products-grid four-in-row products-listing products-col products-isotope" );

?>
<section class="content">

    <div class="<?php echo esc_attr( $class ); ?>">
        <div class="row wrapper-start">
            <div id="products_container" class="col-md-12"><!-- .row.wrapper-start > .products_container -->
				<?php include plugin_dir_path( __FILE__ ) . 'projects-header.php'; ?>
                <div class="outer open">

					<?php include plugin_dir_path( __FILE__ ) . 'left-sidebar.php'; ?>
                    <div class="centerCol">
                        <!-- /.modal -->
                        <div class="<?php echo esc_attr( $pr_class ); ?> clearfix products <?php echo ( $columns ) ? $columns : 1; ?>"
                             data-columns="<?php echo ( $columns ) ? $columns : 1; ?>"><!-- .products-listing -->

                            <div class="in-cat-project-page">
								<?php include plugin_dir_path( __FILE__ ) . 'projects-loop.php'; ?>
                            </div>

                        </div>


                    </div>

                </div>
            </div>
        </div>

</section>

<?php get_footer(); ?>


<script type="text/javascript">

</script>



