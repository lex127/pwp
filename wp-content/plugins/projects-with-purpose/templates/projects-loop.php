<?php

// Extra post classes
$classes   = array();
$classes[] = 'welldone_product product product-preview-wrapper';
$classes[] = '';

?>

<?php if ( have_posts() ) : while ( have_posts() ) :
	the_post(); ?>
	<?php $image = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'welldone-projects-archive-img' ) : WP_PWP::get_default_thumbail( 2 ); ?>
    <div <?php post_class( $classes ); ?>>

        <div class="product-preview">

            <div class="product-preview__image" style="">
                <figure class="shop_catalog owl-carousel product-slider">
                    <a href="<?php the_permalink(); ?>">
                        <img class="product-image" src="<?php echo $image; ?>"/>
                    </a>
                </figure>

            </div>
            <div class="product-preview__info text-center">
                <a href="<?php the_permalink(); ?>">
                    <div class="product-preview__info__title">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </a>
                <div class="product-preview__info__description"><p><?php
						$content         = get_the_content();
						$trimmed_content = wp_trim_words( $content, 15 );
						echo $trimmed_content;
						?></p>

                </div>
                <a class="view-detail-project" href="<?php the_permalink(); ?>"> <span style="font-size: 14px"><?php _e('Find out more >','welldone');?></span></a>
				<?php
                $donate_url = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );
				$current_user     = wp_get_current_user();
				$donate_url .= "?first_name=$current_user->user_firstname&last_name=$current_user->user_lastname&email=$current_user->user_email";
				?>


                <div class="projecs-social-share-button">
                    <div class="product-preview__info__link ">
						<?php if ( is_user_logged_in() ): ?>
                            <a href="<?php echo $donate_url; ?>" class="btn btn--wd donate-button-global"
                               style="padding: 10px 10px;" target="_blank">Donate now</a>
						<?php else: ?>
                            <a href="#" class="btn btn--wd buy-link donate-button-global popmake-register"
                               link="<?php echo get_the_permalink(); ?>"
                               title="<?php echo get_the_title(); ?>" style="padding: 10px 10px;">Donate now</a>
						<?php endif; ?>
                    </div>
                    <div class="hide-in-mobile">
                        <span style="font-size: 12px;color:black;font-family: Gotham Rounded Medium;"><i>This is a project I think is valuable and worth sharing:</i></span>

                        <div class="email-share"></div>


                        <!-- Facebook -->
                        <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"
                           target="_blank">
                            <i class="fa fa-facebook-square" aria-hidden="true"></i></a>

                        <!-- LinkedIn -->
                        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>"
                           target="_blank">
                            <i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        <!-- Google+ -->
                        <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>"
                           target="_blank">
                            <i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                        <!-- Pinterest -->
                        <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                            <i class="fa fa-pinterest-square" aria-hidden="true"></i> </a>
                        <!-- Twitter -->
                        <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo $trimmed_content; ?>&amp;hashtags=<?php the_title(); ?>"
                           target="_blank">
                            <i class="fa fa-twitter-square" aria-hidden="true"></i> </a>
                        <!--                    <span class="share-text">SHARE</span>-->

                        <a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php echo $trimmed_content; ?>link: <?php the_permalink(); ?>"
                           title="Share by Email">
                            <i class="fa fa-envelope" aria-hidden="true"></i></a>
                        <span class="email-text">Email to a friend</span>
                    </div>

                </div>


            </div>

        </div>
    </div><!-- end welldone_product -->

<?php endwhile; ?>
	<?php include plugin_dir_path( __FILE__ ) . 'pagination.php'; ?>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>


