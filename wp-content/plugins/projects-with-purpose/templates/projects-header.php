<?php
$global_header = get_option( 'wppwp_global_header_admin' );
$global_des    = get_option( 'wppwp_global_descriptions_admin' );

if ( is_tax() ) {
	$term                = get_queried_object();
	$term_id             = $term->term_id;
	$taxonomy_name       = $term->taxonomy;
	$alternative_heading = get_term_meta( $term->term_id, 'alternative_heading', true );
	$desc_term           = term_description( $term_id, $taxonomy );
}

$title = ! empty( $alternative_heading ) ? $alternative_heading : $global_header;
$desc  = ! empty( $desc_term ) ? $desc_term : $global_des;
?>
<div class="filters-row row">
	<?php if ( ! empty( $title ) || ! empty( $desc ) ): ?>

        <div class="row">
            <div class="header-cat-desc">
                <h2><?php echo $title; ?></h2>
				<?php echo $desc; ?>
            </div>
        </div>
<!--        if need-->
<!--                            <a class="filters-row__view active link-grid-view icon icon-keyboard"></a>-->
<!--                            <a class="filters-row__view link-row-view icon icon-list"></a>-->
<!--        <a class="btn btn--wd btn--with-icon btn--sm wave" id="showFilter"><span class="icon icon-filter"></span>Filter</a>-->
        <a class="btn btn--wd btn--with-icon btn--sm wave" id="showFilterMobile"><span class="icon icon-filter"></span>Filter</a>
	<?php endif; ?>
</div>

