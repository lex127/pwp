<?php

$terms = get_terms( [
	'taxonomy'   => 'projects_category',
	'hide_empty' => true,
] );

?>
<div id="leftCol">
    <div id="filtersCol" class="filters-col">
        <div class="filters-col__close" id="filtersColClose"><a href="#" class="icon icon-clear"></a></div>
        <aside class="sidebar" role="complementary">

            <div class="box">
                <h4 style="margin-top: 6%;"><?php _e('Category','welldone');?><span class="expand"><i class="down"></i></span></h4>
                <ul>
					<?php
					foreach ( $terms as $term ): ?>
                        <li><a href="<?php echo get_term_link( $term->term_id ); ?>"> <span
                                        class="sidebar-widget-project" style="font-size: 14px;font-family: Gotham Rounded Light;"><?php echo $term->name; ?>
                                    ( <?php echo $term->count; ?> )</span></a></li>
					<?php endforeach; ?>
                </ul>
            </div>

            <hr>

            <div class="box">
                <h4><?php _e('Charity','welldone');?><span class="expand"><i class="down"></i></span></h4>
                <ul>
					<?php
					$terms_charity = get_terms( [
						'taxonomy'   => 'projects_charity',
						'hide_empty' => true,
					] ); ?>
					<?php foreach ( $terms_charity as $term ): ?>

                        <li><a href="<?php echo get_term_link( $term->term_id ); ?>"> <span
                                        class="sidebar-widget-project" style="font-size: 14px;font-family: Gotham Rounded Light;"><?php echo $term->name; ?>
                                    ( <?php echo $term->count; ?> )</span></a></li>

					<?php endforeach; ?>
                </ul>
            </div>

        </aside>
        <div class="md-margin2x clearfix visible-sm visible-xs"></div>
    </div>
</div>
