<aside class="col-md-3 sidebar" role="complementary">
    <div id="text-2" class="page-sidebar widget_text">
        <div class="textwidget"><h4><?php echo $custom_title; ?></h4>
            <div class="card">
                <div class="card__row"><?php echo $post->post_excerpt;?></div>
                <a href="#" class="card__row card__row--icon">
                    <div class="card__row--icon__icon"> START:</div>
                    <div class="card__row--icon__text">
	                    <?php echo $wppwp_start ?>
                    </div>
                </a> <a href="#" class="card__row card__row--icon">
                    <div class="card__row--icon__icon">FINISH:</div>
                    <div class="card__row--icon__text">
	                    <?php echo $wppwp_finish ?>
                    </div>
                </a> <a href="#" class="card__row card__row--icon">
                    <div class="card__row--icon__icon">  FUNDS RAISED:</div>
                    <div class="card__row--icon__text">
	                    <?php echo $funds_raised->currency->symbol . number_format( $funds_raised->cents / 100, 2, '.', ' ' ); ?>
                    </div>
                </a> <a class="card__row card__row--icon">
                    <div class="card__row--icon__icon">  GOAL:</div>
                    <div class="card__row--icon__text">
	                    <?php if ( ! empty( $goal ) ):
		                    echo $funds_raised->currency->symbol . $goal;
	                    endif;
	                    ?>
                    </div>
                </a></div>
        </div>
    </div>
</aside>