<?php
$fb_page  = get_post_meta( get_the_ID(), '_wppwp_facebook_page', true );
$lin_page = get_post_meta( get_the_ID(), '_wppwp_linkdin_page', true );
?>
<section class="content content--fill" style="margin-top: 75px;padding-bottom: 75px!important;">
    <div class="container">
        <div class="col-md-12">
            <div class="project-social-block">
                <h4>Stay Connected</h4>

				<?php if ( ! empty( $fb_page ) ): ?>
                    <div class="social-icon">
                        <div class="social-block-left"> Facebook Page:</div>
                        <div class="social-block-right"><a href="http://<?php echo $fb_page;?>" target="_blank"><?php echo $fb_page;?></a></div>
                    </div>
				<?php endif; ?>
				<?php if ( ! empty( $lin_page ) ): ?>
                    <div class="social-icon">
                        <div class="social-block-left">LinkedIn Page:</div>
                        <div class="social-block-right"><a href="http://<?php echo $lin_page;?>" target="_blank"><?php echo $lin_page;?></a></div>
                    </div>
				<?php endif; ?>
                <div class="social-icon">
                    <div class="social-block-left">Email to a Friend:</div>
                    <div class="social-block-right">
                        <a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php echo $trimmed_content; ?>link: <?php the_permalink(); ?>"
                           title="Share by Email"> <i class="fa fa-envelope" aria-hidden="true"></i></a>

                    </div>
                </div>

                <div class="social-icon">
                    <div class="social-block-left">Share on Facebook:</div>
                    <div class="social-block-right"><a
                                href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i
                                    class="fa fa-facebook-square" aria-hidden="true"></i></a></div>
                </div>

                <div class="social-icon">
                    <div class="social-block-left">
                        <span>Share on LinkedIn:</span></div>
                    <div class="social-block-right"><a
                                href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>"
                                target="_blank">
                            <i class="fa fa-linkedin-square" aria-hidden="true"></i></a></div>
                </div>

                <div class="social-icon">
                    <div class="social-block-left">
                        <span>Share on Google+:</span></div>
                    <div class="social-block-right"><a
                                href="https://plus.google.com/share?url=<?php the_permalink(); ?>"
                                target="_blank">
                            <i class="fa fa-google-plus-square" aria-hidden="true"></i></a></div>
                </div>

                <div class="social-icon">
                    <div class="social-block-left">
                        <span>Share on Pinterest:</span></div>
                    <div class="social-block-right"><a
                                href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                            <i class="fa fa-pinterest-square" aria-hidden="true"></i> </a></div>
                </div>

                <div class="social-icon">
                    <div class="social-block-left">
                        <span>Share on Twitter:</span></div>
                    <div class="social-block-right"><a
                                href="https://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo $trimmed_content; ?>&amp;hashtags=<?php the_title(); ?>"
                                target="_blank">
                            <i class="fa fa-twitter-square" aria-hidden="true"></i> </a></div>
                </div>

            </div>
        </div>
    </div>
</section>