<?php

global $post;
$post_type = $post->post_type;

// Get post type taxonomies.
$taxonomy_names = get_object_taxonomies( $post );
$terms          = wp_get_post_terms( $post->ID, $taxonomy_names );
//var_dump($terms);

$custom_terms          = get_terms( 'custom_taxonomy' );
$tax_query['relation'] = 'OR';
foreach ( $terms as $term ) {
	wp_reset_query();
	$tax_query[] = [
		'taxonomy' => $term->taxonomy,
		'field'    => 'slug',
		'terms'    => $term->slug,
	];

}
$args = array(
	'post_type'      => 'wppwp_projects',
	'post__not_in'   => array( get_the_ID() ),
	'posts_per_page' => 8,
	'post_status'    => 'publish',
	'orderby'        => 'post_date',
	'order'          => 'DESC',
	'tax_query'      => $tax_query,
);

$classes   = array();
$classes[] = 'welldone_product product product-preview-wrapper';
$classes[] = '';

?>
<section class="content product-ups">
    <div class="container">
        <!-- Modal -->
        <div class="modal quick-view zoom" id="quickView" style=" opacity: 1">
            <div class="modal-dialog">
                <div class="modal-content"></div>
            </div>
        </div>
		<?php
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) { ?>
            <h2 class="text-center text-uppercase product-slider-title" style="font-size: 40px;color:#305470; "><?php _e('Projects with Purpose','welldone');?></h2>
            <div class="row product-carousel mobile-special-arrows animated-arrows product-grid four-in-row">
				<?php
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<?php $image = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'welldone-projects-slide-in-single' ) : WP_PWP::get_default_thumbail( 2 ); ?>
					<?php $donate_url = get_post_meta( get_the_ID(), '_wppwp_donate_url', true ); ?>

                    <div <?php post_class( $classes ); ?>>

                        <div class="product-preview" >
                            <div class="product-preview__image" style="min-height: 300px">
                                <figure class="shop_catalog owl-carousel product-slider">
                                    <a href="<?php the_permalink(); ?>">
                                        <img height="200" class="product-image" src="<?php echo $image; ?>"/>
                                    </a>
                                </figure>

                            </div>
                            <div class="product-preview__info text-center" style="text-align: left;">

                                    <div class="product-preview__info__title">
                                        <a href="<?php the_permalink(); ?>"> <h2 style="margin: 0px;margin-bottom: 0px!important;"><?php the_title(); ?></h2>                                </a>
                                        <a class="view-detail-project" href="<?php the_permalink(); ?>"> <span style="font-size: 14px"><?php _e('Find out more >','welldone');?></span></a>
		                                <div style="margin-top: 5%">
		                                        <?php if ( is_user_logged_in() ): ?>
                                                <a href="<?php echo $donate_url; ?>" class="btn btn--wd donate-button-global"
                                                   style="padding: 10px 10px;" target="_blank">Donate now</a>
		                                    <?php else: ?>
                                                <a href="#" class="btn btn--wd buy-link donate-button-global popmake-register"
                                                   link="<?php echo get_the_permalink(); ?>"
                                                   title="<?php echo get_the_title(); ?>" style="padding: 10px 10px;">Donate now</a>
		                                    <?php endif; ?>
                                        </div>
                                    </div>

                            </div>

                        </div>

                    </div>

				<?php endwhile; ?>
            </div>
		<?php } ?>
    </div>
</section>