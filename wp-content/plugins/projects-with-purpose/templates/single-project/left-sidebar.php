<aside class="col-md-4 sidebar" role="complementary">
	<div id="text-2" class="page-sidebar widget_text">
		<div class="textwidget">
			<div class="card">
				<div class="card__row"><span class="table-title"><?php _e('Project timeline','welldone');?></span></div>
				<a href="#" class="card__row card__row--icon">
					<div class="card__row--icon__icon"> START:</div>
					<div class="card__row--icon__text">
						<?php echo $wppwp_start ?>
					</div>
				</a> <a href="#" class="card__row card__row--icon">
					<div class="card__row--icon__icon">FINISH:</div>
					<div class="card__row--icon__text">
						<?php echo $wppwp_finish ?>
					</div>
				</a> <a href="#" class="card__row card__row--icon">
					<div class="card__row--icon__icon">  FUNDS RAISED:</div>
					<div class="card__row--icon__text" id="founds-raised">
						<span><?php echo $funds_raised->currency->symbol . number_format( $funds_raised->cents / 100, 2, '.', ' ' ); ?></span>
					</div>
				</a> <a class="card__row card__row--icon">
					<div class="card__row--icon__icon">  GOAL:</div>
					<div class="card__row--icon__text">
						<?php if ( ! empty( $goal ) ):
							echo $funds_raised->currency->symbol . $goal;
						endif;
						?>
					</div>
				</a></div>
			<div>
				<?php if (!empty($fb_page) || !empty($lin_page)): ?>
				<span class="header-after-table">For more project details:</span>
				<table class="tg">
					<?php if (!empty($fb_page)):?>
					<tr>
						<td class="left-text">Facebook:</td>
                     <td class="right-text">   <a class="right-text" target="_blank" style="display: block; word-wrap: break-word; width: 220px;" href="http://<?php echo $fb_page;?>"><?php echo $fb_page;?></a></td>
					</tr>
					<?php endif;?>
					<?php if (!empty($lin_page)):?>
					<tr>
						<td class="left-text">LinkedIn:</td>
                       <td class="right-text"> <a class="right-text" target="_blank" style="display: block; word-wrap: break-word; width: 220px;" href="http://<?php echo $lin_page;?>"><?php echo $lin_page;?></a></td>
					</tr>
					<?php endif;?>
				</table>
				<?php endif;?>
			</div>
		</div>
	</div>
</aside>