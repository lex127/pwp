<?php
$title   = get_the_title();
$content = get_the_content();

$image               = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'full' ) : WP_PWP::get_default_thumbail( 1 );
$custom_title        = get_post_meta( get_the_ID(), '_wppwp_custom_title', true );
$custom_descriptions = get_post_meta( get_the_ID(), '_wppwp_custom_descriptions', true );
$wppwp_start         = get_post_meta( get_the_ID(), '_wppwp_start_at_formated', true );

$wppwp_finish = get_post_meta( get_the_ID(), '_wppwp_finish_at_formated', true );

$funds_raised    = get_post_meta( get_the_ID(), '_wppwp_funds_raised', true );
$goal            = get_post_meta( get_the_ID(), '_wppwp_goal', true );
$status          = get_post_meta( get_the_ID(), '_wppwp_status', true );
$donate_url      = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );
$current_user    = wp_get_current_user();
$donate_url      .= "?first_name=$current_user->user_firstname&last_name=$current_user->user_lastname&email=$current_user->user_email";
$json_campany    = get_post_meta( get_the_ID(), '_wppwp_json_campany', true );
$content         = apply_filters( 'post_content', $post->post_content );
$trimmed_content = wp_trim_words( $content, 40 );
$home_title      = esc_html__( 'Home', "welldone" );
$fb_page         = get_post_meta( get_the_ID(), '_wppwp_facebook_page', true );
$lin_page        = get_post_meta( get_the_ID(), '_wppwp_linkdin_page', true );

$wppwp_api_id = get_post_meta( get_the_ID(), '_wppwp_api_id', true );


//update_funds_raised_from_api($wppwp_api_id, get_the_ID());

?>

<?php get_header();

wp_enqueue_script( 'wppwp_pwp-ajax' );
    $pwp_ajax = array(
	    'api-id'   => $wppwp_api_id,
	    'page-id'  => get_the_ID(),
	    'nonce'    => wp_create_nonce( 'projectPage' ),
	    'ajax_url' => admin_url( 'admin-ajax.php' )
    );
    wp_localize_script( 'wppwp_pwp-ajax', 'projectPage', $pwp_ajax ); ?>
    <div class="page-project-single">
        <div class="single-page-top-image">
            <img src="<?php echo $image; ?>"/>
        </div>

        <section class="content project-details"><!-- section.content -->
            <div class="container"><!-- section.content > .container -->
                <div class="row wrapper-start"><!-- .container > .row.wrapper-start -->
					<?php include plugin_dir_path( __FILE__ ) . 'single-project/left-sidebar.php'; ?>

                    <div class="col-md-8"><!-- .row.wrapper-start > .products_container -->
                        <div class="product-info">

                            <div class="single-project-summary summary">

                                <div class="product-info__title">
                                    <h2><?php echo $title; ?></h2>
                                </div>
								<?php if ( ! empty( $content ) ): ?>
                                    <div itemprop="description" class="description">
                                        <p><?php echo apply_filters( 'the_content', $content ); ?></p>
                                    </div>
								<?php endif; ?>

								<?php if ( is_user_logged_in() ): ?>
                                    <a target="_blank"
                                       class="single-project-button-donate btn  button alt"
                                       href="<?php echo $donate_url; ?>"> <?php _e( 'Donate now', 'welldone' ); ?>
                                    </a>
								<?php else: ?>
                                    <a title="<?php echo get_the_title(); ?>" link="<?php echo get_the_permalink(); ?>"
                                       class="popmake-register single-project-button-donate btn  button alt"
                                       href="#"> <?php _e( 'Donate now', 'welldone' ); ?>
                                    </a>
								<?php endif; ?>
                            </div>
                        </div>
                        <hr>


                        <div class="single-projecs-social-share-button">

                            <span>This is a project I think is valuable and worth sharing:</span>

                            <div class="email-share"></div>


                            <!-- Facebook -->
                            <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>"
                               target="_blank">
                                <i class="fa fa-facebook-square" aria-hidden="true"></i></a>

                            <!-- LinkedIn -->
                            <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>"
                               target="_blank">
                                <i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                            <!-- Google+ -->
                            <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>"
                               target="_blank">
                                <i class="fa fa-google-plus-square" aria-hidden="true"></i></a>
                            <!-- Pinterest -->
                            <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
                                <i class="fa fa-pinterest-square" aria-hidden="true"></i> </a>
                            <!-- Twitter -->
                            <a href="https://twitter.com/share?url=<?php the_permalink(); ?>&amp;text=<?php echo $trimmed_content; ?>&amp;hashtags=<?php the_title(); ?>"
                               target="_blank">
                                <i class="fa fa-twitter-square" aria-hidden="true"></i> </a>
                            <!--                    <span class="share-text">SHARE</span>-->

                            <a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php echo $trimmed_content; ?>link: <?php the_permalink(); ?>"
                               title="Share by Email">
                                <i class="fa fa-envelope" aria-hidden="true"></i></a>
                            <span class="email-text" style="font-size: 12px;">Email to a friend</span>

                        </div>


                    </div>


                </div>
            </div>
        </section>

		<?php include plugin_dir_path( __FILE__ ) . 'single-project/slider-sections.php'; ?>
    </div>

<?php get_footer() ?>