<?php
/* Template Name: Custom Search */
global $welldone_settings, $post;
$welldone_layout_columns = welldone_page_columns();
$welldone_class_name     = welldone_class_name();
$containerClass          = welldone_main_container_class();
get_header();
global $wp_query, $paged;
$terms_category = get_terms( [
	'taxonomy'   => 'projects_category',
	'hide_empty' => true,
] );
$terms_charity  = get_terms( [
	'taxonomy'   => 'projects_charity',
	'hide_empty' => true,
] );
$search_query   = get_search_query();

$args = array(
	'post_type'      => 'wppwp_projects',
	'posts_per_page' => 9,
	'orderby'        => 'date',
	'post_status'    => 'publish',
	'order'          => 'DESC',
	'paged'          => $paged,

);
if ( ! empty( $search_query ) ) {
	$args['s'] = $search_query;
}
?>

<?php
wp_enqueue_style( WP_PWP::PREFIX . 'search-project-css' );
wp_enqueue_script( WP_PWP::PREFIX . 'search-project' );

$charity_term_id  = get_query_var( 'charity_term_id' );
$category_term_id = get_query_var( 'category_term_id' );

$the_query = new WP_Query( $args );
?>
    <div class="content">
        <div class="container ">
            <div class="row">

                <div class="col-md-12">

                    <div class="page type-page status-publish hentry last">
                        <div class="page">
                            <section class=" search-project-page">
                                <div class="container">
                                    <div class="row wrapper-start">

                                        <div class="col-md-12">

                                            <div class="page-description">
                                                <!--                    <h2 class="text-center text-uppercase">Search</h2>-->

                                                <form role="search" method="get"
                                                      action="<?php echo esc_url( home_url( '/' ) ) ?>">

                                                    <div class="input-group search-cat-filter form-search-page">
                                                        <select class="select--wd select--wd--sm terms_category">
                                                            <option value="0" default selected>Category</option>
															<?php foreach ( $terms_category as $term ): ?>
                                                                <option <?php if ( $category_term_id == $term->term_id ): echo 'selected'; endif; ?>
                                                                        value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
															<?php endforeach; ?>
                                                        </select>
                                                        <select class="select--wd select--wd--sm terms_charity">
                                                            <option value="0" default selected>Charity</option>
															<?php foreach ( $terms_charity as $term ): ?>
                                                                <option <?php if ( $charity_term_id == $term->term_id ): echo 'selected'; endif; ?>
                                                                        value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
															<?php endforeach; ?>
                                                        </select>
                                                        <input type="hidden" name="category_term_id" value=""/>


                                                        <input type="hidden" name="charity_term_id" value=""/>
                                                        <input type="search"
                                                               value="<?php echo get_search_query() ?>"
                                                               name="s"

                                                               class="form-control"
                                                               placeholder="<?php echo esc_html__( ' search...', 'welldone' ) ?>"
                                                               data-loader-icon="<?php echo str_replace( '"', '', apply_filters( 'yith_wcas_ajax_search_icon', '' ) ) ?>"
                                                               data-min-chars="<?php echo esc_attr( get_option( 'yith_wcas_min_chars' ) ); ?>"/>
                                                        <span class="input-group-btn">
                                    <button id="yith-searchsubmit" class="btn" type="submit"> <i
                                                class="fa fa-search"></i> </button>
                                </span>
                                                        <input type="hidden" name="post_type" value="wppwp_projects"/>
                                                    </div>

                                                    <!--                                <h2 class="text-center text-uppercase">Filter</h2>-->

                                            </div>
                                            </form>

                                        </div>

                                    </div>
                                    <div class="search-listing-projects">
										<?php if ( $the_query->have_posts() ) :
										while ( $the_query->have_posts() ) :
											$the_query->the_post(); ?>
                                            <div class="grid-item col-sm-4">
												<?php $image = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'welldone-search-page-img' ) : WP_PWP::get_default_thumbail( 2 ); ?>
                                                <div class="product-preview">
                                                    <div class="product-preview__image" style="min-height: 300px">
                                                        <figure class="shop_catalog owl-carousel product-slider">
                                                            <a href="<?php the_permalink(); ?>">
                                                                <img height="200" class="product-image"
                                                                     src="<?php echo $image; ?>"/>
                                                            </a>
                                                        </figure>
                                                    </div>

                                                    <div class="product-preview__info text-center">
                                                        <div class="product-preview__info__title">
                                                            <a href="<?php the_permalink(); ?>"> <h2 style="margin: 0px;margin-bottom: 0px!important;"><?php the_title(); ?></h2>                                </a>
                                                            <a class="view-detail-project" href="<?php the_permalink(); ?>"> <span style="font-size: 14px"><?php _e('Find out more >','welldone');?></span></a>
                                                            <div style="margin-top: 5%">
			                                                    <?php if ( is_user_logged_in() ):
				                                                    $donate_url   = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );
                                                                    if(!empty($donate_url)) {
	                                                                    $donate_url .= "?first_name=$current_user->user_firstname&last_name=$current_user->user_lastname&email=$current_user->user_email";
                                                                    }
                                                                    ?>
                                                                    <a href="<?php echo $donate_url; ?>" class="btn btn--wd donate-button-global"
                                                                       style="padding: 10px 10px;" target="_blank">Donate now</a>
			                                                    <?php else: ?>
                                                                    <a href="#" class="btn btn--wd buy-link donate-button-global popmake-register"
                                                                       link="<?php echo get_the_permalink(); ?>"
                                                                       title="<?php echo get_the_title(); ?>" style="padding: 10px 10px;">Donate now</a>
			                                                    <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

										<?php endwhile;
										$range = 2;
										if ( empty( $paged ) ) {
											$paged = 1;
										}

										$pages = $the_query->max_num_pages;
										?>

                                    </div>

                                </div>
                                <ul class="pagination" style="position: absolute;bottom: 0;left: 40%;">

		                            <?php

		                            if ( $paged > 1 + $range ) {
			                            echo "<li><a href='" . get_pagenum_link( 1 ) . "' aria-label='First'><span aria-hidden='true'><i class='fa fa-angle-double-left'></i></span></a></li>";
		                            }

		                            $page_links = paginate_links( array(
			                            'base'      => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
			                            'format'    => '',
			                            'add_args'  => '',
			                            'current'   => max( 1, get_query_var( 'paged' ) ),
			                            'total'     => $the_query->max_num_pages,
			                            'prev_text' => "<span aria-hidden='true'><i class='fa fa-angle-left'></i></span>",
			                            'next_text' => "<span aria-hidden='true'><i class='fa fa-angle-right'></i></span>",
			                            'type'      => 'array',
			                            'end_size'  => 1,
			                            'mid_size'  => 1,
		                            ) );

		                            if ( is_array( $page_links ) ) {
			                            foreach ( $page_links as $link ) {
				                            if ( preg_match( "/^<span class='page-numbers current'>/", $link ) ) {
					                            echo "<li class='active'>";
				                            } else {
					                            echo "<li>";
				                            }
				                            echo $link;
				                            echo "</li>";
			                            }
		                            }
		                            if ( $paged < $pages - $range ) {
			                            echo "<li><a href='" . get_pagenum_link( $pages ) . "' aria-label='Last'><span aria-hidden='true'><i class='fa fa-angle-double-right'></i></span></a></li>";
		                            }
		                            ?>
                                </ul>
	                            <?php else : ?>
                                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	                            <?php endif; ?>
	                            <?php wp_reset_postdata(); ?>
                        </div>

                    </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    </div>

<?php get_footer(); ?>