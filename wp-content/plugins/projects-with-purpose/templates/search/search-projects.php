<?php

/* Template Name: Custom Search Page*/
wp_enqueue_style( WP_PWP::PREFIX . 'search-project-css' );
wp_enqueue_script( WP_PWP::PREFIX . 'search-project' );

$terms_category = get_terms( [
	'taxonomy'   => 'projects_category',
	'hide_empty' => true,
] );
$terms_charity  = get_terms( [
	'taxonomy'   => 'projects_charity',
	'hide_empty' => true,
] );
?>
<section class=" search-project-page">
    <div class="container">
        <div class="row wrapper-start">

            <div class="col-md-12">

                <div class="page-description">
                    <!--                        <h2 class="text-center text-uppercase">Search</h2>-->

                    <form role="search" method="get"
                          action="<?php echo esc_url( home_url( '/' ) ) ?>">

                        <div class="input-group search-cat-filter form-search-page">
                            <select class="select--wd select--wd--sm terms_category">
                                <option value="0" default selected>Category</option>
								<?php foreach ( $terms_category as $term ): ?>
                                    <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
								<?php endforeach; ?>
                            </select>
                            <select class="select--wd select--wd--sm terms_charity">
                                <option value="0" default selected>Charity</option>
								<?php foreach ( $terms_charity as $term ): ?>
                                    <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
								<?php endforeach; ?>
                            </select>
                            <input type="hidden" name="category_term_id" value=""/>


                            <input type="hidden" name="charity_term_id" value=""/>
                            <input type="search"
                                   value="<?php echo get_search_query() ?>"
                                   name="s"

                                   class="form-control"
                                   placeholder="<?php echo esc_html__( ' search...', 'welldone' ) ?>"
                                   data-loader-icon="<?php echo str_replace( '"', '', apply_filters( 'yith_wcas_ajax_search_icon', '' ) ) ?>"
                                   data-min-chars="<?php echo esc_attr( get_option( 'yith_wcas_min_chars' ) ); ?>"/>
                            <span class="input-group-btn">
                                    <button id="yith-searchsubmit" class="btn" type="submit"> <i
                                                class="fa fa-search"></i> </button>
                                </span>
                            <input type="hidden" name="post_type" value="wppwp_projects"/>
                        </div>


                        <!--                                <h2 class="text-center text-uppercase">Filter</h2>-->


                </div>
                </form>

            </div>

        </div>

		<?php
		global $wp_query, $paged;
		$args = array(
			'post_type'      => 'wppwp_projects',
			'posts_per_page' => 9,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'paged'          => $paged,
		);

		$the_query = new WP_Query( $args );
		$range     = 2;
		if ( empty( $paged ) ) {
			$paged = 1;
		}

		$pages = $the_query->max_num_pages;

		?>

        <div class="search-listing-projects">
			<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) :
				$the_query->the_post(); ?>
                <div class="grid-item col-sm-4">
					<?php $image = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'welldone-search-page-img' ) : WP_PWP::get_default_thumbail( 2 ); ?>
                    <div class="product-preview">
                        <div class="product-preview__image" style="min-height: 300px">
                            <figure class="shop_catalog owl-carousel product-slider">
                                <a href="<?php the_permalink(); ?>">
                                    <img height="200" class="product-image" src="<?php echo $image; ?>"/>
                                </a>
                            </figure>
                        </div>

                        <div class="product-preview__info text-center">
                            <div class="product-preview__info__title">
                                <a href="<?php the_permalink(); ?>"><h2
                                            style="margin: 0px;margin-bottom: 0px!important;"><?php the_title(); ?></h2>
                                </a>
								<?php $content = get_the_content(); ?>
								<?php $trimmed = wp_trim_words( $content, $num_words = 10, $more = null ); ?>
                                <p style="font-size: 14px;line-height: 1.3; padding-top: 10px;"><?php echo $trimmed; ?></p>
                                <a class="view-detail-project" href="<?php the_permalink(); ?>"> <span
                                            style="font-size: 14px"><?php _e( 'Find out more >', 'welldone' ); ?></span></a>
                                <div style="margin-top: 5%">

									<?php if ( is_user_logged_in() ):
										$current_user = wp_get_current_user();
										$donate_url = get_post_meta( get_the_ID(), '_wppwp_donate_url', true );
										if ( ! empty( $donate_url ) ) {
											$donate_url .= "?first_name=$current_user->user_firstname&last_name=$current_user->user_lastname&email=$current_user->user_email";
                                                                    }
										?>
                                        <a href="<?php echo $donate_url; ?>" class="btn btn--wd donate-button-global"
                                           style="padding: 10px 10px;" target="_blank">Donate now</a>
									<?php else: ?>
                                        <a href="#" class="btn btn--wd buy-link donate-button-global popmake-register"
                                           link="<?php echo get_the_permalink(); ?>"
                                           title="<?php echo get_the_title(); ?>" style="padding: 10px 10px;">Donate
                                            now</a>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

			<?php endwhile;


				?>
			<?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
			<?php endif; ?>
        </div>

    </div>
	<?php include plugin_dir_path( __FILE__ ) . 'pagination.php'; ?>
    </div>
</section>

