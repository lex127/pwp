<?php
global $wp_query, $paged;

$args = array(
	'post_type'      => 'wppwp_projects',
	'posts_per_page' => 9,
	'orderby'        => 'date',
	'order'          => 'DESC',
	'paged'          => $paged,
);

$the_query = new WP_Query( $args );
$range = 2;
if ( empty( $paged ) ) $paged = 1;
// if ( $the_query->max_num_pages <= 1 ) {
// 	return;
// }
$pages = $wp_query->max_num_pages;

?>

<div class="search-listing-projects">
	<?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) :
		$the_query->the_post(); ?>
        <div class="grid-item col-sm-4">
			<?php $image = ! empty( get_the_post_thumbnail_url( get_the_ID(), 'full' ) ) ? get_the_post_thumbnail_url( get_the_ID(), 'welldone-search-page-img' ) : WP_PWP::get_default_thumbail( 2 ); ?>
            <div class="product-preview">
                <div class="product-preview__image" style="min-height: 300px">
                    <figure class="shop_catalog owl-carousel product-slider">
                        <a href="<?php the_permalink(); ?>">
                            <img height="200" class="product-image" src="<?php echo $image; ?>"/>
                        </a>
                    </figure>
                </div>

                <div class="product-preview__info text-center">
                    <a href="<?php the_permalink(); ?>">
                        <div class="product-preview__info__title">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </a>
                    <div class="product-preview__info__description"><?php
                            $content         = get_the_content();
			                $trimmed_content = wp_trim_words( $content, 15 );
			                echo $trimmed_content;
			                ?>
                    </div>
                    <a class="view-detail-project" href="<?php the_permalink(); ?>"> <span>VIEW ></span></a>

                    </div>

                </div>
        </div>

	<?php endwhile;

		$range = 2;
		if ( empty( $paged ) ) $paged = 1;
		if ( $the_query->max_num_pages <= 1 ) {
			return;
		}
		$pages = $the_query->max_num_pages;


		?>
        <ul class="pagination">

			<?php

			if ( $paged > 1 + $range ) echo "<li><a href='" . get_pagenum_link ( 1 ) . "' aria-label='First'><span aria-hidden='true'><i class='fa fa-angle-double-left'></i></span></a></li>";

			$page_links = paginate_links(  array(
				'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
				'format'       => '',
				'add_args'     => '',
				'current'      => max( 1, get_query_var( 'paged' ) ),
				'total'        => $the_query->max_num_pages,
				'prev_text'    => "<span aria-hidden='true'><i class='fa fa-angle-left'></i></span>",
				'next_text'    => "<span aria-hidden='true'><i class='fa fa-angle-right'></i></span>",
				'type'         => 'array',
				'end_size'     => 1,
				'mid_size'     => 1,
			)  );

			foreach($page_links as $link){
				if(preg_match("/^<span class='page-numbers current'>/", $link)){
					echo "<li class='active'>";
				}else{
					echo "<li>";
				}
				echo $link;
				echo "</li>";
			}

			if ( $paged < $pages-$range ) echo "<li><a href='" . get_pagenum_link ( $pages ) . "' aria-label='Last'><span aria-hidden='true'><i class='fa fa-angle-double-right'></i></span></a></li>";
			?>
        </ul>
	<?php else : ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
    <?php wp_reset_postdata();?>
</div>


