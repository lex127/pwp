<?php
/**
 * Plugin Name: Projects with Purpose
 * Description: Projects with Purpose
 * Version: 0.1
 * Author: Piogroup
 * Author URI:
 * Text Domain: projects-with-purpose
 * Domain Path: /languages/
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
define( 'WPPWP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPPWP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
if ( ! class_exists( 'WP_PWP' ) ) :

	/**
	 * Main Class.
	 */
	final class WP_PWP {

		const ADMIN_PATH = 'admin';
		const PREFIX = 'wppwp_';
		const INCLUDE_PATH = 'includes';
		public static $table_name = false;
		public $Project_Setting;

		/**
		 * The single instance of the class.
		 */
		protected static $_instance = null;

		protected $loader;

		protected $post_type;

		protected $virtual_page;
		protected $cron;

		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}

			return self::$_instance;
		}

		/**
		 * Cloning is forbidden.
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-pwp' ), '2.1' );
		}

		/**
		 * Unserializing instances of this class is forbidden.
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'wp-pwp' ), '2.1' );
		}

		public function __construct() {
			global $wpdb;

			self::$table_name = $wpdb->prefix . 'wppwp';
			$this->add_hooks();
			$this->includes();
			$this->load();
			$this->init();
			add_image_size( 'wppwp-single-project', 400, 460, true );

			add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ) );
			//Debugmod
			ini_set( "max_execution_time", 300 );

			set_error_handler( array( $this, 'error_handler' ) );
			add_action( 'shutdown', array( $this, 'fatal_error_handler' ), 1 );

			add_action( 'wppwp_plugin_event_hook', array( $this, 'activated' ) );

		}

		public function fatal_error_handler() {
			$last_error = error_get_last();

			if ( $last_error ) {
				$error = implode( ' ', $last_error );
				error_log( $error, 3, dirname( __FILE__ ) . '/log.txt' );

			}
		}

		public function error_handler( $errno, $errstr, $errfile, $errline ) {
			$path  = dirname( __FILE__ ) . '/log.txt';
			$time  = date( 'Y-m-d H:i:s', time() );
			$ver   = PHP_VERSION_ID;
			$error = "$time $errno $errstr $errfile $errline $ver" . PHP_EOL;
			error_log( $error, 3, $path );
		}


		public function add_hooks() {
			add_action( 'wp_enqueue_scripts', array( $this, 'load_resources' ) );
			add_action( 'admin_enqueue_scripts', array( $this, 'load_resources' ) );
		}

		public function includes() {
			include_once( 'vendor/piogroup/piogroup-wp-autoloader.php' );
			include_once( 'vendor/piogroup/piogroup-wp-template.php' );
			include_once( 'vendor/piogroup/piogroup-wp-cpt.php' );

		}

		public function load() {

			$this->loader = new PioGroup_Wp_Autoloader( $this->get_path() );
			$this->loader->autoload( self::INCLUDE_PATH );
			//todo  include classes
			$this->loader->autoload( self::ADMIN_PATH );

			if ( $this->is_request( 'admin' ) ) {

			}

		}

		public function custom_query_vars_filter( $vars ) {
			$vars[] = 'category_term_id';
			$vars[] .= 'charity_term_id';

			return $vars;
		}


		function alter_query( $query ) {
			//gets the global query var object
			global $wp_query;
			if ( is_admin() ) {
				return;                 // If we're in the admin panel - drop out
			}

			$post_type = get_query_var( 'post_type' );

			if ( $wp_query->is_search && $post_type == 'wppwp_projects' && ! empty( $_GET['category_term_id'] ) && $query->query_vars['post_type'] != 'nav_menu_item' && $query->query_vars['post_type'] !='popup' ) {
				$query->set( 'tax_query', array(
					array(
						'taxonomy' => 'projects_category',
						'field'    => 'term_id',
						'terms'    => array( sanitize_text_field( $_GET['category_term_id'] ) ),
					),
				) );
			}

			if ( $wp_query->is_search && $post_type == 'wppwp_projects' && ! empty( $_GET['charity_term_id'] ) && $query->query_vars['post_type'] != 'nav_menu_item' && $query->query_vars['post_type'] !='popup' ) {

				$query->set( 'tax_query', array(
					array(
						'taxonomy' => 'projects_charity',
						'field'    => 'term_id',
						'terms'    => array( sanitize_text_field( $_GET['charity_term_id'] ) ),
					),
				) );
			}

			if ( ! empty( $_GET['charity_term_id'] ) && ! empty( $_GET['category_term_id'] ) && $query->query_vars['post_type'] != 'nav_menu_item' && $query->query_vars['post_type'] !='popup' ) {
				$query->set( 'tax_query', array(
						'relation' => 'AND',
						array(
							'taxonomy'         => 'projects_category',
							'field'            => 'term_id',
							'terms'            => array( sanitize_text_field( $_GET['category_term_id'] ) ),
							'include_children' => false,
						),
						array(
							'taxonomy'         => 'projects_charity',
							'field'            => 'term_id',
							'terms'            => array( sanitize_text_field( $_GET['charity_term_id'] ) ),
							'include_children' => false,
						),
					)


				);
			}

			return $query;
		}


		public function init() {
			add_action( 'template_include', array( $this, 'load_templatel' ) );
			add_filter( 'query_vars', array( $this, 'custom_query_vars_filter' ) );

			add_action( 'pre_get_posts', array( $this, 'alter_query' ) );


			$this->post_type       = new WPPWP_Post_type();
			$this->Project_Setting = new WPPWP_Project_Setting();
			$this->cron = new WPPWP_Cron_Jobs();
			if ( $this->is_request( 'admin' ) ) {
				new WPPWP_Admin( $this->post_type );
			}

			if ( $this->is_request( 'frontend' ) ) {
				new WPPWP_Shortcode();
			}
			add_action( 'after_switch_theme', array( $this, 'activation' ) );
		}

		/**
		 * What type of request is this?
		 *
		 * @param  string $type admin, ajax, cron or frontend.
		 *
		 * @return bool
		 */
		public
		function is_request(
			$type
		) {
			switch ( $type ) {
				case 'admin' :
					return is_admin();
				case 'ajax' :
					return defined( 'DOING_AJAX' );
				case 'frontend' :
					return ( ! is_admin() || defined( 'DOING_AJAX' ) );
			}
		}

		//TODO refactoring

		public
		static function get_default_thumbail(
			$image = 1
		) {
			if ( $image == 2 ) {
				return untrailingslashit( plugins_url( '/', __FILE__ ) . 'templates/images/empty-image-cat.png' );
			}
			if ( $image == 3 ) {
				return untrailingslashit( plugins_url( '/', __FILE__ ) . 'assets/images/ajaxloader.svg' );
			}

			return untrailingslashit( plugins_url( '/', __FILE__ ) . 'templates/images/empty-image.png' );
		}

		/**
		 * Get the plugin url.
		 * @return string
		 */
		public
		function get_url() {
			return untrailingslashit( plugins_url( '/', __FILE__ ) );
		}

		/**
		 * Get the plugin path.
		 * @return string
		 */
		public
		static function get_path() {
			return untrailingslashit( plugin_dir_path( __FILE__ ) );
		}

		/**
		 * Get the template path.
		 * @return string
		 */
		public
		function get_template_path() {
			return untrailingslashit( plugin_dir_path( __FILE__ ) );

		}

		/**
		 * Get Ajax URL.
		 * @return string
		 */
		public
		function ajax_url() {
			return admin_url( 'admin-ajax.php', 'relative' );
		}


		public
		function load_templatel(
			$template
		) {

			global $wp, $wp_query;

			$queried_object_id = $this->get_queried_object_id();
			if ( is_singular( 'wppwp_projects' ) && ! $this->organiser_is_project_template( $template, 'wppwp_projects' ) ) {
				$template = WPPWP_PLUGIN_DIR . 'templates/single-project-template.php';
			}
			if ( is_post_type_archive( 'wppwp_projects' ) ) {
				$template = WPPWP_PLUGIN_DIR . 'templates/archive-project-template.php';
			}

			if ( is_tax( 'projects_category' ) && ! $this->organiser_is_project_template( $template, 'projects_category' ) ) {
				$template = WPPWP_PLUGIN_DIR . 'templates/projects-category.php';
			}

			if ( is_tax( 'projects_charity' ) && ! $this->organiser_is_project_template( $template, 'projects_charity' ) ) {
				$template = WPPWP_PLUGIN_DIR . 'templates/projects-charity.php';
			}
			$post_type = get_query_var( 'post_type' );
			if ( $wp_query->is_search && $post_type == 'wppwp_projects' ) {
				$template = WPPWP_PLUGIN_DIR . 'templates/search/archive-search.php';
			}
			$slug = ! empty( $wp_query->post->slug ) ? $wp_query->post->slug : '';

			if ( is_object( $wp_query->post ) ) {
				if ( $slug == 'search_page' ) {
					$template = WPPWP_PLUGIN_DIR . 'templates/search/search-projects.php';
				}
			}

			return $template;
		}


		public
		function organiser_is_project_template(
			$templatePath, $context = ''
		) {
			$template = basename( $templatePath );
			switch ( $context ):
				case 'wppwp_projects';
					return $template == 'single-event.php';
				case 'archive':
					return $template == 'archive-project-template.php';
				case 'event-venue':
					if ( ( 1 == preg_match( '/^taxonomy-event-venue((-(\S*))?).php/', $template ) || $template == 'venues-template.php' ) ) {
						return true;
					}

					return false;
				case 'projects_category':
					return ( 1 == preg_match( '/^projects-category((-(\S*))?).php/', $template ) );
				case 'projects_charity':
					return ( 1 == preg_match( '/^projects-charity((-(\S*))?).php/', $template ) );
				case 'event-tag':
					return ( 1 == preg_match( '/^taxonomy-event-tag((-(\S*))?).php/', $template ) );
			endswitch;

			return false;
		}

		public
		function get_queried_object_id() {
			global $wp_query, $wp;

			$queried_object_id = $wp_query->get_queried_object_id();


			if ( $wp_query->is_archive() ) {
				$queried_object_id = '';
			}

			return $queried_object_id;
		}


		/**
		 * Load localization files.
		 */
		public
		function load_plugin_textdomain() {
			load_plugin_textdomain( 'wp-pwp', false, plugin_basename( dirname( __FILE__ ) ) . '/languages' );
		}

		public
		static function load_resources() {
			global $wp_query;

			wp_register_script(
				self::PREFIX . 'admin-js',
				plugins_url( 'assets/js/admin.js', __FILE__ ),
				array( 'jquery' ),
				'1.0.0',
				true
			);
			wp_register_script(
				self::PREFIX . 'main-js',
				plugins_url( 'assets/js/main.js', __FILE__ ),
				array( 'jquery' ),
				'1.0.0',
				true
			);
			wp_register_style(
				self::PREFIX . 'main-css',
				plugins_url( 'assets/css/main.css', __FILE__ ),
				array(),
				'1.0.0',
				'all'
			);
			wp_register_style(
				self::PREFIX . 'main-admin-css',
				plugins_url( 'assets/css/main-admin.css', __FILE__ ),
				array(),
				'1.0.0',
				'all'
			);
			wp_register_script(
				self::PREFIX . 'admin-jquery-confirm',
				plugins_url( 'assets/js/jquery-confirm.js', __FILE__ ),
				array( 'jquery' ),
				'1.0.0',
				true
			);
			wp_register_script(
				self::PREFIX . 'search-project',
				plugins_url( 'assets/js/search-project.js', __FILE__ ),
				array( 'jquery' ),
				'1.0.0',
				true
			);
			wp_register_style(
				self::PREFIX . 'jquery-confirm-css',
				plugins_url( 'assets/css/jquery-confirm.css', __FILE__ ),
				array(),
				'1.0.0',
				'all'
			);
			wp_register_style(
				self::PREFIX . 'search-project-css',
				plugins_url( 'assets/css/search-project.css', __FILE__ ),
				array(),
				'1.0.0',
				'all'
			);

			wp_register_script(
				self::PREFIX .'pwp-ajax',
				plugins_url( 'assets/js/pwp-ajax.js', __FILE__ ),
				array( 'jquery' ),
				'1.0.0',
				true
			);

			if ( is_admin() ) {
				wp_enqueue_script( self::PREFIX . 'admin-js' );
				wp_enqueue_script( self::PREFIX . 'admin-jquery-confirm' );
				wp_enqueue_style( self::PREFIX . 'main-admin-css' );
				wp_enqueue_style( self::PREFIX . 'jquery-confirm-css' );


			}
			if ( is_tax( 'projects_category' ) || is_tax( 'projects_charity' ) || is_singular( 'wppwp_projects' ) || ( is_post_type_archive( 'wppwp_projects' ) && ! is_admin() ) ) {
				wp_enqueue_style( self::PREFIX . 'main-css' );

			}
			wp_enqueue_script( self::PREFIX . 'main-js' );


		}

		public function wppwp_cron_callback_function() {

			$this->Project_Setting->wppwp_get_all_company_callback();

		}

		public function activated() {
			wp_clear_scheduled_hook( 'wppwp_get_post_api' );


			wp_schedule_event( time(), 'twicedaily', 'wppwp_get_post_api' );
			if ( defined( 'DOING_CRON' ) && DOING_CRON ) {
				add_action( 'wppwp_get_prof_api', array( $this, 'wppwp_cron_callback_function' ) );

			}
		}

		public function wppwp_cron_settings() {

		}


	}
endif;

function WP_PWP() {
	register_activation_hook( __FILE__, 'wppwp_activate' );

	return WP_PWP::instance();

}

WP_PWP();


register_deactivation_hook( __FILE__, 'wppwp_deactivation' );

function wppwp_deactivation() {
	wp_clear_scheduled_hook( 'my_hourly_event' );
}

function wppwp_activate() {

	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = WP_PWP::$table_name;

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id bigint(20) NOT NULL AUTO_INCREMENT,
        first_page bigint(20) NOT NULL DEFAULT '1',
        limit_company bigint(10) NOT NULL DEFAULT '50',
        last_update datetime DEFAULT '0000-00-00 00:00:00',
        created_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        created_date_gmt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        modified_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        modified_date_gmt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        PRIMARY KEY  (id)
        ) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

}