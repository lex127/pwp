<?php
/*
Plugin Name: Welldone Shortcodes
Plugin URI: http://www.ethemeuk.com
Description: This plugin add shortcodes in welldone theme
Version: 1.0
Author: Etheme
Author URI: http://www.ethemeuk.com
License: GPL2
*/


function custom_recent_products_FX($atts, $content = null) {
    global $woocommerce_loop, $woocommerce;

    extract(shortcode_atts(array(
        'per_page'  => '12',
        'columns'   => '4',
        'orderby' => 'date',
        'order' => 'desc'
    ), $atts));

    $meta_query = $woocommerce->query->get_meta_query();

    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page' => $per_page,
        'orderby' => $orderby,
        'order' => $order,
        'meta_query' => $meta_query
    );

    ob_start();

    $products = new WP_Query( $args );

    $woocommerce_loop['columns'] = $columns;

    echo '<h2 class="text-center text-uppercase">Featured Products</h2>';

    $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content


    if ( $products->have_posts() ) : ?>

        <?php woocommerce_product_loop_start(); ?>
            <?php echo '<div class="row product-carousel mobile-special-arrows animated-arrows product-grid four-in-row">'; ?>

            <?php while ( $products->have_posts() ) : $products->the_post(); ?>


                <?php woocommerce_get_template_part( 'content', 'product-shortcode-1' ); ?>


            <?php endwhile; // end of the loop. ?>

            <?php echo '</div>'; ?>

        <?php woocommerce_product_loop_end(); ?>

    <?php endif;

    wp_reset_postdata();

    return ob_get_clean();
 }
add_shortcode('custom_recent_products','custom_recent_products_FX');

 // Categories Carousel

 function custom_products_categories($atts) {
global $woocommerce_loop;

        $atts = shortcode_atts( array(
            'per_page'     => null,
            'orderby'    => 'name',
            'order'      => 'ASC',
            'per_page'  => '8',
            'columns'    => '1',
            'hide_empty' => 1,
            'parent'     => '',
            'ids'        => ''
        ), $atts );

        if ( isset( $atts['ids'] ) ) {
            $ids = explode( ',', $atts['ids'] );
            $ids = array_map( 'trim', $ids );
        } else {
            $ids = array();
        }

        $hide_empty = ( $atts['hide_empty'] == true || $atts['hide_empty'] == 1 ) ? 1 : 0;

        // get terms and workaround WP bug with parents/pad counts
        $args = array(
            'orderby'    => $atts['orderby'],
            'order'      => $atts['order'],
            'hide_empty' => $hide_empty,
            'include'    => $ids,
            'pad_counts' => true,
            'child_of'   => $atts['parent']
        );

        $product_categories = get_terms( 'product_cat', $args );

        if ( '' !== $atts['parent'] ) {
            $product_categories = wp_list_filter( $product_categories, array( 'parent' => $atts['parent'] ) );
        }

        if ( $hide_empty ) {
            foreach ( $product_categories as $key => $category ) {
                if ( $category->count == 0 ) {
                    unset( $product_categories[ $key ] );
                }
            }
        }

        if ( $atts['per_page'] ) {
            $product_categories = array_slice( $product_categories, 0, $atts['per_page'] );
        }

        $columns = absint( $atts['columns'] );
        $woocommerce_loop['columns'] = $columns;

        ob_start();

        // Reset loop/columns globals when starting a new loop
        $woocommerce_loop['loop'] = $woocommerce_loop['column'] = '';

        echo '<h2 class="text-center text-uppercase">products categories</h2>';
        if ( $product_categories ) {
            // woocommerce_product_loop_start();

            echo '<div class="product-category-carousel mobile-special-arrows animated-arrows slick">';

            foreach ( $product_categories as $category ) {
                wc_get_template( 'content-product_cat.php', array(
                    'category' => $category
                ) );
            }

            echo '</div>';

            // woocommerce_product_loop_end();
        }



        woocommerce_reset_loop();

        return '<div class="woocommerce welldone-category-slider columns-' . $columns . '">' . ob_get_clean() . '</div>';
 }
 add_shortcode('custom_products_cats','custom_products_categories');



 function welldone_blog_slider($atts, $content = null) {
        $a = shortcode_atts(array(
        'span_size' => 6,
        'numberposts' => 5,
        'tax_query' => array(
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => 'post-format-aside',
            'operator' => 'NOT IN'
        ), 
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => 'post-format-quote',
            'operator' => 'NOT IN'
        ), 
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => 'post-format-video',
            'operator' => 'NOT IN'
        ),
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => 'post-format-audio',
            'operator' => 'NOT IN'
        ),
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => 'post-format-gallery',
            'operator' => 'NOT IN'
        ),
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => 'post-format-link',
            'operator' => 'NOT IN'
        ),
        array(
            'taxonomy' => 'post_format',
            'field'    => 'slug',
            'terms'    => 'post-format-image',
            'operator' => 'NOT IN'
        )
        ),
        'offset' => 0,
        'category' => 0,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' =>'',
        'post_type' => 'post',
        'post_status' => 'publish',
        'suppress_filters' => true
    ), $atts);

    $posts = wp_get_recent_posts( $a, ARRAY_A );

    $output = '';

    if ( !empty($posts) ) {

        $output .= '<div class="blog-widget">';

        add_filter('post_thumbnail_html', 'welldone_blog_slider_image_filter', 10, 5);
        
        $output .= '<h2 class="blog-widget__title text-uppercase">' . esc_html__('FROM THE BLOG', 'welldone') . '</h2>';

        $output .= '<div class="blog-carousel mobile-special-arrows  animated-arrows">';
            
        
        foreach ( $posts as $p ) {

                        
                        // add Az
                      
                        // end add Az

            // $content = explode('<!--more-->', $p['post_content']);

            $output .= '<div class="blog-widget__item">';

            // Output the featured image.
            if ( has_post_thumbnail($p['ID']) ) {

                $image = get_the_post_thumbnail($p['ID'], 'welldone-custom-size');

                $output .= '<div class="blog-widget__item__image-cell pull-left">' . $image . '</div>';

            }


            
            $countar = 60;
            $arr = explode( ' ', $p['post_content'] );
                
            if ( count($arr) > $countar ) {
                $arr = array_slice( $arr, 0, $countar );
                $text = implode( ' ', $arr );
            } else {
                $text = $p['post_content'];
            }
            unset($arr);
            
            $output .= '<div class="blog-widget__item__offset-text pull-right">
                        <h3 class="blog-widget__item__offset-text__title text-uppercase"> '. $p['post_title'] .' </h3>
                        <div class="blog-widget__item__offset-text__date"><span>' . get_the_date() . '</span></div>
                        <div class="blog-widget__item__offset-text__teaser"><p>' . $text . '</p></div>
                        <a href="' . esc_url(get_permalink($p['ID'])) . '" class="btn btn--wd">READ MORE</a>
                    </div></div>';
        }
        remove_filter('post_thumbnail_html', 'welldone_blog_slider_image_filter', 10);

        $output .= '</div></div>';

    }
    return $output;
}
add_shortcode('blog_slider', 'welldone_blog_slider');



// Brands Slider

if ( !function_exists('welldone_brands_slider') ) {
    function welldone_brands_slider($atts) {
        $a = shortcode_atts(array(
            'title' => esc_html__('Product Brands', "welldone"),
            'block_class' => 'brands_block',
            'block_id' => '',
            'hide_empty' => 0,
            'size' => 6,
        ), $atts);

        $output = '<div class="container">';

        if ( $a['title'] ) {
            $output .= '<h2 class="text-center text-uppercase">' . $a['title'] . '</h2>';
        }

        $output .= '<div class="brands carouFredSel row ' . $a['block_class'] . '" ' . ( $a['block_id'] ? 'id="' . $a['block_id'] . '"' : '' ) . '>';

        $args = array(
            'hide_empty' => $a['hide_empty']
        );
        $brands = welldone_get_product_brands($args);

        if ( !empty($brands) ) {

            // Slider controls
           

            $output .= '<div class="brands brands-carousel animated-arrows mobile-special-arrows size_'.$a["size"].'">';
            foreach ( $brands as $brand ) {

                $output .= '<div class="brands__item">';
                $output .= '<a href="' . esc_url(get_term_link( $brand->slug, 'brand')) . '">';

                $thumbnail_id   = absint( get_woocommerce_term_meta( $brand->term_id, 'thumbnail_id', true ) );
                if ($thumbnail_id) {
                    $output .= '<img src="' . welldone_get_image( $thumbnail_id ) . '" alt="' . $brand->name . '" title="' . $brand->name . '" />';
                }

                $output .= '</a>';
                $output .= '</div>';

            }
            $output .= '</div>';

        } else {
            $output .= '<p>' . esc_html__('There are no brands found', "welldone") . '</p>';
        }

        $output .= '</div></div>';

        return $output;
    }
}
add_shortcode('wft_brands_slider', 'welldone_brands_slider');



// currency switcher wpml

if ( function_exists('is_plugin_active') && is_plugin_active( 'woocommerce-multilingual/wpml-woocommerce.php' ) ) {

    function welldone_wpml_currency_switcher($atts) {

        global $woocommerce;

        // format tags
        // Name     - %name
        // Symbol   - %symbol
        // Code     - $code

        extract( shortcode_atts( array(
            'format' => '(%symbol) %name',
        ), $atts ) );

        include_once(home_url() .'/wp-content/plugins/woocommerce-multilingual/inc/multi-currency-support.class.php');

        $mc_obj = new WCML_Multi_Currency_Support;

        $wc_currencies = get_woocommerce_currencies();

        $exchange_rates = $mc_obj->get_exchange_rates();

        $current_currency = $mc_obj->get_client_currency();
        wc_enqueue_js("
            jQuery('.currency_switcher_value').click(function() {

                jQuery('#ajax_loader').show();
                var currency = jQuery(this).attr('data-currency');

                //jQuery('this').find();

                var data = {action: 'wcml_switch_currency', currency: currency, context: 'frontend'}
                jQuery.ajax({
                    url: woocommerce_params.ajax_url,
                    method: 'POST',
                    data: data,
                    complete: function(response) {
                        jQuery('#ajax_loader').hide();
                        //jQuery('.wcml_currency_switcher').removeAttr('disabled');
                        //jQuery('#ajax_loader').hide();
                        location.reload();
                    }
                });

            });
        ");

        echo '<span class="link_label">Currency:</span>';

        ?>

        <div class="fadelink currency_switcher">

            <a href="#" class="current_currency"><?php echo $wc_currencies[$current_currency]; ?></a>

            <div class="ul_wrapper" style="display: none;">
                <ul>
                    <?php foreach ( $exchange_rates as $currency => $rate ) {

                        $currency_format = preg_replace(array('#%name#', '#%symbol#', '#%code#'),
                            array($wc_currencies[$currency], get_woocommerce_currency_symbol($currency), $currency), $format);
                        ?>

                        <li class="currency_switcher_value" data-currency="<?php echo $currency; ?>">
                            <a href="#"><?php echo $currency_format; ?></a>
                        </li>

                    <?php } //endforeach; ?>

                </ul>
            </div>

        </div>

        <?php

    }

    add_shortcode('welldone_wpml_currency_switcher', 'welldone_wpml_currency_switcher');
}



add_shortcode('welldone_recent_products_small','custom_recent_products_small');
function custom_recent_products_small($atts, $content = null) {
    global $woocommerce_loop, $woocommerce, $product;;

    extract(shortcode_atts(array(
        'per_page'  => '12',
        'columns'   => '1',
        'orderby' => 'date',
        'order' => 'desc'
    ), $atts));

    $meta_query = $woocommerce->query->get_meta_query();

    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'ignore_sticky_posts'   => 1,
        'posts_per_page' => $per_page,
        'orderby' => $orderby,
        'order' => $order,
        'meta_query' => $meta_query
    );

    ob_start();

    $products = new WP_Query( $args );

    $woocommerce_loop['columns'] = $columns;
    

    echo '<h4 class="text-uppercase">Recent Products</h4>';

    $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content


    if ( $products->have_posts() ) : ?>

        <?php woocommerce_product_loop_start(); ?>
            <?php echo '<div class="products-widget card"><div class="products-widget-carousel nav-dot">'; ?>
        <?php
           while ( $products->have_posts() ) {
                $products->the_post();
                wc_get_template( 'content-widget-product.php', array( 'show_rating' => false ) );
            }
        ?>

            <?php echo '</div></div>'; ?>

        <?php woocommerce_product_loop_end(); ?>

    <?php endif;

    wp_reset_postdata();

    return ob_get_clean();
 }


   add_shortcode('welldone_onsale_products_small','custom_onsale_products_small');
function custom_onsale_products_small($atts, $content = null) {
    global $woocommerce_loop, $woocommerce;

    extract( shortcode_atts( array(
            'per_page'      => '12',
            'columns'       => '1',
            'orderby'       => 'title',
            'order'         => 'asc'
            ), $atts ) );
        // Get products on sale
        $product_ids_on_sale = woocommerce_get_product_ids_on_sale();
        $meta_query = array();
        $meta_query[] = $woocommerce->query->visibility_meta_query();
        $meta_query[] = $woocommerce->query->stock_status_meta_query();
        $args = array(
            'posts_per_page'=> $per_page,
            'orderby'       => $orderby,
            'order'         => $order,
            'no_found_rows' => 1,
            'post_status'   => 'publish',
            'post_type'     => 'product',
            'orderby'       => 'date',
            'order'         => 'ASC',
            'meta_query'    => $meta_query,
            'post__in'      => $product_ids_on_sale
        );

    ob_start();

    $products = new WP_Query( $args );

    $woocommerce_loop['columns'] = $columns;
    

    echo '<h4 class="text-uppercase">Sale Products</h4>';

    $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content


    if ( $products->have_posts() ) : ?>

        <?php woocommerce_product_loop_start(); ?>
            <?php echo '<div class="products-widget card"><div class="products-widget-carousel nav-dot">'; ?>
        <?php
           while ( $products->have_posts() ) {
                $products->the_post();
                wc_get_template( 'content-widget-product.php', array( 'show_rating' => false ) );
            }
        ?>

            <?php echo '</div></div>'; ?>

        <?php woocommerce_product_loop_end(); ?>

    <?php endif;

    wp_reset_postdata();

    return ob_get_clean();
 }




 add_shortcode( 'bartag', 'bartag_func' );
function bartag_func( $atts, $content = null ) { // New function parameter $content is added!
   extract( shortcode_atts( array(
      'foo' => 'something',
      'color' => '#FFF'
   ), $atts ) );
  
   $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
  
   return "<div style='color:{$color};' data-foo='${foo}'>{$content}</div>";
}


function wft_products_isotope_listing2($atts) {

    global $product_slide_size;

    $a = shortcode_atts(array(
        'per_page'  => '12',
        'title' => '',
        'size' => '3',
        'with_descr' => '1',
        'filter' => true,
                'category' => 'men,women,unisex',
    ), $atts);

    $product_slide_size = $a['size'];
    $per_page = (int) $a['per_page'];
        $category = $a['category'];
    ob_start();
    $products = welldone_get_recent_products($per_page, 'date', 'desc', $category);

            echo '<div class="modal quick-view zoom" id="quickView"  style="opacity: 1">
                <div class="modal-dialog">
                    <div class="modal-content"> </div>
                </div>
            </div>';
    if ( $products->have_posts() ) {

       
              

        if ( $a['title'] ) {
            echo '<h2>' . $a['title'] . '</h2>';
        }
        
        if ( $a['filter'] ) {
            echo '<section class="filters-by-category clearfix">
                    <div class="container">
                    <ul class="option-set" data-option-key="filter">
                      <li><a href="#filter" data-option-value="*" class="selected" data-per-page="' . $per_page . '">'. esc_html__('ALL', "welldone") . '</a></li>
                      <li><a href="#filter" data-option-value=".featured" data-per-page="' . $per_page . '">'. esc_html__('FEATURED', "welldone") . '</a></li>
                      <li><a href="#filter" data-option-value=".new_product" data-per-page="' . $per_page . '">'. esc_html__('NEW PRODUCTS', "welldone") . '</a></li>
                      <li><a href="#filter" data-option-value=".sale" data-per-page="' . $per_page . '">'. esc_html__('ON SALE', "welldone") . '</a></li>
                    </ul>
                    </div>
                  </section>';

        }

        echo '<div class="products-grid products-listing products-col products-isotope four-in-row">';

        while ( $products->have_posts() ) {

            $products->the_post();

            woocommerce_get_template_part( 'content', 'product' );

        } // end of the loop.

        echo '</div>';

    }

    wp_reset_postdata();

    return '<section class="no-padding-bottom content slider-products product_isotope_slider"><div class="row">' . ob_get_clean() . '</div></section>';

}

add_shortcode('products_isotope_listing2', 'wft_products_isotope_listing2');


?>
