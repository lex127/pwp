<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * The Search Terms Class.
 */
class WCSTM_Term {

	private static $instance;
	/**
	 * @var wpdb
	 */
	protected $wpdb;

	/**
	 * @var WCSTM_Utils
	 */
	protected $utils;

	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	private function __construct() {
		require_once WCSTM_PLUGIN_DIR . 'includes/class-wcstm-utils.php';
		$this->wpdb  = $this->get_wpdb();
		$this->utils = new WCSTM_Utils();
	}

	protected function get_wpdb() {
		return $GLOBALS['wpdb'];
	}

	final public function __clone() {
		throw new Exception( 'Clone is forbidden' );
	}

	final public function __wakeup() {
		throw new Exception( '__wakeup usage is forbidden' );
	}

	public function is_redirect( $term ) {
		if ( ! isset( $term['redirect'] ) || ! (int) $term['redirect'] ) {
			return false;
		}

		return true;
	}

	public function get_term_link( $term ) {
		$redirect_obj_type = isset( $term['redirect_obj_type'] ) ? $term['redirect_obj_type'] : '';
		switch ( true ) {
			case $redirect_obj_type == 'custom_link':
				return $this->get_term_link_url( $term );
			case $redirect_obj_type == 'post_type':
				return get_permalink( $term['redirect_obj_id'] );
			case $redirect_obj_type == 'taxonomy':
				return get_term_link( (int) $term['redirect_obj_id'] );
			case ( isset( $_GET['suggestion_id'] ) && get_option( 'autocomplete_redirect' ) == 'item_page' ):
				return get_permalink( esc_attr( $_GET['suggestion_id'] ) );
			default:
				return $this->get_term_link_for_search( $term );
		}
	}

	public function get_term_link_for_search( $term ) {
		return home_url() . '/?' . http_build_query( array( "s" => $term['term'] ) );
	}

	public function get_table_name() {
		return WCSTM::$table_name;
	}

	public function get_term_by_id( $id ) {
		return $this->wpdb->get_row( "SELECT * FROM " . WCSTM::$table_name . " WHERE id='$id'", ARRAY_A );
	}

	public function get_term_by_name( $name ) {
		$query = $this->wpdb->prepare( "SELECT * FROM {$this->get_table_name()} WHERE term='%s'", $name );

		return $this->wpdb->get_row( $query, ARRAY_A );
	}

	public function get_terms_count() {
		return $this->wpdb->get_var( "SELECT COUNT(id) FROM " . WCSTM::$table_name );
	}

	public function get_terms_count_where( $where ) {
		return $this->wpdb->get_var( "SELECT COUNT(id) FROM " . WCSTM::$table_name . ' ' . $where );
	}

	public function get_terms( $orderby, $order, $args = array(), $offset = 0 ) {
		$per_page = isset( $args['number'] ) ? $args['number'] : 0;
		$where = '';
		$where .= apply_filters( 'term_filter', $where );
		if( isset( $args['wcs'] ) && (int)$args['wcs'] ){
			$where .= ' AND wcs = 1';
		}
		$limit = ( $per_page ) ? "LIMIT $offset, $per_page" : '';
		$query = "SELECT * FROM " . WCSTM::$table_name . " $where ORDER BY $orderby $order $limit";

		return $this->wpdb->get_results( $query, ARRAY_A );
	}

	public function delete_terms( $ids ) {
		$this->wpdb->query( "DELETE FROM " . WCSTM::$table_name . " WHERE id IN($ids)" );
	}

	public function insert_term( $item ) {
		$this->wpdb->insert( WCSTM::$table_name, $item );

		return $this->wpdb->insert_id;
	}

	public function update_term( $item ) {
		return $this->wpdb->update( WCSTM::$table_name, $item, array( 'id' => $item['id'] ) );
	}

	public function get_term_link_url( $item ) {
		$link = get_bookmark( $item['redirect_obj_name'] );

		return $link->link_url;
	}

	public function get_empty_term() {
		$empty_date = '0000-00-00 00:00:00';

		return array(
			'id'                   => null,
			'term'                 => '',
			'count'                => 0,
			'results'              => 0,
			'redirect'             => 0,
			'redirect_obj_type'    => 0,
			'redirect_obj_name'    => 0,
			'redirect_obj_id'      => 0,
			'last_search_date'     => $empty_date,
			'last_search_date_gmt' => $empty_date,
			'created_date'         => current_time( 'mysql' ),
			'created_date_gmt'     => current_time( 'mysql', 1 ),
			'modified_date'        => current_time( 'mysql' ),
			'modified_date_gmt'    => current_time( 'mysql', 1 )
		);
	}

	public function get_months() {
		return $this->wpdb->get_results( "SELECT DISTINCT YEAR( created_date ) AS year, MONTH( created_date ) AS month
			FROM " . $this->get_table_name() . " ORDER BY created_date DESC" );
	}

}