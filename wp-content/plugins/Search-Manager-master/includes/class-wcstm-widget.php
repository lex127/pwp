<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Widget {

	public function __construct() {
		add_action( 'widgets_init', array( $this, 'wcstm_register_widgets' ) );
		add_filter( 'term_filter', array( $this, 'get_where_query' ) );
	}

	public function wcstm_register_widgets() {
		register_widget( 'WCSTM_Widget_Recent' );
		register_widget( 'WCSTM_Widget_Popular' );
		register_widget( 'WCSTM_Widget_Cloud' );
	}

	public function get_where_query() {
		return ' WHERE ( (redirect_obj_id IS NOT NULL'
		       . ' AND redirect_obj_id <> 0) OR results <> 0'
		       . ' OR redirect_obj_type="custom_link" )';
	}

}