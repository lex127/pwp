<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Shortcode {
	private $term_model;

	public function __construct() {

		$this->term_model = WCSTM_Term::get_instance();

		add_shortcode( 'wcstm_search_cloud', array( $this, 'cloud_search' ) );
		add_shortcode( 'wcstm_popular_search', array( $this, 'popular_search' ) );
		add_shortcode( 'wcstm_recent_search', array( $this, 'recent_search' ) );
		add_shortcode( 'wcstm_search_form', array( $this, 'search_form' ) );

	}

	function cloud_search( $atts ) {

		$cloud_widget = new WCSTM_Widget_Cloud();
		ob_start();
		?>
		<section>
			<?php if( !empty( $atts['title'] ) ): ?>
				<h2 class="title"><?php echo $atts['title'] ?></h2>
			<?php endif; ?>
			<?php echo $cloud_widget->get_search_terms_cloud( $atts ); ?>
		<section>
		<?php

		return ob_get_clean();
	}

	function popular_search( $atts ) {

		$options = shortcode_atts( array(
			'title'  => __( 'Popular Search Terms' ),
			'number' => 5
		), $atts );

		$popular_terms = $this->term_model->get_terms( 'count', 'DESC', $options['number'] );
		ob_start();
		?>
		<section>
			<h2 class="title"><?php echo $options['title'] ?></h2>
			<ul>
				<?php foreach ( $popular_terms as $term ): ?>
					<li>
						<a href="<?php echo $this->term_model->get_term_link_for_search( $term ); ?>"><?php echo esc_attr( $term['term'] ) ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
		</section>
		<?php

		return ob_get_clean();
	}

	function recent_search( $atts ) {

		$options       = shortcode_atts( array(
			'title'  => __( 'Recent Search Terms' ),
			'number' => 5
		), $atts );

		$popular_terms = $this->term_model->get_terms( 'last_search_date', 'DESC', $options['number'] );
		ob_start();
		?>
		<section>
			<h2 class="title"><?php echo $options['title'] ?></h2>
			<ul>
				<?php foreach ( $popular_terms as $term ): ?>
					<li>
						<a href="<?php echo $this->term_model->get_term_link_for_search( $term ); ?>"><?php echo esc_attr( $term['term'] ) ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
		</section>
		<?php

		return ob_get_clean();
	}

	function search_form( $atts ) {

		ob_start();
		if( isset($atts['wcps']) && $atts['wcps'] && class_exists( 'woocommerce' )) {
			the_widget( 'WC_Widget_Product_Search', 'title=' );
		} else {
			get_search_form();
		}

		return ob_get_clean();
	}

}