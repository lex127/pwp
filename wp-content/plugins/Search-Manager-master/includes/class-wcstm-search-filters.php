<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Search_Filters {

	private static $instance;

	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function is_filter( $option, $query ) {
		if ( $query->is_main_query()
		     && false === is_admin()
		     && get_option( $option )
		     && is_search()
		     && $query->get( 's' )
		) {
			return true;
		}

		return false;
	}

	public function is_wc_search( $query ) {
		return isset( $query->query['post_type'] ) && 'product' == $query->query['post_type'];
	}

	public function search_in_post_comments( $where, $query ) {
		global $wp, $wpdb;

		if ( $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_in_post_comments', $query ) ) {

			$post_ids = array();

			$comments = new WP_Comment_Query( array(
				'search'    => $query->get( 's' ),
				'post_type' => 'post'
			) );
			foreach ( $comments->comments as $comment ) {
				$post_ids[] = $comment->comment_post_ID;
			}
			if ( $post_ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $post_ids ) . "))";
			}
		}

		return apply_filters( 'wcstm_search_in_post_comments', $where );
	}

	public function search_in_product_comments( $where, $query ) {
		global $wp, $wpdb;

		if ( false === $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_product_comments_status', $query ) ) {

			$post_ids = array();

			$comments = new WP_Comment_Query( array(
					'search'    => $query->get( 's' ),
					'post_type' => 'product'
				)
			);
			foreach ( $comments->comments as $comment ) {
				$post_ids[] = $comment->comment_post_ID;
			}
			if ( $post_ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $post_ids ) . "))";
			}
		}

		return apply_filters( 'wcstm_search_in_product_comments', $where );
	}

	public function search_in_product_sku( $where, $query ) {
		global $wpdb;

		if ( false === $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_sku_status', $query ) ) {

			$args = array(
				'posts_per_page' => - 1,
				'fields'         => 'ids',
				'post_type'      => 'product',
				'meta_query'     => array(
					array(
						'key'     => '_sku',
						'value'   => $query->get( 's' ),
						'compare' => 'LIKE'
					)
				),
			);

			$product_ids = get_posts( $args );

			if ( $product_ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $product_ids ) . "))";
			}
		}

		return apply_filters( 'wcstm_search_in_product_sku', $where );
	}

	public function search_in_product_variations_sku( $where, $query ) {
		global $wpdb, $wp_query;

		if ( false === $this->is_wc_search( $query ) ) {
		  return $where;
		}

		if ( $this->is_filter( 'search_sku_status_variations', $query ) ) { //

			$args = array(
				'posts_per_page' => - 1,
				'post_type'      => 'product_variation',
				'fields'         => 'ids',
				'meta_query'     => array(
					array(
						'key'     => '_sku',
						'value'   => $query->get( 's' ),
						'compare' => 'LIKE'
					)
				),
			);

			$product_ids = get_posts( $args );

			if ( $product_ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $product_ids ) . "))";
			}

		}

	    return apply_filters( 'wcstm_search_in_product_variations_sku', $where );
	}

	public function search_in_short_desc( $where, $query ) {
		global $wpdb;

		if ( false === $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_short_desc_status', $query )	) {
			$where .= " OR ({$wpdb->posts}.post_excerpt LIKE '%{$query->get('s')}%'
			AND {$wpdb->posts}.post_status = 'publish') ";
		}

		return apply_filters( 'wcstm_search_in_short_desc', $where );
	}

	public function search_in_excerpt( $where, $query ) {
		global $wpdb;

		if ( $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_in_excerpts', $query ) ) {
			$where .= " AND ( {$wpdb->posts}.post_excerpt LIKE '%{$query->get('s')}%'
			OR ({$wpdb->posts}.post_title LIKE '%{$query->get('s')}%')
			OR ({$wpdb->posts}.post_content LIKE '%{$query->get('s')}%') ) ";
		}

		return apply_filters( 'wcstm_search_in_excerpt', $where );
	}
	
	/**
	 * Search by Slug
	 * 
	 * @global object $wpdb
	 * @param string $where
	 * @param strng $query
	 * @return string
	 */
	public function search_in_slug( $where, $query ) {
		global $wpdb;

		if ( $this->is_wc_search( $query ) ) {
			return $where;
		}
		
		if ( $this->is_filter( 'search_in_slug', $query ) ) {
			$where .= " OR ( {$wpdb->posts}.post_name LIKE '%{$query->get('s')}%'
			OR ({$wpdb->posts}.post_title LIKE '%{$query->get('s')}%')
			OR ({$wpdb->posts}.post_content LIKE '%{$query->get('s')}%') ) ";
		}

		return apply_filters( 'wcstm_search_in_slug', $where );
	}
	
	/**
	 * Hide out of stock products
	 * 
	 * @global object $wpdb
	 * @param string $where
	 * @param string $query
	 * @return string
	 */
	public function hide_out_of_stock( $where, $query ) {
		global $wpdb;

		if ( false === $this->is_wc_search( $query ) ) {
			return $where;
		}
		if ( $this->is_filter( 'hide_out_of_stock', $query ) ) {
			$where .= " AND post_id in ( "
			          . "SELECT {$wpdb->postmeta}.post_id FROM {$wpdb->postmeta} "
			          . "WHERE `meta_key` = '_stock_status' AND `meta_value` = 'instock')";
		}

		return apply_filters( 'wcstm_hide_out_of_stock', $where );
	}

	public function search_in_product_category( $where, $query ) {
		global $wpdb;

		if ( false === $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_product_cat_status', $query ) ) {
			$product_cat_ids = array();
			$product_cats    = get_terms( 'product_cat', array( 'name__like' => $query->get( 's' ) ) );
			foreach ( $product_cats as $cat ) {
				$product_cat_ids[] = $cat->term_id;
			}

			$args = array(
				'posts_per_page' => - 1,
				'fields'         => 'ids',
				'post_type'      => 'product',
				'tax_query'      => array(
					array(
						'taxonomy' => 'product_cat',
						'field'    => 'id',
						'terms'    => $product_cat_ids,
					),
				)
			);

			$product_ids = get_posts( $args );

			if ( $product_ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $product_ids ) . "))";
			}
		}

		return apply_filters( 'wcstm_search_in_product_category', $where );
	}

	public function search_in_post_category( $where, $query ) {
		global $wpdb;

		if ( false === $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_in_post_category', $query ) ) {
			$product_cat_ids = array();
			$product_cats    = get_terms( 'category', array( 'name__like' => $query->get( 's' ) ) );
			foreach ( $product_cats as $cat ) {
				$product_cat_ids[] = $cat->term_id;
			}

			$args = array(
				'posts_per_page' => - 1,
				'fields'         => 'ids',
				'post_type'      => 'post',
				'tax_query'      => array(
					array(
						'taxonomy' => 'category',
						'field'    => 'id',
						'terms'    => $product_cat_ids,
					),
				)
			);

			$product_ids = get_posts( $args );

			if ( $product_ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $product_ids ) . "))";
			}
		}

		return apply_filters( 'wcstm_search_in_post_category', $where );
	}

	public function search_in_product_tags( $where, $query ) {
		global $wpdb;

		if ( false === $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_product_tags_status', $query ) ) {
			$product_tax_ids = array();
			$product_tax     = get_terms( 'product_tag', array( 'name__like' => $query->get( 's' ) ) );
			foreach ( $product_tax as $tax ) {
				$product_tax_ids[] = $tax->term_id;
			}

			$args = array(
				'posts_per_page' => - 1,
				'fields'         => 'ids',
				'post_type'      => 'product',
				'tax_query'      => array(
					array(
						'taxonomy' => 'product_tag',
						'field'    => 'id',
						'terms'    => $product_tax_ids,
					),
				)
			);

			$product_ids = get_posts( $args );

			if ( $product_ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $product_ids ) . "))";
			}
		}

		return apply_filters( 'wcstm_search_in_product_tags', $where );
	}

	public function search_in_post_tags( $where, $query ) {
		global $wpdb;

		if ( $this->is_wc_search( $query ) ) {
			return $where;
		}

		if ( $this->is_filter( 'search_in_post_tags', $query ) ) {
			$tax_ids  = array();
			$post_tax = get_terms( 'post_tag', array( 'name__like' => $query->get( 's' ) ) );
			foreach ( $post_tax as $tax ) {
				$tax_ids[] = $tax->term_id;
			}

			$args = array(
				'posts_per_page' => - 1,
				'fields'         => 'ids',
				'post_type'      => 'post',
				'tax_query'      => array(
					array(
						'taxonomy' => 'post_tag',
						'field'    => 'id',
						'terms'    => $tax_ids,
					),
				)
			);

			$ids = get_posts( $args );

			if ( $ids ) {
				$where .= " OR ({$wpdb->posts}.ID IN (" . implode( ',', $ids ) . "))";
			}
		}

		return apply_filters( 'wcstm_search_in_post_tags', $where );
	}

	public function exclude_post_types_from_search( $query ) {
		global $wp;

		if ( ! is_admin() && $query->is_main_query() && $query->is_search ) {
			if ( is_array( get_option( 'post_types_excluded_from_search' ) )
			     && 'product' !== get_query_var( 'post_type' )
			) {
				$query->set( 'post_type',
					array_diff( get_post_types( array( 'public' => true ) ), get_option( 'post_types_excluded_from_search' ) )
				);
			}
			if ( isset( $query->query_vars['post_type']['product'] ) && is_array( $query->query_vars['post_type'] ) ) {
				unset( $query->query_vars['post_type']['product'] );
				$query->query_vars['post_type'] = array( 'product' => 'product' ) + $query->query_vars['post_type'];
			}
		}
	}

	public function wcstm_modif_query( $q ) {
		if ( $title = $q->get( '_meta_or_title' ) ) {
			add_filter( 'get_meta_sql', function ( $sql ) use ( $title ) {
				global $wpdb;

				// Only run once:
				static $nr = 0;
				if ( 0 != $nr ++ ) {
					return $sql;
				}

				// Modify WHERE part:
				$sql['where'] = sprintf(
					" AND ( %s OR %s ) ",
					$wpdb->prepare( "{$wpdb->posts}.post_title like '%%%s%%'", $title ),
					mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
				);

				return $sql;
			} );
		}
	}

}