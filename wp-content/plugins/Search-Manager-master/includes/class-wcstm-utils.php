<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * The WooCommerce Search Terms Manager Core Plugin Class.
 */
class WCSTM_Utils {

	public function html_2_rgb( $color ) {

		if ( $color[0] == '#' ) {
			$color = substr( $color, 1 );
		}

		if ( strlen( $color ) == 6 ) {
			list( $r, $g, $b ) = array (
				$color[0] . $color[1],
				$color[2] . $color[3],
				$color[4] . $color[5],
			);
		} elseif ( strlen( $color ) == 3 ) {
			list( $r, $g, $b ) = array (
				$color[0] . $color[0],
				$color[1] . $color[1],
				$color[2] . $color[2],
			);
		} else {
			return false;
		}

		$r = hexdec( $r );
		$g = hexdec( $g );
		$b = hexdec( $b );

		return array ( $r, $g, $b );
	}

	public function get_popular_search_default_atts( $atts ) {
		return shortcode_atts( array (
			'smallest' => 10,
			'largest'  => 20,
			'unit'     => 'px',
			'cold'     => 'CCCCCC',
			'hot'      => '000000',
			'before'   => '',
			'after'    => '&nbsp',
			'number'   => 10,
			'wcs'      => 0
		), $atts );
	}

	public function get_taxanomies( $type ) {
		$data  = array ();
		$terms = get_terms( $type );
		foreach ( $terms as $term ) {
			array_push( $data, array (
				'data'  => $term->term_id,
				'value' => $term->name
			) );
		}

		return $data;
	}

	public function get_posts( $type, $limit = '-1', $query = false ) {
		$data = array ();
		$limit = get_option( 'ac_lookup_limit' );
		$args = array (
			'post_type'      => $type,
			'post_status'    => 'publish',
			'posts_per_page' => $limit
		);
		if ( $query ) {
			$args['s'] = $query;
		}
		$my_query = new WP_Query( $args );
		foreach ( $my_query->get_posts() as $item ) {
			array_push( $data, array (
				'data'  => $item->ID,
				'value' => $item->post_title
			) );
		}

		return $data;
	}

	public function get_posts_autocomplete( $type, $limit, $query = false ) {
		if ( empty( $limit ) ) {
			$limit = '-1';
		}
		$meta_query    = array();
		// The Query
		$args['posts_per_page'] = $limit;
		$args['post_type']      = $type;
		$args['post_status']    = 'publish';

		if ( ( get_option( 'search_sku_status' ) == 'on' ) ) {
			$meta_query[] = array (
				'key'     => '_sku',
				'value'   => $query,
				'compare' => 'LIKE'
			);
			$args['_meta_or_title'] = $query;
			$args['meta_query']     = $meta_query;
		} else {
			$args['s'] = $query;
		}

		$the_query = new WP_Query( $args );

		return $the_query->posts;
	}


	public function get_request_var( $var, $default = "" ) {
		return isset( $_REQUEST[ $var ] ) ? $_REQUEST[ $var ] : $default;
	}

	public function is_woocommerce() {
		if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			return true;
		}

		return false;
	}

}