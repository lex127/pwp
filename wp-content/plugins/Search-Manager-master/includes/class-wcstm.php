<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * The WooCommerce Search Terms Manager Core Plugin Class.
 */
class WCSTM {

	const DB_VERSION = '1.0';
	const PLUGIN_VERSION = '1.0';

	const PERMANENT_REDIRECT = '301';
	const TEMPORARY_REDIRECT = '302';

	public static $settings = false;
	public static $table_name = false;

	/**
	 * @var WCSTM_Term
	 */
	public $term_model;

	/**
	 * @var WCSTM_Utils
	 */
	private $utils;
	private $additional_filters;

	private static $instance;

	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	public function __construct() {
		global $wpdb;

		$this->term_model = WCSTM_Term::get_instance();

		if ( is_admin() ) {
			new WCSTM_Admin( $this->term_model );
		}

		self::$settings   = get_option( 'wcstm_settings' );
		self::$table_name = $wpdb->prefix . 'wcstm_searchterms';

		new WCSTM_Shortcode();
		new WCSTM_Widget();

		add_action( 'wp_enqueue_scripts', array ( $this, 'enqueue_scripts' ) );
		add_action( 'template_redirect', array ( $this, 'app' ) );

		add_action( 'wp_ajax_nopriv_ajax_search', array ( $this, 'get_autocomplete_data' ) );
		add_action( 'wp_ajax_ajax_search', array ( $this, 'get_autocomplete_data' ) );

		$this->additional_filters = WCSTM_Search_Filters::get_instance();

		//wordpress filters
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_excerpt' ), 10, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_post_comments' ), 10, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_post_category' ), 10, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_post_tags' ), 10, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_slug' ), 10, 2 );

		//woocommerce filters
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_short_desc' ), 20, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_product_sku' ), 7, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_product_comments' ), 10, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_product_category' ), 10, 2 );
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_product_tags' ), 10, 2 );
		//add_filter( 'posts_where', array ( $this->additional_filters, 'hide_out_of_stock' ), 8, 2 );
		
		
		//variable sku
		add_filter( 'posts_where', array ( $this->additional_filters, 'search_in_product_variations_sku' ), 20, 2 );
		add_action( 'template_redirect', array ( $this, 'wcstm_page_template_redirect' ) );
		add_action( 'pre_get_posts', array ( $this->additional_filters,'wcstm_modif_query'), 10, 1 ); //Search by title and meta simultaneously
		//Template variable sku
		add_action( 'init', array ( $this, 'wcstm_woocommerce_remove' ), 15, 2 );
		add_action( 'woocommerce_before_shop_loop_item', array ( $this, 'wcstm_woocommerce_template_item' ), 10 );
		add_action( 'woocommerce_shop_loop_item_title', array ( $this, 'wcstm_woocommerce_template_title'), 10 );

		add_filter( 'post_class', array ( $this,'wcstm_add_css_class') );
		///end
		add_filter( 'pre_get_posts', array ( $this->additional_filters, 'exclude_post_types_from_search' ) );

		add_filter( 'plugin_action_links_' . WCSTM_BASENAME, array ( $this, 'action_links' ) );

		$this->utils = new WCSTM_Utils();

	}

	public function enqueue_scripts() {
		if( $this->is_ac_enabled() ) {
			wp_enqueue_script( 'sm_typeahead', WCSTM_PLUGIN_URL . 'js/typeahead.bundle.min.js', array ( 'jquery' ), '', true );
			wp_enqueue_script( 'sm_handlebars', WCSTM_PLUGIN_URL . 'js/handlebars.min.js', array ( 'jquery' ), '', true );
			wp_enqueue_script( 'wcstm', WCSTM_PLUGIN_URL . 'js/wcstm.js', array ( 'sm_handlebars' ), self::PLUGIN_VERSION, true );
			wp_localize_script( 'wcstm', 'wcstm',
				array (
					'ajax_url'  => admin_url( 'admin-ajax.php' ),
					'selector'  => get_option( 'search_field_selector' ),
					'minChars'  => get_option( 'ac_min_chars' ),
					'limit'     => get_option( 'ac_lookup_limit' ),
					'text'      => get_option( 'no_search_result_field' ) ? get_option( 'no_search_result_field' ) : __( 'No results', 'wcstm' ),
					'hint'      => get_option( 'ac_check_hint' ) ? get_option( 'ac_check_hint' ) : false ,
				)
			);

			wp_enqueue_style( 'wcstm_autocomplete_styles', WCSTM_PLUGIN_URL . 'css/styles.css' );
		}
	}

	public function get_autocomplete_data() {
		$results    = array ();
		$base_url   = get_site_url();
		$limit      = get_option( 'ac_lookup_limit' );
		$query      = (string) $_REQUEST['terms'];
		$post_types = get_post_types( array ( 'public' => true ) );
		$ex_p_t     = get_option( 'post_types_excluded_from_search' );
		$post_types = array_diff($post_types,$ex_p_t );	//excluded post type from search setting

		if ( get_option( 'search_sku_status' ) == 'on' AND get_option( 'ac_hide_variations' ) == 'on' ) {
			$post_types['product_variation'] = 'product_variation';
		}
		//excluded post type from autocomplete setting
		if ( get_option( 'autocomplete_posttypes' ) ) {
			$post_types = array_diff( $post_types, get_option( 'autocomplete_posttypes' ) );
		}

		$data_post_types = $this->utils->get_posts_autocomplete( $post_types, $limit, $query );

		if ( $data_post_types ) {

			foreach ( $data_post_types as $the_post ) {

				$title = get_the_title( $the_post->ID );
				$price = false;

				if ( get_option( 'autocomplete_redirect' ) == 'search_page' ) {
					$data_url = $base_url . '?' . http_build_query( array ( 's' => $title ) );
				} else {
					$data_url = get_permalink( $the_post->ID );
				}
				//Enable Post Thumbnail
				if ( get_option( 'autocomplete_post_thumbnail' ) == 'on' ) {
					$image = get_the_post_thumbnail_url( $the_post->ID, 'thumbnail', true );
				}
				//Enable Price WooCommerce
				if ( ( $this->utils->is_woocommerce() ) && ( get_option( 'search_product_price_status' ) == 'on' ) ) {
					if ( $the_post->post_type == 'product' ) {
						$product = wc_get_product( $the_post );
						$price   = $product->get_price_html();
					}
				}
				/// Fix WooCommerce URL and Tittle
				if ( $the_post->post_type == 'product_variation' ) {
					$wc_prod_var_o = new WC_Product_Variation( $the_post->ID );
					$title         = $the_post->title = $wc_prod_var_o->get_title();
					$data_url      = $the_post->link = $wc_prod_var_o->get_permalink();
				}

				$results[] = array (
					'value'  => html_entity_decode( $title ),
					'url'    => $data_url,
					'tokens' => explode( ' ', $title ),
					'image'  => $image,
					'price'  => $price,
				);
			}

		}

		wp_send_json( $results );

		die();
	}

	public function is_ac_enabled() {
		if ( get_option( 'is_auto_complete' ) == 'on' ) {
			return true;
		}

		return false;
	}

	public function app() {
		if ( is_search() && get_search_query() ) {
			$term         = $this->track_term( get_search_query() );
			$redirect_url = ( $this->term_model->is_redirect( $term ) || isset( $_GET['suggestion_id'] ) ) ? $this->term_model->get_term_link( $term ) : false;
			if ( $redirect_url ) {
				$this->redirect( $redirect_url );
			}
		}
	}

	public function redirect( $url ) {
		$target_url = $url;
		wp_redirect( esc_url_raw( $target_url ), apply_filters( 'wcstm_redirect_type', self::TEMPORARY_REDIRECT ) );
		exit();
	}

	public function track_term( $search_term ) {
		$args = array (
			's' => trim( $search_term ),
		);

		$the_query = new WP_Query( $args );
		$item      = $this->term_model->get_term_by_name( $search_term );

		if ( is_null( $item ) ) {
			$item = array (
				'term'                 => sanitize_text_field( $search_term ),
				'count'                => 1,
				'wcs'                 => (int)( 'product' == get_query_var( 'post_type' ) ),
				'results'              => $the_query->found_posts,
				'last_search_date'     => current_time( 'mysql' ),
				'last_search_date_gmt' => current_time( 'mysql', 1 ),
				'created_date'         => current_time( 'mysql' ),
				'created_date_gmt'     => current_time( 'mysql', 1 ),
				'modified_date'        => current_time( 'mysql' ),
				'modified_date_gmt'    => current_time( 'mysql', 1 )
			);

			$this->term_model->insert_term( $item );
		} else {
			$item['count'] += 1;
			$item['wcs'] = (int)( 'product' == get_query_var( 'post_type' ) );
			$item['results']              = $the_query->found_posts;
			$item['last_search_date']     = current_time( 'mysql' );
			$item['last_search_date_gmt'] = current_time( 'mysql', 1 );

			$this->term_model->update_term( $item );
		}

		return $item;
	}

	public function action_links( $links ) {

		$links[] = '<a href="' . esc_url( get_admin_url( null, 'admin.php?page=wcstm_settings' ) ) . '">Settings</a>';

		return $links;
	}

    /*
     *  Redirect on product if enter full SKU
     */
	public function wcstm_page_template_redirect() {
		global $wp_query;

		$post_type   = is_object( $wp_query->post ) ? $wp_query->post->post_type : "";
		$posts_array = is_array( $wp_query->posts ) ? $wp_query->posts : "";
		if ( is_search() && $post_type == 'product_variation'
		     && count( $posts_array ) == 1
		) {
			foreach ( $posts_array as $k => $post_obj ) {
				$wc_prod_var_o   = new WC_Product_Variation( $post_obj->ID );
				$post_obj->link  = $wc_prod_var_o->get_permalink();
			}
			wp_redirect( $post_obj->link );
			exit();
		}
	}

	/*
	 * Remove standard WooCommerce action
	 */
	public function wcstm_woocommerce_remove() {
		remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
		remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
	}

	/*
	 * Modified product_variation link
	 */
	public function wcstm_woocommerce_template_item() {
		global $wp_query;

		$posts_array = is_array( $wp_query->posts ) ? $wp_query->posts : "";

			$post = $wp_query->post;
			$link = get_permalink();
			if ( $post->post_type == 'product_variation' ) {

				$wc_prod_var_o   = new WC_Product_Variation( $post->ID );
				$post->title = $wc_prod_var_o->get_title();
				$link  = $wc_prod_var_o->get_permalink();

			}  ?> <a href="<?php echo $link; ?>"> <?php
	}

	public function wcstm_woocommerce_template_title() {
		global $wp_query;
		$posts_array = is_array( $wp_query->posts ) ? $wp_query->posts : "";
		foreach ( $posts_array as $k => $post_obj ) {

			$title = get_the_title();
			if ( $post_obj->post_type == 'product_variation' ) {
				$wc_prod_var_o   = new WC_Product_Variation( $post_obj->ID );
				$title = $wc_prod_var_o->get_title();

			}

		}
		?><h3><?php echo $title; ?></h3><?php

	}

	public function wcstm_add_css_class( $classes ) {
		global $post;
		if ( $post->post_type == 'product_variation' ) {
			$classes[] = 'product';
		}

		return $classes;
	}

}