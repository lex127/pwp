<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Widget_Recent extends WP_Widget {

	/**
	 * @var WCSTM_Term
	 */
	private $term_model;

	public function __construct() {
		$this->term_model = WCSTM_Term::get_instance();
		$widget_ops       = array( 'classname'   => 'widget_recent_entries',
		                           'description' => __( "Your site&#8217;s most recent search terms." )
		);
		parent::__construct( 'recent-terms', __( 'Search Manager: Recent Search Terms' ), $widget_ops );
		$this->alt_option_name = 'widget_recent_search_terms';
	}

	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Search Terms' );

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number ) {
			$number = 5;
		}

		$recent_terms = $this->term_model->get_terms( 'last_search_date', 'DESC', $number );

		if ( count( $recent_terms ) ):
			?>
			<?php echo $args['before_widget']; ?>
			<?php if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>
			<ul>
				<?php foreach ( $recent_terms as $term ): ?>
					<li>
						<a href="<?php echo $this->term_model->get_term_link_for_search( $term ); ?>"><?php echo wp_trim_words( esc_attr( $term['term'] ), 5 ) ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php echo $args['after_widget']; ?>
			<?php
		endif;
	}

	public function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['title']  = sanitize_text_field( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];

		return $instance;
	}

	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/>
		</p>

		<p><label
				for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of terms to show:' ); ?></label>
			<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>"
			       name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1"
			       value="<?php echo $number; ?>" size="3"/></p>
		<?php
	}

}