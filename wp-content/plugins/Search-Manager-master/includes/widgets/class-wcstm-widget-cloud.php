<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Widget_Cloud extends WP_Widget {

	/**
	 * @var WCSTM_Term
	 */
	private $term_model;

	/**
	 * @var WCSTM_Utils
	 */
	protected $utils;

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		$this->term_model = WCSTM_Term::get_instance();
		$this->utils      = new WCSTM_Utils();
		parent::__construct(
			'widget_wcstm_popular',
			__( 'Search Manager: Search Terms Cloud', 'wcstm' ),
			array( 'description' => __( 'Display Search Terms Cloud', 'wcstm' ) )
		);
	}

	function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$wcs = isset( $instance['wcs'] ) ? absint( $instance['wcs'] ) : 0;
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">
				<?php _e( 'Title', 'wcstm' ); ?>: <input class="widefat"
				                                         id="<?php echo $this->get_field_id( 'title' ); ?>"
				                                         name="<?php echo $this->get_field_name( 'title' ); ?>"
				                                         type="text" value="<?php echo esc_attr( $title ); ?>"/>
			</label>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of terms to show:' ); ?></label>
			<input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>"
			       name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1"
			       value="<?php echo $number; ?>" size="3"/></p>
		<p>
			<label for="<?php echo $this->get_field_id( 'wcs' ); ?>"><?php _e( 'Woocommerce only:' ); ?></label>
			<input <?php checked(1, $wcs); ?> id="<?php echo $this->get_field_id( 'wcs' ); ?>" 
				name="<?php echo $this->get_field_name( 'wcs' ); ?>" type="checkbox" value="1" />
		</p>
		<?php
	}


	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	function update( $new_instance, $old_instance ) {
		$instance           = $old_instance;
		$instance['title']  = strip_tags( $new_instance['title'] );
		$instance['number'] = (int) $new_instance['number'];
		$instance['wcs'] = (int) $new_instance['wcs'];

		return $instance;
	}


	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	function widget( $args, $instance ) {
		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Search Terms Cloud' );
		echo $args['before_widget'];
		echo $args['before_title'] . $title . $args['after_title'];

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number ) {
			$number = 5;
		}

		$wcs = ( ! empty( $instance['wcs'] ) ) ? absint( $instance['wcs'] ) : 0;
		
		echo $this->get_search_terms_cloud( array( 'number' => $number, 'wcs' => $wcs ) );
		echo $args['after_widget'];
	}

	protected function get_result_count( $result ) {
		return $result['count'];
	}

	public function get_search_terms_cloud( $args = array() ) {

		$args = $this->utils->get_popular_search_default_atts( $args );
		$output  = '';
		$results = $this->term_model->get_terms( 'count', 'DESC', $args );
		
		if ( $results ) {
			$counts = array_map( array( $this, 'get_result_count' ), $results );

			$min = min( $counts );
			$max = max( $counts );

			$spread = $max - $min;

			// Calculate various font sizes
			if ( $args['largest'] != $args['smallest'] ) {
				$fontspread = $args['largest'] - $args['smallest'];
				if ( 0 != $spread ) {
					$fontstep = $fontspread / $spread;
				} else {
					$fontstep = 0;
				}
			}

			// Calculate colors
			if ( $args['hot'] != $args['cold'] ) {
				$hotdec  = $this->utils->html_2_rgb( $args['hot'] );
				$colddec = $this->utils->html_2_rgb( $args['cold'] );
				for ( $i = 0; $i < 3; $i ++ ) {
					$coldval[]     = $colddec[ $i ];
					$hotval[]      = $hotdec[ $i ];
					$colorspread[] = $hotdec[ $i ] - $colddec[ $i ];
					if ( 0 != $spread ) {
						$colorstep[] = ( $hotdec[ $i ] - $colddec[ $i ] ) / $spread;
					} else {
						$colorstep[] = 0;
					}
				}
			}

			foreach ( $results as $result ) {

				$search_term = wp_trim_words( esc_attr( $result['term'] ), 5 );
				$url         = apply_filters( 'wcstm_search_cloud_term_url', $this->term_model->get_term_link_for_search( $result ) );

				$fraction = $result['count'] - $min;
				$fontsize = $args['smallest'] + $fontstep * $fraction;
				$color    = '';
				for ( $i = 0; $i < 3; $i ++ ) {
					$color .= dechex( $coldval[ $i ] + ( $colorstep[ $i ] * $fraction ) );
				}
				$style = 'style="';
				if ( $args['largest'] != $args['smallest'] ) {
					$style .= 'font-size:' . round( $fontsize ) . $args['unit'] . ';';
				}
				if ( $args['hot'] != $args['cold'] ) {
					$style .= 'color:#' . $color . ';';
				}
				$style .= '"';

				$output .= $args['before'] . '<a href="' . $url . '" title="';
				$output .= sprintf( _n( 'Search for %1$s (%2$s search)', 'Search for %1$s (%2$s searches)', $result['count'], 'wcstm' ), $search_term, $result['count'] );
				$output .= '" ' . $style;
				$output .= '>' . $search_term . '</a>' . $args['after'] . ' ';
			}
		} else {
			$output = __( 'No searches made yet', 'wcstm' );
		}

		return $output;
	}

}