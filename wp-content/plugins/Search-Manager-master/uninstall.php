<?php
/**
 * WCSTM Uninstall
 *
 * Deletes all plugin data.
 *
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit;
}

global $wpdb;

// Delete tables.
$wpdb->query( "DROP TABLE IF EXISTS {$wpdb->prefix}wcstm_searchterms" );
// Delete options.
$wpdb->query("DELETE FROM $wpdb->options WHERE option_name LIKE 'wcstm\_%';");