(function ($) {

    var searchResult = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: wcstm.ajax_url + '?action=ajax_search&terms=%QUERY',
        remote: {
            url: wcstm.ajax_url + '?action=ajax_search&terms=%QUERY',
            wildcard: '%QUERY'
        }
    });

    var check_hint = Boolean(wcstm.hint);
    var text = wcstm.text;
    var count = parseInt(wcstm.minChars);

    $(wcstm.selector).typeahead({
            minLength: count, highlight: true, hint: check_hint,
        },
        {
            highlight: true,
            display: 'value',
            limit: 999,
            source: searchResult,
            templates: {
                empty: [
                    '<div class="empty-message">&nbsp;', text, '</div>'
                ].join('\n'),
                suggestion: Handlebars.compile('<div>{{#if url}}<a href="{{url}}">{{/if}}' +
                    '{{#if image}}<img src="{{image}}" class="wcstm" />{{/if}} ' +
                    '<span>{{value}} {{#if price}} - {{{price}}} {{/if}}</span>' +
                    '{{#if url}}</a>{{/if}}</div>')
            }

    });

})(jQuery);