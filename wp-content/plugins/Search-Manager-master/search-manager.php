<?php
/**
 * Plugin Name: Search Manager
 * Plugin URI:  http://searchmanagerwp.com/
 * Description: All-in-one solution for managing your WordPress and WooCommerce search. Plugins allows you to track search terms and create redirects for them if needed.
 * Version:     3.8
 * Author:      TeamDev Ltd
 * Author URI:  https://www.teamdev.com/
 * Text Domain: wcstm
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

define( 'WCSTM_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WCSTM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'WCSTM_INCLUDES_DIR', 'includes/' );
define( 'WCSTM_WIDGETS_DIR', 'includes/widgets/' );
define( 'WCSTM_ADMIN_DIR', 'admin/' );
define( 'WCSTM_ADMIN_DIR_VIEW', WCSTM_PLUGIN_DIR . WCSTM_ADMIN_DIR . 'view/' );
define( 'WCSTM_BASENAME', plugin_basename( __FILE__ ) );

spl_autoload_register( 'autoload_includes' );
spl_autoload_register( 'autoload_admin' );
spl_autoload_register( 'autoload_widgets' );

/**
 * Begins execution of the plugin.
 */

WCSTM::get_instance();

register_activation_hook( __FILE__, 'wcstm_activate' );

function autoload_includes( $class ) {
	load( $class, WCSTM_PLUGIN_DIR . WCSTM_INCLUDES_DIR . formating_class_name( $class ) );
}

function autoload_admin( $class ) {
	if ( is_admin() ) {
		load( $class, WCSTM_PLUGIN_DIR . WCSTM_ADMIN_DIR . formating_class_name( $class ) );
	}
}

function autoload_widgets( $class ) {
	load( $class, WCSTM_PLUGIN_DIR . WCSTM_WIDGETS_DIR . formating_class_name( $class ) );
}

function load( $class, $path ) {
	if ( file_exists( $path ) ) {
		include $path;
	}
}

function formating_class_name( $class ) {
	$class = strtolower( $class );

	return 'class-' . str_replace( '_', '-', $class ) . '.php';
}

function wcstm_activate() {
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = WCSTM::$table_name;

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
        id bigint(20) NOT NULL AUTO_INCREMENT,
        term text NOT NULL,
        count bigint(20) NOT NULL DEFAULT '0',
        results bigint(20) NOT NULL DEFAULT '0',
        redirect int(1) NOT NULL DEFAULT '0',
        redirect_obj_type varchar(32),
        redirect_obj_name varchar(32),
        redirect_obj_id bigint(20),
        last_search_date datetime DEFAULT '0000-00-00 00:00:00',
        last_search_date_gmt datetime DEFAULT '0000-00-00 00:00:00',
        created_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        created_date_gmt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        modified_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        modified_date_gmt datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        PRIMARY KEY  (id)
        ) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	dbDelta( $sql );
	
	/* add 'wcs' column if not exists */
	$row = $wpdb->get_results(  "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = $table_name AND column_name = 'wcs'"  );
	if( empty($row) ){
		$wpdb->query( "ALTER TABLE {$table_name} ADD `wcs` INT(1) NOT NULL DEFAULT '0' AFTER `count`" );
	}

}