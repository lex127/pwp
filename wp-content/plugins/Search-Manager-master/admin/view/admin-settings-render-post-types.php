<?php
/**
 * @var $this WCSTM_Admin_View
 */

$selected_types = get_option( $this->getData( 'type' ) );
if ( ! $selected_types ) {
	$selected_types = array();
}
$type  = $this->getData( 'type' );
$items = $this->get_post_types();
?>
<?php if ( empty( $items ) ) { ?>
	<p><?php _e( 'There are no available post types to select', 'wcstm' ) ?></p>
<?php } else { ?>
	<p>
		<?php foreach ( $items as $item ): ?>
			<label>
				<input name="wcstm[<?php echo $type ?>][]" class="$type" id="<?php echo $type . '-' . $item->name; ?>"
				       type="checkbox"
				       value="<?php echo $item->name; ?>" <?php checked( in_array( $item->name, $selected_types ), true ); ?>>
				<?php echo $item->labels->name; ?>
			</label>
			<br>
		<?php endforeach; ?>
	</p>
<?php } ?>