<?php
/**
 * @var $this WCSTM_Admin_View
 */

$type  = $this->getData( 'type' );
$value = '';

if ( $this->getData( 'item' ) || isset( $_REQUEST['id'] ) ) {
	$item  = ( $this->getData( 'item' ) ) ? $this->getData( 'item' ) : $this->term_model->get_term_by_id( $_REQUEST['id'] );
	$value = ( $item['redirect_obj_type'] == 'custom_link' ) ? $this->term_model->get_term_link_url( $item ) : $value;
}

?>
<td id="<?php echo $type ?>">
	<input id="<?php echo $type ?>"
	       name="<?php echo $type ?>"
	       type="text"
	       value="<?php echo $value ?>"
	       placeholder="<?php _e( 'Custom Link', 'wcstm' ) ?>" required>
</td>