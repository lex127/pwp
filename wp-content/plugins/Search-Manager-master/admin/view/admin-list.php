<?php
/**
 * @var $this WCSTM_Admin_View
 */
$message    = $this->getData( 'message' );
$table      = $this->getData( 'table' );
if ( ! empty( $_REQUEST['s'] ) ) {
	$search_var = (string) $_REQUEST['s'];
	$search_var = trim( $_REQUEST['s'] );
} else {
	$search_var = '';
}
?>

<div class="wrap">
	<h1>
		<?php _e( 'Search Terms', 'wcstm' ) ?>
		<a class="page-title-action" href="<?php echo get_admin_url( get_current_blog_id(), 'admin.php?page=wcstm_edit_term' ); ?>"><?php _e( 'Add new', 'wcstm' ) ?></a>
		<?php
		if ( ! empty( $search_var ) ) {
			printf( ' <span class="subtitle">' . __( 'Search results for &#8220;%s&#8221;' ) . '</span>', esc_attr( $search_var ) );
		}
		?>
	</h1>
	<?php echo $message; ?>
	<form id="search-terms-table" method="GET">
		<p class="search-box">
			<label class="screen-reader-text" for="post-search-input">Search Pages:</label>
			<input type="search" id="post-search-input" name="s" value="<?php echo esc_attr( $search_var ) ?>">
			<input type="submit" id="search-submit" class="button" value="Search">
		</p>
		<input type="hidden" name="page" value="<?php echo esc_attr( $_REQUEST['page'] ) ?>"/>
		<?php $table->display() ?>
	</form>
</div>