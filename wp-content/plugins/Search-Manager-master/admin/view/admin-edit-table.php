<?php
/**
 * @var $this WCSTM_Admin_View
 * @var $menu WCSTM_Admin_Menus
 */
?>

<?php $item = $this->getData( 'item' ) ?>
<?php $menu = $this->getData( 'menu' ) ?>

<table id="search-term" class="wcstm-edit-table form-table">
	<tbody>
	<?php if ( $item['id'] ): ?>
		<tr>
			<th>
				<label><?php _e( 'ID', 'wcstm' ) ?></label>
			</th>
			<td>
				<span id="id"><?php echo esc_attr( $item['id'] ) ?></span>
			</td>
		</tr>
	<?php endif; ?>
	<tr>
		<th>
			<label for="term"><?php _e( 'Term', 'wcstm' ) ?></label>
		</th>
		<td>
			<input id="term" name="term" type="text" value="<?php echo esc_attr( $item['term'] ) ?>"
			       placeholder="<?php _e( 'Search Term', 'wcstm' ) ?>" required>
		</td>
	</tr>
	<tr>
		<th>
			<label><?php _e( 'Hit Count', 'wcstm' ) ?></label>
		</th>
		<td>
			<span class="total_count"><?php echo esc_attr( $item['count'] ) ?></span>
			<input id="count" type="hidden" value="<?php echo esc_attr( $item['count'] ) ?>" name="count">
			<?php if ( $item['count'] ): ?>
				<span class="reset_button" data-confirm="<?php _e( 'Are you sure?', 'wcstm' ) ?>">
                        <button type="button"
                                class="button button-small hide-if-no-js"><?php _e( 'Reset count', 'wcstm' ) ?></button>
                    </span>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th>
			<label><?php _e( 'Search Results', 'wcstm' ) ?></label>
		</th>
		<td>
			<span><?php echo esc_attr( $item['results'] ) ?></span>
			<?php if ( $item['redirect_obj_id'] || $item['results'] || $item['redirect_obj_type'] === 'custom_link' ): ?>
				<span style="margin-left:5px">
                    <a target="_blank" href="<?php echo get_search_link( $item['term'] ) ?>" id="results" type="button"
                       class="button button-small hide-if-no-js"><?php _e( 'Result Page', 'wcstm' ) ?></a>
                </span>
			<?php endif; ?>
		</td>
	</tr>

	<tr>
		<th>
			<label for="redirect"><?php _e( 'Enable Redirect', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="redirect" name="redirect"
                       type="checkbox" <?php if ( $item['redirect'] ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr>
	<tr id="redirect_opt" <?php if ( ! $item['redirect'] ): echo 'style= "display: none"'; endif; ?>>
		<th>
			<label><?php _e( 'Redirect to', 'wcstm' ) ?></label>
		</th>
		<?php $menu->the_redirect_options( $this->the_redirect_obj_types(), 'redirect_obj_type' ) ?>

		<?php if ( $item['redirect_obj_name'] && $item['redirect_obj_type'] !== 'custom_link' ): ?>
			<?php $menu->the_redirect_options( $menu->get_obj_type( $item['redirect_obj_type'] ), 'redirect_obj_name' ); ?>
		<?php elseif ( $item['redirect_obj_type'] ): ?>
			<?php $menu->the_custom_link( 'redirect_obj_name' ) ?>
		<?php endif; ?>

		<?php if ( $item['redirect_obj_type'] && $item['redirect_obj_type'] !== 'custom_link' ): ?>
			<?php $menu->the_obj_id( 'redirect_obj_id' ) ?>
		<?php endif; ?>
	</tr>

	<?php if ( $item['id'] ): ?>
		<tr>
			<th>
				<label for="name"><?php _e( 'Last Search Date', 'wcstm' ) ?></label>
			</th>
			<td>
				<span><?php echo esc_attr( $item['last_search_date'] ) ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="name"><?php _e( 'Created Date', 'wcstm' ) ?></label>
			</th>
			<td>
				<span><?php echo esc_attr( $item['created_date'] ) ?></span>
			</td>
		</tr>
		<tr>
			<th>
				<label for="name"><?php _e( 'Modified Date', 'wcstm' ) ?></label>
			</th>
			<td>
				<span><?php echo esc_attr( $item['modified_date'] ) ?></span>
			</td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>