<?php
/**
 * @var $this WCSTM_Admin_View
 */
function exclude_attachment() {
	return array( 'attachment' );
}

add_filter( 'wcstm_ac_filter_types', 'exclude_attachment' );
?>

<tr>
	<th style="width: 250px">
		<label for="search_in_excerpts"><?php _e( 'Enable Search by Excerpts', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="search_in_excerpts" name="wcstm[search_in_excerpts]"
                   type="checkbox" <?php if ( get_option( 'search_in_excerpts' ) == 'on' ): echo 'checked'; endif; ?>>
        </span>
	</td>
</tr>
<tr>
	<th>
		<label for="search_in_post_comments"><?php _e( 'Enable Search by Post Comments', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="search_in_post_comments" name="wcstm[search_in_post_comments]"
                   type="checkbox" <?php if ( get_option( 'search_in_post_comments' ) == 'on' ): echo 'checked'; endif; ?>>
        </span>
	</td>
</tr>
<tr>
	<th>
		<label for="search_in_post_tags"><?php _e( 'Enable Search by Post Tags', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="search_in_post_tags" name="wcstm[search_in_post_tags]"
                   type="checkbox" <?php if ( get_option( 'search_in_post_tags' ) == 'on' ): echo 'checked'; endif; ?>>
        </span>
	</td>
</tr>
<tr>
	<th>
		<label for="search_in_post_category"><?php _e( 'Enable Search by Post Categories', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="search_in_post_category" name="wcstm[search_in_post_category]"
                   type="checkbox" <?php if ( get_option( 'search_in_post_category' ) == 'on' ): echo 'checked'; endif; ?>>
        </span>
	</td>
</tr>
<tr>
	<th>
		<label for="search_by_slug"><?php _e( 'Enable Search by Slug', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="search_in_slug" name="wcstm[search_in_slug]"
                   type="checkbox" <?php if ( get_option( 'search_in_slug' ) == 'on' ): echo 'checked'; endif; ?>>
        </span>
	</td>
</tr>
<tr>
	<th>
		<label for="post_stypes"><?php _e( 'Exclude Post Types from Search', 'wcstm' ) ?></label>
	</th>
	<td>
		<?php $this->get_settings_field( 'post_types_excluded_from_search' ) ?>
		<p class="description"><?php _e( 'Select post types to exclude from the search results.', 'wcstm' ); ?></p>
	</td>
</tr>