<?php
/**
 * @var $this WCSTM_Admin_View
 * @var $report WCSTM_Admin_Report
 */
?>
<?php $report = $this->getData( 'report' ); ?>
<?php $results = $report->get_stats_results() ?>
<?php $is_successes = $report->is_successes() ?>

<?php if ( count( $results ) ): ?>

	<table cellpadding="3" cellspacing="2">
		<tbody>
		<tr class="alternate">
			<th class="stats-text"><?php _e( 'Term', 'wcstm' ) ?></th>
			<th><?php _e( 'Hit Count', 'wcstm' ) ?></th>
			<?php if ( $is_successes ): ?>
				<th><?php _e( 'Search Results', 'wcstm' ) ?></th>
			<?php endif; ?>
		</tr>
		<?php foreach ( $results as $result ): ?>
			<tr class="alternate">
			<td>
				<a href="<?php echo get_admin_url() . 'admin.php?page=wcstm_edit_term&id=' . $result['id'] . '&action=edit' ?>"><?php echo $result['term'] ?></a>
			</td>
			<td class="stats-number"><?php echo $result['count'] ?></td>
			<?php if ( $is_successes ): ?>
				<td class="stats-number"><?php echo $result['results'] ?></td></tr>
			<?php endif; ?>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php else: ?>
	<p><em><?php _e( 'No searches recorded for this period.', 'wcstm' ) ?></em></p>
<?php endif;