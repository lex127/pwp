<?php
/**
 * @var $this WCSTM_Admin_View
 */
?>

<form id="form" method="POST">
	<?php wp_nonce_field( 'settings_form', 'settings_form_nonce' ); ?>
	<div class="wrap">
		<h1><?php _e( 'Search Manager Settings', 'wcstm' ) ?></h1>
		<h2 class="nav-tab-wrapper">
			<?php foreach ( $this->get_setting_tabs() as $name => $label ): ?>
				<a href="<?php echo admin_url( 'admin.php?page=wcstm_settings&tab=' . $name ) ?>"
				   class="nav-tab <?php echo( $this->get_current_tab() == $name ? 'nav-tab-active' : '' ) ?>"><?php echo $label ?></a>
			<?php endforeach; ?>
		</h2>
		<?php if ( $this->get_current_tab() != 'docs' ): ?>
		<table class="form-table">
			<tbody>
			<?php do_action( 'wcstm_admin_' . $this->get_current_tab() ); ?>
			</tbody>
		</table>
	</div>
<?php $this->submit_button() ?>
<?php else: ?>
	<?php do_action( 'wcstm_admin_' . $this->get_current_tab() ); ?>
<?php endif ?>
</form>