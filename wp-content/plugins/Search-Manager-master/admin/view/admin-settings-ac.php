<?php
/**
 * @var $this WCSTM_Admin_View
 */
$is_complete = ( get_option( 'is_auto_complete' ) == 'on' ) ? 1 : 0;
$display = ( $is_complete ) ? '' : 'style="display: none"';
function get_excluded_types() {
	$excluded = get_option( 'post_types_excluded_from_search' );
	if ( ! is_array( $excluded ) ) {
		$excluded = array();
	}
	return array_merge( array( 'attachment' ), $excluded );
}
add_filter( 'wcstm_ac_filter_types', 'get_excluded_types' );
?>

<?php $menu = $this->getData( 'menu' ) ?>
<tr>
	<th style="width: 250px">
		<label for="autocomplete_status"><?php _e( 'Enable Autocomplete', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="autocomplete_status" name="wcstm[is_auto_complete]" type="checkbox" <?php checked( $is_complete ) ?>>
        </span>
	</td>
</tr>

<tr <?php if ( ! $is_complete )
	echo 'style="display: none"' ?>>
	<th style="width: 250px">
		<label for="autocomplete_post_thumbnail"><?php _e( 'Show Post Thumbnail', 'wcstm' ) ?></label>
	</th>
	<td>
            <span>
                <input id="autocomplete_post_thumbnail" name="wcstm[autocomplete_post_thumbnail]"
					   type="checkbox" <?php checked( get_option( 'autocomplete_post_thumbnail' ), 'on' );
				?>>
            </span>
	</td>
<?php if ( $this->utils->is_woocommerce() ) : ?>
<tr <?php if ( ! $is_complete )
	echo 'style="display: none"' ?>>
	<th>
		<label for="search_product_price_status"><?php _e( 'Show Search Product Price', 'wcstm' ) ?></label>
	</th>
	<td>
            <span>
                <input id="search_product_price_status" name="wcstm[search_product_price_status]"
					   type="checkbox" <?php checked( get_option( 'search_product_price_status' ), 'on' );
				?>>
            </span>
	</td>
</tr>
<tr <?php if ( ! $is_complete )
	echo 'style="display: none"' ?>>
	<th>
		<label for="ac_hide_variations"><?php _e( 'Show Product Variations', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="ac_hide_variations" name="wcstm[ac_hide_variations]"
                   type="checkbox" <?php checked( get_option( 'ac_hide_variations' ), 'on' );
            ?>>
        </span>
	</td>
</tr>
<tr <?php if ( ! $is_complete )
	echo 'style="display: none"' ?>>
	<th style="width: 250px">
		<label for="ac_check_hint"><?php _e( 'Enable Advice', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="ac_check_hint" name="wcstm[ac_check_hint]"
				   type="checkbox" <?php checked( get_option( 'ac_check_hint' ), 'on' );
			?>>
        </span>
		<p class="description"><?php _e( '	Add word completion to the search.', 'wcstm' ); ?></p>
	</td>
</tr>
<?php endif; ?>
<tr <?php echo $display ?>>
	<th>
		<label for="search_selector"><?php _e( 'Search Field Selector', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="search_selector" class="regular-text" name="wcstm[search_field_selector]" type="text"
                   value='<?php echo get_option( 'search_field_selector' ) ? esc_html( ( get_option( 'search_field_selector' ) ) ) : WCSTM_Admin_Menus::DEFAULT_SEARCH_FIELD_SELECTOR ?>'>
        </span>
		<p class="description"><?php _e( 'Search form input CSS selector (you need to setup this field if you have customized search form).', 'wcstm' ); ?></p>
	</td>
</tr>

<tr <?php echo $display ?>>
	<th>
		<label for="ac_min_chars"><?php _e( 'Autosuggest min number of characters', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="ac_min_chars" class="regular-text" name="wcstm[ac_min_chars]" min="1" type="number"
                   value='<?php echo get_option( 'ac_min_chars' ) ? esc_html( ( get_option( 'ac_min_chars' ) ) ) : 1 ?>'>
        </span>
		<p class="description"><?php _e( 'Minimum number of characters required to trigger autosuggest.', 'wcstm' ); ?></p>
	</td>
</tr>

<tr <?php echo $display ?>>
	<th>
		<label for="ac_lookup_limit"><?php _e( 'Autosuggest lookup limit', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="ac_lookup_limit" class="regular-text" name="wcstm[ac_lookup_limit]" min="0" type="number"
                   value='<?php echo get_option( 'ac_lookup_limit' ) ? esc_html( ( get_option( 'ac_lookup_limit' ) ) ) : 0 ?>'>
        </span>
		<p class="description"><?php _e( 'Number of maximum results to display for local lookup. Default: 0 (no limit)', 'wcstm' ); ?></p>
	</td>
</tr>

<tr <?php echo $display ?>>
	<th style="width: 250px">
		<label for="autocomplete_redirect"><?php _e( 'Results Page', 'wcstm' ) ?></label>
	</th>
	<td>
		<select class='post_form' id="autocomplete_redirect" name="wcstm[autocomplete_redirect]">
			<option
				value="search_page" <?php selected( get_option( 'autocomplete_redirect' ), 'search_page' ); ?>><?php _e( 'Search Page' ) ?></option>
			<option
				value="item_page" <?php selected( get_option( 'autocomplete_redirect' ), 'item_page' ); ?>><?php _e( 'Item Page' ) ?></option>
		</select>
		<p class="description"><?php _e( 'Select page that should be displayed after clicking on item from the autocomplete list.', 'wcstm' ); ?></p>
	</td>
</tr>

<tr <?php echo $display ?>>
	<th>
		<label for="post_stypes"><?php _e( 'Exclude Post Types from Autocomplete', 'wcstm' ) ?></label>
	</th>
	<td>
		<?php $this->get_settings_field( 'autocomplete_posttypes' ) ?>
		<p class="description"><?php _e( 'Select post types to exclude from the autocomplete results.', 'wcstm' ); ?></p>
	</td>
</tr>

<tr <?php echo $display ?>>
	<th>
		<label for="no_search_selector"><?php _e( 'Text of "no results"', 'wcstm' ) ?></label>
	</th>
	<td>
        <span>
            <input id="no_search_selector" class="regular-text" name="wcstm[no_search_result_field]" type="text"
                   value='<?php echo get_option( 'no_search_result_field' ) ? esc_html( ( get_option( 'no_search_result_field' ) ) ) : __( 'No results', 'wcstm' ) ?>'>
        </span>
		<p class="description"><?php _e( 'Show this text for no results', 'wcstm' ); ?></p>
	</td>
</tr>