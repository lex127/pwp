<?php
/**
 * @var $this WCSTM_Admin_View
 */

$item    = $this->getData( 'item' );
$type    = $this->getData( 'type' );
$options = array_diff_assoc( $this->getData( 'options' ), $this->get_options_for_exclude() );

?>
<td id="<?php echo $type ?>">
	<select name="<?php echo $type ?>" required>
		<option value="0" <?php echo ( '0' == $item[ $type ] ) ? 'selected="selected"' : '' ?> disabled></option>
		<?php foreach ( $options as $value => $title ): ?>
			<option value="<?php echo $value ?>"
				<?php echo ( $value == (string) $item[ $type ] ) ? ' selected="selected"' : '' ?>>
				<?php echo $title ?>
			</option>
		<?php endforeach; ?>
	</select>
</td>
