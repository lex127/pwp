<?php
/**
 * @var $this WCSTM_Admin_View
 */

$item = $this->getData( 'item' );
$type = $this->getData( 'type' );

?>


<td id="<?php echo $type . '_template' ?>">
	<input id="<?php echo $type ?>" type="search" value="<?php echo $this->get_the_title( $item ) ?>"
	       placeholder="<?php _e( 'Item name', 'wcstm' ) ?> " required>
	<input id="<?php echo $type . '_main' ?>" name="redirect_obj_id" type="text"
	       value="<?php echo esc_attr( $item[ $type ] ) ?>" placeholder="<?php _e( 'Item name', 'wcstm' ) ?>"
	       style="display: none">
</td>
