<?php
/**
 * @var $this WCSTM_Admin_View
 */

$item     = $this->getData( 'item' );
$action   = $this->getData( 'action' );
$messages = $this->getData( 'messages' );

if( $item['id'] ){
	$page_title = __( 'Edit Search Term', 'wcstm' );
} else {
	$page_title = __( 'Add New Search Term', 'wcstm' );
}
?>

<div class="wrap search-manager-wrap">
	<h1><?php echo $page_title ?>
		<a class="page-title-action"
		   href="<?php echo get_admin_url( get_current_blog_id(), 'admin.php?page=wcstm' ); ?>"><?php _e( 'back to list', 'wcstm' ) ?></a>
	</h1>

	<?php if ( ! empty( $messages['notice'] ) ): ?>
		<div id="notice" class="error"><p><?php echo $messages['notice'] ?></p></div>
	<?php endif; ?>

	<?php if ( ! empty( $messages['message'] ) ): ?>
		<div id="message" class="updated"><p><?php echo $messages['message'] ?></p></div>
	<?php endif; ?>

	<form id="form" method="POST">
		<?php wp_nonce_field( 'edit_terms_meta_box', 'edit_terms_meta_box_nonce' ); ?>
		<input type="hidden" name="id" value="<?php echo $item['id'] ?>">
		<input type="hidden" name="action" value="<?php echo $action ?>">

		<div class="metabox-holder" id="poststuff">
			<div id="post-body">
				<div id="post-body-content">
					<?php do_meta_boxes( 'edit_terms_meta_box', 'normal', $item ); ?>
					<?php submit_button() ?>
				</div>
			</div>
		</div>
	</form>
</div>