<?php
/**
 * @var $this WCSTM_Admin_View
 */
?>
<?php
/**
 * @var $report WCSTM_Admin_Report
 */
$report  = $this->getData( 'report' );
$periods = $report->get_default_date_periods();
?>

<div class="wrap stats-table">
	<h1><?php _e( 'Reports', 'wcstm' ) ?></h1>

	<h2><?php _e( 'Search Summary', 'wcstm' ) ?></h2>

	<p><?php _e( 'The most popular searches for the given time periods.', 'wcstm' ) ?></p>

	<?php foreach ( apply_filters( 'wcstm_custom_period_with_results', $periods ) as $period ): ?>
		<div>
			<h3><?php echo $period['title'] ?></h3>
			<?php echo $report->get_stats( $period ); ?>
		</div>
	<?php endforeach; ?>

	<p class="stats-clear"></p>

	<h2><?php _e( 'Searches with no Results', 'wcstm' ) ?></h2>

	<p><?php _e( 'These tables show you which terms people searched for and there were no search results on your site. Maybe you should give them what they want.', 'wcstm' ) ?></p>

	<?php foreach ( apply_filters( 'wcstm_custom_period_without_results', $periods ) as $period ): ?>
		<div>
			<h3><?php echo $period['title'] ?></h3>
			<?php echo $report->get_stats( $period, false ); ?>
		</div>
	<?php endforeach; ?>
</div>