<div class="wrap">
	<h3>Plugin Shortcodes</h3>
	<h4>Description</h4>
	<p>Search Manager plugin provides with three shortcodes for embedding into the post content.</p>
	<h4>Example #1 - Search Cloud</h4>
	<code>[wcstm_search_cloud smallest=5 largest=10 cold='CCCCCC' hot='000000' number=5]</code>
	<h4>Parameters</h4>
	<ul>
		<li><strong>smallest="..."</strong> - font size for elements with less search results (optional)</li>
		<li><strong>largest="..."</strong> - font size for elements with more search results (optional)</li>
		<li><strong>cold="..."</strong> - font color for elements with less search results (optional)</li>
		<li><strong>hot="..."</strong> - font color for elements with more search results (optional)</li>
		<li><strong>number="..."</strong> - number of elements that will be displayed (optional)</li>
		<li><strong>wcs="1"</strong> - show only woocommerce search (optional)</li>
	</ul>
	<br>
	<h4>Example #2 - Popular Search</h4>
	<code>[wcstm_popular_search title='Popular Search Terms' number=5]</code>
	<ul>
		<li><strong>title="..."</strong> - title of Popular Search section (optional)</li>
		<li><strong>number="..."</strong> - number of elements that will be displayed (optional)</li>
	</ul>
	<br>
	<h4>Example #3 - Recent Search</h4>
	<code>[wcstm_recent_search title='Recent Search Terms' number=5]</code>
	<ul>
		<li><strong>title="..."</strong> - title of Recent Search section (optional)</li>
		<li><strong>number="..."</strong> - number of elements that will be displayed (optional)</li>
	</ul>
	<br>
	<h4>Example #4 - Search Form</h4>
	<code>[wcstm_search_form wcps='1']</code>
	<ul>
		<li><strong>wcps="1"</strong> - WooCommerce search form, remove if you want to use default WordPress search form</li>
	</ul>
	<br>
	<h3>Plugin Filters and Actions</h3>
	<p>Hooks in WordPress core, plugins and themes essentially allow you to manipulate code without editing core files.
		<br>So you can change third-party plugin behaviour from your own theme or plugin and keep future updates as
		well.</p>
	<h4>Plugin Filters</h4>
	<ol>
		<li><strong>wcstm_custom_periods_with_results</strong> - allows to add/update report section data with results
			(Example #4)
		</li>
		<li><strong>wcstm_custom_periods_without_results</strong> - allows to add/update reports data section without
			results
		</li>
		<li><strong>wcstm_redirect_type</strong> - allows changing of the redirection type (301, 302, 307, etc.) before
			redirect is processed
		</li>
	</ol>
	<h4>Example #4 - Reports Filter</h4>
	<p>Let's add custom statistical data to the report table:</p>
    <pre>
        add_filter('wcstm_custom_period_with_results', function($periods) {
            $periods['custom']['title'] = __('Custom period');
            $periods['custom']['start_date'] = '2016-01-02';
            $periods['custom']['end_date'] = '2016-01-05';
            return $periods;
        });
    </pre>
</div>