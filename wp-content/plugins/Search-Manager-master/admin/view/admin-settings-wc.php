<?php
/**
 * @var $this WCSTM_Admin_View
 */
?>
<?php if ( $this->utils->is_woocommerce() ): ?>
	<tr>
		<th style="width: 250px">
			<label for="search_short_desc_status"><?php _e( 'Enable Search by Short Description', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="search_short_desc_status" name="wcstm[search_short_desc_status]"
                       type="checkbox" <?php if ( get_option( 'search_short_desc_status' ) == 'on' ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr>
	<tr>
		<th>
			<label for="search_sku_status"><?php _e( 'Enable Search by SKU', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="search_sku_status" name="wcstm[search_sku_status]"
                       type="checkbox" <?php if ( get_option( 'search_sku_status' ) == 'on' ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr>
	<tr>
		<th>
			<label for="search_sku_status"><?php _e( 'Enable Search by SKU (Variations)', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="search_sku_status" name="wcstm[search_sku_status_variations]"
                       type="checkbox" <?php if ( get_option( 'search_sku_status_variations' ) == 'on' ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr>
	<tr>
		<th>
			<label
				for="search_product_comments_status"><?php _e( 'Enable Search by Product Comments', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="search_product_comments_status" name="wcstm[search_product_comments_status]"
                       type="checkbox" <?php if ( get_option( 'search_product_comments_status' ) == 'on' ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr>
	<tr>
		<th>
			<label for="search_product_tags_status"><?php _e( 'Enable Search by Product Tags', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="search_product_tags_status" name="wcstm[search_product_tags_status]"
                       type="checkbox" <?php if ( get_option( 'search_product_tags_status' ) == 'on' ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr>
	<tr>
		<th>
			<label for="search_product_cat_status"><?php _e( 'Enable Search by Product Categories', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="search_product_cat_status" name="wcstm[search_product_cat_status]"
                       type="checkbox" <?php if ( get_option( 'search_product_cat_status' ) == 'on' ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr>
	<!--<tr>
		<th>
			<label for="hide_out_of_stock"><?php _e( 'Hide Out of Stock Products', 'wcstm' ) ?></label>
		</th>
		<td>
            <span>
                <input id="hide_out_of_stock" name="wcstm[hide_out_of_stock]"
                       type="checkbox" <?php if ( get_option( 'hide_out_of_stock' ) == 'on' ): echo 'checked'; endif; ?>>
            </span>
		</td>
	</tr> -->
<?php else: ?>
	<tr>
		<th>
			<label><?php _e( 'You need WooCommerce plugin installed to use this functions.' ) ?></label>
		</th>
	</tr>
<?php endif; ?>
