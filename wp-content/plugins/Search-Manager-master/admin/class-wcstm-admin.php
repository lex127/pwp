<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Admin {

	protected $terms;
	public $widgets;

	public function __construct( $terms ) {
		add_action( 'init', array( $this, 'init_menu' ) );
		$this->terms   = $terms;
		$this->widgets = new WCSTM_Admin_Widget( $terms );
		add_action( 'wp_dashboard_setup', array( $this->widgets, 'add_dashboard_widgets' ) );
		add_action( 'admin_footer', array( $this, 'enqueue_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
	}

	public function init_menu() {
		if ( is_admin() ) {
			new WCSTM_Admin_Menus( $this->terms );
		}
	}

	public function enqueue_scripts() {
		wp_enqueue_script( 'wcstm_validation', WCSTM_PLUGIN_URL . 'admin/js/jquery.validate.min.js', array( 'jquery' ) );
		wp_enqueue_script( 'wcstm_autocomplete', WCSTM_PLUGIN_URL . 'admin/js/jquery.autocomplete.js', array( 'jquery' ) );
		wp_enqueue_script( 'wcstm_admin', WCSTM_PLUGIN_URL . 'admin/js/wcstm-admin.js', array( 'wcstm_autocomplete' ) );
		wp_localize_script( 'wcstm_admin', 'wcstm', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}

	public function enqueue_styles() {
		wp_enqueue_style( 'wcstm_admin_styles', WCSTM_PLUGIN_URL . 'admin/css/styles.css' );
	}

}