<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Admin_Menus {

	const DEFAULT_SEARCH_FIELD_SELECTOR = '[name="s"]';
	/**
	 * List of settings fields.
	 */
	public $fields = array();
	/**
	 * @var WCSTM_Term
	 */
	protected $terms;


	/**
	 * @var WCSTM_Utils
	 */
	protected $utils;

	/**
	 * @var WCSTM_Admin_View
	 */
	protected $view;

	protected $item;
	protected $message;
	protected $notice;
	protected $action;


	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct( $terms ) {
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		$this->terms = $terms;
		$this->utils = new WCSTM_Utils();
		$this->view  = WCSTM_Admin_View::get_instance();

		add_action( 'wp_ajax_redirect_obj_name', array( $this, 'redirect_obj_type' ) );
		add_action( 'wp_ajax_redirect_obj_id', array( $this, 'redirect_obj_id_template' ) );
		add_action( 'wp_ajax_get_obj_ids', array( $this, 'redirect_obj_ids' ) );

		add_action( 'wcstm_admin_' . $this->view->get_current_tab(), array(
			$this,
			'display_' . $this->view->get_current_tab()
		) );
	}

	public function redirect_obj_id_template() {
		$type = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';
		if ( isset( $_REQUEST['notEmpty'] ) && $_REQUEST['notEmpty'] == 'true' ) {
			$this->the_obj_id( $type );
		}
		exit();
	}

	public function redirect_obj_ids() {
		$type = isset( $_REQUEST['type'] ) ? $_REQUEST['type'] : '';
		$name = isset( $_REQUEST['name'] ) ? $_REQUEST['name'] : '';
		if ( $type == 'post_type' ) {
			$data = $this->utils->get_posts( $name );
		} else {
			$data = $this->utils->get_taxanomies( $name );
		}
		wp_send_json( $data );
		exit();
	}

	public function redirect_obj_type() {
		$obj_type = isset( $_REQUEST['obj_type'] ) ? $_REQUEST['obj_type'] : '';
		$type     = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';

		if ( $obj_type == 'custom_link' ) {
			$this->the_custom_link( $type );
		} else {
			$options = $this->get_obj_type( $obj_type );
			$this->the_redirect_options( $options, $type );
		}

		exit();
	}

	public function get_obj_type( $obj_type ) {
		$data      = array();
		$type_args = array(
			'public' => true
		);
		switch ( $obj_type ) {
			case 'post_type':
				$type_args['exclude_from_search'] = false;
				$data                             = get_post_types( $type_args );
				break;
			case 'taxonomy':
				$data = get_taxonomies( $type_args );
				break;
			case 'custom_link':
				$data[] = 'custom_link';
				break;
			default:
				$data[] = 'not in option list';
		}

		return $data;
	}

	public function add_admin_menu() {

		add_menu_page(
			__( 'Search Manager', 'wcstm' ),
			__( 'Search Manager', 'wcstm' ),
			'activate_plugins',
			'wcstm',
			array( $this, 'term_list_handler' ),
			'dashicons-search'
		);

		add_submenu_page(
			'wcstm',
			__( 'Search Terms', 'wcstm' ),
			__( 'Search Terms', 'wcstm' ),
			'activate_plugins',
			'wcstm',
			array( $this, 'term_list_handler' )
		);

		add_submenu_page(
			'wcstm',
			__( 'Add new', 'wcstm' ),
			__( 'Add new', 'wcstm' ),
			'activate_plugins',
			'wcstm_edit_term',
			array( $this, 'term_edit_handler' )
		);

		add_submenu_page(
			'wcstm',
			__( 'Reports', 'wcstm' ),
			__( 'Reports', 'wcstm' ),
			'activate_plugins',
			'wcstm_reports',
			array( $this, 'report_handler' )
		);

		add_submenu_page(
			'wcstm',
			__( 'Settings', 'wcstm' ),
			__( 'Settings', 'wcstm' ),
			'activate_plugins',
			'wcstm_settings',
			array( $this, 'settings_handler' )
		);

	}


	public function save_settings( $options ) {
		$options = wp_parse_args( $options, $this->view->get_default_settings() );
		foreach ( $options as $option_name => $new_value ) {
			if ( ! is_array( $new_value ) ) {
				$new_value = stripslashes( $new_value );
			}
			if ( get_option( $option_name ) !== false ) {
				update_option( $option_name, $new_value );
			} else {
				add_option( $option_name, $new_value, null, 'no' );
			}
		}
	}

	public function settings_handler() {
		$nonce = isset( $_REQUEST['settings_form_nonce'] ) ? $_REQUEST['settings_form_nonce'] : false;
		if ( $nonce && wp_verify_nonce( $nonce, 'settings_form' ) ) {
			$this->save_settings( $this->utils->get_request_var( 'wcstm' ) );
		}
		$this->view->get_template( 'settings' );
	}


	public function display_main_options() {
		$this->view->get_template( 'settings-main' );
	}

	public function display_docs() {
		$this->view->get_template( 'settings-docs' );
	}


	public function display_autocomplete_options() {
		$this->view->setData( 'menu', $this );
		$this->view->get_template( 'settings-ac' );
	}

	public function display_wc_options() {
		$this->view->get_template( 'settings-wc' );
	}


	public function term_list_handler() {
		$table = new WCSTM_Admin_Term_List_Table();
		$table->prepare_items();

		$message = '';
		if ( $table->current_action() === 'delete' ) {
			$message = '<div class="updated below-h2" id="message"><p>' . sprintf( __( 'Items deleted: %d', 'wcstm' ), count( $_REQUEST['id'] ) ) . '</p></div>';
		}
		$this->view->setData( 'message', $message );
		$this->view->setData( 'table', $table );
		$this->view->get_template( 'list' );
	}

	public function get_term_item() {
		$item         = shortcode_atts( $this->terms->get_empty_term(), $_REQUEST );
		$item['term'] = esc_attr( stripslashes( $item['term'] ) );
		unset( $item['results'] );
		$item['redirect'] = $item['redirect'] ? 1 : 0;

		if ( $item['redirect_obj_type'] === 'custom_link' ) {
			$item['redirect_obj_name'] = wp_insert_link( array(
				'link_name' => $item['term'],
				'link_url'  => $item['redirect_obj_name']
			) );
		}

		return $item;
	}

	public function report_handler() {
		$this->view->setData( 'report', WCSTM_Admin_Report::get_instance() );
		$this->view->get_template( 'reports' );
	}

	public function term_edit_handler() {

		$this->action = $this->utils->get_request_var( 'action', 'create' );
		$nonce        = $this->utils->get_request_var( 'edit_terms_meta_box_nonce', false );
		$this->item   = $this->get_term_item();

		if ( $nonce && wp_verify_nonce( $nonce, 'edit_terms_meta_box' ) ) {
			$item_valid = $this->validate_term( $this->item );
			if ( $item_valid === true ) {
				$this->do_action();
			} else {
				$this->notice = $item_valid;
			}
		}

		$this->item = ( $this->item['id'] )
			? $this->terms->get_term_by_id( $this->item['id'] )
			: shortcode_atts( $this->terms->get_empty_term(), $this->item );

		add_meta_box( 'edit_terms_meta_box', 'Search Term Data', array(
			$this,
			'term_meta_box_handler'
		), 'edit_terms_meta_box', 'normal', 'default' );

		$this->view->setData( 'messages',
			array(
				'message' => $this->message,
				'notice'  => $this->notice
			)
		);
		$this->view->setData( 'item', $this->item );
		$this->view->setData( 'action', $this->action );
		$this->view->get_template( 'edit' );

	}

	public function do_action() {
		switch ( $this->action ) {
			case 'create':
				$this->item['id'] = $this->terms->insert_term( $this->item );
				$this->action     = 'edit';
				if ( $this->item['id'] ) {
					$this->message = __( 'Item was successfully saved', 'wcstm' );
				} else {
					$this->notice = __( 'There was an error while saving item', 'wcstm' );
				}
				break;
			default:
				unset( $this->item['created_date'] );
				unset( $this->item['created_date_gmt'] );
				unset( $this->item['last_search_date'] );
				unset( $this->item['last_search_date_gmt'] );

				$this->item['modified_date']     = current_time( 'mysql' );
				$this->item['modified_date_gmt'] = current_time( 'mysql', 1 );
				$result                          = $this->terms->update_term( $this->item );
				if ( $result ) {
					$this->message = __( 'Item was successfully updated', 'wcstm' );
				} else {
					$this->notice = __( 'There was an error while updating item', 'wcstm' );
				}
				break;
		}
	}

	public function validate_term( $item ) {
		$messages = array();
		$_term    = trim( $item['term'] );
		switch ( true ) {
			case empty( $_term ):
				$messages[] = __( 'Search term is required', 'wcstm' );
				break;
			case ( $this->action == 'create' && is_array( $this->terms->get_term_by_name( $item['term'] ) ) ):
				$messages[] = __( 'Term with the same name already exists.', 'wcstm' );
				break;
			case ( $item['redirect'] && $item['redirect_obj_type'] != 'custom_link' && ! $item['redirect_obj_id'] ):
				$messages[] = __( 'Redirect object is incorrect.' );
				break;
		}
		if ( empty( $messages ) ) {
			return true;
		}
		$_messages = implode( '<br />', $messages );

		return $_messages;
	}

	public function the_redirect_options( $options, $type ) {
		if ( isset( $_REQUEST['redirect'] ) && ! isset( $this->item['redirect'] ) ) {
			$this->item['redirect'] = $_REQUEST['redirect'];
			$this->item[ $type ]    = 0;
		}
		$this->view->setData( 'type', $type );
		$this->view->setData( 'options', $options );
		$this->view->setData( 'item', $this->item );
		$this->view->get_template( 'edit-table-options' );

	}

	public function the_custom_link( $type ) {
		$this->view->setData( 'type', $type );
		$this->view->get_template( 'edit-table-custom-link' );
	}

	public function the_obj_id( $type ) {
		$this->view->setData( 'type', $type );
		$this->view->setData( 'item', $this->item );
		$this->view->get_template( 'edit-table-obj-id' );
	}

	public function term_meta_box_handler( $item ) {
		$this->view->setData( 'menu', $this );
		$this->view->setData( 'item', $item );
		$this->view->get_template( 'edit-table' );
	}
}