<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Admin_View {
	/**
	 * @var Singleton reference to singleton instance
	 */
	private static $instance;

	/**
	 * gets the instance via lazy initialization (created on first usage).
	 *
	 * @return self
	 */

	private $data = array();

	/**
	 * @var WCSTM_Utils
	 */
	private $utils;

	/**
	 * @var WCSTM_Term
	 */
	private $term_model;

	protected $exclude_types = array();

	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * is not allowed to call from outside: private!
	 */
	private function __construct() {
		$this->utils      = new WCSTM_Utils();
		$this->term_model = WCSTM_Term::get_instance();
	}

	public function setData( $name, $value ) {
		$this->data[ $name ] = $value;
	}

	public function getData( $name ) {
		if ( array_key_exists( $name, $this->data ) ) {
			return $this->data[ $name ];
		}
	}

	public function get_template( $slug ) {
		$this->wcstm_load_template( WCSTM_ADMIN_DIR_VIEW . "admin-{$slug}.php", false );
	}

	public function wcstm_load_template( $_template_file, $require_once = true ) {
		if ( $require_once ) {
			require_once( $_template_file );
		} else {
			require( $_template_file );
		}
	}

	public function get_setting_tabs() {
		return array(
			'main_options'         => __( 'Main Options' ),
			'autocomplete_options' => __( 'Autocomplete Options' ),
			'wc_options'           => __( 'WooCommerce Search Options' ),
			'docs'                 => __( 'Docs' )
		);
	}

	public function get_current_tab() {
		return $this->utils->get_request_var( 'tab', 'main_options' );
	}

	public function get_the_title( $item ) {
		$title = '';
		if ( ! $item ) {
			return $title;
		}
		if ( $item['redirect_obj_type'] == 'post_type' ) {
			$items = $this->utils->get_posts( $item['redirect_obj_name'] );
		} else {
			$items = $this->utils->get_taxanomies( $item['redirect_obj_name'] );
		}
		foreach ( $items as $data ) {
			if ( $item['redirect_obj_id'] == $data['data'] ) {
				$title = $data['value'];
			}
		}

		return $title;
	}

	public function get_options_for_exclude() {
		return array(
			'attachment'             => 'attachment',
			'post_tag'               => 'post_tag',
			'post_format'            => 'post_format',
			'product_tag'            => 'product_tag',
			'product_shipping_class' => 'product_shipping_class'
		);
	}


	public function get_default_settings() {
		switch ( $this->get_current_tab() ) {
			case 'autocomplete_options':
				return array(
					'is_auto_complete'            => 0,
					'autocomplete_posttypes'      => array(),
					'autocomplete_post_thumbnail' => 0,
					'search_product_price_status' => 0,
					'ac_check_hint'               => 0,
					'ac_hide_variations'          => 1,
				);
			case 'wc_options':
				return array(
					'search_short_desc_status'         => 0,
					'search_sku_status'                => 0,
					'search_sku_status_variations'     => 0,
					'search_product_comments_status'   => 0,
					'search_product_tags_status'       => 0,
					'search_product_cat_status'        => 0,
					'search_product_tags_status'       => 0,
					'search_product_cat_status'        => 0,
					'hide_out_of_stock'                => 0,
				);
			default:
				return array(
					'search_in_excerpts'              => 0,
					'search_in_post_comments'         => 0,
					'search_in_post_tags'             => 0,
					'search_in_post_category'         => 0,
					'search_in_slug'                  => 0,
					'post_types_excluded_from_search' => array()
				);
		}
	}


	public function get_settings_field( $type ) {
		$this->setData( 'type', $type );
		$this->get_template( 'settings-render-post-types' );
	}

	public function get_post_types() {
		$this->exclude_types = apply_filters( 'wcstm_ac_filter_types', $this->exclude_types );
		$types               = get_post_types( array( 'public' => true, 'exclude_from_search' => false ), 'objects' );

		return $this->exclude_types( $types );
	}

	public function exclude_types( $types ) {
		foreach ( $types as $name => $data ) {
			if ( is_array( $this->exclude_types ) && in_array( $name, $this->exclude_types ) ) {
				unset( $types[ $name ] );
			}
		}

		return $types;
	}

	public function the_redirect_obj_types() {
		return array(
			'post_type'   => __( 'Post Type' ),
			'taxonomy'    => __( 'Taxonomy' ),
			'custom_link' => __( 'Custom Link' )
		);
	}

	public function submit_button() {
		if ( $this->get_current_tab() == 'wc_options' && ! $this->utils->is_woocommerce() ) {
			return '';
		}
		submit_button();
	}

}