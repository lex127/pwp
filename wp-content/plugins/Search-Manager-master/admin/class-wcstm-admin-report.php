<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Admin_Report {
	/**
	 * @var Singleton reference to singleton instance
	 */
	private static $instance;

	/**
	 * @var WCSTM_Term
	 */
	private $term_model;

	/**
	 * @var WCSTM_Admin_View
	 */
	private $view;

	protected $results;
	private $period_date = array();

	protected $is_successes;

	public static function get_instance() {
		if ( ! self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	/**
	 * is not allowed to call from outside: private!
	 */
	private function __construct() {
		remove_all_filters( 'term_filter' );
		$this->term_model = WCSTM_Term::get_instance();
		$this->view       = WCSTM_Admin_View::get_instance();
	}

	public function get_stats( $period, $is_successes = true ) {
		$this->period_date['start_date'] = $period['start_date'];
		$this->period_date['end_date']   = $period['end_date'];
		$this->is_successes              = $is_successes;

		add_filter( 'term_filter', array( $this, 'get_date_filter' ) );
		$this->results = $this->term_model->get_terms( 'count', 'DESC' );
		$this->view->get_template( 'reports-results' );
	}

	public function get_date_filter( $where ) {
		$start_date   = $this->period_date['start_date'];
		$end_date     = $this->period_date['end_date'];
		$is_successes = ( $this->is_successes ) ? 'AND results <> 0' : 'AND results = 0';
		$filter       = ( $start_date ) ? " WHERE (last_search_date >= '$start_date' AND last_search_date <= '$end_date') $is_successes" : '';
		$where .= $filter;

		return $where;
	}


	public function get_default_date_periods() {
		return array(
			'day'   => array(
				'title'      => __( 'Last Day', 'wcstm' ),
				'start_date' => date( 'Y-m-d', strtotime( ' -1 day' ) ),
				'end_date'   => current_time( 'mysql' )
			),
			'week'  => array(
				'title'      => __( 'Last Week', 'wcstm' ),
				'start_date' => date( 'Y-m-d', strtotime( ' -1 week' ) ),
				'end_date'   => current_time( 'mysql' )
			),
			'month' => array(
				'title'      => __( 'Last Month', 'wcstm' ),
				'start_date' => date( 'Y-m-d', strtotime( ' -1 month' ) ),
				'end_date'   => current_time( 'mysql' )
			)
		);
	}

	public function get_stats_results() {
		return $this->results;
	}

	public function is_successes() {
		return $this->is_successes;
	}

}