<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Admin_Term_List_Table extends WP_List_Table {

	/**
	 * @var WCSTM_Term
	 */
	private $terms;

	public function __construct( $args = array() ) {

		global $status, $page;
		remove_all_filters( 'term_filter' );
		$this->terms = WCSTM_Term::get_instance();

		parent::__construct( array(
			'singular' => 'term',
			'plural'   => 'terms',
		) );

	}

	function column_default( $item, $column_name ) {

		return $item[ $column_name ];

	}

	function column_term( $item ) {

		$actions = array(
			'edit'   => sprintf( '<a href="?page=wcstm_edit_term&id=%s&action=edit">%s</a>', $item['id'], __( 'Edit', 'wcstm' ) ),
			'delete' => sprintf( '<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __( 'Delete', 'wcstm' ) ),
		);

		return sprintf(
			'%s %s',
			esc_html( $item['term'] ),
			$this->row_actions( $actions )
		);

	}

	function column_cb( $item ) {

		return sprintf(
			'<input type="checkbox" name="id[]" value="%s" />',
			$item['id']
		);

	}

	function get_columns() {

		$columns = array(
			'cb'               => '<input type="checkbox" />', //Render a checkbox instead of text
			'term'             => __( 'Term', 'wcstm' ),
			'count'            => __( 'Hit Count', 'wcstm' ),
			'results'          => __( 'Search Results', 'wcstm' ),
			'created_date'     => __( 'Created Date', 'wcstm' ),
			'last_search_date' => __( 'Last Search Date', 'wcstm' )
		);

		return $columns;

	}

	function get_sortable_columns() {

		$sortable_columns = array(
			'term'             => array( 'term', true ),
			'count'            => array( 'count', false ),
			'results'          => array( 'results', false ),
			'created_date'     => array( 'created_date', true ),
			'last_search_date' => array( 'last_search_date', true )
		);

		return $sortable_columns;

	}

	function get_bulk_actions() {

		$actions = array(
			'delete' => 'Delete'
		);

		return $actions;

	}

	function process_bulk_action() {

		if ( 'delete' === $this->current_action() ) {

			$ids = isset( $_REQUEST['id'] ) ? $_REQUEST['id'] : array();

			if ( is_array( $ids ) ) {
				$ids = implode( ',', $ids );
			}

			if ( ! empty( $ids ) ) {
				$this->terms->delete_terms( $ids );
			}

		}

	}

	function prepare_items() {

		$per_page = 20;
		$columns  = $this->get_columns();
		$hidden   = array();
		$sortable = $this->get_sortable_columns();

		// here we configure table headers, defined in our methods
		$this->_column_headers = array( $columns, $hidden, $sortable );

		// process bulk action if any
		$this->process_bulk_action();

		// will be used in pagination settings
		if ( isset( $_REQUEST['s'] ) )
			$search_term = trim( $_REQUEST['s'] );
		if ( !empty( $search_term ) ) {
			$search_where = "WHERE `term` " . sprintf( "LIKE '%%%s%%'", esc_attr( $search_term ) );
			$total_items  = $this->terms->get_terms_count_where( $search_where );
		} else {
			$total_items = $this->terms->get_terms_count();
		}

		// prepare query params, as usual current page, order by and order direction
		$paged   = isset( $_REQUEST['paged'] ) ? max( 0, intval( $_REQUEST['paged'] ) - 1 ) : 0;
		$orderby = ( isset( $_REQUEST['orderby'] ) && in_array( $_REQUEST['orderby'], array_keys( $this->get_sortable_columns() ) ) ) ? $_REQUEST['orderby'] : 'last_search_date';
		$order   = ( isset( $_REQUEST['order'] ) && in_array( $_REQUEST['order'], array(
				'asc',
				'desc'
			) ) ) ? $_REQUEST['order'] : 'desc';

		$_SERVER['REQUEST_URI'] = remove_query_arg( '_wp_http_referer', $_SERVER['REQUEST_URI'] );

		if ( isset( $_REQUEST['m'] ) && 0 != $_REQUEST['m'] ) {
			add_filter( 'term_filter', array( $this, 'get_date_filter' ) );
		}

		if ( isset( $_REQUEST['s'] ) ) {
			add_filter( 'term_filter', array( $this, 'get_search_filter' ) );
		}

		$this->items = $this->terms->get_terms( $orderby, $order, $per_page, $paged * $per_page );

		// configure pagination
		$this->set_pagination_args( array(
			'total_items' => $total_items, // total items defined above
			'per_page'    => $per_page, // per page constant defined at top of method
			'total_pages' => ceil( $total_items / $per_page ) // calculate pages count
		) );

	}

	public function get_date_filter( $where ) {

		$date_filter_value = ( isset( $_REQUEST['m'] ) ) ? $_REQUEST['m'] : '';
		$date              = str_split( $date_filter_value, 4 );
		$like              = ( count( $date ) == 2 ) ? sprintf( "LIKE '%%%s-%s%%'", $date[0], $date[1] ) : '';
		$where .= "WHERE `created_date` $like";

		return $where;

	}

	public function get_search_filter( $where ) {

		$search_term = trim( $_REQUEST['s'] );
		$search_term = esc_attr( $search_term );
		$like        = sprintf( "LIKE '%%%s%%'", $search_term );
		if ( $where ) {
			$where .= " AND `term` $like";
		} else {
			$where = "WHERE `term` $like";
		}

		return $where;

	}

	protected function extra_tablenav( $which ) {

		?>
		<div class="alignleft actions">
			<?php
			if ( 'top' === $which ) {
				$this->wcstm_months_dropdown();

				do_action( 'restrict_manage_posts' );
				submit_button( __( 'Filter' ), 'button', 'filter_action', false, array( 'id' => 'post-query-submit' ) );
			}
			?>
		</div>
		<?php
		/**
		 * Fires immediately following the closing "actions" div in the tablenav for the posts
		 * list table.
		 */
		do_action( 'manage_posts_extra_tablenav', $which );

	}

	protected function wcstm_months_dropdown() {

		global $wp_locale;

		$months = $this->terms->get_months();

		$month_count = count( $months );

		if ( ! $month_count || ( 1 == $month_count && 0 == $months[0]->month ) ) {
			return;
		}

		$m = isset( $_GET['m'] ) ? (int) $_GET['m'] : 0;
		?>
		<label for="filter-by-date" class="screen-reader-text"><?php _e( 'Filter by date' ); ?></label>
		<select name="m" id="filter-by-date">
			<option<?php selected( $m, 0 ); ?> value="0"><?php _e( 'All dates' ); ?></option>
			<?php
			foreach ( $months as $arc_row ) {
				if ( 0 == $arc_row->year ) {
					continue;
				}

				$month = zeroise( $arc_row->month, 2 );
				$year  = $arc_row->year;

				printf( "<option %s value='%s'>%s</option>\n",
					selected( $m, $year . $month, false ),
					esc_attr( $arc_row->year . $month ),
					/* translators: 1: month name, 2: 4-digit year */
					sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $month ), $year )
				);
			}
			?>
		</select>
		<?php

	}

}