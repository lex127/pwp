<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class WCSTM_Admin_Widget {

	protected $terms;

	public function __construct( $terms ) {
		$this->terms = $terms;
	}

	public function add_dashboard_widgets() {
		wp_add_dashboard_widget( 'dashboard_top_search_widget', 'Top Search Terms', array(
			$this,
			'display_top_search'
		) );
		wp_add_dashboard_widget( 'dashboard_recent_search_widget', 'Recent Search Terms', array(
			$this,
			'display_recent_search'
		) );
	}

	public function display_top_search() {
		$popular_terms = $this->terms->get_terms( 'count', 'DESC', 5 );
		echo $this->get_widget( $popular_terms );
	}

	public function display_recent_search() {
		$recent_terms = $this->terms->get_terms( 'last_search_date', 'DESC', 5 );
		echo $this->get_widget( $recent_terms );
	}

	protected function get_widget( $terms ) {
		$out = '';
		if ( count( $terms ) ) {
			$out .=
				'<table class="widefat fixed">'
				. '<thead>'
				. '<tr>'
				. '<th>' . __( 'Search Term' ) . '</th>'
				. '<th>' . __( 'Results' ) . '</th>'
				. '<th>' . __( 'Hits' ) . '</th>'
				. '</tr>'
				. '</thead>'
				. '<tbody>';
			foreach ( $terms as $term ) {
				$out .=
					'<tr>'
					. '<td>' . $term['term'] . '</td>'
					. '<td>' . $term['results'] . '</td>'
					. '<td>' . $term['count'] . '</td>'
					. '</tr>';
			}
			$out .=
				'</tbody>'
				. '</table>';
		}

		return $out;
	}

}