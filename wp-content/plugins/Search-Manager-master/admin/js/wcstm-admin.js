(function($){

    $('#redirect').change(function(){
        wcstm.changeStatus(this);
    });

    $('.term .delete').click(function(e){
        var answer = confirm('Are you sure you want to delete this item?');
        if(!answer){
            e.preventDefault();
        }
    });

    $('#redirect_obj_type').change(function(){
        $('#redirect_obj_id_template').remove();
        $('#redirect_obj_name').remove();
        if($('#redirect_obj_type option:selected').val().length) {
            wcstm.getTypes();

        }
    });

    if ($('#redirect_obj_id').length) {
        $('#redirect_obj_id').keypress(function(){
            $('#redirect_obj_id_main').attr('value', '');
        });
    }

    $('#redirect_obj_name').change(function(){
        $('#redirect_obj_id_template').remove();

        if ($('#redirect_obj_type option:selected').val() == 'custom_link') {
            return '';
        }
        if(!$('#redirect_obj_id_template').length && $('#redirect_obj_type option:selected').val().length) {
            wcstm.getIdTemplate();
        }
    });


    $('#autocomplete_status').on('click', function(){
        $('tr:not(:first-child)').toggle();
    });

    $('#form').validate();

    wcstm.getTypes = function() {
        $.ajax({
            url: wcstm.ajax_url,
            data: {
                'action': 'redirect_obj_name',
                'obj_type': $('#redirect_obj_type option:selected').val(),
                'redirect': true,
                'id': $('#id').html()
            },
            success: function(response) {
                $('#redirect_obj_type').after(response);

                $('#redirect_obj_name').change(function(){
                    $('#redirect_obj_id_template').remove();

                    if ($('#redirect_obj_type option:selected').val() == 'custom_link') {
                        return '';
                    }
                    if(!$('#redirect_obj_id_template').length && $('#redirect_obj_type option:selected').val().length) {
                        wcstm.getIdTemplate();
                    }
                });
            }
        });
    };

    wcstm.getIdTemplate = function() {
        $.ajax({
           url: wcstm.ajax_url,
            data: {
                'action': 'redirect_obj_id',
                'notEmpty': ($('#redirect_obj_name option:selected').val().length !== 0)
            },
            success: function(response) {
                $('#redirect_obj_name').after(response);
                wcstm.getIds();
                $('#redirect_obj_id').keypress(function(){
                    $('#redirect_obj_id_main').attr('value', '');
                });
            }
        });
    };

    wcstm.getIds = function () {
        $.ajax({
            url: wcstm.ajax_url,
            data: {
                'action': 'get_obj_ids',
                'type': $('#redirect_obj_type option:selected').val(),
                'name': $('#redirect_obj_name option:selected').val()
            },
            success: function(response) {
                var data = $.makeArray(response);
                wcstm.autocomplete(data);
            }
        });
    };

    wcstm.autocomplete  = function(data) {
        $('#redirect_obj_id').autocomplete({
            lookup: data,
            onSelect: function(suggestion) {
                $('#redirect_obj_id_main').attr('value', suggestion.data);
            }
        });
    };

    wcstm.resetCount = function(){

        var btn = $('.reset_button');

        btn.on('click', function(e){

            e.preventDefault();

            var sureText = btn.data('confirm');
            var sure = confirm(sureText);

            if (sure === true) {
                $('#count').attr('value', 0);
                $('.total_count').text('0');
                btn.hide();
            }
        });
    };

    wcstm.changeStatus = function(el) {
        if($(el).is(':checked')) {
            $('#redirect_opt').show();
            $('#redirect_obj_type').show();
        } else {
            $('#redirect_opt').hide();
            $('#redirect_obj_type').hide();
            $('#redirect_obj_type select').val([]);
            $('#redirect_obj_name').remove();
            $('#redirect_obj_id_template').remove();
        }
    };

    if ($('#redirect_obj_id').length) {
        wcstm.getIds();
    }

    wcstm.resetCount();

})(jQuery);